package com.unio.sample.kotlin.views

import android.os.Bundle
import android.view.View
import com.unio.base.UActivity
import com.unio.sample.kotlin.R
import com.unio.ui.annotation.LayoutId

@LayoutId(R.layout.activity_textview)
class TextViewActivity : UActivity<View>()
{
    override fun onInit(savedInstanceState: Bundle?) {}
}