package com.unio.sample.kotlin.views

import android.Manifest
import com.unio.app.ScannerActivity
import com.unio.sample.kotlin.MainActivity
import com.unio.ui.ScannerView
import com.unio.ui.annotation.RequestPermissions
import com.unio.util.helper.Alert
import com.unio.util.manager.ActivityManager

// Enable Camera permission
@RequestPermissions(Manifest.permission.CAMERA)
class ScannerActivity : ScannerActivity()
{
    /**
     * Receives scanned result
     */
    override fun onFind(response: ScannerView.Response?) {
        Alert.success(response?.toString())
    }

    override fun onBackPressed() {
        ActivityManager.change(MainActivity::class.java, ActivityManager.Param.CLOSE_CURRENT)
    }
}