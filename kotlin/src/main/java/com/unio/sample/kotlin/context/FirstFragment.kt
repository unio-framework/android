package com.unio.sample.kotlin.context

import android.os.Bundle
import android.view.View
import com.unio.base.UFragment
import com.unio.sample.kotlin.R
import com.unio.ui.annotation.LayoutId

/**
 * Fragment to show in ContextActivity
 */
@LayoutId(R.layout.fragment_context_first)
class FirstFragment : UFragment<SampleContextActivity, View>()
{
    override fun onInit(savedInstanceState: Bundle?) {
        // ...
    }
}