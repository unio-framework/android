package com.unio.sample.kotlin.context

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import com.unio.debug.Trace
import com.unio.sample.kotlin.MainActivity
import com.unio.ui.SwipeRefreshLayout
import com.unio.ui.context.*
import com.unio.ui.features.fonts.FontAwesome
import com.unio.ui.recycler.AdapterItem
import com.unio.ui.recycler.AdapterItemType
import com.unio.ui.recycler.item.SimpleIconItem
import com.unio.util.statics.constants.Time
import com.unio.util.helper.Alert
import com.unio.util.manager.ActivityManager
import com.unio.util.manager.ActivityManager.Param

/**
 * SampleContextActivity
 *
 * Example using [ContextActivity] class, with all addon interfaces
 *
 * [IGlobalContext] represents left hamburger menu
 * [IRefreshContext] represents swipe to refresh action, callable from user or programmatically
 */
class SampleContextActivity : ContextActivity(), IGlobalContext, ILocalContext, IRefreshContext
{
    /**
     * Menu fragment
     */
    override fun getGlobalFragment(): Class<out Fragment> { return MenuFragment::class.java }

    /**
     * First fragment to load
     */
    override fun getFragment(): Class<out Fragment> { return FirstFragment::class.java }

    /**
     * Add local menu
     */
    override fun getLocalMenu(): Array<AdapterItem<*>> {
        return arrayOf(
            LocalMenuItem(FontAwesome.ANDROID, "Teste")
        )
    }

    /**
     * Customize each local menu
     */
    override fun onLocalItemCustomize(position: Int, holder: RecyclerView.ViewHolder?, item: AdapterItem<out AdapterItemType<*, *>>?) {
        if (item is SimpleIconItem) {
            holder?.itemView?.setOnClickListener {
                Alert.success("Clicked in position $position")
            }
        }
    }

    /**
     * User can swipe refresh. If is false, swipe refresh can be called only programmatically
     */
    override fun isUserInteractionEnabled(): Boolean { return true }

    /**
     * Action when refresh is swiped or called programmatically
     */
    override fun onRefresh(layout: SwipeRefreshLayout?, target: Int) {
        Trace.log(this, "Refresh start")
        Handler().postDelayed({
            Trace.log(this, "Refresh finish")
            setRefreshing(false)
        }, (Time.Milisecond.SECOND * 5).toLong())
    }

    /**
     * Activity onInit
     */
    override fun onInit(savedInstanceState: Bundle?) {
        super.onInit(savedInstanceState)
        title = "SampleContextActivity"
    }

    /**
     * Back button
     */
    override fun onBackPressed() {
        ActivityManager.change(MainActivity::class.java, Param.CLOSE_CURRENT)
    }
}