package com.unio.sample.kotlin.recycler

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.unio.base.UActivity
import com.unio.debug.Trace
import com.unio.sample.kotlin.MainActivity
import com.unio.sample.kotlin.R
import com.unio.ui.annotation.FindViewById
import com.unio.ui.annotation.LayoutId
import com.unio.ui.features.drawables.RippleDrawable
import com.unio.ui.features.fonts.FontAwesome
import com.unio.ui.recycler.Adapter
import com.unio.ui.recycler.AdapterItem
import com.unio.ui.recycler.item.*
import com.unio.ui.recycler.type.SimpleType
import com.unio.util.manager.ActivityManager
import com.unio.util.manager.ActivityManager.Param

/**
 * SampleRecyclerActivity
 *
 * Example using RecyclerView with Unio's custom Adapter and these items
 */
@LayoutId(R.layout.activity_main)
class SampleRecyclerActivity : UActivity<View>()
{
    @FindViewById
    var collection: RecyclerView? = null

    override fun onInit(savedInstanceState: Bundle?) {
        title = "SampleRecyclerActivity"

        val adapter = Adapter()
        adapter.add(SimpleItem("Simple Item"))
        adapter.add(CompleteItem("Complete Item", "Description"))
        adapter.add(SimpleIconItem(FontAwesome.ANDROID, "Simple Icon Item"))
        adapter.add(CompleteIconItem(FontAwesome.ANDROID, "Complete Icon Item", "Description"))
        adapter.add(SimpleNotificationItem("Simple Notification Item", 10))
        adapter.add(CompleteNotificationItem("Complete Notification Item", "Description", 20))
        adapter.add(SimpleIconNotificationItem(FontAwesome.ANDROID, "Simple Icon Notification Item", 10))
        adapter.add(CompleteIconNotificationItem(FontAwesome.ANDROID, "Complete Icon Notification Item", "Description", 20))

        adapter.addOnItemCustomize(object : Adapter.IOnItemCustomize {
            @Adapter.Types(SimpleType.ID)
            override fun onItemCustomize(position: Int, holder: RecyclerView.ViewHolder, item: AdapterItem<*>) {
                Trace.log(this, "Only Simple Item customization", position)
            }
        })

        adapter.addOnItemCustomize { position, holder, item ->
            Trace.log(this, position, item.type)
            RippleDrawable.attach(holder.itemView)
        }

        collection?.layoutManager = LinearLayoutManager(this)
        collection?.adapter = adapter
    }

    override fun onBackPressed() {
        ActivityManager.change(MainActivity::class.java, Param.CLOSE_CURRENT)
    }
}