package com.unio.sample.kotlin

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.unio.base.UActivity
import com.unio.sample.kotlin.context.SampleContextActivity
import com.unio.sample.kotlin.recycler.SampleRecyclerActivity
import com.unio.sample.kotlin.views.ScannerActivity
import com.unio.sample.kotlin.views.TextViewActivity
import com.unio.ui.annotation.FindViewById
import com.unio.ui.annotation.LayoutAnnotation
import com.unio.ui.annotation.LayoutId
import com.unio.ui.features.drawables.RippleDrawable
import com.unio.ui.recycler.Adapter
import com.unio.ui.recycler.item.SimpleItem
import com.unio.util.manager.ActivityManager
import com.unio.util.manager.ActivityManager.Param

/**
 * Unio fragment example project
 */
@LayoutId(R.layout.activity_main)
class MainActivity : UActivity<View>()
{
    @FindViewById
    var collection: RecyclerView? = null

    override fun onInit(savedInstanceState: Bundle?) {
        setTitle(R.string.app_name)

        val adapter = Adapter()
        adapter.add(
            SimpleItem("SampleRecyclerActivity"),
            SimpleItem("SampleContextActivity"),
            SimpleItem("TextViewActivity"),
            SimpleItem("ScannerActivity")
        )

        adapter.addOnItemCustomize { position, holder, _ ->
            RippleDrawable.attach(holder.itemView)
            holder.itemView.setOnClickListener {
                var activity: Class<out Activity>? = null
                when (position) {
                    0 -> activity = SampleRecyclerActivity::class.java
                    1 -> activity = SampleContextActivity::class.java
                    2 -> activity = TextViewActivity::class.java
                    3 -> activity = ScannerActivity::class.java
                }
                if (activity != null) {
                    // Validates target activity required permissions
                    if (requestPermission(this, activity))
                        ActivityManager.change(activity, Param.CLOSE_CURRENT)
                }
            }
        }

        collection?.layoutManager = LinearLayoutManager(this)
        collection?.adapter = adapter
    }

    // Check if all permissions are enabled, and next step
    override fun onPermissionsResult(permissionClass: Class<*>, permissions: Array<String>, grantResults: IntArray) {
        super.onPermissionsResult(permissionClass, permissions, grantResults)
        if (LayoutAnnotation.isPermissionsEnabled(permissionClass)) {
            // Permission enabled
            ActivityManager.change(permissionClass, Param.CLOSE_CURRENT)
        } else {
            this.requestPermission(this)
        }
    }
}