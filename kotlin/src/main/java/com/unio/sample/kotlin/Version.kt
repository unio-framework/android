package com.unio.sample.kotlin

import com.unio.database.preference.StorageItem
import com.unio.debug.Trace
import com.unio.modules.VersionControl

class Version : VersionControl()
{
    override fun getAllVersions(): Array<String> {
        return arrayOf(
            "1.0.0"
        )
    }

    override fun onBeforeInit(cache: StorageItem?) {
        Trace.log(this, "Before start version check")
    }

    override fun onFirstUse() {
        Trace.log(this, "When runs for first time")
    }

    override fun onAfterUpdate() {
        Trace.log(this, "If version has updated")
    }
}