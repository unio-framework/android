package com.unio.sample.kotlin.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue

@RunWith(AndroidJUnit4::class)
class DatabaseModelTest
{
    @Before
    fun createDatabase() {
        // Clean database before each test
        TestTable.table().delete()
        TestTable.table().create()
    }

    @Test
    fun insertTest() {
        val resulted = insert()
        assertTrue(resulted)
    }

    @Test
    fun insertAndFindTest() {
        insert()
        val resulted = findAfterInsert()
        assertTrue(resulted)
    }

    @Test
    fun updateTest() {
        insert()
        val resulted = update()
        assertTrue(resulted)
    }

    @Test
    fun updateAndFindTest() {
        insert()
        update()
        val resulted = updateAndFind()
        assertTrue(resulted)
    }

    @Test
    fun deleteTest() {
        insert()
        val model = TestTable.model().filter("myColumn1=?", arrayOf<Any>("col1")).find<TestTable>()
        val resulted = model != null && model.delete()
        assertTrue(resulted)
    }

    @Test
    fun findAllTest() {
        insert()
        insert()
        val model = TestTable.model().filter("myColumn1=?", arrayOf<Any>("col1")).findAll<TestTable>()
        assertEquals(2, model.size.toLong())
    }

    private fun insert(): Boolean {
        val model = TestTable()
        model.myColumn1 = "col1"
        model.myColumn2 = 2
        return model.save()
    }

    private fun findAfterInsert(): Boolean {
        val model = TestTable.model().filter("myColumn1=?", arrayOf<Any>("col1")).find<TestTable>()
        return model != null && model.myColumn2 == 2
    }

    private fun update(): Boolean {
        val model = TestTable.model().filter("myColumn1=?", arrayOf<Any>("col1")).find<TestTable>() ?: return false
        model.myColumn2 = 3
        return model.save()
    }

    private fun updateAndFind(): Boolean {
        val model = TestTable.model().filter("myColumn1=?", arrayOf<Any>("col1")).find<TestTable>()
        return model != null && model.myColumn2 == 3
    }
}