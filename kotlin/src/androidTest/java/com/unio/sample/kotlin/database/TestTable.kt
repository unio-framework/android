package com.unio.sample.kotlin.database

import com.unio.database.sql.builder.QueryBuilder
import com.unio.database.sql.orm.Model
import com.unio.database.sql.orm.Table
import com.unio.database.sql.orm.annotation.AColumn
import com.unio.database.sql.orm.annotation.ATable

@ATable
class TestTable : Model()
{
    @JvmField
    @AColumn
    var myColumn1: String? = null

    @JvmField
    @AColumn
    var myColumn2: Int = 0

    companion object {
        // Model handlers
        fun model(): QueryBuilder { return model(TestTable::class.java) }
        fun table(): Table { return table(TestTable::class.java) }
    }
}