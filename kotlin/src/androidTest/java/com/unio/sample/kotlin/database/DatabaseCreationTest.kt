package com.unio.sample.kotlin.database

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.Assert.assertEquals

@RunWith(AndroidJUnit4::class)
class DatabaseCreationTest
{
    @Test
    fun createDatabase() {
        val resulted = TestTable.table().create()
        val expected = TestTable.table().exists()
        assertEquals(resulted, expected)
    }

    @Suppress("SimplifyBooleanWithConstants")
    @Test
    fun deleteDatabase() {
        val resulted = TestTable.table().delete()
        val expected = TestTable.table().exists() == false
        assertEquals(resulted, expected)
    }
}