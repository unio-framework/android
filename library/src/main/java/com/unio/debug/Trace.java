package com.unio.debug;

import android.app.Activity;
import android.support.annotation.StringDef;
import android.util.Log;
import com.unio.app.model.UnioDebug;
import com.unio.core.Unio;
import com.unio.modules.Device;
import com.unio.util.control.Date;
import com.unio.util.control.Network;
import com.unio.util.helper.Check;
import com.unio.util.io.Hash;
import com.unio.util.rest.Http;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Trace
 *
 * Debug trace class
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class Trace
{
    /**
     * System
     *
     * Identify system type
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public static final class System
    {
        public static final String ANDROID = "ANDROID";
        public static final String IOS     = "IOS";
        public static final String MACOS   = "MACOS";
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({ System.ANDROID, System.IOS, System.MACOS })
    public @interface SystemType {}

    /** Current activity */
    private static Class<? extends Activity> _currentActivity;

    /**
     * Check if Trace is started
     * Because UncaughtException needs an activity instance, is added in UActivity,
     *   and need to check every time a new Activity is created
     */
    private static boolean _started = false;

    /**
     * Show logs in each level
     * See more in {@link EDebug}
     *
     * @param m message groups
     */
    public static void log(Object... m) {
        if (Check.isEmpty(m) == false) {
            EDebug eTarget;
            List<Object> messages = new LinkedList<>(Arrays.asList(m));
            if (messages.get(0) instanceof EDebug) {
                eTarget = (EDebug) messages.get(0);
                messages.remove(0);
            } else {
                eTarget = EDebug.DEVELOPMENT;
            }
            int target = eTarget.ordinal();

            int mode = Unio.app() == null || Unio.debugMode() == null ? EDebug.DEFAULT.ordinal() : Unio.debugMode().ordinal();
            if (mode != EDebug.DEFAULT.ordinal()) {
                StringBuilder message = new StringBuilder();
                int total = messages.size();
                Object instance = total > 1 ? null : eTarget.toString();
                for (int key = 0; key < total; key++) {
                    if (key == 0 && total > 1) {
                        instance = messages.get(key);
                        if (instance instanceof Class) {
                            instance = ((Class) instance).getName();
                        } else {
                            instance = instance.getClass().getName();
                        }
                    } else {
                        message.append(messages.get(key) == null ? "null" : String.valueOf(messages.get(key)));
                        message.append(key < total - 1 ? " - " : "");
                    }
                }
                if (Check.isEmpty(message.toString())) message = new StringBuilder("[empty]");
                if (mode <= target) {
                    if (eTarget == EDebug.SEND) Trace.send(null, instance+": "+message.toString());
                    // Show log if is not production
                    // Only show in production, if uses EDebug.ALL flag
                    if (mode != EDebug.PRODUCTION.ordinal() || target == EDebug.ALL.ordinal())
                        Log.i(String.valueOf(instance), message.toString());
                }
            }
        }
    }

    /**
     * Get exception content from try/catch
     *
     * @param e Exception
     */
    public static void get(Throwable e) { get(e, null); }

    /**
     * Get exception content from try/catch
     *
     * @param e       Exception
     * @param message additional message, ex {@link Http}
     */
    public static void get(Throwable e, String message) {
        if (Unio.app() != null) {
            EDebug type = Unio.debugMode();
            if (type == EDebug.PRODUCTION) {
                // Send to server by webservice
                send(e, message);
            } else if (type != EDebug.DEFAULT) {
                e.printStackTrace();
                if (Check.isEmpty(message) == false) Trace.log(e.getClass(), message);
            }
        }
    }

    /**
     * Start trace in Activity
     *
     * @param context Activity context
     */
    public static void start(Activity context) {
        if (Unio.app() == null) return;
        if (_started == false) {
            EDebug debugMode = Unio.debugMode();
            if (debugMode != EDebug.DEFAULT) {
                Thread.UncaughtExceptionHandler defaultException = Thread.getDefaultUncaughtExceptionHandler();
                Thread.setDefaultUncaughtExceptionHandler(new ExceptionLoader(context, defaultException));
            }
            _started = true;
        }
    }

    /**
     * Send error from WebService, in JSON
     * Works only in {@link EDebug#PRODUCTION} or {@link #log} with {@link EDebug#SEND} as first parameter
     *
     * @param ex Exception
     */
    public static void send(final Throwable ex) { send(ex, null); }

    /**
     * Send error from WebService, in JSON
     * Works only in {@link EDebug#PRODUCTION} or {@link #log} with {@link EDebug#SEND} as first parameter
     *
     * Fields:
     * - Debug : POST parameter, receives content as JSON
     *   - Package       : app package
     *   - Date          : datetime when exception occurs
     *   - System        : system type (see {@link com.unio.debug.Trace.System})
     *   - SystemVersion : Operational System version, ex: Android, iOS
     *   - AppVersion    : app version
     *   - UnioVersion   : framework version
     *   - Manufacturer  : mobile phone manufacturer
     *   - Model         : mobile phone model
     *   - IMID          : primary IMEI, this is unique method to identify a phone
     *   - IMIDError     : error message if not catch IMEI
     *   - Exception     : exception content
     *   - Message       : other additional message
     *
     * @param ex      Exception
     * @param message additional message
     */
    public static void send(final Throwable ex, String message) {
        Device device = new Device();
        Network network = new Network();

        String url = Unio.url(Unio.META_URL_DEBUG);
        if (Check.isEmpty(url)) {
            log(Trace.class, "Metadata URL_DEBUG not found");
            return;
        }
        Hash log = new Hash();

        log.put("packageName",   Unio.app().getPackageName());
        log.put("dateReal",      Date.withTime());
        log.put("systemName",    System.ANDROID);
        log.put("systemVersion", android.os.Build.VERSION.RELEASE);
        log.put("appVersion",    Unio.version().getAppFullVersion());
        log.put("unioVersion",   Unio.VERSION);

        log.put("manufacturer", device.getManufacturer());
        log.put("model",        device.getModel());
        String imid;
        String imidError = "";
        try {
            imid = device.getIdentificator();
        } catch(SecurityException e) {
            imid      = device.getSerial(); // Returns serial as IMEI is not allowed
            imidError = "IMEI ignored by SecurityException. Need android.permission.READ_PHONE_STATE";
        }
        log.put("imid",      imid);
        log.put("imidError", imidError);

        String idUnique = imid + java.lang.System.currentTimeMillis();
        log.put("idUnique", idUnique);

        if (Check.isEmpty(message)) message = "";
        log.put("exception", format(ex));
        log.put("message", message);

        UnioDebug model = new UnioDebug();
        model.setAnswer(log);
        model.save();
    }

    /**
     * Catch printStackTrace as String
     *
     * @param ex Exception
     *
     * @return String
     */
    public static String format(Throwable ex) {
        if (ex == null) return "";
        Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        ex.printStackTrace(printWriter);
        return writer.toString();
    }
}