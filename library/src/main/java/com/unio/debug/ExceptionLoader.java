package com.unio.debug;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import com.unio.core.Unio;

/**
 * ExceptionLoader
 *
 * Callbacks when occurs an exception crash
 * Only in {@link EDebug#DEVELOPMENT} and {@link EDebug#PRODUCTION}
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 29/11/2014 11:26
 */
public final class ExceptionLoader implements Thread.UncaughtExceptionHandler
{
    /** Crash flag */
    private volatile boolean mCrashing = false;

    /** Activity context */
    private Activity _context;

    /** Default exception handler */
    private Thread.UncaughtExceptionHandler _default;

    /**
     * Constructor
     *
     * @param context          Activity context
     * @param defaultException Default exception handler
     */
    public ExceptionLoader(Activity context, Thread.UncaughtExceptionHandler defaultException) {
        this._context = context;
        this._default = defaultException;
    }

    /**
     * Processes after throw crash exception
     *
     * @param thread Thread
     * @param ex     Exception
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        EDebug debug = Unio.debugMode();
        try {
            if (this.mCrashing == false) {
                this.mCrashing = true;

                if (debug == EDebug.DEVELOPMENT) {
                    this._showLog(ex);
                    this._showAlert(ex);
                } else if (debug == EDebug.PRODUCTION) {
                    Trace.send(ex);
                }
            }
        } finally {
            if (debug == EDebug.PRODUCTION) {
                this._default.uncaughtException(thread, ex);
            } else {
                this._context.finish();
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(10);
            }
        }
    }

    /**
     * Show an alert with exception content
     *
     * @param ex Exception
     */
    private void _showAlert(Throwable ex) {
        Intent intent = new Intent(this._context, UException.class);
        intent.putExtra("exception", Trace.format(ex));
        this._context.startActivity(intent);
    }

    /**
     * Show exception content in LogCat log
     *
     * @param ex Exception
     */
    private void _showLog(Throwable ex) { Log.e("AndroidRuntime", "FATAL EXCEPTION", ex); }
}