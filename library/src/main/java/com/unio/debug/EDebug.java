package com.unio.debug;

/**
 * EDebug
 *
 * Debug level enum
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 28/11/2014 23:52
 */
public enum EDebug
{
    /**
     * Use during development phase
     * Show logs and crash exception in dialog
     */
    DEVELOPMENT,

    /**
     * Use in production
     * Hide all logcat {@link Trace#log} logs, and send all crash exception to webservice request
     */
    PRODUCTION,

    /**
     * Android default
     * Show logs and crash exceptions in logcat only
     */
    DEFAULT,

    /**
     * Use only in {@link Trace#log} first parameter
     * Use to show logs and crash exception in logcat always
     */
    ALL,

    /**
     * Use only in {@link Trace#log} first parameter
     * Works only when system debug level is {@link #PRODUCTION}
     * Will send all {@link Trace#log} content to webservice request
     */
    SEND,
}