package com.unio.debug;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.view.WindowManager;
import com.unio.R;
import com.unio.base.UActivity;
import com.unio.shortcuts.Android;

/**
 * UException
 *
 * Activity to show exception crash error content in alert
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 29/11/2014 11:51
 */
public final class UException extends UActivity
{
    /**
     * Set dialog basic theme
     *
     * @return int
     */
    protected int getAlertTheme() {
        if (Android.CURRENT_VERSION > Android.KITKAT_WATCH) {
            // From Lollipop, use Material Theme
            return android.R.style.Theme_Material_NoActionBar;
        } else {
            // Use Holo Theme
            return android.R.style.Theme_Holo_NoActionBar;
        }
    }

    /**
     * Create method
     *
     * @param savedInstanceState Bundle
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setTheme(this.getAlertTheme());
        super.onCreate(savedInstanceState);
    }

    /**
     * Layout
     *
     * @return Object
     */
    @Override
    public Object getLayout() { return null; }

    /**
     * Start method
     */
    @Override
    public void onInit(Bundle savedInstanceState) {
        String message = getIntent().getStringExtra("exception");

        AlertDialog dialog = new AlertDialog.Builder(this, android.support.v7.appcompat.R.style.Theme_AppCompat)
            .setTitle(R.string.error)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(10);
                    dialog.dismiss();
                }
            })
            .create();

        Window window = dialog.getWindow();
        WindowManager.LayoutParams attributes = window.getAttributes();
        attributes.width  = WindowManager.LayoutParams.MATCH_PARENT;
        attributes.height = WindowManager.LayoutParams.MATCH_PARENT;
        attributes.horizontalMargin = 50;
        attributes.verticalMargin   = 50;
        window.setAttributes(attributes);

        dialog.show();
    }
}