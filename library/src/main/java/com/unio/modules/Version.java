package com.unio.modules;

import android.support.annotation.IntDef;
import com.unio.base.UModule;
import com.unio.core.Unio;
import com.unio.database.preference.StorageItem;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.helper.Methods;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Version
 *
 * Module for version control of an app or library
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Version extends UModule
{
    /** version contro types, libraries will be updates before app */
    public static final int LIBRARY = 1;
    public static final int APP     = 2;

    /** Old VersonControl flags */
    public static final String OLD_VC_FLAG = "_VC_FLAG";
    public static final String OLD_VC_VERSION = "_VC_VERSION";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ LIBRARY, APP })
    public @interface VersionType {}

    /** Relation of version control */
    private Map<Integer, List<Class<? extends VersionControl>>> _relations = new LinkedHashMap<>();
    private Map<Class<? extends VersionControl>, VersionControl> _instantions = new LinkedHashMap<>();

    /** Flag for identify version variables */
    private static final String _ID = "_UMVersion$ID";
    private boolean _started = false;

    /**
     * Add version control of a library
     *
     * @param vcClass class
     *
     * @return boolean
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean addLibrary(Class<? extends VersionControl> vcClass) { return this.add(vcClass, LIBRARY); }

    /**
     * Add version control of an app
     *
     * @param vcClass class
     *
     * @return boolean
     */
    public boolean addApp(Class<? extends VersionControl> vcClass) { return this.add(vcClass, APP); }

    /**
     * Add version class
     *
     * @param vcClass class
     * @param type    type
     *
     * @return boolean
     */
    public boolean add(Class<? extends VersionControl> vcClass, @VersionType int type) {
        List<Class<? extends VersionControl>> collection = this._relations.get(type);
        if (collection == null) collection = new LinkedList<>();
        if (collection.contains(vcClass) == false) {
            VersionControl c = this._addInstantiation(vcClass);
            if (c != null) {
                collection.add(vcClass);
                this._relations.put(type, collection);
                return true;
            }
        }
        return false;
    }

    /**
     * Add version instantiation
     *
     * @param vcClass class
     *
     * @return boolean
     */
    private VersionControl _addInstantiation(Class<? extends VersionControl> vcClass) {
        try {
            VersionControl vc = vcClass.newInstance();
            this._instantions.put(vcClass, vc);
            return vc;
        } catch (InstantiationException e) {
            Trace.get(e);
        } catch (IllegalAccessException e) {
            Trace.get(e);
        }
        return null;
    }

    /**
     * Get current version
     *
     * @param c Version control class
     *
     * @return String
     */
    public String getVersion(Class<? extends VersionControl> c) { return this.getVersion(c, false); }

    /**
     * Get current version
     *
     * @param c    Version control class
     * @param full return with Build if exists
     *
     * @return String
     */
    public String getVersion(Class<? extends VersionControl> c, boolean full) {
        VersionControl vc = this._instantions.get(c);
        if (vc == null) return "";
        String version = Unio.cache(_ID+"_"+vc.getIdentification()).get().toString();
        if (Check.isEmpty(version)) version = vc.getCurrentVersion();
        // If has BuildCode, return only version
        if (version != null && version.contains("_") && full == false) version = version.split("_")[0];
        return version;
    }

    /**
     * Get app version
     *
     * @return String
     */
    public String getAppVersion() {
        List<Class<? extends VersionControl>> cs = this._relations.get(APP);
        return cs == null || cs.size() == 0 ? "" : this.getVersion(cs.get(0));
    }

    /**
     * Get app full version
     *
     * @return String
     */
    public String getAppFullVersion() {
        List<Class<? extends VersionControl>> cs = this._relations.get(APP);
        return cs == null || cs.size() == 0 ? "" : this.getVersion(cs.get(0), true);
    }

    /**
     * Get App version control
     *
     * @return UMVersionControl
     */
    public VersionControl getAppVersionControl() {
        List<Class<? extends VersionControl>> cs = this._relations.get(APP);
        return cs.size() == 0 ? null : this._instantions.get(cs.get(0));
    }

    /**
     * Start method
     */
    public void start() {
        if (this._started == false && this._instantions.size() > 0) {
            this._startType(LIBRARY);
            this._startType(APP);
            this._started = true;
        }
    }

    /**
     * Method to restart initial version check
     */
    public void restart() {
        this._started = false;
        this.start();
    }

    /**
     * Update process of each control
     * Will be checked last updated version and execute all available to current
     * After finished, execute {@link VersionControl#onAfterUpdate()}
     */
    protected void onUpdate(VersionControl vc) {
        String lastUpdatedVersion = this._version(vc).get().toString();
        if (Check.isEqual(lastUpdatedVersion, vc.getCurrentVersion()) == false) {
            List<String> updateMethods = this._getUpdateMethods(vc);
            if (updateMethods.size() > 0) {
                for (String method : updateMethods) {
                    if (Methods.exists(vc, method)) Methods.call(vc, method);
                }
                this._version(vc).set(vc.getCurrentVersion());
                vc.onAfterUpdate();
            }
        }
    }

    /**
     * Start version validation of a type
     *
     * @param type type
     */
    private void _startType(int type) {
        List<Class<? extends VersionControl>> collection = this._relations.get(type);
        if (collection == null) collection = new LinkedList<>();
        if (collection.size() > 0) {
            for (Class<? extends VersionControl> c : collection) {
                VersionControl vc = this._instantions.get(c);
                StorageItem cache = this._version(vc);
                vc.onBeforeInit(cache);
                if (this._version(vc).exists()) {
                    this.onUpdate(vc);
                } else {
                    vc.onFirstUse();
                    this._version(vc).set(vc.getCurrentVersion());
                }
            }
        }
    }

    /**
     * Get cache instance of version
     *
     * @param vc {@link VersionControl} object
     *
     * @return LocalStorage.Item
     */
    private StorageItem _version(VersionControl vc) { return Unio.cache(_ID+"_"+vc.getIdentification()); }

    /**
     * Get all update methods of a version, based in version history
     *
     * @return List
     */
    private List<String> _getUpdateMethods(VersionControl vc) {
        List<String> methods = new LinkedList<>();
        if (vc.getAllVersions().length == 0) {
            return methods;
        } else {
            boolean startCount = false;
            for (String version : vc.getAllVersions()) {
                if (startCount) {
                    String method = "update"+version.replace(".", "_");
                    methods.add(method);
                }
                // Start in next version after last updated
                if (Check.isEqual(version, this._version(vc).get())) startCount = true;
                // Finish in current
                if (Check.isEqual(version, vc.getCurrentVersion())) startCount = false;
            }
            return methods;
        }
    }
}