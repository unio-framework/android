package com.unio.modules;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresPermission;
import com.unio.base.UModule;
import com.unio.core.Unio;
import com.unio.util.control.Network;
import com.unio.util.helper.Check;
import com.unio.util.helper.Format;
import com.unio.util.helper.Permission;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;

/**
 * Device
 *
 * Module to find device data
 *
 * Permissions for #getConnectionType and #isOnline
 *     <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 *
 * Permissions for #getIdentificator and #getSerial
 *     <uses-permission android:name="android.permission.READ_PHONE_STATE" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Device extends UModule
{
    /**
     * Get device serial
     *
     * @return String
     */
    @SuppressWarnings("deprecation")
    @SuppressLint({"MissingPermission", "HardwareIds"})
    public String getSerial() {
        // From Android O with permission, uses new code
        if (Permission.isEnabled(Manifest.permission.READ_PHONE_STATE) && Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            return Build.getSerial();

        return Build.SERIAL;
    }

    /**
     * Get device identification
     *
     * May consider as identification:
     * - IMEI
     * - Serial, if IMEI has not found or not exists
     *
     * @return String
     */
    public String getIdentificator() {
        String deviceId = Unio.module(Phone.class).getIMEI();
        if (Check.isEmpty(deviceId) == false) return deviceId;
        return this.getSerial();
    }

    /**
     * Get manufacturer
     *
     * @return String
     */
    public String getManufacturer() { return Format.capitalize(android.os.Build.MANUFACTURER); }

    /**
     * Get model
     *
     * @return String
     */
    public String getModel() {
        String manufacturer = android.os.Build.MANUFACTURER;
        String model = android.os.Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return Format.capitalize(model);
        } else {
            return Format.capitalize(manufacturer) + " " + model;
        }
    }

    /**
     * Get current battery level
     *
     * @return String
     */
    public double getBatteryLevel() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryIntent = Unio.app().registerReceiver(null, filter);
        if (batteryIntent == null) return 0;
        int rawLevel = batteryIntent.getIntExtra("level", -1);
        double scale = batteryIntent.getIntExtra("scale", -1);
        double level = -1;

        if (rawLevel >= 0 && scale > 0) level = (rawLevel/scale);
        return level*100; // Return as percentage
    }

    /**
     * Get current connection type
     * Requires android.permission.ACCESS_NETWORK_STATE
     *
     * @see ConnectivityManager
     *
     * @return int
     */
    @Deprecated
    @RequiresPermission(ACCESS_NETWORK_STATE)
    public int getConnectionType() { return Network.getConnectionType(); }

    /**
     * Check if device is online
     * Requires android.permission.ACCESS_NETWORK_STATE
     *
     * @return boolean
     */
    @Deprecated
    @RequiresPermission(ACCESS_NETWORK_STATE)
    public boolean isOnline() { return Network.isOnline(); }

    /**
     * Get network data
     *
     * @return NetworkInfo
     */
    @Deprecated
    public NetworkInfo getNetworkInfo() { return Network.getNetworkInfo(); }
}