package com.unio.modules;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.unio.base.UModule;
import com.unio.core.Unio;
import com.unio.util.helper.Check;
import com.unio.util.helper.Permission;
import java.lang.reflect.Method;

/**
 * Phone
 *
 * Phone SIM management module
 *
 * Required permission
 *     <uses-permission android:name="android.permission.READ_PHONE_STATE" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Phone extends UModule {
    /** TelephonyManager **/
    private TelephonyManager _TelephonyManager;

    /** SIM status **/
    private boolean _isSIM1Ready;
    private boolean _isSIM2Ready;

    /** IMEI **/
    private String _imeiSIM1;
    private String _imeiSIM2;

    /** Carrier **/
    private String _carrier1;
    private String _carrier2;

    /** IMSI **/
    private String _subscriber1;
    private String _subscriber2;

    /** Serial **/
    private String _serial1;
    private String _serial2;

    /**
     * Flag to define if module will be recreated in every call
     * False by default to use in cache
     *
     * @return boolean
     */
    @Override
    public boolean reloadWhenCalled() {
        return true;
    }

    /**
     * onInit
     */
    @Override
    public void onInit() {
        this._TelephonyManager = (TelephonyManager) Unio.app().getSystemService(Context.TELEPHONY_SERVICE);
        this.setSIMState();
        this.setIMEI();
        this.setCarrier();
        this.setIMSI();
        this.setSerial();
    }

    /**
     * Check SIM status
     */
    private void setSIMState() {
        this._isSIM1Ready = this._TelephonyManager.getSimState() == TelephonyManager.SIM_STATE_READY;
        this._isSIM2Ready = false;

        try {
            this._isSIM1Ready = getSIMStateBySlot("getSimStateGemini", 0);
            this._isSIM2Ready = getSIMStateBySlot("getSimStateGemini", 1);
        } catch (GeminiMethodNotFoundException e) {
            try {
                this._isSIM1Ready = getSIMStateBySlot("getSimState", 0);
                this._isSIM2Ready = getSIMStateBySlot("getSimState", 1);
            } catch (GeminiMethodNotFoundException ignored) {
            }
        }
    }

    /**
     * Get IMEI
     */
    @SuppressWarnings("deprecation")
    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void setIMEI() {
        if (Permission.isEnabled(Manifest.permission.READ_PHONE_STATE) == false) return;

        this._imeiSIM1 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ?
            this._TelephonyManager.getImei() : this._TelephonyManager.getDeviceId();
        this._imeiSIM2 = null;

        if (this._isSIM2Ready) {
            try {
                this._imeiSIM1 = getValueBySlot("getDeviceIdGemini", 0);
                this._imeiSIM2 = getValueBySlot("getDeviceIdGemini", 1);
            } catch (GeminiMethodNotFoundException e1) {
                try {
                    this._imeiSIM1 = getValueBySlot("getImei", 0);
                    this._imeiSIM2 = getValueBySlot("getImei", 1);
                } catch (GeminiMethodNotFoundException e2) {
                    try {
                        this._imeiSIM1 = getValueBySlot("getDeviceId", 0);
                        this._imeiSIM2 = getValueBySlot("getDeviceId", 1);
                    } catch (GeminiMethodNotFoundException ignored) {
                    }
                }
            }
            // Single slot devices can nullify previous data
            // So, if is empty, set again
            if (Check.isEmpty(this._imeiSIM1)) {
                this._imeiSIM1 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ?
                    this._TelephonyManager.getImei() : this._TelephonyManager.getDeviceId();
            }
        }
    }

    /**
     * Set carrier
     */
    private void setCarrier() {
        this._carrier1 = this._TelephonyManager.getSimOperatorName();
        this._carrier2 = null;
        if (this._isSIM2Ready) {
            try {
                this._carrier1 = getValueBySlot("getSimOperatorNameGemini", 0);
                this._carrier2 = getValueBySlot("getSimOperatorNameGemini", 1);
            } catch (GeminiMethodNotFoundException e) {
                try {
                    this._carrier1 = getValueBySlot("getSimOperatorName", 0);
                    this._carrier2 = getValueBySlot("getSimOperatorName", 1);
                } catch (GeminiMethodNotFoundException ignored) {
                }
            }
        }
    }

    /**
     * Set IMSI
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void setIMSI() {
        if (Permission.isEnabled(Manifest.permission.READ_PHONE_STATE) == false) return;

        this._subscriber1 = this._TelephonyManager.getSubscriberId();
        this._subscriber2 = null;
        if (this._isSIM2Ready) {
            try {
                this._subscriber1 = getValueBySlot("getSubscriberIdGemini", 0);
                this._subscriber2 = getValueBySlot("getSubscriberIdGemini", 1);
            } catch (GeminiMethodNotFoundException e) {
                try {
                    this._subscriber1 = getValueBySlot("getSubscriberId", 0);
                    this._subscriber2 = getValueBySlot("getSubscriberId", 1);
                } catch (GeminiMethodNotFoundException ignored) {
                }
            }
        }
    }

    /**
     * Set SIM Serial
     */
    @SuppressLint({"MissingPermission", "HardwareIds"})
    private void setSerial() {
        if (Permission.isEnabled(Manifest.permission.READ_PHONE_STATE) == false) return;

        this._serial1 = this._TelephonyManager.getSimSerialNumber();
        this._serial2 = null;
        if (this._isSIM2Ready) {
            try {
                this._serial1 = getValueBySlot("getSimSerialNumberGemini", 0);
                this._serial2 = getValueBySlot("getSimSerialNumberGemini", 1);
            } catch (GeminiMethodNotFoundException e) {
                try {
                    this._serial1 = getValueBySlot("getSimSerialNumber", 0);
                    this._serial2 = getValueBySlot("getSimSerialNumber", 1);
                } catch (GeminiMethodNotFoundException ignored) {}
            }
        }
    }

    /**
     * Find additional slot data
     * Android class by default only works with single slots and manufacturers include other slot methods
     * So need to search on the class for additional slots
     *
     * @param predictedMethodName method name
     * @param slotID              slot id
     *
     * @return String
     */
    private String getValueBySlot(String predictedMethodName, int slotID)
        throws GeminiMethodNotFoundException {
        String value = null;
        try{
            Class<?> telephonyClass = Class.forName(this._TelephonyManager.getClass().getName());

            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method valueMethod = telephonyClass.getMethod(predictedMethodName, parameter);

            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object response = valueMethod.invoke(this._TelephonyManager, obParameter);

            if (response != null) value = response.toString();
        } catch (Exception e) {
            throw new GeminiMethodNotFoundException(predictedMethodName);
        }
        return value;
    }

    /**
     * Get SIM state by slot
     *
     * @param predictedMethodName method
     * @param slotID              slot id
     *
     * @return boolean
     */
    private boolean getSIMStateBySlot(String predictedMethodName, int slotID)
        throws GeminiMethodNotFoundException {
        boolean isReady = false;
        String response = getValueBySlot(predictedMethodName, slotID);
        if (response != null) {
            int simState = Integer.parseInt(response);
            if (simState == TelephonyManager.SIM_STATE_READY) isReady = true;
        }
        return isReady;
    }

    /**
     * Old emulators have 000000000000000 as IMEI. Replaces to 12345678901234
     *
     * @param rawIMEI initial value
     * @param slot    slot
     *
     * @return String
     */
    private String formatIMEI(String rawIMEI, int slot) {
        if (rawIMEI == null) return null;
        return rawIMEI.equals("000000000000000") ? slot+"12345678901234" : rawIMEI;
    }

    /**
     * Get SIM status
     *
     * @return boolean
     */
    public boolean isSIMReady() { return isSIMReady(1); }
    public boolean isSIMReady(int slot) { return slot == 1 ? _isSIM1Ready : _isSIM2Ready; }

    /**
     * Get IMEI
     *
     * @return String
     */
    public String getIMEI() { return getIMEI(1); }
    public String getIMEI(int slot) { return slot == 1 ? this.formatIMEI(_imeiSIM1, slot) : this.formatIMEI(_imeiSIM2, slot); }

    /**
     * Get carrier
     *
     * @return String
     */
    public String getCarrier() { return getCarrier(1); }
    public String getCarrier(int slot) { return slot == 1 ? _carrier1 : _carrier2; }

    /**
     * Get IMSI
     *
     * @return String
     */
    public String getIMSI() { return getIMSI(1); }
    public String getIMSI(int slot) { return slot == 1 ? _subscriber1 : _subscriber2; }

    /**
     * Get Serial
     *
     * @return String
     */
    public String getSerial() { return getSerial(1); }
    public String getSerial(int slot) { return slot == 1 ? _serial1 : _serial2; }

    /**
     * GeminiMethodNotFoundException
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    @SuppressWarnings("UnusedDeclaration")
    private class GeminiMethodNotFoundException extends Exception {
        private final long serialVersionUID = -996812356902545308L;
        public GeminiMethodNotFoundException(String info) { super(info); }
    }
}