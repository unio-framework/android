package com.unio.modules;

import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import com.unio.base.UModule;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.ui.features.fonts.FontAwesome;
import com.unio.ui.features.fonts.MaterialIcon;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.ui.features.fonts.base.IconFont;
import com.unio.util.helper.Check;
import com.unio.util.manager.ResourceManager;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * UMFormatter
 *
 * Module to create variable contained texts
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 14/02/2016 22:17
 */
public class Formatter extends UModule
{
    /** Initial rules */
    public static final String RESOURCE_ID   = "RESOURCE_ID";
    public static final String FONT_AWESOME  = "FONT_AWESOME";
    public static final String MATERIAL_ICON = "MATERIAL_ICON";
    public static final String SIMPLE        = "SIMPLE";

    /**
     * Regex to identify variable
     * Accepted:
     * - {{variable}}
     * - {{ variable }}, and any space between {{ and }}
     */
    @SuppressWarnings("RegExpRedundantEscape")
    final String REGEX = "\\{\\{(.*?)\\}\\}";

    /** Rule to format resource ID. Example: {{\@string/value}} */
    private final IRule _$ResourceCallback = new IRule() {
        @Override
        public String name() { return RESOURCE_ID; }

        @Override
        public Typeface typeface() { return null; }

        @Override
        public String run(String text) {
            if (text.startsWith("@")) {
                String temp = text.replaceFirst("@", "");
                String[] ps = temp.split("/");
                if (ps.length == 2) {
                    int id = ResourceManager.getId(ps[1], ps[0]);
                    if (id != 0) return ResourceManager.get(id, ps[0]).toString();
                }
            }
            return text;
        }
    };

    /** Rule to format {@link com.unio.ui.features.fonts.FontAwesome} variables. Example: {{#fa/android}} */
    private final IRule _$FontAwesomeCallback = new IRule() {
        @Override
        public String name() { return FONT_AWESOME; }

        @Override
        public Typeface typeface() { return FontAwesome.typeface(); }

        @Override
        public String run(String text) { return fontIconFormat(text, "fa", FontAwesome.class); }
    };

    /** Rule to format {@link com.unio.ui.features.fonts.MaterialIcon} variables. Example: {{#mi/android}} */
    private final IRule _$MaterialIconCallback = new IRule() {
        @Override
        public String name() { return MATERIAL_ICON; }

        @Override
        public Typeface typeface() { return MaterialIcon.typeface(); }

        @Override
        public String run(String text) { return fontIconFormat(text, "mi", MaterialIcon.class); }
    };

    /** Rule list */
    private Map<String, IRule> _rules = new LinkedHashMap<>();

    /**
     * Initial method
     */
    @Override
    public void onInit() {
        super.onInit();
        // Add default rules
        this.addRule(this._$ResourceCallback);
        this.addRule(this._$FontAwesomeCallback);
        this.addRule(this._$MaterialIconCallback);
    }

    /**
     * Add a rule
     *
     * @param rule {@link IRule} instance
     */
    public void addRule(IRule rule) {
        if (rule != null && !Check.isEmpty(rule.name()) && !this._rules.containsKey(rule.name()))
            this._rules.put(rule.name(), rule);
    }

    /**
     * Remove a rule
     *
     * @param name rule identifier
     */
    public void removeRule(String name) { if (Check.isEmpty(name) == false) this._rules.remove(name); }

    /**
     * Format a text
     *
     * @param text text
     *
     * @return CharSequence to adapt Views parameter
     */
    public CNText format(CharSequence text) { return this.format(text, null); }

    /**
     * Format a text
     *
     * @param text   text
     * @param params simple parameters
     *
     * @return CharSequence to adapt Views parameter
     */
    public CNText format(CharSequence text, Map<String, Object> params) {
        if (Check.isEmpty(params) == false) {
            final Map<String, String> p = new LinkedHashMap<>();
            for (String k : params.keySet()) {
                Object o = params.get(k);
                if (Check.isEmpty(o)) continue;
                p.put(k, o.toString());
            }
            if (Check.isEmpty(p) == false) {
                this.addRule(new IRule() {
                    @Override
                    public String name() { return SIMPLE; }

                    @Override
                    public Typeface typeface() { return null; }

                    @Override
                    public String run(String text) { return p.containsKey(text) ? p.get(text) : text; }
                });
            }
        }
        return this._format(text, null);
    }

    /**
     * Format variables in text
     *
     * @param text     text
     * @param typeface typeface for recursive actions
     *
     * @return CNText
     */
    @SuppressWarnings("RegExpRedundantEscape")
    private CNText _format(CharSequence text, Typeface typeface) {
        if (Check.isEmpty(text)) return new CNText("", null);

        String formatted = text.toString();
        Matcher m = Pattern.compile(REGEX).matcher(text);
        List<String> ignored = new LinkedList<>();
        while (m.find()) {
            String v = m.group();
            if (ignored.contains(v)) continue;

            String name = v.replaceAll("(\\{\\{|\\}\\})", "");
            name = name.trim();
            String newValue = "";
            if (Check.isPresent(name)) {
                for (String key : this._rules.keySet()) {
                    IRule rule = this._rules.get(key);
                    newValue = rule.run(name);
                    if (Check.isNotEqual(name, newValue)) {
                        formatted = formatted.replace(v, newValue);
                        if (Unio.enabled()) typeface = rule.typeface();
                        break;
                    }
                }
            }
            if (Check.isEqual(name, newValue) && ignored.contains(v) == false) ignored.add(v);
        }
        CNText response = this._needRerun(formatted, ignored) ? this._format(formatted, typeface) : new CNText(formatted, typeface);
        this.removeRule(SIMPLE);
        return response;
    }

    /**
     * Check if need rerun {@link #format(CharSequence)} for internal variables
     *
     * @param text    text
     * @param ignored ignored variables list
     *
     * @return boolean
     */
    @SuppressWarnings("RegExpRedundantEscape")
    private boolean _needRerun(String text, List<String> ignored) {
        // Run recursivelly to format internal variable results
        Matcher m = Pattern.compile(REGEX).matcher(text);
        boolean rerun = false;
        while (m.find()) {
            String v = m.group();
            if (ignored.contains(v)) continue;

            v = v.replaceAll("(\\{\\{|\\}\\})", "");
            v = v.trim();
            // To prevent StackOverflowException when string is "{{}}"
            if (Check.isEmpty(v) == false) {
                rerun = true;
                break;
            }
        }
        return rerun;
    }

    /**
     * Validation rule for font icons
     *
     * @param rule rule, only name, by default font variable is "#[name]/"
     * @param c    IconFont class
     *
     * @return String
     */
    public String fontIconFormat(String text, String rule, Class<? extends IconFont> c) {
        if (Check.isEmpty(rule)) return text;

        if (rule.equals("#"+rule+"/") == false) rule = "#"+rule+"/";
        if (text.startsWith(rule)) {
            String[] ps = text.split("/");
            if (ps.length == 2 && Check.isPresent(ps[1])) {
                String name = ps[1].toUpperCase();
                try {
                    Field field = c.getField(name);
                    IconCharacter ic = (IconCharacter) field.get(null);
                    return String.valueOf(ic.icon());
                } catch (NoSuchFieldException e) {
                    if (Unio.enabled()) Trace.get(e);
                } catch (IllegalAccessException e) {
                    if (Unio.enabled()) Trace.get(e);
                }
            }
        }
        return text;
    }

    /**
     * CNText
     *
     * Constant class to return complete text and a custom typeface
     * By operational issue, it will accept only one typeface, the last typeface that will return from format
     *
     * Optionally has static class {@link CustomTypefaceSpan} to set own custom typeface in {@link android.text.Spannable}
     */
    public static class CNText
    {
        /** Attributes */
        public final CharSequence text;
        public final Typeface typeface;
        public final SpannableString spannable;

        /**
         * Constructor
         *
         * @param text     text
         * @param typeface typeface
         */
        public CNText(CharSequence text, Typeface typeface) {
            this.text = text;
            this.typeface = typeface;
            SpannableString spannable = null;
            if (Check.isEmpty(this.text) == false) {
                spannable = new SpannableString(text);
                if (this.typeface != null)
                    spannable.setSpan(
                        new CustomTypefaceSpan(this.typeface),
                        0,
                        this.text.length()-1,
                        Spanned.SPAN_EXCLUSIVE_INCLUSIVE
                    );
            }
            this.spannable = spannable;
        }

        /**
         * toString
         *
         * @return String
         */
        @Override
        public String toString() { return "text="+this.text+"; typeface: "+this.typeface; }

        /**
         * CustomTypefaceSpan
         *
         * Class to add custom typeface to spannable
         */
        public static class CustomTypefaceSpan extends TypefaceSpan
        {
            /** Typeface */
            private final Typeface _newType;

            /**
             * Constructor
             */
            public CustomTypefaceSpan(Typeface type) { this("", type); }

            /**
             * Constructor
             */
            public CustomTypefaceSpan(String family, Typeface type) {
                super(family);
                this._newType = type;
            }

            /**
             * updateDrawState
             *
             * @param ds ds
             */
            @Override
            public void updateDrawState(@NonNull TextPaint ds) { this._applyCustomTypeFace(ds, this._newType); }

            /**
             * updateMeasureState
             *
             * @param paint paint
             */
            @Override
            public void updateMeasureState(@NonNull TextPaint paint) { this._applyCustomTypeFace(paint, this._newType); }

            /**
             * Create font
             *
             * @param paint paint
             * @param tf    typeface
             */
            private void _applyCustomTypeFace(Paint paint, Typeface tf) {
                Typeface old = paint.getTypeface();
                int oldStyle = old == null ? 0 : old.getStyle();
                int fake = oldStyle & ~tf.getStyle();
                if ((fake & Typeface.BOLD) != 0) paint.setFakeBoldText(true);
                if ((fake & Typeface.ITALIC) != 0) paint.setTextSkewX(-0.25f);
                paint.setTypeface(tf);
            }
        }
    }

    /**
     * IRule
     *
     * Format rule
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 14/02/2016 22:17
     */
    public interface IRule
    {
        /**
         * Rule identifier
         *
         * @return String
         */
        String name();

        /**
         * Get typeface if need
         *
         * @return Typeface, or {@code null} if not need
         */
        Typeface typeface();

        /**
         * Rule
         *
         * @param text variable
         *
         * @return String
         */
        String run(String text);
    }
}