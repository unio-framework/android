package com.unio.modules;

import com.unio.database.preference.StorageItem;

/**
 * VersionControl
 *
 * Model for version control
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class VersionControl
{
    /**
     * Get identification for this version control
     * System will update based in version of this identification
     * You can pass package name. Need to differentiate only between Version Controls
     *
     * @return String
     */
    protected String getIdentification() { return this.getClass().getPackage().getName(); }

    /**
     * Get current version
     *
     * Using underscore "_", is possible validate version codes too:
     * 1.0.0_1001
     *   |_____|_____ Is human readable version
     *         |_____ Is version code
     *   If has a version to homologate and need to keep readable version, use version code to add new updates.
     *   When use {@link Version#getVersion} version code will be removed, getting only readable part.
     *
     * @return String
     */
    public String getCurrentVersion() {
        // Versão atual é o último do histórico
        String[] allVersions = this.getAllVersions();
        return allVersions[allVersions.length-1];
    }

    /**
     * Abstract
     * Get all version historic, in update order
     * The update process is based here, and is possible update in any old version
     *
     * Example:
     *   new String[] {
     *       "1.0.0",
     *       "1.0.1",
     *       "1.0.1_1011",
     *       "1.0.1_1012",
     *       "1.0.2",
     *   }
     *
     * @return String[]
     */
    public abstract String[] getAllVersions();

    /**
     * Abstract
     * Actions before start update process
     *
     * @param cache Unio.cache of assigned version
     */
    protected abstract void onBeforeInit(StorageItem cache);

    /**
     * Abstract
     * Actions when is first use
     */
    public abstract void onFirstUse();

    /**
     * Abstract
     * Actions after update finish
     */
    protected abstract void onAfterUpdate();
}