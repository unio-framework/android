package com.unio.modules;

import com.unio.base.UModule;
import com.unio.core.Unio;
import com.unio.database.sql.connection.Db;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;

import java.util.*;

/**
 * Database
 *
 * Core module, add and retrieve databases
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Database extends UModule
{
    /** Default DB version */
    public static final int DB_DEFAULT_VERSION = 1;

    /** Attributes */
    private static List<String> _names = new LinkedList<>();
    private static Map<String, Db> _dbs = new LinkedHashMap<>();

    /**
     * onInit
     *
     * At least the database name is mandatory to start
     */
    @Override
    public void onInit() {
        if (this.isRunning() == false)
            Trace.log(this, "Module Database is mandatory at least the name of database");
    }

    /**
     * Add a database to module
     *
     * @param name database name
     */
    public void onInit(String name) { this.onInit(name, DB_DEFAULT_VERSION); }

    /**
     * Add a database to module
     *
     * @param name    database name
     * @param version database version
     */
    public void onInit(String name, int version) {
        if (Check.isEmpty(name) == false && _dbs.containsKey(name) == false) {
            Db db = new Db(Unio.app(), name, version);
            _dbs.put(name, db);
            _names.add(name);
            this.isRunning = true;
        }
    }

    /**
     * Get default database
     *
     * @return Db
     */
    public synchronized Db get() { return this.get(null); }

    /**
     * Get a database from name
     * If name is null, will return first added database as default
     *
     * @param name database name
     *
     * @return Db
     */
    public synchronized Db get(String name) {
        if (this.isRunning == false || _dbs.size() == 0) return null;

        if (Check.isEmpty(name)) name = _names.get(0);
        Db db = _dbs.get(name);
        if (db.isOpen() == false) db.start();
        return db;
    }

    /**
     * Get all databases
     *
     * @return Db[]
     */
    public synchronized Db[] all() {
        if (_dbs.size() == 0) {
            return new Db[0];
        } else {
            List<Db> dbs = new LinkedList<>();
            Set<String> keys = _dbs.keySet();
            for (String key : keys) {
                if (Check.isEqual(key, "__unio")) continue;
                dbs.add(_dbs.get(key));
            }
            return dbs.toArray(new Db[0]);
        }
    }
}