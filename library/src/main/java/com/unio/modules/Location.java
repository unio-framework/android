package com.unio.modules;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.*;
import android.os.Bundle;
import android.os.Looper;
import com.unio.base.UModule;
import com.unio.base.URunnable;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.shortcuts.Android;
import com.unio.util.io.Hash;
import com.unio.util.control.Date;
import com.unio.util.helper.*;
import com.unio.util.rest.Http;
import com.unio.util.statics.constants.Time;
import com.unio.util.statics.interfaces.IAsync;
import java.io.IOException;
import java.util.*;

/**
 * Location
 *
 * Module to work with GPS nativelly
 *
 * Required permissions
 *     <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
 *     <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
 *     <uses-permission android:name="android.permission.ACCESS_MOCK_LOCATION" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
@SuppressWarnings({"MissingPermission"})
public class Location extends UModule implements LocationListener
{
    /** Location types */
    public enum EType { GPS, NETWORK, PASSIVE, FUSED, NONE }
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Constants */
    protected static final long  DEFAULT_MIN_TIME = Time.Milisecond.SECOND*90;
    protected static final float MIN_DISTANCE     = 0;
    protected static final float MIN_ACCURACY     = 80;
    protected static final float MIN_NEWEST_TIME  = TWO_MINUTES;

    /** Min time */
    protected long minTime = DEFAULT_MIN_TIME;

    /** Instances */
    private LocationManager _LocationManager;
    private android.location.Location _GpsLocation = null;
    private android.location.Location _NetLocation = null;
    private Address       _NetAddressResponse;

    protected android.location.Location _Location;
    protected Result _LocationResponse;

    private android.location.Location _CurrentBest;
    private EType _currentProvider = EType.NONE;

    /** Flags */
    private boolean _hasGPS               = false;
    private boolean _hasNetwork           = false;
    private boolean _gpsSearchEnabled     = false;
    private boolean _networkSearchEnabled = false;
    protected boolean _canGetLocation     = false;

    protected boolean _started         = false;
    protected boolean _runnableRunning = true;

    /** Routine management */
    private Map<Long, Map<String, IRoutine>> _routines = new LinkedHashMap<>();
    private Map<Long, URunnable> _runnables            = new LinkedHashMap<>();
    private List<EType> _types                         = new LinkedList<>();

    /**
     * onInit
     *
     * Will start with GPS and NETWORK
     */
    @Override
    public void onInit() { this.onInit(EType.GPS, EType.NETWORK); }

    /**
     * onInit
     *
     * Set location type
     */
    public void onInit(EType type) { this.onInit(DEFAULT_MIN_TIME, new EType[] { type }); }

    /**
     * onInit
     *
     * Set min update time and location type
     */
    public void onInit(long minTime, EType type) { this.onInit(minTime, new EType[] { type }); }

    /**
     * onInit
     */
    public void onInit(EType... types) { this.onInit(DEFAULT_MIN_TIME, types); }

    /**
     * onInit
     */
    public void onInit(long minTime, EType... types) {
        List<EType> t = Arrays.asList(types);
        this.onInit(minTime, t);
    }

    /**
     * onInit
     */
    public void onInit(List<EType> types) { this.onInit(DEFAULT_MIN_TIME, types); }

    /**
     * onInit
     */
    public void onInit(long minTime, List<EType> types) {
        this.minTime = minTime;
        if (this._started == false) {
            try {
                this._LocationManager = (LocationManager) Unio.app().getSystemService(Context.LOCATION_SERVICE);
                this.updateType(types);
                this.start();
            } catch (Exception e) {
                Trace.get(e);
            }
        }
    }

    /**
     * Start module
     */
    public void start() {
        if (this._started == false && (this._hasGPS || this._hasNetwork)) {
            this._started = true;
            this.update();
            this.runRunnables();
        }
    }

    /**
     * Stop module
     */
    public void stop() {
        if (this._started) {
            //noinspection MissingPermission
            this._LocationManager.removeUpdates(this);
            for (Map.Entry<Long, URunnable> entry : this._runnables.entrySet()) {
                if (entry.getValue() == null) continue;
                entry.getValue().stop();
            }
            this._started         = false;
            this._runnableRunning = false;

            this._gpsSearchEnabled     = false;
            this._networkSearchEnabled = false;
        }
    }

    /**
     * Check if service is started
     *
     * @return boolean
     */
    public boolean isStarted() { return this._started; }

    /**
     * If Runnables exists and is not running, run
     */
    protected void runRunnables() {
        if (this._runnableRunning == false) {
            for (Map.Entry<Long, URunnable> entry : this._runnables.entrySet()) {
                if (entry.getValue() == null) continue;
                entry.getValue().run();
            }
            this._runnableRunning = true;
        }
    }

    /**
     * Update types
     *
     * @param types {@link EType}
     */
    public void updateType(List<EType> types) {
        this._types = types;
        if (this._types.contains(EType.GPS)) {
            this._hasGPS = this._LocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            if (this._hasGPS && this._started && this._gpsSearchEnabled == false) {
                this._gpsSearchEnabled = true;
                this._GpsLocation = this._getLocation(LocationManager.GPS_PROVIDER);
            } else if (this._hasGPS == false) {
                this._gpsSearchEnabled = false;
            }
        }
        if (this._types.contains(EType.NETWORK)) {
            this._hasNetwork = this._LocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            if (this._hasNetwork && this._started && this._networkSearchEnabled == false) {
                this._networkSearchEnabled = true;
                this._NetLocation = this._getLocation(LocationManager.NETWORK_PROVIDER);
            } else if (this._hasNetwork == false) {
                this._networkSearchEnabled = false;
            }
        }
    }

    /**
     * Search for best location
     */
    public void update() {
        this.updateType(this._types);
        if (this._started) {
            android.location.Location CurrentBest;
            EType    currentProvider;

            android.location.Location gpsLastKnow = this._hasGPS     ? this._LocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)     : null;
            android.location.Location netLastKnow = this._hasNetwork ? this._LocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) : null;

            // Compara o obtido por callback e o obtido por getLastKnownLocation
            this._GpsLocation = this.getBestOfTwo(this._GpsLocation, gpsLastKnow);
            this._NetLocation = this.getBestOfTwo(this._NetLocation, netLastKnow);
            this._Location    = this.getBestOfTwo(this._GpsLocation, this._NetLocation); // Compara as 2 melhores localizações para pegar o melhor dos 2, com prioridade no GPS
            this._LocationResponse  = new Result(this._Location);
            this._canGetLocation    = this._Location != null;
        } else {
            this.start();
        }
    }

    /**
     * Search for best location of 2
     *
     * @param fromCallback LocationListener location
     * @param fromLastKnow getLastKnownLocation location
     *
     * @return Location
     */
    public android.location.Location getBestOfTwo(android.location.Location fromCallback, android.location.Location fromLastKnow) {
        if (fromLastKnow == null) {
            // A new location is always better than no location
            return fromCallback;
        } else if (fromCallback == null) {
            return fromLastKnow;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = fromCallback.getTime() - fromLastKnow.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If is newer and accuracy is within minimus, accept this
        if (isNewer && fromCallback.getAccuracy() <= MIN_ACCURACY) return fromCallback;

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (fromCallback.getAccuracy() - fromLastKnow.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > MIN_ACCURACY;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = this._isSameProvider(fromCallback.getProvider(), fromLastKnow.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (timeDelta == 0 && isMoreAccurate) {
            return fromCallback;
        } else if (isMoreAccurate && isSignificantlyOlder == false) {
            return fromCallback;
        } else if (isNewer && isLessAccurate == false) {
            return fromCallback;
        } else if (isNewer && isSignificantlyLessAccurate == false && isFromSameProvider) {
            return fromCallback;
        }
        return fromLastKnow;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean _isSameProvider(String provider1, String provider2) {
        if (provider1 == null) return provider2 == null;
        return provider1.equals(provider2);
    }

    /**
     * Add routine
     *
     * @param routine routine
     *
     * @return Location
     */
    public Location addRoutine(final IRoutine routine) {
        if (routine != null) {
            Map<String, IRoutine> r = this._routines.get(routine.interval());
            if (r == null) r = new LinkedHashMap<>();
            r.remove(routine.name());
            r.put(routine.name(), routine);
            this._routines.put(routine.interval(), r);
            if (this._runnables.containsKey(routine.interval()) == false) {
                URunnable runnable = new URunnable() {
                    @Override
                    protected void settings() {
                        super.settings();
                        this.interval = routine.interval();
                    }

                    @Override
                    protected void onInit() {
                        update();
                        boolean stop = false;
                        if (_routines.containsKey(this.interval)) {
                            Map<String, IRoutine> rs = _routines.get(this.interval);
                            for (Map.Entry<String, IRoutine> entry : rs.entrySet()) {
                                if (entry.getValue() == null) {
                                    rs.remove(entry.getKey());
                                    continue;
                                }
                                entry.getValue().run(Location.this);
                            }
                            if (rs.size() == 0) {
                                stop = true;
                            } else {
                                _routines.put(this.interval, rs);
                            }
                        } else {
                            stop = true;
                        }
                        if (stop) {
                            _routines.remove(this.interval);
                            URunnable r = _runnables.get(this.interval);
                            r.stop();
                            _runnables.remove(this.interval);
                        }
                    }
                };
                this._runnables.put(routine.interval(), runnable);
                runnable.start();
            }
        }
        return this;
    }

    /**
     * Check if routine exists
     *
     * @param interval interval
     * @param name     name
     *
     * @return boolean
     */
    public boolean hasRoutine(long interval, String name) {
        Map<String, IRoutine> routine = this._routines.get(interval);
        return routine != null && routine.containsKey(name);
    }

    /**
     * Remove routine
     *
     * @param interval interval
     * @param name     name
     */
    public void removeRoutine(long interval, String name) {
        Map<String, IRoutine> routine = this._routines.get(interval);
        if (routine != null) {
            routine.remove(name);
            if (routine.size() == 0) {
                this._routines.remove(interval);
                URunnable r = this._runnables.get(interval);
                r.stop();
                this._runnables.remove(interval);
            }
        }
    }

    /**
     * Get raw location from provider
     *
     * @param provider meio para obter a localização
     */
    private android.location.Location _getLocation(String provider) {
        this._LocationManager.requestLocationUpdates(provider, this.minTime, MIN_DISTANCE, this, Looper.getMainLooper());
        return this._LocationManager.getLastKnownLocation(provider);
    }

    /**
     * Action when location changes
     *
     * @param location new location
     */
    @Override
    public void onLocationChanged(android.location.Location location) {
        if (location != null) {
            boolean needUpdate = false;
            switch (location.getProvider()) {
                case LocationManager.GPS_PROVIDER:
                    needUpdate = this.needUpdate(this._GpsLocation, location);
                    this._GpsLocation = this.getBestOfTwo(location, this._GpsLocation);
                    break;
                case LocationManager.NETWORK_PROVIDER:
                    needUpdate = this.needUpdate(this._NetLocation, location);
                    this._NetLocation = this.getBestOfTwo(location, this._NetLocation);
                    break;
            }
            if (needUpdate) this._getLocation(location.getProvider());
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onProviderDisabled(String provider) {}

    /**
     * Validation if need update location request
     *
     * @param l1 maintened location
     * @param l2 new obteined location
     *
     * @return boolean
     */
    protected boolean needUpdate(android.location.Location l1, android.location.Location l2) {
        return this.isEqual(l1, l2) || l2 == null || l2 != null && l2.getAccuracy() > MIN_ACCURACY;
    }

    /**
     * Check if is same location
     *
     * @param l1 location 1
     * @param l2 location 2
     *
     * @return boolean
     */
    protected boolean isEqual(android.location.Location l1, android.location.Location l2) {
        if (l1 == null && l2 == null) {
            return true;
        } else if (l1 != null && l2 != null) {
            return l1.getLatitude() == l2.getLatitude() && l1.getLongitude() == l2.getLongitude();
        }
        return false;
    }

    /**
     * Check if able to get a location
     *
     * @return boolean
     */
    public boolean canGetLocation() { return this._canGetLocation; }

    /**
     * Check if location search if enabled
     *
     * @return boolean
     */
    public boolean isActive() { return this._hasGPS || this._hasNetwork; }

    /**
     * Check if provider is enabled
     *
     * @return boolean
     */
    public boolean isEnabled(EType type) {
        switch (type) {
            case GPS:     return this._hasGPS;
            case NETWORK: return this._hasNetwork;
            default:      return false;
        }
    }

    /**
     * Check if provider is searching location
     *
     * @return boolean
     */
    public boolean isSearching(EType type) {
        switch (type) {
            case GPS:     return this._gpsSearchEnabled;
            case NETWORK: return this._networkSearchEnabled;
            default:      return false;
        }
    }

    /**
     * Get {@link Result}
     *
     * @return Result
     */
    public Result getResult() { return this._LocationResponse; }

    /**
     * Get {@link Result} by provider
     *
     * @return Result
     */
    public Result getResult(EType provider) {
        android.location.Location location = null;
        switch (provider) {
            case GPS:
                location = this._GpsLocation;
                break;
            case NETWORK:
                location = this._NetLocation;
                break;
        }
        return new Result(location);
    }

    /**
     * Find an address from current location
     * Need an internet connection
     *
     * @return Address or null
     */
    public Address getAddress() {
        if (this._started == false) this.onInit();
        if (this.isActive() && this._Location != null) {
            return this.getAddress(this._Location.getLatitude(), this._Location.getLongitude());
        } else {
            return null;
        }
    }

    /**
     * Find an address from a location
     * Need an internet connection
     *
     * @param latitude  latitude
     * @param longitude longitude
     *
     * @return Address, caso encontre, ou {@code null}
     */
    public Address getAddress(double latitude, double longitude) {
        try {
            Geocoder geocoder = new Geocoder(Unio.app(), new Locale("pt", "BR"));
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            List<android.location.Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() == 0) {
                return null;
            } else {
                return new Address(addresses.get(0));
            }
        } catch (IOException e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * Find for address from Google API
     *
     * @return Address
     */
    public Address getAddressFromNetwork() {
        if (this._started == false) this.onInit();
        if (this.isActive() && this._Location != null) {
            return this.getAddressFromNetwork(this._Location.getLatitude(), this._Location.getLongitude());
        } else {
            return null;
        }
    }

    /**
     * Find for address from Google API
     * The API has limitation of 2500 requests per day, 125 requests per try
     *
     * @param latitude  latitude
     * @param longitude longitude
     *
     * @return Address
     */
    public Address getAddressFromNetwork(double latitude, double longitude) {
        String url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latitude+","+longitude+"&sensor=false&language=pt_BR";
        Address address = null;

        Http request = new Http(url).setTimeout(30 * Time.Milisecond.SECOND); // Tempo mínimo para conexões EDGE e inferiores
        request.execute(IAsync.EExecution.SYNCHRONOUS); // Execução síncrona para poder trabalhar com a informação direto
        if (request.response() != null) {
            Trace.log(this, "raw response", request.response().toString());
            Hash response = request.response().toHash();
            if (response.get("results").toHash().length() > 0) address = new Address(request.response().toHash());
        }
        return address;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("startLocationSearch=");
        sb.append(this._started);
        sb.append("\n");
        sb.append("locationTypes=");
        sb.append(Convert.toString(this._types));
        sb.append("\n");
        sb.append("canGetLocation=");
        sb.append(this.canGetLocation());
        sb.append("\n");
        sb.append("isGPSActive=");
        sb.append(this.isActive());
        sb.append("\n");
        for (EType type : this._types) {
            sb.append("isEnabled(").append(type).append(")=");
            sb.append(this.isEnabled(type));
            sb.append("\n");
            sb.append("isSearching(").append(type).append(")=");
            sb.append(this.isSearching(type));
            sb.append("\n");
        }
        if (this._LocationResponse == null) {
            return sb.toString();
        } else {
            return sb.toString()+this._LocationResponse.toString();
        }
    }

    /**
     * Result
     *
     * Location result
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public static class Result
    {
        /** Fields */
        public final float accuracy;
        public final double latitude;
        public final double longitude;
        public final float speed; // em m/s
        public final EType provider;
        public final long time;
        public final String dateTime;

        public final String accuracyString;
        public final String latitudeString;
        public final String longitudeString;
        public final String speedString;
        public final String providerString;

        public final boolean isInValidAccuracy;
        public final boolean isOld;
        public final boolean isMock;
        public final boolean exists;
        public final android.location.Location Raw;

        /**
         * Constructor
         *
         * @param location Location
         */
        @SuppressLint("NewApi")
        public Result(android.location.Location location) {
            this.Raw    = location;
            this.exists = location != null;

            boolean isMock = false;
            if (location == null) {
                this.accuracy  = -1;
                this.latitude  = 0;
                this.longitude = 0;
                this.speed     = -1;
                this.provider  = EType.NONE;
                this.time      = 0;
                this.dateTime  = "";

                this.isInValidAccuracy = false;
                this.isOld             = true;
                isMock                 = false;
            } else {
                this.accuracy  = location.getAccuracy();
                this.latitude  = location.getLatitude();
                this.longitude = location.getLongitude();
                this.speed     = location.getSpeed();
                this.provider  = this._getProvider(location.getProvider());
                this.time      = location.getTime();
                this.dateTime  = Date.format(location.getTime(), Date.DATETIME);

                this.isInValidAccuracy = location.getAccuracy() <= MIN_ACCURACY;
                this.isOld             = System.currentTimeMillis()-location.getTime() > MIN_NEWEST_TIME;
                // isFromMockProvider is implements starting in Android 4.3 (API 18)
                if (Android.CURRENT_VERSION > Android.JELLY_BEAN_MR1) isMock = location.isFromMockProvider();
            }
            this.isMock          = isMock;
            this.accuracyString  = String.valueOf(this.accuracy);
            this.latitudeString  = String.valueOf(this.latitude);
            this.longitudeString = String.valueOf(this.longitude);
            this.speedString     = String.valueOf(this.speed);
            this.providerString  = String.valueOf(this.provider);
        }

        /**
         * Get distance from
         *
         * @param l CNLocation
         *
         * @return float
         */
        public float distanceFrom(Result l) { return (this.exists && l != null && l.exists) ? this.Raw.distanceTo(l.Raw) : 0; }

        /**
         * Get type from provider {@link Location.EType}
         *
         * @param provider provider
         *
         * @return EType
         */
        private EType _getProvider(String provider) {
            switch (provider) {
                case LocationManager.GPS_PROVIDER:     return EType.GPS;
                case LocationManager.NETWORK_PROVIDER: return EType.NETWORK;
                case LocationManager.PASSIVE_PROVIDER: return EType.PASSIVE;
                case "fused":                          return EType.FUSED; // LocationManager.FUSED_PROVIDER está como @hide, não podendo usar a constante
                default:                               return EType.NONE;
            }
        }

        @Override
        public String toString() {
            return "accuracy=" + this.accuracy + "\n" +
                "latitude=" + this.latitude + "\n" +
                "longitude=" + this.longitude + "\n" +
                "speed=" + this.speed + "\n" +
                "provider=" + this.provider + "\n" +
                "time=" + this.time + "\n" +
                "dateTime=" + this.dateTime + "\n" +
                "isInValidAccuracy=" + this.isInValidAccuracy + "\n" +
                "isOld=" + this.isOld + "\n" +
                "isMock=" + this.isMock;
        }
    }

    /**
     * Address
     *
     * Address class
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public static class Address
    {
        /** Fields */
        public final String street;
        public final String number;
        public final String district;
        public final String city;
        public final String state;
        public final String stateCode;
        public final String country;
        public final String countryCode;
        public final String zip;
        public final String latitude;
        public final String longitude;

        /**
         * Constructor
         */
        public Address(android.location.Address address) {
            this.street = address.getThoroughfare();
            this.number = address.getFeatureName();
            this.state  = address.getAdminArea();

            String district  = null;
            String stateCode = null;
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                String line = address.getAddressLine(i);
                if (Check.isEmpty(line) == false) {
                    if (line.contains(this.street+", ") && Check.isEmpty(district)) {
                        line = line.replace(this.street+", ", "");
                        if (line.contains(this.number+" - ")) line = line.replace(this.number+" - ", "");
                        line = line.trim();
                        if (Check.isEmpty(line) == false) district = line;
                    }

                    if (line.contains(this.state+" - ") && Check.isEmpty(stateCode)) {
                        line = line.replace(this.state + " - ", "");
                        line = line.trim();
                        if (Check.isEmpty(line) == false) stateCode = line;
                    }

                    if (Check.isEmpty(district) == false && Check.isEmpty(stateCode) == false) break;
                }
            }
            this.district = district;

            this.city         = address.getSubAdminArea();
            this.stateCode   = stateCode;
            this.country      = address.getCountryName();
            this.countryCode = address.getCountryCode();
            this.zip          = this._formatZip(address.getPostalCode());

            this.latitude     = address.hasLatitude()  ? String.valueOf(address.getLatitude())  : null;
            this.longitude    = address.hasLongitude() ? String.valueOf(address.getLongitude()) : null;
        }

        /**
         * Constructor
         */
        public Address(Hash response) {
            Hash result = response.get("results").toHash().get(0).toHash();
            Hash addressComponents = result.get("address_components").toHash();

            this.street      = addressComponents.get(1).toHash().get("long_name").toString();
            this.number      = addressComponents.get(0).toHash().get("long_name").toString();
            this.district    = addressComponents.get(2).toHash().get("long_name").toString();
            this.city        = addressComponents.get(3).toHash().get("long_name").toString();
            this.state       = addressComponents.get(5).toHash().get("long_name").toString();
            this.stateCode   = addressComponents.get(5).toHash().get("short_name").toString();
            this.country     = addressComponents.get(6).toHash().get("long_name").toString();
            this.countryCode = addressComponents.get(6).toHash().get("short_name").toString();
            this.zip         = this._formatZip(addressComponents.get(7).toHash().get("long_name").toString());

            Hash location = result.get("geometry").toHash().get("location").toHash();
            this.latitude  = location.get("lat").toString();
            this.longitude = location.get("lng").toString();
        }

        /**
         * Zip format
         *
         * @param zip zip
         *
         * @return String
         */
        private String _formatZip(String zip) {
            if (Check.isEqual(this.countryCode, "BR") && Check.isPresent(zip)) {
                zip = zip.replace("-", "");
                zip = Format.rpad(zip, '0', 8);
                zip = zip.substring(0, 5)+"-"+zip.substring(5);
            }
            return zip;
        }

        @Override
        public String toString() {
            return "street=" + this.street + "\n" +
                "number=" + this.number + "\n" +
                "district=" + this.district + "\n" +
                "city=" + this.city + "\n" +
                "state=" + this.state + "\n" +
                "stateCode=" + this.stateCode + "\n" +
                "country=" + this.country + "\n" +
                "countryCode=" + this.countryCode + "\n" +
                "zip=" + this.zip + "\n" +
                "latitude=" + (this.latitude == null ? "null" : this.latitude) + "\n" +
                "longitude=" + (this.longitude == null ? "null" : this.longitude);
        }
    }

    /**
     * IRoutine
     */
    public interface IRoutine<T extends Location>
    {
        /**
         * Routine name
         *
         * @return String
         */
        String name();

        /**
         * Routine interval
         *
         * @return long
         */
        long interval();

        /**
         * Action
         *
         * @param location Location
         */
        void run(T location);
    }
}