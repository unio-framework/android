package com.unio.modules;

import android.annotation.SuppressLint;
import android.net.TrafficStats;
import com.unio.base.UModule;
import com.unio.base.URunnable;
import com.unio.util.control.AppInfo;
import com.unio.util.control.Date;
import com.unio.util.helper.Check;
import com.unio.util.statics.constants.Time;
import com.unio.util.statics.interfaces.IAsync;
import java.util.*;

/**
 * Traffic
 *
 * Get device all app's data traffic
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Traffic extends UModule
{
    /** Main runnable */
    private URunnable _Runnable;

    /** Routes to run, by package */
    private Map<String, IRoutine> _Routines    = new LinkedHashMap<>();

    /** Last data */
    private Map<String, CNData> _LastKnowDatas = new LinkedHashMap<>();

    /** If device not has support to this class, not run */
    private boolean _removeWhenUnsupported = true;

    /** Async Task */
    private IAsync _Task;

    /**
     * onInit
     */
    @Override
    public void onInit() { this.onInit(Time.Milisecond.MINUTE*30); }

    /**
     * onInit
     *
     * @param time interval to catch
     */
    public void onInit(final int time) {
        super.onInit();
        if (this._Runnable == null) {
            this._Runnable = new URunnable() {
                @Override
                protected void settings() {
                    super.settings();
                    this.interval = time;
                }

                @SuppressLint("StaticFieldLeak")
                @Override
                protected void onInit() {
                    if (Check.isEmpty(_Routines) == false) {
                        Set<String> ks = _Routines.keySet();
                        final List<String> keys = new LinkedList<>(ks);
                        new IAsync() {
                            @SuppressWarnings("ForLoopReplaceableByForEach")
                            @Override
                            protected void onInit() {
                                List<String> removes = new LinkedList<>();
                                for (int i = 0; i < keys.size(); i++) {
                                    final String name = keys.get(i);
                                    final IRoutine r = _Routines.get(name);
                                    if (r == null) {
                                        _Routines.remove(name);
                                        continue;
                                    }
                                    double lastUpload   = -1;
                                    double lastDownload = -1;

                                    CNData lastData = _LastKnowDatas.get(name);
                                    if (lastData != null && lastData.isAvailable) {
                                        lastUpload   = lastData.grandUpload;
                                        lastDownload = lastData.grandDownload;
                                    } else {
                                        if (r.lastUpload()   != -1) lastUpload   = r.lastUpload();
                                        if (r.lastDownload() != -1) lastDownload = r.lastDownload();
                                    }

                                    final CNData data = new CNData(name, lastUpload, lastDownload);
                                    _LastKnowDatas.put(name, data);
                                    if (data.isAvailable == false && _removeWhenUnsupported) {
                                        _Routines.remove(name);
                                    } else {
                                        // Run user control action in UI to prevent Background Exception
                                        this.addOnUI(new IOnUI() {
                                            @Override
                                            public void onExecuteInUI() { r.run(data); }
                                        });
                                    }
                                }
                            }
                        }.execute();
                    }
                }
            };
        }
    }

    /**
     * Add a routine to get data
     *
     * @param routine {@link IRoutine}
     */
    public void addRoutine(IRoutine routine) {
        if (routine != null) {
            this._Routines.put(routine.name(), routine);
            if (this._Runnable.isRunning() == false) _Runnable.start();
        }
    }

    /**
     * Remove a rotine
     *
     * @param name pacote
     */
    public void removeRoutine(String name) {
        if (this._Routines.containsKey(name)) {
            this._Routines.remove(name);
            if (this._Routines.size() == 0 && this._Runnable.isRunning()) this._Runnable.stop();
        }
    }

    /**
     * Add a monitor, only to get data from module
     *
     * @param name pacote
     */
    public void monitore(final String name) {
        if (this._Routines.containsKey(name) == false) {
            this.addRoutine(new IRoutine() {
                @Override public String name() { return name; }
                @Override public void run(CNData data) {}
            });
        }
    }

    /**
     * Start module
     */
    public void start() { if (this._Runnable.isRunning() == false && this._Routines.size() > 0) this._Runnable.run(); }

    /**
     * Stop module
     */
    public void stop() { if (this._Runnable.isRunning()) this._Runnable.stop(); }

    /**
     * Check if is started
     *
     * @return boolean
     */
    public boolean isStarted() { return this._Runnable.isRunning(); }

    /**
     * Restart module
     */
    public void restart() {
        this.stop();
        this.start();
    }

    /**
     * Get last stored data
     *
     * @param name package
     *
     * @return CNData
     */
    public CNData getLastKnowData(String name) { return this._LastKnowDatas.get(name); }

    /**
     * Clear last {@link CNData}
     *
     * @param name package name
     */
    public void clear(String name) { this._LastKnowDatas.remove(name); }

    /**
     * Change flag
     *
     * @param flag flag
     */
    public void removeWhenUnsupported(boolean flag) { this._removeWhenUnsupported = flag; }

    /**
     * CNData
     *
     * Class to store data
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public static class CNData
    {
        /** App data */
        final public String name;
        final public String dateTime;
        final public boolean isAvailable;

        /** Data of now */
        final public double total;
        final public double upload;
        final public double download;

        /** Total data */
        final public double grandTotal;
        final public double grandUpload;
        final public double grandDownload;

        /**
         * Método construtor
         *
         * @param name         pacote
         * @param lastUpload   último upload
         * @param lastDownload último download
         */
        public CNData(String name, double lastUpload, double lastDownload) {
            this.name     = name;
            this.dateTime = Date.withTime();

            AppInfo app = new AppInfo(name);
            if (app.uid == 0) {
                this.isAvailable   = false;
                this.grandTotal    = TrafficStats.UNSUPPORTED;
                this.grandUpload   = TrafficStats.UNSUPPORTED;
                this.grandDownload = TrafficStats.UNSUPPORTED;
                this.total         = TrafficStats.UNSUPPORTED;
                this.upload        = TrafficStats.UNSUPPORTED;
                this.download      = TrafficStats.UNSUPPORTED;
            } else {
                double grandDownload = (double) TrafficStats.getUidRxBytes(app.uid) / (1024 * 1024); // MB
                double grandUpload   = (double) TrafficStats.getUidTxBytes(app.uid) / (1024 * 1024); // MB
                if (grandDownload == TrafficStats.UNSUPPORTED && grandUpload == TrafficStats.UNSUPPORTED) {
                    this.isAvailable = false;
                    this.grandTotal    = TrafficStats.UNSUPPORTED;
                    this.grandUpload   = TrafficStats.UNSUPPORTED;
                    this.grandDownload = TrafficStats.UNSUPPORTED;
                    this.total         = TrafficStats.UNSUPPORTED;
                    this.upload        = TrafficStats.UNSUPPORTED;
                    this.download      = TrafficStats.UNSUPPORTED;
                } else {
                    this.isAvailable  = true;
                    this.grandTotal    = grandDownload + grandUpload;
                    this.grandUpload   = grandUpload;
                    this.grandDownload = grandDownload;

                    double lastTotal = -1;
                    if (lastUpload != -1 || lastDownload != -1) {
                        if (lastDownload != -1 && lastUpload != -1) {
                            lastTotal = lastDownload + lastUpload;
                        } else {
                            lastTotal = lastUpload == -1 ? lastDownload : lastUpload;
                        }
                    }

                    this.total    = lastTotal    == -1 ? 0 : this.grandTotal-lastTotal;
                    this.upload   = lastUpload   == -1 ? 0 : this.grandUpload-lastUpload;
                    this.download = lastDownload == -1 ? 0 : this.grandDownload-lastDownload;
                }
            }
        }

        /**
         * Format to string
         *
         * @return String
         */
        @SuppressLint("DefaultLocale")
        @Override
        public String toString() {
            return "name=" + this.name + "\n" +
                "isAvailable=" + this.isAvailable + "\n" +
                "dateTime=" + this.dateTime + "\n" +
                "total=" + String.format("%.3f", this.total) + " MB" + "\n" +
                "upload=" + String.format("%.3f", this.upload) + " MB" + "\n" +
                "download=" + String.format("%.3f", this.download) + " MB" + "\n" +
                "grandTotal=" + String.format("%.3f", this.grandTotal) + " MB" + "\n" +
                "grandUpload=" + String.format("%.3f", this.grandUpload) + " MB" + "\n" +
                "grandDownload=" + String.format("%.3f", this.grandDownload) + " MB";
        }
    }

    /**
     * IRoutine
     *
     * Routine to process data
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 15/01/2015 16:22
     */
    public static abstract class IRoutine
    {
        /**
         * Package name
         *
         * @return String
         */
        public abstract String name();

        /**
         * Last upload value, to use as base
         *
         * @return double
         */
        public double lastUpload() { return -1; }

        /**
         * Last download value, to use as base
         *
         * @return double
         */
        public double lastDownload() { return -1; }

        /**
         * Action
         *
         * @param data CNData
         */
        public abstract void run(CNData data);
    }
}