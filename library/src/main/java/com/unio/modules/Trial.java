package com.unio.modules;

import android.util.Base64;
import com.unio.base.UModule;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.util.helper.Convert;
import com.unio.util.io.File;
import java.util.concurrent.TimeUnit;

/**
 * Trial
 *
 * Core module, will calculate and validate trial use period of app
 * Call: Unio.module(Trial.class)
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Trial extends UModule
{
    private static final String TRIAL             = "TRIAL";
    private static final String TRIAL_FOLDER_PATH = ".unio";

    private int _period;
    private boolean _isValid = false;

    /**
     * Trial period is mandatory
     */
    @Override
    public void onInit() { Trace.log(this, "Module Trial need a period as parameter to start"); }

    /**
     * Start trial validation
     *
     * @param period trial period in days
     */
    public void onInit(int period) {
        if (period > 0) {
            String key = Unio.key();
            String date;
            // Armazena o tempo criptografado num arquivo escondido, para calcular em caso de reinstalação
            File f = new File(TRIAL_FOLDER_PATH+"/"+key+".lic");
            if (Unio.system(TRIAL).exists() == false) {
                if (f.exists()) {
                    date = f.content();
                } else {
                    date = Base64.encodeToString(Convert.stringToByte(String.valueOf(System.currentTimeMillis())), Base64.NO_WRAP);
                    f.save(Convert.stringToByte(date));
                }
                Unio.system(TRIAL).set(date);
            } else {
                date = Unio.system(TRIAL).get().toString();
                if (f.exists() == false) f.save(Convert.stringToByte(date));
            }
            long time = Long.valueOf(Convert.byteToString(Base64.decode(date, Base64.NO_WRAP)));
            this._isValid = System.currentTimeMillis() < (time + TimeUnit.DAYS.toMillis(period));

            super.onInit();
        } else {
            Trace.log(this, "Module Trial need a period as parameter to start");
        }
    }

    /**
     * Now is valid in trial period?
     *
     * @return boolean
     */
    public boolean isValid() { return this._isValid; }

    /**
     * Trial period in days
     *
     * @return int
     */
    public int period() { return this._period; }
}