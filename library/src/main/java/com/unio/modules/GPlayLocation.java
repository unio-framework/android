package com.unio.modules;

import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.*;
import com.unio.core.Unio;
import android.location.Location;
import java.util.List;

/**
 * GPlayLocation
 *
 * Get fused location too
 *
 * Required permissions
 *     Location
 *     <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
 *     <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
 *
 *     GoogleAPI
 *     <meta-data android:name="com.google.android.gms.version" android:value="@integer/google_play_services_version" />
 *     <meta-data android:name="com.google.android.maps.v2.API_KEY" android:value="[key]" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class GPlayLocation extends com.unio.modules.Location
    implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener
{
    /** Flags */
    private boolean hasFused = false;
    private boolean fusedSearchEnabled = false;
    private boolean connected = false;

    /** Instances */
    private GoogleApiClient client;
    private FusedLocationProviderClient fusedClient;
    private Location fusedLocation;
    private LocationCallback callback;

    /**
     * onInit
     */
    @Override
    public void onInit() { this.onInit(EType.GPS, EType.NETWORK, EType.FUSED); }

    /**
     * onInit
     */
    @Override
    public void onInit(long minTime, List<EType> types) {
        super.onInit(minTime, types);
        if (types.contains(EType.FUSED)) {
            this.hasFused = true;
            this.fusedSearchEnabled = true;
            this._startClient();
        }
    }

    /**
     * Start GoogleApiClient
     */
    private void _startClient() {
        this.client = new GoogleApiClient.Builder(Unio.app())
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build();
        this.client.connect();
    }

    /**
     * onConnected
     *
     * @param bundle bundle
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        this.connected = true;

        this.callback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                onLocationChanged(locationResult.getLastLocation());
            }
        };

        //noinspection MissingPermission
        this.fusedClient = LocationServices.getFusedLocationProviderClient(Unio.app());
        this.fusedLocation = this.fusedClient.getLastLocation().getResult();
        this._requestLocationUpdates();
        this.update();
    }

    /**
     * onConnectionFailed
     *
     * @param connectionResult resultado
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        this.connected = false;
        this.fusedSearchEnabled = false;
    }

    /**
     * Default method to get location update
     */
    private void _requestLocationUpdates() {
        if (this.client != null && this.client.isConnected()) {
            LocationRequest request = new LocationRequest();
            request.setInterval(this.minTime);
            request.setFastestInterval(1000);
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            // Inicia a requisição
            this.fusedClient.requestLocationUpdates(request, this.callback, Looper.getMainLooper());
        }
    }

    /**
     * Start location search
     */
    @Override
    public void start() {
        if (this._started == false && this.hasFused) this._startClient();
        super.start();
    }

    /**
     * Stop module
     */
    @Override
    public void stop() {
        if (this._started && this.fusedSearchEnabled && this.isConnected()) {
            this.fusedClient.removeLocationUpdates(this.callback);
            this.fusedSearchEnabled = false;
        }
        super.stop();
    }

    /**
     * Check if is connected
     *
     * @return boolean
     */
    public boolean isConnected() { return this.client != null && this.client.isConnected(); }

    /**
     * Check if GPS is active
     *
     * @return boolean
     */
    @Override
    public boolean isActive() { return super.isActive() || this.hasFused; }

    /**
     * Check if provider is enabled
     *
     * @return boolean
     */
    @Override
    public boolean isEnabled(EType type) {
        if (type == EType.FUSED) return this.hasFused;
        return super.isEnabled(type);
    }

    /**
     * Check if provider is searching location
     *
     * @return boolean
     */
    @Override
    public boolean isSearching(EType type) {
        if (type == EType.FUSED) return this.fusedSearchEnabled;
        return super.isSearching(type);
    }

    /**
     * Update types
     *
     * @param types list of {@link EType}
     */
    @Override
    public void updateType(List<EType> types) {
        super.updateType(types);
        if (types.contains(EType.FUSED)) {
            this.hasFused = true;
            if (this.hasFused && this._started && this.fusedSearchEnabled == false) {
                this.fusedSearchEnabled = true;
                if (this.client != null && this.client.isConnected() && this.connected) {
                    this._requestLocationUpdates();
                } else {
                    this._startClient();
                }
                this._startClient();
            } else if (this.hasFused == false) {
                this.fusedSearchEnabled = false;
            }
        }
    }

    /**
     * Search for best location
     */
    @Override
    public void update() {
        super.update();

        // Only run if is connected
        if (isConnected()) {
            Location fusedLastKnow = fusedClient.getLastLocation().getResult();
            fusedLocation    = getBestOfTwo(fusedLocation, fusedLastKnow);

            // Get best of 2
            _Location = fusedLocation != null ? fusedLocation : this.getBestOfTwo(_Location, fusedLocation);

            _LocationResponse = new Result(_Location);
            _canGetLocation   = _Location != null;
        }
    }

    /**
     * Get {@link Result} by provider
     *
     * @return Result
     */
    @Override
    public Result getResult(EType provider) {
        if (provider == EType.FUSED) {
            return new Result(this.fusedLocation);
        } else {
            return super.getResult(provider);
        }
    }

    /**
     * Action when location changes
     *
     * @param location new location
     */
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            if (location.getProvider().equals("fused")) {
                if (this.needUpdate(this.fusedLocation, location)) this._requestLocationUpdates();
                this.fusedLocation = this.getBestOfTwo(location, this.fusedLocation);
            } else {
                super.onLocationChanged(location);
            }
        }
    }

    /**
     * onConnectionSuspended
     */
    @Override
    public void onConnectionSuspended(int cause) {}

    public static class Callback extends LocationCallback
    {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            locationResult.getLastLocation();
        }
    }
}