package com.unio.modules;

import com.unio.base.UModule;
import com.unio.core.Unio;
import com.unio.util.helper.Check;
import com.unio.util.statics.interfaces.IAsync;

/**
 * Auth
 *
 * Module class for user authentication
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 09/10/2016 20:32
 */
public class Auth extends UModule
{
    /** Flags de armazenamento **/
    private static String _UMAUTH_USERNAME = "_UMAUTH_USERNAME";
    private static String _UMAUTH_PASSWORD = "_UMAUTH_PASSWORD";
    private static String _UMAUTH_SESSION  = "_UMAUTH_SESSION";

    /** Recebe o {@link Auth.Delegate} */
    public Delegate delegate;

    /** Usuário e senha para validação */
    public String username;
    public String password;

    /**
     * Execute login process
     *
     * @return boolean
     */
    public boolean login(IAsync task) { return this._login(task, false); }

    /**
     * Execute parallel login
     *
     * @return boolean
     */
    public boolean parallelLogin(IAsync task) { return this._login(task, true); }

    /**
     * Execute login process
     *
     * @return boolean
     */
    private boolean _login(IAsync task, boolean isParallel) {
        if (this.delegate != null) {
            if (Check.isEmpty(this.username) || Check.isEmpty(this.password)) {
                if (this.isLogged()) {
                    this.username = this.username();
                    this.password = this.password();
                } else {
                    this.logout();
                    return false;
                }
            }
            if (this.delegate.onLogin(this, task, this.username, this.password, isParallel)) {
                this.update(this.username, this.password);
                return true;
            }
        }
        return false;
    }

    /**
     * Start login with validation
     *
     * @param username username
     * @param password password
     */
    public boolean login(IAsync task, String username, String password) {
        return this.validate(username, password)
            && this.login(task);
    }

    /**
     * Validate fields
     *
     * @param username usuário
     * @param password senha
     *
     * @return boolean
     */
    public boolean validate(String username, String password) {
        if (this.delegate == null) return false;
        this.username = username == null ? null : username.trim();
        this.password = password == null ? null : password.trim();
        return this.delegate.onValidate(this, this.username, this.password);
    }

    /**
     * Update username and password
     *
     * @param username username
     * @param password password
     */
    public void update(String username, String password) {
        Unio.system(_UMAUTH_USERNAME).set(username);
        Unio.system(_UMAUTH_PASSWORD).set(password);
        Unio.session(_UMAUTH_SESSION).set(true);
    }

    /**
     * Logout action
     */
    public void logout() {
        Unio.system(_UMAUTH_USERNAME).clear();
        Unio.system(_UMAUTH_PASSWORD).clear();
        Unio.session(_UMAUTH_SESSION).clear();
        if (this.delegate != null) this.delegate.onLogout(this);
    }

    /**
     * Check if session is started
     *
     * @return boolean
     */
    public final boolean isSessionStarted() { return Unio.session(_UMAUTH_SESSION).get().toBoolean(); }

    /**
     * Check if is logged
     *
     * @return boolean
     */
    public final boolean isLogged() { return Check.isEmpty(this.username()) == false && Check.isEmpty(this.password()) == false; }

    /**
     * Get valid username
     *
     * @return String
     */
    public final String username() { return Unio.system(_UMAUTH_USERNAME).get().toString(); }

    /**
     * Get valid password
     *
     * @return String
     */
    public final String password() { return Unio.system(_UMAUTH_PASSWORD).get().toString(); }

    /**
     * Delegate
     *
     * Authentication validation delegate
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 27/05/2015 13:29
     */
    public interface Delegate
    {
        /**
         * Field validations
         *
         * @param auth     {@link Auth} instance
         * @param username username
         * @param password password
         *
         * @return boolean
         */
        boolean onValidate(Auth auth, String username, String password);

        /**
         * Authentication
         *
         * @param auth       {@link Auth} instance
         * @param username   username
         * @param password   password
         * @param isParallel parallel login
         *
         * @return boolean
         */
        boolean onLogin(Auth auth, IAsync task, String username, String password, boolean isParallel);

        /**
         * Logout
         *
         * @param auth {@link Auth} instance
         */
        void onLogout(Auth auth);
    }
}