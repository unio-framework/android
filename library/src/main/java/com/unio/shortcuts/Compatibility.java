package com.unio.shortcuts;

import android.os.StrictMode;
import android.view.View;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Compatibility
 *
 * Class to simplify some Android versions incompatiblities
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class Compatibility {
    /** For view id generation */
    private final static AtomicInteger sNextGeneratedId = new AtomicInteger(1);

    /**
     * Generate a view id programmatically
     * For old versions, that View.generateViewId() not exists
     *
     * Generate a value suitable for use in
     * This value will not collide with ID values generated at build time by aapt for R.id.
     *
     * @return int a generated ID value
     */
    public static int getNewViewId() {
        if (Android.CURRENT_VERSION > Android.JELLY_BEAN) {
            return View.generateViewId();
        } else {
            for (; ; ) {
                final int result = sNextGeneratedId.get();
                // aapt-generated IDs have the high byte nonzero; clamp to the range under that.
                int newValue = result + 1;
                if (newValue > 0x00FFFFFF) newValue = 1; // Roll over to 1, not 0.
                if (sNextGeneratedId.compareAndSet(result, newValue)) return result;
            }
        }
    }

    /**
     * Allow all permissions of Thread Policy
     * From Android 9, StrictMode disables http request on Main Thread. This method enables
     */
    public static void allowAllThreadPolicy() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}