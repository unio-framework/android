package com.unio.shortcuts;

import android.os.Build;

/**
 * Android
 *
 * Simplify getting Android current version and history
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class Android extends Build.VERSION_CODES
{
    /** Current version */
    public static final int CURRENT_VERSION = Build.VERSION.SDK_INT;
}