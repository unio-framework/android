package com.unio.shortcuts;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import com.unio.core.Unio;
import com.unio.util.manager.*;

/**
 * Manage
 *
 * Shortcut for Context control
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class Manage
{
    /**
     * Add current {@link Activity} instance
     *
     * @param activity Activity
     */
    public static void set(Activity activity) { ActivityManager.set(activity); }

    /**
     * Add current {@link Service} instance
     *
     * @param service Service
     */
    public static void set(Service service) { ServiceManager.set(service); }

    /**
     * Get {@link Context} instance, primary in {@link Activity},
     *  if null in {@link Service},
     *  if null in {@link android.app.Application},
     *  else {@code null}
     *
     * @return Context
     */
    public static Context get() {
        Context c = ActivityManager.get();
        if (c == null) c = ServiceManager.get();
        if (c == null) c = Unio.app();
        return c;
    }
}