package com.unio.ui.annotation;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import com.unio.base.UFragment;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.helper.Permission;
import com.unio.util.manager.ResourceManager;
import java.lang.reflect.Field;

/**
 * LayoutHelper
 *
 * Layout internal helper
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class LayoutAnnotation
{
    /**
     * Return {@link LayoutId} resource ID
     *
     * @param c Class
     *
     * @return int
     */
    public static int findLayoutId(Context context, Class<?> c) {
        if (c.isAnnotationPresent(LayoutId.class)) {
            LayoutId layoutAnnotation = c.getAnnotation(LayoutId.class);
            if (layoutAnnotation != null) {
                int layout = layoutAnnotation.value();
                if (layout == 0) {
                    String layoutName = layoutAnnotation.name();
                    if (Check.isEmpty(layoutName) == false)
                        layout = context.getResources()
                            .getIdentifier(layoutName, ResourceManager.LAYOUT, context.getPackageName());
                }
                return layout;
            }
        }
        return 0;
    }

    /**
     * Instantiate views with {@link FindViewById}
     *
     * @param o Activity or UFragment instance
     */
    public static void attributeViews(Context context, Object o) {
        Field[] fields = o.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(FindViewById.class)) {
                FindViewById viewAnnotation = field.getAnnotation(FindViewById.class);
                String name = field.getName();
                field.setAccessible(true);
                if (Check.isEmpty(name) == false) {
                    int id = viewAnnotation.value();
                    if (id == 0) {
                        String viewName = viewAnnotation.name();
                        if (Check.isEmpty(viewName) == false)
                            id = context.getResources()
                                .getIdentifier(viewName, ResourceManager.ID, context.getPackageName());
                    }
                    if (id == 0) id = ResourceManager.getId(name, ResourceManager.ID);
                    if (id != 0) {
                        View v = null;
                        if (o instanceof Activity) {
                            v = ((Activity) o).findViewById(id);
                        } else if (o instanceof UFragment) {
                            v = ((UFragment) o).findViewById(id);
                        }
                        if (v != null) {
                            try {
                                field.set(o, field.getType().cast(v));
                            } catch (IllegalAccessException e) {
                                Trace.get(e);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Validate permission of a class
     *
     * @param activity activity result callback
     * @param c        annotation class
     *
     * @return boolean
     */
    public static boolean validatePermissions(Activity activity, Class<?> c) {
        if (c.isAnnotationPresent(RequestPermissions.class)) {
            RequestPermissions annotation = c.getAnnotation(RequestPermissions.class);
            String[] permissions = annotation.value();
            if (Permission.isEnabled(permissions) == false) {
                Permission.request(activity, permissions, c);
                return false;
            }
        }
        return true;
    }

    /**
     * Validate permission of a class
     *
     * @param c annotation class
     *
     * @return boolean
     */
    public static boolean isPermissionsEnabled(Class<?> c) {
        if (c.isAnnotationPresent(RequestPermissions.class)) {
            RequestPermissions annotation = c.getAnnotation(RequestPermissions.class);
            String[] permissions = annotation.value();
            return Permission.isEnabled(permissions);
        }
        return true;
    }
}