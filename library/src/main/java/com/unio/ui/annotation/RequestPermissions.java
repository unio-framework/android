package com.unio.ui.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * RequestPermission
 *
 * Annotation load dialog for one or more permissions before start Activity
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RequestPermissions
{
    /** Activity permissions */
    String[] value();
}