package com.unio.ui.annotation;

import android.support.annotation.LayoutRes;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Layout
 *
 * Annotation for {@link com.unio.base.UActivity#getLayout()} and {@link com.unio.base.UFragment#getLayout()} attribuition
 * Only if is layout from resource ID
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 01/04/2016 21:39
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface LayoutId
{
    /** Layout resource ID */
    @LayoutRes
    int value() default 0;

    /** Layout resource name for library */
    String name() default "";
}