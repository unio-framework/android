package com.unio.ui.annotation

import android.support.annotation.IdRes

/**
 * FindViewById
 *
 * Annotation for [android.app.Activity.findViewById] attribuition
 *
 * For attribuition, attribute need starts with "$" for public or protected or "_$" for private
 * Is not need to write full resource, in case of ommition, will search from attribute name
 *
 * Need to use as Kotlin class, as Kotlin not recognize as Java class
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac></leakira.uac>@gmail.com>
 */
@kotlin.annotation.Retention
@Target(AnnotationTarget.FIELD)
annotation class FindViewById(
        /** View resource ID  */
        @IdRes
        val value: Int = 0,

        /** View resource name for library  */
        val name: String = ""
)