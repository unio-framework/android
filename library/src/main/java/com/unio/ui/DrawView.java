package com.unio.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.*;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import com.unio.util.helper.Screen;
import com.unio.util.helper.Color;
import com.unio.util.io.Image;
import com.unio.util.helper.Layouts;
import java.util.LinkedList;
import java.util.List;

/**
 * DrawView
 *
 * View for draw lines
 * Based on Raghunandan idea (http://stackoverflow.com/questions/16650419/draw-in-canvas-by-finger-android)
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class DrawView extends View
{
    /** Lista de filtros */
    public enum EFilter { EMBOSS, CUSTOM, BLUR, NONE }

    /** Valores padrões de definição */
    private static final float MINP = 0.25f;
    private static final float MAXP = 0.75f;
    private static final float TOUCH_TOLERANCE = 4;

    /** Instâncias */
    private Bitmap _Bitmap;
    private Canvas _Canvas;
    private Path _Path;
    private Paint _BitmapPaint;
    private Paint _Paint;

    /** Atributos */
    private float _x, _y;
    private int _width, _height;
    private EFilter _current = EFilter.NONE;
    private boolean _isEmpty = true;
    private boolean _isDrawing = false;
    private boolean _isMoved = false;

    /** Callback groups */
    private List<IOnDrawing> _drawings = new LinkedList<>();

    /**
     * Método construtor
     */
    public DrawView(Context context) { this(context, null); }

    /**
     * Método construtor
     */
    public DrawView(Context context, AttributeSet attrs) { this(context, attrs, 0); }

    /**
     * Método construtor
     */
    public DrawView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (this.isInEditMode()) return;
        this.setDrawingCacheEnabled(true); // Para manter os traços

        this._Path = new Path();
        this._BitmapPaint = new Paint(Paint.DITHER_FLAG);

        this._Paint = new Paint();
        this._Paint.setAntiAlias(true);
        this._Paint.setDither(true);
        this._Paint.setColor(Color.BLACK);
        this._Paint.setStyle(Paint.Style.STROKE);
        this._Paint.setStrokeJoin(Paint.Join.ROUND);
        this._Paint.setStrokeCap(Paint.Cap.ROUND);

        int dpSize = 2;
        DisplayMetrics dm = getResources().getDisplayMetrics() ;
        float strokeWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpSize, dm);
        this._Paint.setStrokeWidth(strokeWidth);

        this.measure(0, 0);
        if (this.getMeasuredHeight() == 0) this.setMinimumHeight(Layouts.asDp(300));
        if (this.getMeasuredWidth() == 0)  this.setMinimumWidth((int) (Screen.getWidth()*0.9));
    }

    /**
     * Altera o pincel
     *
     * @param paint pincel instância de Paint
     */
    public void setPaint(Paint paint) { this._Paint = paint; }

    /**
     * Retorna o pincel
     *
     * @return Paint
     */
    public Paint getPaint() { return this._Paint; }

    /**
     * Altera a cor do pincel
     *
     * @param color cor
     */
    public void setColor(int color) { this._Paint.setColor(color); }

    /**
     * Retorna a cor do pincel
     * Atalho para {@link Paint#getColor()}
     *
     * @return int
     */
    public int getColor() { return this._Paint.getColor(); }

    /**
     * Aplicar filtro de desfoque
     *
     * @param apply usar ou não
     */
    public void blur(boolean apply) {
        MaskFilter filter = apply ? new BlurMaskFilter(8, BlurMaskFilter.Blur.NORMAL) : null;
        this.applyMask(filter, EFilter.BLUR);
    }

    /**
     * Aplica filtro de relevo
     *
     * @param apply usar ou não
     */
    public void emboss(boolean apply) {
        MaskFilter filter = apply ? new EmbossMaskFilter(new float[] { 1, 1, 1 }, 0.4f, 6, 3.5f) : null;
        this.applyMask(filter, EFilter.EMBOSS);
    }

    /**
     * Apaga o conteúdo desenhado
     */
    public void erase() {
        this._Paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        this._Paint.setAlpha(0x80);
    }

    /**
     * Limpa a tela
     */
    public void clear() {
        this.setDrawingCacheEnabled(false);
        this.onSizeChanged(this._width, this._height, this._width, this._height);
        this.invalidate();
        this.setDrawingCacheEnabled(true);
        this._isEmpty = true;
    }

    /**
     * Add IOnDrawing
     *
     * @param c IOnDrawing
     */
    public void addOnDrawing(IOnDrawing c) { if (c != null) this._drawings.add(c); }

    /**
     * Remove IOnDrawing
     *
     * @param c IOnDrawing
     */
    public void removeOnDrawing(IOnDrawing c) { if (c != null) this._drawings.remove(c); }

    /**
     * Check if is drawed
     *
     * @return boolean
     */
    public boolean isEmpty() { return this._isEmpty; }

    /**
     * Check if is drawing
     *
     * @return boolean
     */
    public boolean isDrawing() { return this._isDrawing; }

    /**
     * Salva o conteúdo desenhado
     *
     * @param path caminho
     *
     * @return IOImage instância de IOImage, {@code null} caso não dê certo, ou falte permissão
     */
    public Image save(String path) {
        boolean backgroundChanged = false;
        // Se o fundo for transparente, deixa branco
        // Fundo transparente tem dificuldade de enxergar, e acaba influenciando até na compressão da imagem
        if (this.getDrawingCacheBackgroundColor() == Color.TRANSPARENT) {
            this.setDrawingCacheBackgroundColor(Color.WHITE);
            backgroundChanged = true;
        }
        Bitmap bitmap = this.getDrawingCache();
        if (backgroundChanged) this.setDrawingCacheBackgroundColor(Color.TRANSPARENT);
        Image image = new Image(path);
        return image.save(bitmap) ? image : null;
    }

    /**
     * Aplica um filtro
     *
     * @param filter filtro
     */
    public void applyMask(MaskFilter filter) { this.applyMask(filter, EFilter.CUSTOM); }

    /**
     * Aplica um filtro
     *
     * @param filter filtro
     * @param type   tipo do filtro, ver {@link DrawView.EFilter}
     */
    public void applyMask(MaskFilter filter, EFilter type) {
        this._Paint.setMaskFilter(filter);
        this._current = filter == null ? EFilter.NONE : type;
    }

    /**
     * Retorna o tipo do filtro usado
     *
     * @return EFilter
     */
    public EFilter getFilterType() { return this._current; }

    /**
     * Início
     */
    private void _touchStart(float x, float y) {
        this._Path.reset();
        this._Path.moveTo(x, y);
        this._x = x;
        this._y = y;
        this._isEmpty = false;
        this._isDrawing = true;
        if (this._drawings.size() > 0) for (IOnDrawing c : this._drawings) c.onDrawStart(this);
    }

    /**
     * Mover
     */
    private void _touchMove(float x, float y) {
        float dx = Math.abs(x - this._x);
        float dy = Math.abs(y - this._y);

        boolean drawed = (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) || (dx == 0 && dy == 0);
        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            this._Path.quadTo(this._x, this._y, (x + this._x)/2, (y + this._y)/2);
        } else if (dx == 0 && dy == 0) {
            this._Path.addCircle(this._x, this._y, this._Paint.getStrokeWidth()/4f, Path.Direction.CW);
        }
        if (drawed) {
            this._x = x;
            this._y = y;
            if (this._drawings.size() > 0) for (IOnDrawing c : this._drawings) c.onDrawing(this);
        }
    }

    /**
     * Fim
     */
    private void _touchUp() {
        this._Path.lineTo(this._x, this._y);
        this._Canvas.drawPath(this._Path, this._Paint);
        this._Path.reset();
        this._isDrawing = false;
        if (this._drawings.size() > 0) for (IOnDrawing c : this._drawings) c.onDrawFinish(this);
    }

    /**
     * Evento de toque
     *
     * @param event evento
     *
     * @return boolean
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        if (this.isInEditMode()) return true;

        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                this._isMoved = false;
                this._touchStart(x, y);
                this.invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                this._isMoved = true;
                this._touchMove(x, y);
                this.invalidate();
                break;
            case MotionEvent.ACTION_UP:
                if (this._isMoved == false) this._touchMove(x, y);
                this._isMoved = false;
                this._touchUp();
                this.invalidate();
                break;
        }
        return true;
    }

    /**
     * onSizeChanged
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (this.isInEditMode()) return;
        this._width = w;
        this._height = h;
        if (this._width > 0 && this._height > 0) {
            this._Bitmap = Bitmap.createBitmap(this._width, this._height, Bitmap.Config.ARGB_8888);
            this._Canvas = new Canvas(this._Bitmap);
        }
    }

    /**
     * onDraw
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.isInEditMode()) return;
        canvas.drawBitmap(this._Bitmap, 0, 0, this._BitmapPaint);
        canvas.drawPath(this._Path, this._Paint);
    }

    /**
     * IOnDrawing
     *
     * Interface for catch each draw action
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 21/09/2016 23:46
     */
    public interface IOnDrawing
    {
        void onDrawStart(DrawView v);
        void onDrawing(DrawView v);
        void onDrawFinish(DrawView v);
    }
}