package com.unio.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.MotionEventCompat;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import com.unio.shortcuts.Android;

/**
 * SwipeRefreshLayout
 *
 * Custom SwipeRefreshLayout with refresh stopper when children is scrolling
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class SwipeRefreshLayout extends android.support.v4.widget.SwipeRefreshLayout
{
    private static final int DEFAULT_CIRCLE_TARGET = 64;

    /** Flag */
    private boolean _hasFirstMoviment = false;
    private boolean _previousFlag = true;
    private int _dyConsumed = 0;
    private boolean userInteractionEnabled = true;

    /**
     * Constructor
     */
    public SwipeRefreshLayout(Context context) { super(context); }

    /**
     * Constructor
     */
    public SwipeRefreshLayout(Context context, AttributeSet attrs) { super(context, attrs); }

    /**
     * Start nested scroll
     * Receive current enabled status
     *
     * @return boolean
     */
    @Override
    public boolean onStartNestedScroll(View child, View target, int nestedScrollAxes) {
        this._hasFirstMoviment = false;
        this._previousFlag = this.isEnabled();
        return super.onStartNestedScroll(child, target, nestedScrollAxes);
    }

    /**
     * onInterceptTouchEvent
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        final int action = this.getActionMasked(ev);
        if (action == MotionEvent.ACTION_MOVE && this._dyConsumed == 0 && this.isEnabled() == false) {
            return true;
        } else {
            if (action == MotionEvent.ACTION_UP) this.setEnabled(this._previousFlag);
            return super.onInterceptTouchEvent(ev);
        }
    }

    /**
     * onInterceptTouchEvent
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        final int action = this.getActionMasked(ev);
        if (action == MotionEvent.ACTION_MOVE && this._dyConsumed == 0 && this.isEnabled() == false) {
            return true;
        } else {
            if (action == MotionEvent.ACTION_UP) this.setEnabled(this._previousFlag);
            return super.onTouchEvent(ev);
        }
    }

    @Override
    public void onNestedScroll(View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed) {
        super.onNestedScroll(target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed);
        this._dyConsumed = dyConsumed;
        if (this._hasFirstMoviment == false) {
            this._hasFirstMoviment = true;
            this.setEnabled(dyConsumed == 0);
        }
    }

    /**
     * Deprecated validation
     */
    @SuppressWarnings("deprecation")
    private int getActionMasked(MotionEvent ev) {
        if (Android.CURRENT_VERSION > Android.N_MR1) {
            return ev.getAction();
        } else {
            return MotionEventCompat.getActionMasked(ev);
        }
    }

    /**
     * Set if user can swipe refresh
     *
     * @param flag flag
     */
    public void setUserInteractionEnabled(boolean flag) {
        if (flag) {
            // Call setDistanceToTriggerSync to restore swipe to refresh action
            final DisplayMetrics metrics = this.getResources().getDisplayMetrics();
            int distance = (int) (DEFAULT_CIRCLE_TARGET * metrics.density);
            this.setDistanceToTriggerSync(distance);
        } else {
            // It will disable swipe to refresh action
            this.setDistanceToTriggerSync(999999);
        }
        this.userInteractionEnabled = flag;
    }

    /**
     * Swipe refresh from user action is enabled or disabled?
     *
     * @return boolean
     */
    public boolean isUserInteractionEnabled() { return this.userInteractionEnabled; }
}