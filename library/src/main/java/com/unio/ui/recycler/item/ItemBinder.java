package com.unio.ui.recycler.item;

import android.view.View;
import android.widget.LinearLayout;
import com.unio.R;
import com.unio.ui.TextView;
import com.unio.ui.features.fonts.base.IconCharacter;

/**
 * ItemBinder
 *
 * Common bind command
 */
class ItemBinder
{
    public static void bindNotification(View layout, int counter) {
        LinearLayout recyclerNotificationContainer = layout.findViewById(R.id.view_recycler_notification_container);
        if (counter == 0) {
            recyclerNotificationContainer.setVisibility(View.GONE);
        } else {
            recyclerNotificationContainer.setVisibility(View.VISIBLE);

            TextView recyclerNotification = layout.findViewById(R.id.view_recycler_notification);
            recyclerNotification.setText(String.valueOf(counter));
        }
    }

    public static void bindIcon(View layout, IconCharacter icon) {
        TextView recyclerIcon = layout.findViewById(R.id.view_recycler_icon);
        if (icon == null) {
            recyclerIcon.setVisibility(View.GONE);
        } else {
            recyclerIcon.setVisibility(View.VISIBLE);
            recyclerIcon.setIcon(icon);
        }
    }
}