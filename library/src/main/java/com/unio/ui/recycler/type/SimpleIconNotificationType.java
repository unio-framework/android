package com.unio.ui.recycler.type;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import com.unio.R;
import com.unio.ui.recycler.AdapterItemType;

/**
 * SimpleType
 *
 * Type map for R.layout.view_recycler_simple_icon_notification
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class SimpleIconNotificationType extends AdapterItemType
{
    /** Id. Need to be unique */
    public static final int ID = 7;

    /**
     * Get item view
     *
     * @param parent parent context to instantiate
     *
     * @return View
     */
    @Override
    public View getView(@NonNull ViewGroup parent) { return this.loadFromResource(parent, R.layout.view_recycler_simple_icon_notification); }
}