package com.unio.ui.recycler.item;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.unio.R;
import com.unio.ui.TextView;
import com.unio.ui.recycler.Adapter;
import com.unio.ui.recycler.AdapterItem;
import com.unio.ui.recycler.type.CompleteType;

/**
 * CompleteItem
 *
 * {@link com.unio.ui.recycler.Adapter} item
 * Model: { title, description }
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class CompleteItem extends AdapterItem
{
    /**
     * Constructor
     *
     * @param values data
     */
    public CompleteItem(Object... values) { super(values); }

    /**
     * Get title
     *
     * @return String
     */
    public String getTitle() {
        if (this.values.size() == 0) {
            return "-";
        } else {
            return String.valueOf(this.values.get(0));
        }
    }

    /**
     * Get title
     *
     * @return String
     */
    public String getDescription() {
        if (this.values.size() == 1) {
            return "-";
        } else {
            return String.valueOf(this.values.get(1));
        }
    }

    /**
     * Get item type id
     *
     * @return int
     */
    @Override
    public int getId() { return CompleteType.ID; }

    /**
     * Get item type
     *
     * @return SimpleType
     */
    @Override
    public CompleteType getType() { return new CompleteType(); }

    /**
     * Bind data
     *
     * @param holder ViewHolder
     */
    @Override
    public void bindData(Adapter adapter, int position, RecyclerView.ViewHolder holder) {
        View layout = holder.itemView;
        TextView recyclerTitle = layout.findViewById(R.id.view_recycler_title);
        recyclerTitle.setText(this.getTitle());

        TextView recyclerDescription = layout.findViewById(R.id.view_recycler_description);
        recyclerDescription.setText(this.getDescription());
    }
}