package com.unio.ui.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import com.unio.debug.Trace;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.util.*;

/**
 * Adapter
 *
 * Multiple items and customizable Unio {@link RecyclerView} adapter
 * For Decorator, use https://github.com/yqritc/RecyclerView-FlexibleDivider
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    /** Item list */
    private List<AdapterItem> _itemValues = new LinkedList<>();

    /** Item type list */
    private Map<Integer, AdapterItemType> _itemTypes = new LinkedHashMap<>();

    /** Item customization list */
    private List<IOnItemCustomize> _customizes = new LinkedList<>();

    /**
     * Add an item or multiple items
     *
     * @param items Items
     *
     * @return int, last item position
     */
    public int add(@NonNull AdapterItem... items) {
        for (AdapterItem item : items) {
            this._itemValues.add(item);
            this.setType(item.getId(), item.getType());
        }
        return this._itemValues.size()-1;
    }

    /**
     * Get an item
     *
     * @param position item position
     *
     * @return Item
     */
    public AdapterItem get(int position) { return this._itemValues.get(position); }

    /**
     * Set a type for mapping
     *
     * @param id   type id
     * @param type type
     */
    public void setType(int id, AdapterItemType type) {
        if (this._itemTypes.containsKey(id) && this._itemTypes.get(id).getClass() != type.getClass()) {
            // Is only permitted 1 id for 1 type. If receives a new type with same id, crash to alert
            String settedClass = this._itemTypes.get(id).getClass().getName();
            String newSetClass = type.getClass().getName();
            String message = "Type ID "+id+" of "+newSetClass+" is already allocated to "+settedClass+" in this Adapter";
            throw new RuntimeException(message);
        } else {
            this._itemTypes.put(id, type);
        }
    }

    /**
     * Remove an item
     *
     * @param position item position
     */
    public void remove(int position) {
        this._itemValues.remove(position);
        this.notifyItemRemoved(position);
        this.notifyItemRangeChanged(position, this._itemValues.size());
    }

    /**
     * Clear RecyclerView items
     */
    public void clear() {
        final int size = this._itemValues.size();
        this._itemValues.clear();
        this._itemTypes.clear();
        this._customizes.clear();
        this.notifyItemRangeRemoved(0, size);
    }

    /**
     * Add item customize listener
     *
     * @param listener IOnItemCustomize
     */
    public void addOnItemCustomize(IOnItemCustomize listener) {
        if (listener != null)
            this._customizes.add(listener);
    }

    /**
     * Remove item customize listener
     *
     * @param listener IOnItemCustomize
     */
    public void removeOnItemCustomize(IOnItemCustomize listener) { this._customizes.remove(listener); }

    /**
     * Check if {@link IOnItemCustomize} listener has {@link Types} annotation to filter type
     *
     * @param listener IOnItemCustomize
     *
     * @return List<Integer>
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    private List<Integer> _getAnnotationFilteredTypes(IOnItemCustomize listener) {
        List<Integer> types = new LinkedList<>();
        try {
            Method method = listener.getClass().getDeclaredMethod("onItemCustomize",
                int.class,
                RecyclerView.ViewHolder.class,
                AdapterItem.class
            );
            Types annotation = method.getAnnotation(Types.class);
            if (annotation != null) {
                int[] ids = annotation.value();
                for (int i = 0; i < ids.length; i++)
                    types.add(ids[i]);
            }
        } catch (NoSuchMethodException ignore) {}
        return types;
    }

    /**
     * Get position item type to render view
     *
     * @param position position
     *
     * @return int
     */
    @Override
    public int getItemViewType(int position) { return this._itemValues.get(position).getId(); }

    /**
     * Create view holder
     *
     * @param parent   parent view, probably {@link RecyclerView}
     * @param viewType view type
     *
     * @return RecyclerView.ViewHolder
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        AdapterItemType type = this._itemTypes.get(viewType);
        View view = type.getView(parent);
        return type.getViewHolder(view);
    }

    /**
     * Bind data to view holder
     *
     * @param holder   ViewHolder
     * @param position item position
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        AdapterItem item = this._itemValues.get(position);
        item.bindData(this, position, holder);

        Trace.log(this, "Adapter::Customizes", "Total: "+this._customizes.size());
        for (int i = 0; i < this._customizes.size(); i++) {
            IOnItemCustomize listener = this._customizes.get(i);

            // If listener has @Adapter.Types annotation with specific types, directs only to it
            List<Integer> types = this._getAnnotationFilteredTypes(listener);
            Trace.log(this, "Adapter::Customize", "Types: "+types);
            if (types.size() > 0 && types.contains(item.getId()) == false)
                continue;

            listener.onItemCustomize(position, holder, item);
        }
    }

    /**
     * Get added item total
     *
     * @return int
     */
    @Override
    public int getItemCount() { return this._itemValues.size(); }

    /**
     * IOnItemCustomize
     *
     * Customize item layout and action
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public interface IOnItemCustomize { void onItemCustomize(int position, RecyclerView.ViewHolder holder, AdapterItem item); }

    /**
     * Types
     *
     * Annotation to filter types
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Types { int[] value(); }
}