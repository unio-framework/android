package com.unio.ui.recycler;

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.unio.ui.annotation.LayoutAnnotation;

/**
 * AdapterItemType
 *
 * {@link Adapter} item type, will be used to map item view and ViewHolder
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class AdapterItemType<V extends View, VH extends RecyclerView.ViewHolder>
{
    /**
     * Get item view
     * Can be a Object View or resource layout
     *
     * @param parent parent view to instantiate
     *
     * @return V extends View
     */
    public V getView(@NonNull ViewGroup parent) {
        int layoutId = LayoutAnnotation.findLayoutId(parent.getContext(), this.getClass());
        if (layoutId == 0)
            throw new RuntimeException("View not found for "+this.getClass().getName()+" View object or resource layout is mandatory");

        return this.loadFromResource(parent, layoutId);
    }

    /**
     * Get item view holder
     * Use {@link ViewHolder} if not has custom View Holder
     *
     * @param view item view
     *
     * @return VH extends RecyclerView.ViewHolder
     */
    public VH getViewHolder(V view) { return (VH) new AdapterItemType.ViewHolder(view); }

    /**
     * Inflate resource layout to RecyclerView
     *
     * @param parent   parent view
     * @param layoutId resource layout id
     *
     * @return V
     */
    public V loadFromResource(@NonNull ViewGroup parent, @LayoutRes int layoutId) {
        return (V) LayoutInflater.from(parent.getContext())
            .inflate(layoutId, parent, false);
    }

    /**
     * ViewHolder
     *
     * {@link Adapter} basic View Holder.
     * Original RecyclerView.ViewHolder is abstract and is not possible to instantiate directly
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        /**
         * Constructor
         *
         * @param itemView item view
         */
        public ViewHolder(View itemView) { super(itemView); }
    }
}