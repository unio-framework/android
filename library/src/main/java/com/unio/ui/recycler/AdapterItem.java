package com.unio.ui.recycler;

import android.support.v7.widget.RecyclerView;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * AdapterItem
 *
 * {@link Adapter} item base
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class AdapterItem<T extends AdapterItemType>
{
    /** Item data */
    protected List<Object> values;

    /**
     * Constructor
     *
     * @param values data
     */
    public AdapterItem(Object... values) { this.update(values); }

    /**
     * Get added data as array
     *
     * @return Object[]
     */
    public Object[] getData() { return this.values.toArray(); }

    /**
     * Set new data
     *
     * @param values data
     */
    public void update(Object[] values) {
        // Instantiate as LinkedList. ArrayList has poor features
        this.values = new LinkedList<>(Arrays.asList(values));
    }

    /**
     * Abstract
     * Get item view type
     *
     * @return int
     */
    public abstract int getId();

    /**
     * Abstract
     * Get item type instance, extended from {@link AdapterItemType}
     *
     * @return T
     */
    public abstract T getType();

    /**
     * Abstract
     * Bind added data to View Holder
     *
     * @param adapter Adapter
     * @param position current position
     * @param holder ViewHolder
     */
    public abstract void bindData(Adapter adapter, int position, RecyclerView.ViewHolder holder);
}