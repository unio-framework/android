package com.unio.ui.recycler.type;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import com.unio.R;
import com.unio.ui.recycler.AdapterItemType;

/**
 * CompleteType
 *
 * Type map for R.layout.view_recycler_complete
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class CompleteType extends AdapterItemType
{
    /** Id. Need to be unique */
    public static final int ID = 2;

    /**
     * Get item view
     *
     * @param parent parent context to instantiate
     *
     * @return SimpleView
     */
    @Override
    public View getView(@NonNull ViewGroup parent) { return this.loadFromResource(parent, R.layout.view_recycler_complete); }
}