package com.unio.ui.recycler.item;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.unio.R;
import com.unio.ui.TextView;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.ui.recycler.Adapter;
import com.unio.ui.recycler.AdapterItem;
import com.unio.ui.recycler.type.SimpleIconNotificationType;
import com.unio.ui.recycler.type.SimpleNotificationType;

/**
 * SimpleIconNotificationItem
 *
 * {@link com.unio.ui.recycler.Adapter} item
 * Use model: { icon, title, notification }
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class SimpleIconNotificationItem extends AdapterItem
{
    /**
     * Constructor
     *
     * @param values data
     */
    public SimpleIconNotificationItem(Object... values) { super(values); }

    /**
     * Get icon
     *
     * @return IconCharacter
     */
    public IconCharacter getIcon() {
        if (this.values.size() == 0 || (this.values.get(0) instanceof IconCharacter) == false) {
            return null;
        } else {
            return (IconCharacter) this.values.get(0);
        }
    }

    /**
     * Get title
     *
     * @return String
     */
    public String getTitle() {
        if (this.values.size() == 1) {
            return "-";
        } else {
            return String.valueOf(this.values.get(1));
        }
    }

    /**
     * Get notification counter
     *
     * @return int
     */
    public int getNotificationCounter() {
        if (this.values.size() == 2) {
            return 0;
        } else {
            return Integer.valueOf(String.valueOf(this.values.get(2)));
        }
    }

    /**
     * Get item type id
     *
     * @return int
     */
    @Override
    public int getId() { return SimpleIconNotificationType.ID; }

    /**
     * Get item type
     *
     * @return SimpleIconNotificationType
     */
    @Override
    public SimpleIconNotificationType getType() { return new SimpleIconNotificationType(); }

    /**
     * Bind data
     *
     * @param holder ViewHolder
     */
    @Override
    public void bindData(Adapter adapter, int position, RecyclerView.ViewHolder holder) {
        View layout = holder.itemView;
        TextView recyclerTitle = layout.findViewById(R.id.view_recycler_title);
        recyclerTitle.setText(this.getTitle());

        int counter = this.getNotificationCounter();
        ItemBinder.bindNotification(layout, counter);

        IconCharacter icon = this.getIcon();
        ItemBinder.bindIcon(layout, icon);
    }
}