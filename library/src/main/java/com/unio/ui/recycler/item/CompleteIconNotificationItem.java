package com.unio.ui.recycler.item;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.unio.R;
import com.unio.ui.TextView;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.ui.recycler.Adapter;
import com.unio.ui.recycler.AdapterItem;
import com.unio.ui.recycler.type.CompleteIconNotificationType;

/**
 * CompleteIconNotificationItem
 *
 * {@link com.unio.ui.recycler.Adapter} item
 * Model: { icon, title, description, notification }
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class CompleteIconNotificationItem extends AdapterItem
{
    /**
     * Constructor
     *
     * @param values data
     */
    public CompleteIconNotificationItem(Object... values) { super(values); }

    /**
     * Get notification counter
     *
     * @return int
     */
    public IconCharacter getIcon() {
        if (this.values.size() == 0 || (this.values.get(0) instanceof IconCharacter) == false) {
            return null;
        } else {
            return (IconCharacter) this.values.get(0);
        }
    }

    /**
     * Get title
     *
     * @return String
     */
    public String getTitle() {
        if (this.values.size() == 1) {
            return "-";
        } else {
            return String.valueOf(this.values.get(1));
        }
    }

    /**
     * Get title
     *
     * @return String
     */
    public String getDescription() {
        if (this.values.size() == 2) {
            return "-";
        } else {
            return String.valueOf(this.values.get(2));
        }
    }

    /**
     * Get notification counter
     *
     * @return int
     */
    public int getNotificationCounter() {
        if (this.values.size() == 3) {
            return 0;
        } else {
            return Integer.valueOf(String.valueOf(this.values.get(3)));
        }
    }

    /**
     * Get item type id
     *
     * @return int
     */
    @Override
    public int getId() { return CompleteIconNotificationType.ID; }

    /**
     * Get item type
     *
     * @return CompleteIconNotificationType
     */
    @Override
    public CompleteIconNotificationType getType() { return new CompleteIconNotificationType(); }

    /**
     * Bind data
     *
     * @param holder ViewHolder
     */
    @Override
    public void bindData(Adapter adapter, int position, RecyclerView.ViewHolder holder) {
        View layout = holder.itemView;
        TextView recyclerTitle = layout.findViewById(R.id.view_recycler_title);
        recyclerTitle.setText(this.getTitle());

        TextView recyclerDescription = layout.findViewById(R.id.view_recycler_description);
        recyclerDescription.setText(this.getDescription());

        int counter = this.getNotificationCounter();
        ItemBinder.bindNotification(layout, counter);

        IconCharacter icon = this.getIcon();
        ItemBinder.bindIcon(layout, icon);
    }
}