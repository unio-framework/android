package com.unio.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import com.unio.ui.features.drawables.CircularProgressDrawable;

/**
 * ProgressBar
 *
 * Material Design loading progress bar
 *
 * Based:
 *   dmide (https://gist.github.com/dmide/7506c7d9614eed90805d)
 *   castorflex (https://gist.github.com/castorflex/4e46a9dc2c3a4245a28e - fork)
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class ProgressBar extends View
{
    /** Instância do Drawable para gerar a animação **/
    private CircularProgressDrawable mDrawable;

    /**
     * Método construtor
     *
     * @param context Context da Activity
     */
    public ProgressBar(Context context) { this(context, null); }

    /**
     * Método construtor
     *
     * @param context Context da Activity
     * @param attrs   Atributos
     */
    public ProgressBar(Context context, AttributeSet attrs) { this(context, attrs, 0); }

    /**
     * Método construtor
     *
     * @param context Context da Activity
     * @param attrs   Atributos
     */
    public ProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mDrawable = new CircularProgressDrawable(Color.parseColor("#1E88E5"), 3);
        mDrawable.setCallback(this);
        mDrawable.start();
    }

    /**
     * Muda a cor do ProgressBar
     *
     * @param color cor
     */
    public void setColor(int color) { this.mDrawable.setColor(color); }

    /**
     * Change ProgressBar border width
     *
     * @param width width
     */
    public void setBorderWidth(int width) { this.mDrawable.setmBorderWidth(width); }

    /**
     * Pára/continua dependendo do estado da View
     * Válido a partir da API 8 (2.2)
     *
     * @param changedView instância da View
     * @param visibility  Estado
     */
    @Override
    @TargetApi(Build.VERSION_CODES.FROYO)
    protected void onVisibilityChanged(@NonNull View changedView, int visibility) {
        super.onVisibilityChanged(changedView, visibility);
        if (mDrawable != null) {
            if (visibility == VISIBLE) {
                mDrawable.start();
            } else {
                mDrawable.stop();
            }
        }
    }

    /**
     * Ação quando o tamanho mudar
     */
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mDrawable.setBounds(0, 0, w, h);
    }

    /**
     * Desenha o ProgressBar
     */
    @Override
    public void draw(@NonNull Canvas canvas) {
        super.draw(canvas);
        mDrawable.draw(canvas);
    }

    /**
     * Verifica o Drawable
     */
    @Override
    protected boolean verifyDrawable(@NonNull Drawable who) { return who == mDrawable || super.verifyDrawable(who); }
}
