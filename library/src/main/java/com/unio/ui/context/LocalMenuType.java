package com.unio.ui.context;

import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import com.unio.R;
import com.unio.ui.recycler.AdapterItemType;
import com.unio.ui.recycler.type.SimpleIconType;

/**
 * LocalMenuType
 *
 * Type map for R.layout.view_context_local
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class LocalMenuType extends SimpleIconType
{
    /** Id. Need to be unique */
    public static final int ID = 101;

    /**
     * Get item view
     *
     * @param parent parent context to instantiate
     *
     * @return View
     */
    @Override
    public View getView(@NonNull ViewGroup parent) { return this.loadFromResource(parent, R.layout.view_context_local); }
}