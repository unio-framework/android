package com.unio.ui.context;

import com.unio.ui.recycler.item.SimpleIconItem;

/**
 * LocalMenuItem
 *
 * {@link com.unio.ui.recycler.Adapter} item
 * Use model: { icon, title }
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class LocalMenuItem extends SimpleIconItem
{
    /**
     * Constructor
     *
     * @param values data
     */
    public LocalMenuItem(Object... values) { super(values); }

    /**
     * Get item type id
     *
     * @return int
     */
    @Override
    public int getId() { return LocalMenuType.ID; }

    /**
     * Get item type
     *
     * @return SimpleType
     */
    @Override
    public LocalMenuType getType() { return new LocalMenuType(); }
}