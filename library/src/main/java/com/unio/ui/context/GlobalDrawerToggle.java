package com.unio.ui.context;

import android.app.Activity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import com.unio.R;

/**
 * GlobalDrawerToggle
 *
 * Customized ActionBarDrawerToggle, to avoid drawer action when Local Menu is enabled
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
class GlobalDrawerToggle extends ActionBarDrawerToggle
{
    private final DrawerLayout mDrawerLayout;

    public GlobalDrawerToggle(Activity activity, DrawerLayout drawerLayout, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
        super(activity, drawerLayout, openDrawerContentDescRes, closeDrawerContentDescRes);
        mDrawerLayout = drawerLayout;
    }

    public GlobalDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
        super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        mDrawerLayout = drawerLayout;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {
        if (drawerView.getId() == R.id.frame_context_left)
            super.onDrawerSlide(drawerView, slideOffset);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        if (drawerView.getId() == R.id.frame_context_left)
            super.onDrawerOpened(drawerView);
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (drawerView.getId() == R.id.frame_context_left)
            super.onDrawerClosed(drawerView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean isLocalMenuOpened = mDrawerLayout.isDrawerVisible(GravityCompat.END);
        return !isLocalMenuOpened && super.onOptionsItemSelected(item);
    }
}