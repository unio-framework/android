package com.unio.ui.context;

import android.support.v4.app.Fragment;

/**
 * IGlobalContext
 *
 * Define left global menu to show on an Activity instance of {@link ContextActivity}
 *   or a fragment to load inside this activity
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public interface IGlobalContext
{
    /**
     * Set left global fragment
     *
     * @return Class<? extends Fragment>
     */
    Class<? extends Fragment> getGlobalFragment();
}