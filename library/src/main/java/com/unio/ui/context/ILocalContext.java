package com.unio.ui.context;

import android.support.v7.widget.RecyclerView;
import com.unio.ui.recycler.AdapterItem;

/**
 * ILocalContext
 *
 * Define right local menu to show on an Activity instance of {@link ContextActivity}
 *   or a fragment to load inside this activity
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public interface ILocalContext
{
    /**
     * Send local menu items
     *
     * @return AdapterItem[]
     */
    AdapterItem[] getLocalMenu();

    /**
     * Customize each local item
     *
     * @param position item position
     * @param holder RecyclerView ViewHolder
     * @param item FloatingActionButton
     */
    void onLocalItemCustomize(int position, RecyclerView.ViewHolder holder, AdapterItem item);
}