package com.unio.ui.context;

import com.unio.ui.SwipeRefreshLayout;

/**
 * IRefreshContext
 *
 * Define refresh action on {@link ContextActivity} or its fragments
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public interface IRefreshContext
{
    /**
     * Action when refresh is swiped or called programmatically
     *
     * @param layout SwipeRefreshLayout
     * @param target a flag to identify current refresh. Called as user action, and default value is 0
     */
    void onRefresh(SwipeRefreshLayout layout, int target);

    /**
     * User can swipe refresh. If is false, swipe refresh can be called only programmatically
     *
     * @return boolean
     */
    boolean isUserInteractionEnabled();
}