package com.unio.ui.context;

import android.annotation.SuppressLint;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import com.unio.R;
import com.unio.base.UActivity;
import com.unio.base.UFragment;
import com.unio.debug.Trace;
import com.unio.ui.SwipeRefreshLayout;
import com.unio.ui.features.drawables.RippleDrawable;
import com.unio.ui.recycler.Adapter;
import com.unio.ui.recycler.AdapterItem;
import com.unio.util.helper.Screen;
import com.unio.util.manager.ActivityManager;
import com.unio.util.manager.FragmentManager;
import com.unio.util.helper.Layouts;
import java.lang.ref.WeakReference;
import java.util.LinkedList;
import java.util.List;

/**
 * ContextActivity
 *
 * Activity solution to implement:
 * - Refresh action: option for "swipe to refresh" action
 * - Global menu: left side swipe menu, can contain app global options
 *
 * All these actions can be set globally, from activity, or locally, from current loaded fragment.
 * By default will be priorized fragment action, with fallback to activity action.
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class ContextActivity extends UActivity
    implements SwipeRefreshLayout.OnRefreshListener, DrawerLayout.DrawerListener
{
    /** Current main fragment instance */
    private WeakReference<Fragment> mainFragment;

    /** Loaded fragment history **/
    private List<Class<? extends Fragment>> fragments = new LinkedList<>();

    /** Global menu toggle action */
    private ActionBarDrawerToggle globalToggle;

    /** Context views */
    SwipeRefreshLayout contextRefresh;
    FrameLayout contextLeft;
    FrameLayout contextRight;

    /**
     * Abstract
     * Define first fragment
     *
     * @return Class<? extends Fragment>
     */
    protected abstract Class<? extends Fragment> getFragment();

    /**
     * Retorn layout's resource ID or {@link View} instance
     *
     * @return Object
     */
    @Override
    protected final Object getLayout() { return R.layout.activity_context; }

    /**
     * Initiation method
     *
     * @param savedInstanceState Bundle
     */
    @Override
    protected void onInit(Bundle savedInstanceState) {
        this.getDrawerLayout().addDrawerListener(this);
        this.findMainFromParam();
        this.loadViews();
        this.reload();
    }

    /**
     * Load or reload activity based in current configuration
     */
    public void reload() {
        this.loadMain();
        this.loadRefreshContext();
        this.loadGlobalContext();
        this.loadLocalContext();
    }

    /**
     * Load a new body fragment
     *
     * @param fragmentClass class of fragment to load
     */
    public void loadFragment(final Class<? extends Fragment> fragmentClass) { this.loadFragment(fragmentClass, null, true); }

    /**
     * Load a new body fragment
     *
     * @param fragmentClass class of fragment to load
     * @param params fragment local parameters
     */
    public void loadFragment(final Class<? extends Fragment> fragmentClass, ActivityManager.Param params) {
        this.loadFragment(fragmentClass, params, true);
    }

    /**
     * Load a new body fragment
     *
     * @param fragmentClass class of fragment to load
     * @param params local parameters
     * @param attach attach this fragment to history, to reload when back is pressed
     */
    public void loadFragment(final Class<? extends Fragment> fragmentClass, ActivityManager.Param params, boolean attach) {
        if (attach) fragments.add(fragmentClass);

        try {
            // Fixes java.lang.IllegalStateException with android.support.v4.app.FragmentManagerImpl.checkStateLoss
            this.onPostResume();

            Fragment fragment = FragmentManager.change(this, fragmentClass, R.id.frame_context_main, params);
            // Add real instanced fragment, to preserve called attributes
            mainFragment = new WeakReference<>(fragment);
        } catch (Exception e) {
            Trace.get(e);
        }
    }

    /**
     * Get current loaded fragment instance
     *
     * @return Fragment
     */
    public Fragment getCurrentFragment() { return mainFragment == null ? null : mainFragment.get(); }

    /**
     * Get {@link IRefreshContext} from current fragment or activity
     * Return null if not found
     *
     * @return IRefreshContext
     */
    public IRefreshContext getRefreshContext() {
        IRefreshContext o = null;

        Fragment mainFragment = this.getCurrentFragment();
        if (mainFragment instanceof IRefreshContext) {
            o = (IRefreshContext) mainFragment;
        } else if (this instanceof IRefreshContext) {
            o = (IRefreshContext) this;
        }
        return o;
    }

    /**
     * Perform refresh action programmatically
     * Using this method will call {@link IRefreshContext} action too
     *
     * @param flag refresh action flag
     */
    public void setRefreshing(boolean flag) { this.setRefreshing(flag, 0); }

    /**
     * Perform refresh action programmatically
     * Using this method will call {@link IRefreshContext} action too
     *
     * @param flag refresh action flag
     * @param target when need to identify a refresh action
     */
    public void setRefreshing(final boolean flag, final int target) {
        this.layout.post(new Runnable() {
            @Override
            public void run() {
                contextRefresh.setRefreshing(flag);
                if (flag) {
                    final IRefreshContext o = getRefreshContext();
                    if (o == null) {
                        // If callback not exists, finish refresh to avoid infinite presentation
                        contextRefresh.setRefreshing(false);
                    } else {
                        o.onRefresh(contextRefresh, target);
                    }
                }
            }
        });
    }

    /**
     * Toggle global menu
     */
    public void toggleGlobalMenu() { this.toggleMenu(GravityCompat.START); }

    /**
     * Toggle local menu
     */
    public void toggleLocalMenu() { this.toggleMenu(GravityCompat.END); }

    /**
     * Toggle menu
     *
     * @param gravity menu side
     */
    private void toggleMenu(int gravity) {
        if (this.getDrawerLayout().isDrawerOpen(gravity)) {
            this.getDrawerLayout().closeDrawer(gravity);
        } else {
            this.getDrawerLayout().openDrawer(gravity);
        }
    }

    /**
     * If has a specific fragment when activity is loaded, load it instead of {@link #getFragment()}
     */
    private void findMainFromParam() {
        String fragmentName = this.getIntent().getStringExtra("fragment");
        if (fragmentName != null) {
            try {
                Class fragment = Class.forName(fragmentName);
                Object f = fragment.newInstance();
                if (f instanceof Fragment)
                    mainFragment = new WeakReference<>((Fragment) fragment.newInstance());
            } catch (ClassNotFoundException e) {
                Trace.get(e);
            } catch (InstantiationException e) {
                Trace.get(e);
            } catch (IllegalAccessException e) {
                Trace.get(e);
            }
        }
    }

    /**
     * Load context layout views
     */
    private void loadViews() {
        final int orientation = getResources().getConfiguration().orientation;

        // SwipeRefreshLayout
        this.contextRefresh = this.findViewById(R.id.view_context_refresh);
        this.contextRefresh.setColorSchemeResources(
            R.color.context_refresh_color1,
            R.color.context_refresh_color2,
            R.color.context_refresh_color3
        );

        // Left global menu
        this.contextLeft = this.findViewById(R.id.frame_context_left);
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            int width = Screen.getWidth()/2;
            DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) this.contextLeft.getLayoutParams();
            params.width = width;
        }

        // Right local menu
        this.contextRight = this.findViewById(R.id.frame_context_right);
        int width = Screen.getWidth()/2;
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) this.contextRight.getLayoutParams();
        params.width = width;
    }

    /**
     * Prepare refresh action based on {@link IRefreshContext}
     */
    private void loadRefreshContext() {
        IRefreshContext o = null;

        Fragment mainFragment = this.getCurrentFragment();
        if (mainFragment instanceof IRefreshContext) {
            o = (IRefreshContext) mainFragment;
        } else if (this instanceof IRefreshContext) {
            o = (IRefreshContext) this;
        }
        boolean hasRefreshContext = o != null;

        if (hasRefreshContext) this.contextRefresh.setOnRefreshListener(this);
        this.contextRefresh.setUserInteractionEnabled(hasRefreshContext && o.isUserInteractionEnabled());
    }

    /**
     * Load left global menu context based on activity and current fragment
     */
    public void loadGlobalContext() {
        IGlobalContext o = null;

        Fragment mainFragment = this.getCurrentFragment();
        if (mainFragment instanceof IGlobalContext) {
            o = (IGlobalContext) mainFragment;
        } else if (this instanceof IGlobalContext) {
            o = (IGlobalContext) this;
        }
        boolean hasLeftContext = o != null && o.getGlobalFragment() != null;

        int visibility = hasLeftContext ? View.VISIBLE : View.GONE;
        this.contextLeft.setVisibility(visibility);

        int gravity    = hasLeftContext ? Gravity.START : Gravity.NO_GRAVITY;
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) this.contextLeft.getLayoutParams();
        params.gravity = gravity;

        this.contextLeft.removeAllViews();
        if (hasLeftContext) {
            int title = this.getApplicationInfo().labelRes;
            this.globalToggle = new GlobalDrawerToggle(this, this.getDrawerLayout(), title, title);
            this.getDrawerLayout().addDrawerListener(this.globalToggle);

            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            this.getSupportActionBar().setHomeButtonEnabled(true);

            FragmentManager.change(this, o.getGlobalFragment(), R.id.frame_context_left);
        }
    }

    /**
     * Renderize local right menu based on activity and current fragment
     */
    public void loadLocalContext() {
        ILocalContext o = null;

        Fragment mainFragment = this.getCurrentFragment();
        if (mainFragment instanceof ILocalContext) {
            o = (ILocalContext) mainFragment;
        } else if (this instanceof ILocalContext) {
            o = (ILocalContext) this;
        }
        boolean hasRightContext = o != null;
        AdapterItem[] items = {};
        if (hasRightContext) {
            items = o.getLocalMenu();
            hasRightContext = items != null && items.length > 0;
        }

        int visibility = hasRightContext ? View.VISIBLE : View.GONE;
        this.contextRight.setVisibility(visibility);

        if (hasRightContext) {
            RecyclerView localMenu = this.getLocalMenuView();
            Adapter adapter = new Adapter();
            adapter.add(items);

            final ILocalContext localMenuInterface = o;
            adapter.addOnItemCustomize(new Adapter.IOnItemCustomize() {
                @Override
                public void onItemCustomize(int position, RecyclerView.ViewHolder holder, AdapterItem item) {
                    customizeLocalItem(position, holder, item);
                    localMenuInterface.onLocalItemCustomize(position, holder, item);
                }
            });
            localMenu.setAdapter(adapter);
            this.contextRight.addView(localMenu);
        }
    }

    /**
     * Load main fragment
     * Ignore history if has called from notification
     */
    private void loadMain() {
        Fragment currentFragment = this.getCurrentFragment();
        if (currentFragment == null || this.notification().called) {
            Class<? extends Fragment> mainLayout = this.getFragment();
            if (mainLayout != null) this.loadFragment(mainLayout);
        } else if (currentFragment != null) {
            this.loadFragment(currentFragment.getClass());
        }
    }

    /**
     * Get drawer layout
     *
     * @return DrawerLayout
     */
    private DrawerLayout getDrawerLayout() { return (DrawerLayout) this.layout; }

    /**
     * Prepare Local Menu View
     *
     * @return RecyclerView
     */
    protected RecyclerView getLocalMenuView() {
        RecyclerView localMenu = new RecyclerView(this);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        );
        params.gravity = Gravity.BOTTOM;
        params.topMargin = Layouts.asDp(10);
        params.bottomMargin = Layouts.asDp(10);
        localMenu.setLayoutParams(params);

        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setStackFromEnd(true);
        manager.setReverseLayout(true);
        localMenu.setLayoutManager(manager);

        return localMenu;
    }

    /**
     * Customize local menu item behavior
     *
     * @param position position
     * @param holder RecyclerView ViewHolder
     * @param item item
     */
    protected void customizeLocalItem(int position, RecyclerView.ViewHolder holder, AdapterItem item) {
        RippleDrawable.attach(holder.itemView);
    }

    /**
     * Back action
     */
    @SuppressLint("SwitchIntDef")
    public void onBackPressed() {
        if (this.getDrawerLayout().isDrawerOpen(GravityCompat.START)) {
            this.getDrawerLayout().closeDrawer(GravityCompat.START);
            return;
        }
        
        if (this.getDrawerLayout().isDrawerOpen(GravityCompat.END)) {
            this.getDrawerLayout().closeDrawer(GravityCompat.END);
            return;
        }

        Fragment rawCurrentFragment = this.getCurrentFragment();
        boolean hasFragment = rawCurrentFragment instanceof UFragment;
        // Opened menu, or ActionView is treated by parent back behavior
        if (hasFragment == false) {
            super.onBackPressed();
        } else if (hasFragment) {
            UFragment fragment = (UFragment) rawCurrentFragment;
            int result = fragment.onBackPressed();
            switch (result) {
                case UFragment.BACK_DEFAULT:
                    if (this.fragments.size() <= 1) {
                        this.fragments.clear();
                        this.finish();
                    } else {
                        int last = this.fragments.size()-1;
                        this.fragments.remove(last);
                        if (last > 0) {
                            try {
                                mainFragment = new WeakReference<>(this.fragments.get(last - 1).newInstance());
                                this.reload(); // Update activity content
                            } catch (InstantiationException e) {
                                Trace.get(e);
                            } catch (IllegalAccessException e) {
                                Trace.get(e);
                            }
                            this.getSupportFragmentManager().popBackStack();
                        } else {
                            this.finish();
                        }
                    }
                    break;
                case UFragment.BACK_ONLY_POP:
                    if (this.fragments.size() > 0)
                        this.fragments.remove(this.fragments.size()-1);
                    break;
            }
        }
    }

    /**
     * Use default onRefresh action to call IRefreshContext action
     */
    @Override
    public final void onRefresh() {
        IRefreshContext o = this.getRefreshContext();
        if (o == null) {
            // If callback not exists, finish refresh to avoid infinite presentation
            contextRefresh.setRefreshing(false);
        } else {
            o.onRefresh(contextRefresh, 0);
        }
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {
        if (drawerView.getId() == R.id.frame_context_right) {
            this.getDrawerLayout().setScrimColor(0x99FFFFFF);
        } else {
            this.getDrawerLayout().setScrimColor(0x99000000);
        }
    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {}

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {}

    @Override
    public void onDrawerStateChanged(int newState) {}

    /********************************************************************/
    /**                Default settings, don't change!                 **/
    /********************************************************************/

    @Override
    protected final void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (this.globalToggle != null) this.globalToggle.syncState();
    }

    @Override
    public final void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (this.globalToggle != null) this.globalToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public final boolean onOptionsItemSelected(MenuItem item) {
        if (this.globalToggle == null) return super.onOptionsItemSelected(item);
        return this.globalToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }
}