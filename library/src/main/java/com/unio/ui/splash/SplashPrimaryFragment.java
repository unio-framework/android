package com.unio.ui.splash;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.unio.base.UFragment;
import com.unio.ui.TextView;
import com.unio.ui.annotation.FindViewById;
import com.unio.ui.annotation.LayoutId;

/**
 * SplashPrimaryFragment
 *
 * Primary layout Fragment
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 13/03/2015 23:27
 */
@LayoutId(name = "fragment_splash_primary")
public class SplashPrimaryFragment extends UFragment<SplashLoginActivity, View>
{
    /** Attributes **/
    private ViewTreeObserver _Observer;

    /** Container height **/
    private int _containerHeight;

    @FindViewById(name = "view_splash_title")
    TextView splashTitle;

    @FindViewById(name = "view_splash_progress_bar")
    ContentLoadingProgressBar splashProgressBar;

    /**
     * Get height after secondary layout shows
     *
     * @return int
     */
    protected int getCollapsedHeight() { return (this._containerHeight/3)*2; }

    /**
     * Initial method
     */
    @Override
    public void onInit(Bundle savedInstanceState) {
        this._Observer = this.layout.getViewTreeObserver();
        this._setContainerHeight();
    }

    /**
     * Get container height
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private void _setContainerHeight() {
        this._Observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                _containerHeight = layout.getMeasuredHeight();
            }
        });
    }

    /**
     * Show secondary layout
     *
     * @param animated with animation?
     */
    public final void showSecondaryContainer(boolean animated) {
        if (animated) {
            Animation showLoginAnimation = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    layout.getLayoutParams().height = (int)(_containerHeight - (getCollapsedHeight() * interpolatedTime));
                    splashTitle.getLayoutParams().height -= (int)((splashTitle.getLayoutParams().height * interpolatedTime));
                    splashProgressBar.getLayoutParams().height -= (int)((splashProgressBar.getLayoutParams().height * interpolatedTime));
                    layout.requestLayout();
                }
            };
            showLoginAnimation.setDuration(500);
            layout.startAnimation(showLoginAnimation);
        } else {
            this.layout.getLayoutParams().height = this._containerHeight - this.getCollapsedHeight();
            this.splashTitle.setVisibility(View.GONE);
            this.splashProgressBar.setVisibility(View.GONE);
            this.layout.requestLayout();
        }
    }
}