package com.unio.ui.splash;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.unio.R;
import com.unio.base.UActivity;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.modules.Auth;
import com.unio.util.statics.interfaces.IAsync;

/**
 * SplashLoginActivity
 *
 * Activity for splash login
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 13/03/2015 23:41
 */
public abstract class SplashLoginActivity extends UActivity implements Auth.Delegate
{
    /** Fragments **/
    protected SplashPrimaryFragment primary;
    protected SplashSecondaryFragment secondary;

    /** For savedInstance */
    static String SECONDARY_VISIBLE = "splashSecondaryIsVisible";

    /** Flags **/
    private boolean _secondaryIsVisible = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.setTheme(R.style.Theme_AppCompat_Light_NoActionBar);
        super.onCreate(savedInstanceState);
    }

    /**
     * Layout
     *
     * @return Object
     */
    @Override
    protected Object getLayout() { return R.layout.activity_splash; }

    /**
     * Behavior before start initial process
     */
    protected void onBeforeLoad() { Unio.module(Auth.class).delegate = this; }

    /**
     * Background actions while is starting
     * All http request need to execute as synchronous
     *
     * @param background Executing IAsync
     */
    protected boolean onWhileStarting(IAsync background) { return true; }

    /**
     * Method called when login button is pressed
     * Is called if is valid
     *
     * @param fragment {@link SplashSecondaryFragment}
     */
    protected void onLoginButtonPressed(SplashSecondaryFragment fragment) {
        String username = fragment.username;
        String password = fragment.password;
        this.onBeforeLogin(username, password);
    }

    /**
     * Actions before start login
     * Need to use as a hook to do a same behavior between session login and default login, before finish login process
     */
    protected void onBeforeLogin(String username, String password) {
        if (Unio.module(Auth.class).isSessionStarted()) {
            this.onLogged();
        } else {
            this.secondary.onClickLoginButton();
        }
    }

    /**
     * Ação após validação bem sucedida
     */
    protected abstract void onLogged();

    /**
     * Logout
     *
     * @param auth {@link Auth} instance
     */
    @Override
    public void onLogout(Auth auth) {}

    /**
     * Método inicial
     */
    @Override
    protected void onInit(Bundle savedInstanceState) {
        boolean savedSecondIsVisible = false;
        if (savedInstanceState != null)
            savedSecondIsVisible = savedInstanceState.getBoolean(SECONDARY_VISIBLE, savedSecondIsVisible);

        this.onBeforeLoad();
        this.loadPrimary();
        if (Unio.module(Auth.class).isSessionStarted()) {
            String username = Unio.module(Auth.class).username();
            String password = Unio.module(Auth.class).password();
            this.onBeforeLogin(username, password);
        } else {
            this.loadSecondary(savedInstanceState);

            Runnable runnable;
            long delayTime = 1500;
            if (savedSecondIsVisible) {
                // Need a little delay to load fragment layout
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        _secondaryIsVisible = true;
                        primary.showSecondaryContainer(false);
                    }
                };
                delayTime = 100;
            } else {
                // Continue after init
                runnable = new Runnable() {
                    @Override
                    public void run() { onAfterInit(); }
                };
            }
            // Start after delay
            Unio.ui().postDelayed(runnable, delayTime);
        }
    }

    /**
     * Save FrameLayout id for prevent Exception
     *
     * @param outState Bundle
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        this.secondary.onSaveInstanceState(outState);
        outState.putBoolean(SECONDARY_VISIBLE, this._secondaryIsVisible);
    }

    /**
     * After load all, execute for finalize initial process
     */
    @SuppressLint("StaticFieldLeak")
    protected void onAfterInit() {
        new IAsync() {
            @Override
            protected void onInit() {
                if (onWhileStarting(this)) {
                    this.addOnUI(new IOnUI() {
                        @Override
                        public void onExecuteInUI() { onStarting(); }
                    });
                }
            }
        }.execute();
    }

    /**
     * After starting validation
     * If {@link #onWhileStarting(IAsync)} returns false, need to call this method manually
     */
    protected void onStarting() {
        if (this.isLogged()) {
            this.secondary._onClickLoginButtonBeforeIntegration();
        } else {
            this.showSecondaryContainer();
        }
    }

    /**
     * Back behavior, always finish activity
     */
    @Override
    public void onBackPressed() { this.finish(); }

    /**
     * Check if is logged
     *
     * @return boolean
     */
    protected boolean isLogged() {
        Auth auth = Unio.module(Auth.class);
        if (auth.isLogged()) this.secondary.updateForm(auth.username(), auth.password());
        return auth.isLogged();
    }

    /**
     * Shortcut for show progress
     */
    public void showWait() { this.secondary.showWait(); }

    /**
     * Shortcut for hide progress
     */
    public void hideWait() { this.secondary.hideWait(); }

    /**
     * Primary fragment class
     *
     * @return Class
     */
    protected Class<? extends SplashPrimaryFragment> getPrimaryFragment() { return SplashPrimaryFragment.class; }

    /**
     * Secondary fragment class
     *
     * @return Class
     */
    protected Class<? extends SplashSecondaryFragment> getSecondaryFragment() { return SplashSecondaryFragment.class; }

    /**
     * Load primary fragment
     */
    protected void loadPrimary() {
        if (this.getPrimaryFragment() != null) {
            try {
                this.primary = this.getPrimaryFragment().newInstance();
                this._changeFragment(R.id.frame_splash_primary, this.primary);
            } catch (InstantiationException e) {
                this.primary = null;
                Trace.get(e);
            } catch (IllegalAccessException e) {
                this.primary = null;
                Trace.get(e);
            }
        }
    }

    /**
     * Load secondary fragment
     * Only if will show login form
     */
    protected void loadSecondary(Bundle savedInstanceState) {
        if (this.getSecondaryFragment() != null) {
            try {
                this.secondary = this.getSecondaryFragment().newInstance();
                if (savedInstanceState != null) this.secondary.setArguments(savedInstanceState);
                this._changeFragment(R.id.frame_splash_secondary, this.secondary);
            } catch (InstantiationException e) {
                this.secondary = null;
                Trace.get(e);
            } catch (IllegalAccessException e) {
                this.secondary = null;
                Trace.get(e);
            }
        }
    }

    /**
     * Show secondary fragment
     */
    public final void showSecondaryContainer() {
        this._secondaryIsVisible = true;
        this.primary.showSecondaryContainer(true);
    }

    /**
     * Check if secondary Fragment is visible
     *
     * @return boolean
     */
    public boolean secondaryIsVisible() { return this._secondaryIsVisible; }

    /**
     * Insere o fragment no layout
     *
     * @param frame    resource ID da view
     * @param fragment instância do fragment
     */
    private void _changeFragment(int frame, Fragment fragment) {
        FragmentManager fragmentManager = this.getSupportFragmentManager();
        if (fragmentManager != null) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(frame, fragment);
            //transaction.addToBackStack(null);
            transaction.commit();
        }
    }
}