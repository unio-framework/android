package com.unio.ui.splash;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.widget.ContentLoadingProgressBar;
import android.support.v7.widget.AppCompatButton;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import com.unio.R;
import com.unio.base.UFragment;
import com.unio.core.Unio;
import com.unio.modules.Auth;
import com.unio.ui.TextView;
import com.unio.ui.annotation.FindViewById;
import com.unio.ui.annotation.LayoutId;
import com.unio.ui.features.fonts.FontAwesome;
import com.unio.ui.features.fonts.MaterialIcon;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.util.helper.Check;
import com.unio.util.statics.interfaces.IAsync;
import org.jetbrains.annotations.NotNull;

/**
 * SplashSecondaryFragment
 *
 * Secondary fragment
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 13/03/2015 23:34
 */
@LayoutId(name = "fragment_splash_secondary")
public class SplashSecondaryFragment extends UFragment<SplashLoginActivity, View>
{
    /** SplashActivity **/
    private static final String USERNAME = "SplashSecondaryUsername";
    private static final String PASSWORD = "SplashSecondaryPassword";

    @FindViewById(name = "view_splash_username")
    EditText splashUsername;

    @FindViewById(name = "view_splash_password")
    EditText splashPassword;

    @FindViewById(name = "view_splash_login")
    AppCompatButton splashLogin;

    @FindViewById(name = "view_splash_progress_bar")
    ContentLoadingProgressBar splashProgressBar;

    /** Username/password **/
    public String username;
    public String password;

    /**
     * Initial
     */
    @Override
    public void onInit(Bundle savedInstanceState) {
        TextView splashUsernameIcon = (TextView) this.findViewById(R.id.view_splash_username_icon);
        splashUsernameIcon.setIcon(FontAwesome.USER);

        TextView splashPasswordIcon = (TextView) this.findViewById(R.id.view_splash_password_icon);
        splashPasswordIcon.setIcon(FontAwesome.LOCK);

        final TextView splashPasswordVisibility = (TextView) this.findViewById(R.id.view_splash_password_visibility);
        splashPasswordVisibility.setIcon(MaterialIcon.VISIBILITY);
        splashPasswordVisibility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = splashPassword.getText().toString();
                if (Check.isEmpty(text) == false) {
                    int newType;
                    IconCharacter icon;
                    if (splashPassword.getInputType() == (InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD)) {
                        newType = InputType.TYPE_CLASS_TEXT;
                        icon = MaterialIcon.VISIBILITY_OFF;
                    } else {
                        newType = InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD;
                        icon = MaterialIcon.VISIBILITY;
                    }
                    splashPassword.setInputType(newType);
                    splashPassword.setSelection(text.length());
                    splashPasswordVisibility.setIcon(icon);
                }
            }
        });

        this.splashLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { _onClickLoginButtonBeforeIntegration(); }
        });

        if (this.getArguments() != null) {
            String text;
            text = this.getArguments().getString(USERNAME);
            if (Check.isEmpty(text) == false) this.splashUsername.setText(text);
            text = this.getArguments().getString(PASSWORD);
            if (Check.isEmpty(text) == false) this.splashPassword.setText(text);
        }
    }

    /**
     * Persist username and password
     *
     * @param outState Bundle
     */
    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);

        String username = this.splashUsername.getText().toString();
        String password = this.splashPassword.getText().toString();
        if (Check.isEmpty(username) == false) outState.putString(USERNAME, username);
        if (Check.isEmpty(password) == false) outState.putString(PASSWORD, password);
    }

    /**
     * Show Progress
     */
    public final void showWait() {
        this.splashLogin.setVisibility(View.GONE);
        this.splashProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * Hide Progress
     */
    public final void hideWait() {
        this.splashLogin.setVisibility(View.VISIBLE);
        this.splashProgressBar.setVisibility(View.GONE);
    }

    /**
     * Update form for automatic login
     *
     * @param username username
     * @param password password
     */
    public final void updateForm(String username, String password) {
        this.username = username;
        this.password = password;
        if (Check.isEmpty(this.username) == false) this.splashUsername.setText(this.username);
        if (Check.isEmpty(this.password) == false) this.splashPassword.setText(this.password);
    }

    /**
     * Add parameters for login validation and start
     */
    protected void _onClickLoginButtonBeforeIntegration() {
        this.showWait();

        this.username = null;
        if (this.splashUsername.getText() != null) this.username = this.splashUsername.getText().toString();
        this.password = null;
        if (this.splashPassword.getText() != null) this.password = this.splashPassword.getText().toString();

        boolean isValid = Unio.module(Auth.class).validate(this.username, this.password);
        if (isValid) {
            this.activity().onLoginButtonPressed(this);
        } else {
            if (activity().secondaryIsVisible() == false) activity().showSecondaryContainer();
            this.hideWait();
        }
    }

    /**
     * Login submit
     */
    public final void onClickLoginButton() {
        // Start after delay
        Runnable runnable = new Runnable() {
            @SuppressLint("StaticFieldLeak")
            @Override
            public void run() {
                new IAsync() {
                    @Override
                    protected void onInit() {
                        final boolean isLogged = Unio.module(Auth.class).login(this);
                        this.addOnUI(new IOnUI() {
                            @Override
                            public void onExecuteInUI() {
                                if (isLogged) {
                                    activity().onLogged();
                                } else {
                                    if (activity().secondaryIsVisible() == false) activity().showSecondaryContainer();
                                    hideWait();
                                }
                            }
                        });
                    }
                }.execute();
            }
        };
        Unio.ui().postDelayed(runnable, 1500);
    }
}