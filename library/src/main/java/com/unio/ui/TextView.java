package com.unio.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import com.unio.core.Unio;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.modules.Formatter;
import com.unio.util.helper.Methods;
import com.unio.util.helper.Views;
import com.unio.util.manager.ResourceManager;
import java.util.Map;

/**
 * TextView
 *
 * Custom {@link android.widget.TextView}
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class TextView extends AppCompatTextView
{
    /**
     * Constructor
     */
    public TextView(Context context)                                       { super(context);                      }
    public TextView(Context context, AttributeSet attrs)                   { super(context, attrs);               }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public TextView(Context context, AttributeSet attrs, int defStyleAttr) { super(context, attrs, defStyleAttr); }

    /**
     * Set text with simple params
     *
     * @param text   text
     * @param params params
     */
    public void setText(CharSequence text, Map<String, Object> params) {
        Formatter.CNText response = Unio.module(Formatter.class).format(text, params);
        String t = response.text.toString();
        super.setText((t.contains("</") || t.contains("/>")) ? Views.htmlFormat(t) : t);
        if (response.typeface != null) this.setTypeface(response.typeface);
    }

    /**
     * Default set text with formatter
     *
     * @param text text
     * @param type type
     */
    @Override
    public void setText(CharSequence text, BufferType type) {
        // Needs here because on last super, has setted the text
        if (this.isInEditMode()) ResourceManager.set(this.getContext());

        if (this.isInEditMode() && Methods.loadClass("org.json.JSONException") == null) {
            super.setText(text, type);
        } else {
            Formatter.CNText response = Unio.module(Formatter.class).format(text);
            String t = response.text.toString();
            super.setText((t.contains("</") || t.contains("/>")) ? Views.htmlFormat(t) : t, type);
            if (response.typeface != null) this.setTypeface(response.typeface);
        }
    }

    /**
     * Set text icon
     *
     * @param i IconCharacter instance
     */
    public void setIcon(IconCharacter i) {
        this.setText(String.valueOf(i.icon()));
        this.setTypeface(i.typeface());
    }
}