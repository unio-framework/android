package com.unio.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import java.util.Arrays;

/**
 * _Styleable
 *
 * All general res/styleable attributes
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 14/02/2016 13:54
 */
class _Styleable
{
    /** Text appearance */
    public static final int[] STYLES = {
        // General
        android.R.attr.enabled,
        // Text
        android.R.attr.text,
        android.R.attr.textSize,
        android.R.attr.textColor,

        // Hint
        android.R.attr.hint,
        android.R.attr.textColorHint,
    };

    private Context _$Context;
    private AttributeSet _$Attrs;

    public _Styleable(Context context, AttributeSet attrs) {
        this._$Context = context;
        this._$Attrs = attrs;
    }

    /**
     * Check if contains id
     *
     * @param id check id
     *
     * @return boolean
     */
    public boolean contains(int id) { return this.get(id).equals(-1)==false; }

    /**
     * Get id content
     *
     * @param id id
     *
     * @return Object
     */
    public Object get(int id) {
        int[] styleable = STYLES;
        Arrays.sort(styleable);
        TypedArray a = this._$Context.obtainStyledAttributes(this._$Attrs, styleable);
        for (int attrIndex = 0; attrIndex < styleable.length; attrIndex++) {
            int attribute = styleable[attrIndex];
            if (a.hasValue(attrIndex) && attribute == id) {
                switch (attribute) {
                    case android.R.attr.text:
                    case android.R.attr.hint:
                        return a.getText(attrIndex);
                    case android.R.attr.textSize:
                        return a.getDimensionPixelSize(attrIndex, 12);
                    case android.R.attr.textColor:
                    case android.R.attr.textColorHint:
                        return a.getColorStateList(attrIndex);
                    case android.R.attr.enabled:
                        return a.getBoolean(attrIndex, true);
                }
            }
        }
        a.recycle();
        return -1;
    }
}