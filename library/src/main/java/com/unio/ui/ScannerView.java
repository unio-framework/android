package com.unio.ui;

import android.content.Context;
import android.util.AttributeSet;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * UDScannerView
 *
 * Custom implementation of {@link ZXingScannerView}
 * Need to add "compile 'me.dm7.barcodescanner:zxing:[version]'" in gradle
 *
 * See https://github.com/dm77/barcodescanner
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class ScannerView extends ZXingScannerView implements ZXingScannerView.ResultHandler
{
    /** Callback */
    private List<IOnFind> _onFinds = new LinkedList<>();

    /**
     * Constructor
     */
    public ScannerView(Context context) { this(context, null); }
    public ScannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        super.setResultHandler(this);
   }

    /**
     * Method disabled, must use {@link #addOnFind(IOnFind)}
     *
     * @param resultHandler ResultHandler
     */
    @Override
    public void setResultHandler(ResultHandler resultHandler) {
        final String message = "Method disabled, must use #addOnFind(IOnFind)";
        throw new RuntimeException(message);
    }

    /**
     * Set acceptable barcode formats as array
     */
    public void setFormats(BarcodeFormat... formats) {
        List<BarcodeFormat> listFilter = new LinkedList<>(Arrays.asList(formats));
        this.setFormats(listFilter);
    }

    /**
     * IOnFind control
     *
     * @param c callback
     */
    public void addOnFind(IOnFind c)    { if (c != null) this._onFinds.add(c);    }
    public void removeOnFind(IOnFind c) { if (c != null) this._onFinds.remove(c); }
    public void clearOnFinds()          { this._onFinds.clear();                  }

    /**
     * Callback when find a result
     *
     * @param result result
     */
    @Override
    public void handleResult(Result result) {
        if (this._onFinds.size() > 0) {
            Response response = new Response(result.getBarcodeFormat(), result.getText());
            for (IOnFind c : this._onFinds) c.onFind(response);
        }
    }

    /**
     * IOnFindHandler
     *
     * Callback when find a result
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 18/10/2016 20:09
     */
    public interface IOnFind
    {
        /**
         * Callback when find a result
         *
         * @param response response
         */
        void onFind(Response response);
    }

    /**
     * Response
     *
     * Find code and this type
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 18/10/2016 20:09
     */
    public static class Response
    {
        /** Attributes */
        public final BarcodeFormat type;
        public final String result;

        /**
         * Constructor
         *
         * @param type   type
         * @param result result
         */
        public Response(BarcodeFormat type, String result) {
            this.result = result;
            this.type   = type;
        }

        /**
         * Format and show
         *
         * @return String
         */
        @Override
        public String toString() {
            return "BarcodeFormat=" + this.type + "\n" +
                "result=" + this.result;
        }
    }
}