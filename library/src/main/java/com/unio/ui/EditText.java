package com.unio.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.unio.util.helper.Screen;
import com.unio.util.helper.Check;
import com.unio.util.manager.ResourceManager;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import android.support.v7.widget.AppCompatAutoCompleteTextView;

/**
 * EditText
 *
 * {@link EditText} with label transform hint option, formatter, and auto complete
 * Based in TextInputLayout
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 19/02/2016 19:00
 */
public class EditText extends TextInputLayout
{
    /** Views */
    public final AutoCompleteTextView $Edit;
    public final AutoCompleteSetting AutoComplete;

    /**
     * Constructor
     */
    public EditText(Context context) { this(context, null); }

    /**
     * Constructor
     */
    public EditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.$Edit = new AppCompatAutoCompleteTextView(context, attrs) {
            @Override
            public void showDropDown() {
                // Need this change
                // When EditText is near keyboard will throw NullPointerException
                // It's a PopupWindow behavior bug
                boolean hasNullException = false;
                try {
                    super.showDropDown();
                } catch(NullPointerException e) {
                    int tempHeight = Screen.getHeight();
                    this.setDropDownHeight(tempHeight/2);
                    super.showDropDown();
                    hasNullException = true;
                } finally {
                    if (hasNullException) this.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            }
        };
        this.AutoComplete = new AutoCompleteSetting(this.$Edit);
        this._prepare(context, attrs);
    }

    /**
     * Constructor
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public EditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.$Edit = new AppCompatAutoCompleteTextView(context, attrs, defStyleAttr) {
            @Override
            public void showDropDown() {
                // Need this change
                // When EditText is near keyboard will throw NullPointerException
                // It's a PopupWindow behavior bug
                boolean hasNullException = false;
                try {
                    super.showDropDown();
                } catch(NullPointerException e) {
                    int tempHeight = Screen.getHeight();
                    this.setDropDownHeight(tempHeight/2);
                    super.showDropDown();
                    hasNullException = true;
                } finally {
                    if (hasNullException) this.setDropDownHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                }
            }
        };
        this.AutoComplete = new AutoCompleteSetting(this.$Edit);
        this._prepare(context, attrs);
    }

    /**
     * Prepare view
     */
    private void _prepare(Context context, AttributeSet attrs) {
        this.$Edit.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.addView(this.$Edit);

        // Custom
        int[] s = ResourceManager.getStyleables("UDEditText");
        if (s != null && s.length > 0) {
            final TypedArray typedArray = context.obtainStyledAttributes(attrs, s);
            CharSequence[] sugestions = typedArray.getTextArray(ResourceManager.getStyleable("UDEditText_autoCompleteSugestions"));
            if (Check.isEmpty(sugestions) == false) {
                this.AutoComplete.add(sugestions);
                this.AutoComplete.render();
            }
            typedArray.recycle();
        }
    }

    public void setText(CharSequence c) {this.$Edit.setText(c); }
    public void setImeOptions(int imeOptions) { this.$Edit.setImeOptions(imeOptions); }
    public void setTextColor(int color) { this.$Edit.setTextColor(color); }
    public void setTextSize(float size) { this.$Edit.setTextSize(size); }
    public void setSingleLine() { this.$Edit.setSingleLine(); }
    public void setInputType(int type) { this.$Edit.setInputType(type); }
    public Editable getText() { return this.$Edit.getText(); }
    public void addTextChangedListener(TextWatcher watcher) { this.$Edit.addTextChangedListener(watcher); }

    /**
     * AutoComplete
     *
     * Class to AutoCompleteTextView settings
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 22/02/2016 16:01
     */
    public static class AutoCompleteSetting extends ArrayAdapter<String>
    {
        /** AutoCompleteTextView */
        private AutoCompleteTextView _View;

        /** Sugestion list */
        private List<String> _items      = new LinkedList<>();
        private List<String> _sugestions = new LinkedList<>();

        /** Callback to search sugestion when digits */
        public IOnAutoCompleteDigit OnDigited;

        /** Callback when selects a sugestion */
        public IOnAutoCompleteSelect OnSelected;

        /** Filter */
        final Filter FILTER = new Filter()
        {
            @Override
            public String convertResultToString(Object resultValue) { return String.valueOf(resultValue); }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                if (constraint != null) {
                    _sugestions.clear();
                    String text = constraint.toString();

                    if (OnDigited == null) {
                        for (String item : _items) {
                            String comp = item.toLowerCase();
                            if (comp.contains(text.toLowerCase()) || constraint.equals(" ")) _sugestions.add(item);
                        }
                    } else {
                        _sugestions = OnDigited.onDigited(text);
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = _sugestions;
                    filterResults.count = _sugestions.size();
                    return filterResults;
                } else {
                    return new FilterResults();
                }
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    List<String> filteredList = (List<String>)results.values;
                    clear();
                    for (String l : filteredList) AutoCompleteSetting.super.add(l);
                    notifyDataSetChanged();
                }
            }
        };

        /**
         * Constructor
         */
        AutoCompleteSetting(AutoCompleteTextView view) {
            super(view.getContext(), android.R.layout.simple_dropdown_item_1line);
            this._View = view;
            this._View.setThreshold(1);
        }

        /**
         * Add a sugestion
         *
         * @param item sugestion
         */
        @Override
        public void add(String item) {
            super.add(item);
            this._items.add(item);
        }

        /**
         * Add a list of sugestions
         *
         * @param items list of sugestions as CharSequence array
         */
        public void add(CharSequence[] items) {
            if (items != null) {
                for (CharSequence item : items) {
                    if (item == null) continue;
                    this.add(item.toString());
                }
            }
        }

        /**
         * Add a list of sugestions
         *
         * @param items list of sugestions as String array
         */
        public void add(String[] items) { this._items = new LinkedList<>(Arrays.asList(items)); }

        /**
         * Add a list of sugestions
         *
         * @param items list of sugestions as String List
         */
        public void add(List<String> items) { this._items = items; }

        /**
         * Item filter
         *
         * @return Filter
         */
        @NonNull
        @Override
        public Filter getFilter() { return this.FILTER; }

        /**
         * Render
         */
        public void render() {
            this._View.setAdapter(this);

            // Add OnSelected when click in an item
            this._View.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (OnSelected != null) {
                        TextView textView = (TextView) view;
                        String text = textView.getText().toString();
                        OnSelected.onSelected(position, text);
                    }
                }
            });
        }
    }

    /**
     * IOnDigit
     *
     * Interface for search sugestion when digit
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 11/12/2014 00:13
     */
    public interface IOnAutoCompleteDigit
    {
        /**
         * Método de execução
         *
         * @param text texto digitado
         *
         * @return List
         */
        List<String> onDigited(String text);
    }

    /**
     * IOnSelect
     *
     * Interface for when selects a sugestion
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 11/12/2014 00:13
     */
    public interface IOnAutoCompleteSelect
    {
        /**
         * Método de execução
         *
         * @param text texto escolhido
         */
        void onSelected(int position, String text);
    }
}