package com.unio.ui.features.effects;

import android.text.Editable;
import android.text.TextWatcher;
import com.unio.ui.EditText;
import java.util.HashSet;
import java.util.Set;

/**
 * InputMask
 *
 * Class to create a mask in {@link EditText} and {@link EditText}
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 22/09/2016 16:12
 */
public class InputMask// implements TextWatcher
{
//    /** Brazilian models */
//    public static String CPF     = "###.###.###-##";
//    public static String CELULAR = "(##) #####-#####";
//    public static String CEP     = "#####-###";
//
//    /** Attributes */
//    private String mMask;
//    private EditText mEditText;
//    private Set<String> symbolMask = new HashSet<>();
//    private boolean isUpdating;
//    private String old = "";
//
//    /**
//     * Constructor
//     */
//    public InputMask(String mask, android.widget.EditText editText) {
//        mMask = mask;
//        mEditText = editText;
//        initSymbolMask();
//    }
//
//    /**
//     * Constructor
//     */
//    public InputMask(String mask, EditText editText) {
//        mMask = mask;
//        mEditText = editText.$Edit;
//        initSymbolMask();
//    }
//
//    /**
//     * Constructor
//     */
//    private void initSymbolMask(){
//        for (int i=0; i < mMask.length(); i++){
//            char ch = mMask.charAt(i);
//            if (ch != '#')
//                symbolMask.add(String.valueOf(ch));
//        }
//    }
//
//    /**
//     * Get raw text
//     *
//     * @return String
//     */
//    public String getRawText() { return unmask(this.mEditText.getText().toString(), symbolMask); }
//
//    /**
//     * onTextChanged
//     */
//    @Override
//    public void onTextChanged(CharSequence s, int start, int before, int count) {
//        String str = unmask(s.toString(), symbolMask);
//        String mascara;
//
//        if (isUpdating) {
//            old = str;
//            isUpdating = false;
//            return;
//        }
//
//        if(str.length() > old.length())
//            mascara = mask(mMask,str);
//        else
//            mascara = s.toString();
//
//        isUpdating = true;
//
//        mEditText.setText(mascara);
//        mEditText.setSelection(mascara.length());
//    }
//
//    public static String unmask(String s, Set<String> replaceSymbols) {
//        for (String symbol : replaceSymbols)
//            s = s.replaceAll("["+symbol+"]","");
//        return s;
//    }
//
//    public static String mask(String format, String text) {
//        String maskedText="";
//        int i =0;
//        for (char m : format.toCharArray()) {
//            if (m != '#') {
//                maskedText += m;
//                continue;
//            }
//            try {
//                maskedText += text.charAt(i);
//            } catch (Exception e) {
//                break;
//            }
//            i++;
//        }
//        return maskedText;
//    }
//
//    @Override
//    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
//
//    @Override
//    public void afterTextChanged(Editable s) {}
}