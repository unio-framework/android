package com.unio.ui.features.fonts.base;

import android.graphics.Typeface;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.util.io.File;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * IconTypeface
 *
 * Classe para manter os typefaces em cache
 *
 * Caso não use a fonte .ttf pelo /assets, obrigatório:
 *   <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 21/09/2015 16:33
 */
public class IconTypeface
{
    /**
     * Armazena as fontes de ícone para reaproveitamento
     * Ficar gerando typeface várias vezes impacta na performance do aplicativo
     */
    private static Map<String, Typeface> _typefaces = new LinkedHashMap<>();

    /**
     * Adiciona uma fonte de ícone na lista para reaproveitar
     *
     * @param name nome da fonte
     * @param font Typeface
     */
    public static void set(String name, Typeface font) { _typefaces.put(name, font); }

    /**
     * Retorna uma fonte de ícone
     *
     * @param name nome da fonte
     *
     * @return Typeface
     */
    public static Typeface get(String name) {
        Typeface font = _typefaces.get(name);
        if (font == null) {
            font = _get(name);
            set(name, font);
        }
        return font;
    }

    /**
     * Gera o Typeface
     *
     * @param name nome do arquivo
     *
     * @return Typeface
     */
    private static Typeface _get(String name) {
        try {
            name += ".ttf";
            if (Arrays.asList(Unio.app().getResources().getAssets().list("fonts")).contains(name)) {
                // Se existir no assets, gera por ele
                return Typeface.createFromAsset(Unio.app().getAssets(), "fonts/"+name);
            } else {
                File f;
                String directory = Unio.app().getFilesDir() == null ? Unio.app().getCacheDir().toString() : Unio.app().getFilesDir().toString();
                f = new File(directory + "/fonts/" + name);
                if (f.exists()) return Typeface.createFromFile(f.Raw);

                f = new File(directory + "/fonts");
                f.create();
                if (f.exists()) {
                    java.io.File outPath = new java.io.File(f.Raw, name);
                    if (outPath.exists()) return Typeface.createFromFile(outPath);

                    InputStream inputStream = Unio.class.getClassLoader().getResourceAsStream("fonts/"+name);
                    byte[] buffer = new byte[inputStream.available()];
                    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(outPath));
                    int l;
                    while ((l = inputStream.read(buffer)) > 0) bos.write(buffer, 0, l);
                    bos.close();
                    return Typeface.createFromFile(outPath);
                }
            }
        } catch (IOException e) {
            Trace.get(e);
        }
        return null;
    }
}