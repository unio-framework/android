package com.unio.ui.features.drawables;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;

/**
 * PressedDrawable
 *
 * Drawable para mudar o background no clique
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 19/03/2015 20:06
 */
public class PressedDrawable extends StateListDrawable
{
    /**
     * Renderiza o Drawable na cor padrão
     *
     * @return PressedDrawable
     */
    public static PressedDrawable render() { return render(Color.parseColor("#3E73A7")); }

    /**
     * Renderiza o Drawable com uma cor customizada
     *
     * @param color cor
     *
     * @return PressedDrawable
     */
    public static PressedDrawable render(int color) {
        PressedDrawable d = new PressedDrawable();
        d.addState(new int[] { android.R.attr.state_pressed }, new ColorDrawable(color));
        return d;
    }

    public static ColorStateList list(int pressed, int normal) {
        return new ColorStateList(
            new int[][] {
                new int[]{ android.R.attr.state_pressed },
                new int[]{}
            },
            new int[] {
                pressed,
                normal,
            }
        );
    }
}