package com.unio.ui.features.fonts.base;

import android.graphics.Typeface;

/**
 * IconFont
 *
 * Classe base para trabalhar com ícone
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 11/04/2015 15:38
 */
public abstract class IconFont
{
    /**
     * Seta o ícone e o Typeface
     *
     * @param v    ícone
     * @param name nome do arquivo sem .ttf
     *
     * @return IconCharacter
     */
    protected static IconCharacter set(char v, String name) { return new IconCharacter(v, name); }

    /**
     * Retorna um Typeface
     *
     * @param name nome do arquivo da fonte
     *
     * @return Typeface
     */
    protected static Typeface typeface(String name) { return IconTypeface.get(name); }
}