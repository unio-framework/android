package com.unio.ui.features.fonts.base;

import android.graphics.Typeface;

/**
 * IconCharacter
 *
 * Gera os valores necessários para trabalhar com ícone
 *
 * Caso não use a fonte .ttf pelo /assets, obrigatório:
 *   <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 11/04/2015 15:37
 */
public class IconCharacter
{
    /** Atributos */
    private char _icon;
    private String _name;

    /**
     * Método construtor
     *
     * @param v    char do ícone
     * @param name nome do arquivo sem .ttf
     */
    public IconCharacter(char v, String name) {
        this._icon = v;
        this._name = name;
    }

    /**
     * Retorna o ícone char
     *
     * @return char
     */
    public char icon() { return this._icon; }

    /**
     * Gera e retorna o Typeface
     *
     * @return Typeface
     */
    public Typeface typeface() { return IconTypeface.get(this._name); }
}