package com.unio.ui.features.drawables;

import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.StateSet;
import com.unio.util.helper.Color;

/**
 * NoActionDrawable
 *
 * Drawable para caso sem ação, caso tenha, tira a ação antes existente
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 28/03/2015 22:47
 */
public class NoActionDrawable extends StateListDrawable
{
    /**
     * Renderiza o Drawable para o caso sem ação, ou seja, transparente
     *
     * @return NoActionDrawable
     */
    public static NoActionDrawable render() {
        NoActionDrawable d  = new NoActionDrawable();
        d.addState(StateSet.WILD_CARD, renderToState());
        return d;
    }
    /**
     * Renderiza o Drawable para o caso sem ação, ou seja, transparente
     * Renderiza para ser usado no addState
     *
     * @return ColorDrawable
     */
    public static ColorDrawable renderToState() {
        return new ColorDrawable(Color.TRANSPARENT);
    }
}