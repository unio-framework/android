package com.unio.ui.features.drawables;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.*;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import com.unio.util.helper.Color;
import com.unio.util.helper.Views;
import org.xmlpull.v1.XmlPullParser;
import static com.unio.ui.features.drawables.RippleDrawable.RectUtils.adjustCenter;
import static com.unio.ui.features.drawables.RippleDrawable.RectUtils.adjustRadius;

/**
 * RippleDrawable
 *
 * Drawable for ripple effect
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 16/06/2016 13:14
 */
public class RippleDrawable extends Drawable implements View.OnTouchListener
{
    /** Default alpha */
    public static final int DEFAULT_ALPHA = 90;
    public static final int DEFAULT_COLOR = Color.LTGRAY;

    /** Attributes */
    private final AnimationImpl mAnimationImpl = new AnimationImpl();
    private View mHost = null;  // host is default to null.
    private Paint mPaint = new Paint();
    private boolean _touched = false;
    private GestureDetectorCompat _gestureDetector;

    /**
     * Default constructor
     */
    public RippleDrawable() { this(DEFAULT_COLOR); }

    /**
     * Constructor with color
     *
     * @param color color
     */
    public RippleDrawable(@ColorInt int color) { this(DEFAULT_ALPHA, color); }

    /**
     * Constructor
     *
     * @param alpha alpha
     * @param color color
     */
    public RippleDrawable(int alpha, @ColorInt int color) {
        this.mPaint.setAntiAlias(true);
        this.mPaint.setStyle(Paint.Style.FILL);
        this.setColor(color);
        this.setAlpha(alpha);
    }

    /**
     * Attach
     *
     * @param view view
     */
    public RippleDrawable attachToView(View view) {
        LayerDrawable layer = new LayerDrawable(
            view.getBackground() != null ?
                new Drawable[] { view.getBackground(), this } :
                new Drawable[] { this }
        );
        Views.setBackground(view, layer);
        view.setOnTouchListener(this);
        setHost(view);
        setVisible(false, false);
        view.setClickable(true);

        final boolean isClickable = view.isClickable();
        this._gestureDetector = new GestureDetectorCompat(view.getContext(), new GestureDetector.OnGestureListener() {
            @Override
            public boolean onDown(MotionEvent e) { return false; }

            @Override
            public void onShowPress(MotionEvent e) {}

            @Override
            public boolean onSingleTapUp(MotionEvent event) {
                mAnimationImpl.setCenter((int) event.getX(), (int) event.getY());
                mAnimationImpl.press();
                return false;
            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) { return false; }

            @Override
            public void onLongPress(MotionEvent event) {
                mAnimationImpl.setCenter((int) event.getX(), (int) event.getY());
                mAnimationImpl.press();
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) { return false; }
        });
        return this;
    }

    /**
     * Attach to view
     *
     * @param view view
     *
     * @return RippleDrawable
     */
    public static RippleDrawable attach(View view) { return attach(view, DEFAULT_COLOR); }

    /**
     * Attach to view
     *
     * @param view  view
     * @param color color
     *
     * @return RippleDrawable
     */
    public static RippleDrawable attach(View view, int color) {
        return new RippleDrawable(color)
            .attachToView(view);
    }

    /**
     * Set view
     *
     * @param host view
     */
    public RippleDrawable setHost(View host) {
        mHost = host;
        return this;
    }

    /**
     * Set color
     *
     * @param color color
     */
    public RippleDrawable setColor(int color) {
        mPaint.setColor(color);
        return this;
    }

    @Override
    public void inflate(@NonNull Resources r, @NonNull XmlPullParser parser, @NonNull AttributeSet attrs) {
        throw new UnsupportedOperationException("Inflating from xml not supported, do it in code");
    }

    @Override
    public void draw(@NonNull Canvas canvas) {
        //Trace.log(this, isVisible());
        // Drawable visibility is not being checked by default. We check and only draw
        // when necessary.
        if (this._touched) {
            canvas.drawCircle(
                getBounds().centerX(),
                getBounds().centerY(),
                RectUtils.getRadius(getBounds()),
                mPaint);
        }
    }

    @Override
    public void setAlpha(int alpha) { mPaint.setAlpha(alpha); }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // ColorFilter isn't meaningful to ripple effect.
        throw new UnsupportedOperationException("color filter not supported");
    }

    @Override
    public int getOpacity() { return PixelFormat.TRANSLUCENT; }

    Runnable dismiss = new Runnable() {
        @Override
        public void run() { mAnimationImpl.enterReleaseAnimation(); }
    };

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        this._gestureDetector.onTouchEvent(event);
        return false;
    }

    private class AnimationImpl
    {
        private ObjectAnimator mAnimator;

        public int getRadius() { return RectUtils.getRadius(getBounds()); }

        public void setRadius(int radius) {
            setBounds(adjustRadius(getBounds(), radius));
            invalidateSelf();
        }

        public void setCenter(int x, int y) {
            setBounds(adjustCenter(getBounds(), x, y));
            invalidateSelf();
        }

        public void press() {
            assert mHost != null;
            cancel();
            _touched = true;
            setVisible(true, true);
            mAnimator = ObjectAnimator.ofInt(this, "radius", (int) OtherUtils.toPx(mHost, 16), (int) OtherUtils.toPx(mHost, 32));
            mAnimator.setDuration(10);
            mAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            mAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}

                @Override
                public void onAnimationEnd(Animator animator) { enterReleaseAnimation(); }

                @Override
                public void onAnimationCancel(Animator animator) { enterReleaseAnimation(); }

                @Override
                public void onAnimationRepeat(Animator animator) {}
            });
            mAnimator.start();
        }

        public void enterPressAnimation() {
            assert mHost != null;
            cancel();
            _touched = true;
            setVisible(true, true);
            mAnimator = ObjectAnimator.ofInt(this, "radius", (int) OtherUtils.toPx(mHost, 16), (int) OtherUtils.toPx(mHost, 32));
            mAnimator.setDuration(200);
            mAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            mAnimator.start();
        }

        public void enterReleaseAnimation() {
            assert mHost != null;
            //cancel();
            mAnimator = ObjectAnimator.ofInt(this, "radius", getRadius(), OtherUtils.maxEdge(mHost));
            mAnimator.setDuration(550);
            mAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
            mAnimator.addListener(
                new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {}

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        // reset to invisible.
                        setVisible(false, true);
                        _touched = false;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {}

                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                }
            );
            mAnimator.start();
        }

        private void cancel() { if (mAnimator != null && mAnimator.isRunning()) mAnimator.cancel(); }
    }

    static final class RectUtils
    {
        // Square
        public static Rect of(int centerX, int centerY, int radius) {
            return new Rect(
                centerX - radius,
                centerY - radius,
                centerX + radius,
                centerY + radius
            );
        }

        public static int getRadius(Rect square) { return square.centerX() - square.left; }
        public static Rect adjustCenter(Rect rect, int x, int y) { return of(x, y, getRadius(rect)); }
        public static Rect adjustRadius(Rect rect, int radius) { return of(rect.centerX(), rect.centerY(), radius); }
        // Prevent instantiation
        private RectUtils() {}
    }

    static final class OtherUtils
    {
        public static float toPx(Resources res, int dp) {
            return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                res.getDisplayMetrics()
            );
        }

        public static float toPx(View v, int dp) { return toPx(v.getResources(), dp); }
        public static int maxEdge(View view) { return Math.max(view.getHeight(), view.getWidth()); }
        private OtherUtils() {}
    }

    /**
     * Used to detect drawable state changes.
     */
    static class DrawableStateChangeDetector
    {
        private final int[] mStateIds;
        private final Listener mListener;

        /**
         * @param stateIds a list of state ids of our interests.
         * @param listener the listener to who notification is sent.
         */
        public DrawableStateChangeDetector(int[] stateIds, Listener listener) {
            mStateIds = stateIds;
            mListener = listener;
        }

        interface Listener { void onStateChanged(int stateId, boolean value); }

        public void compare(int[] former, int[] later) {
            for (int id : mStateIds)
                if (contains(former, id) != contains(later, id))
                    mListener.onStateChanged(id, contains(later, id));
        }

        private boolean contains(int[] array, int element) {
            for (int e : array)
                if (e == element)
                    return true;
            return false;
        }
    }
}