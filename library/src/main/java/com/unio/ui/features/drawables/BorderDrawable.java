package com.unio.ui.features.drawables;

import android.content.res.Resources;
import android.graphics.drawable.GradientDrawable;
import com.unio.util.helper.Convert;

/**
 * BorderDrawable
 *
 * Border style drawable
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 19/03/2015 20:15
 */
public class BorderDrawable extends GradientDrawable
{
    /**
     * Constructor
     */
    public BorderDrawable() { this.setShape(GradientDrawable.RECTANGLE); }

    /**
     * Constructor
     * Set a custom model
     */
    public BorderDrawable(int shape) { this.setShape(shape); }

    /**
     * Set border color
     *
     * @param color color
     */
    public BorderDrawable setBorder(int color) { return this.setBorder(color, (int)(Resources.getSystem().getDisplayMetrics().density)); }

    /**
     * Set border color and width
     *
     * @param color color
     * @param width width
     */
    public BorderDrawable setBorder(int color, int width) {
        this.setStroke(width, color);
        return this;
    }

    /**
     * Set border radius
     *
     * @param radius radius in dp
     */
    public BorderDrawable setBorderRadius(int radius) {
        this.setCornerRadius((float)(Convert.dpToPx(radius)));
        return this;
    }

    /**
     * Set border for each side
     *
     * @param leftTop     left-top radius
     * @param rightTop    right-top radius
     * @param rightBottom right-bottom radius
     * @param leftBottom  left-bottom radius
     */
    public BorderDrawable setBorderRadius(int leftTop, int rightTop, int rightBottom, int leftBottom) {
        this.setCornerRadii(new float[] {
            leftTop,     leftTop,
            rightTop,    rightTop,
            rightBottom, rightBottom,
            leftBottom,  leftBottom,
        });
        return this;
    }
}