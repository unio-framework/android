package com.unio.ui.features.effects;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.Window;
import com.unio.debug.Trace;
import com.unio.util.helper.Screen;
import com.unio.util.helper.Check;
import com.unio.util.helper.Color;
import com.unio.util.manager.ActivityManager;
import com.unio.util.manager.ResourceManager;

/**
 * Blur
 *
 * Create blur images and filtered drawables
 * Based on https://github.com/PomepuyN/BlurEffectForAndroidDesign
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 17/02/2016 19:49
 */
public class Blur
{
    /** iOS 7+ default blur filter color */
    public static final int IOS_FILTER = Color.parseColor("#AEC0FE");

    /** Default blur radius */
    public static final int DEFAULT_RADIUS = 25;

    /**
     * Render blur image based in current screen image
     *
     * @return Bitmap
     */
    public static Bitmap render() { return render(DEFAULT_RADIUS); }

    /**
     * Render blur image based in current screen image
     *
     * @param radius radius
     *
     * @return Bitmap
     */
    public static Bitmap render(int radius) {
        Activity parent = ActivityManager.get();
        if (parent != null) {
            Window window = parent.getWindow();
            View v = window.getDecorView();

            Rect frame = new Rect();
            v.getWindowVisibleDisplayFrame(frame);
            int statusBarHeight = frame.top;

            int width = Screen.getWidth();
            int height = Screen.getHeight();

            v.setDrawingCacheEnabled(true);
            v.measure(View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(height - statusBarHeight, View.MeasureSpec.EXACTLY));
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());

            Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
            v.destroyDrawingCache();
            return render(b, radius);
        }
        return null;
    }

    /**
     * Render blur image from view
     *
     * @param v      view
     * @param radius radius
     *
     * @return Bitmap
     */
    public static Bitmap render(View v, int radius) {
        Trace.log(Blur.class, v);
        v.setDrawingCacheEnabled(true);

        // this is the important code :)
        // Without it the view will have a dimension of 0,0 and the bitmap will be null
        v.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        v.layout(0, 0, Screen.getWidth(), v.getMeasuredHeight());

        Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
        v.destroyDrawingCache();
        return render(b, radius);
    }

    /**
     * Render blur image
     *
     * @param sentBitmap image
     * @param radius     radius, between 1 and 25
     *
     * @return Bitmap
     */
    @SuppressLint("NewApi")
    public static Bitmap render(Bitmap sentBitmap, int radius) {
        // Stack Blur v1.0 from
        // http://www.quasimondo.com/StackBlurForCanvas/StackBlurDemo.html
        //
        // Java Author: Mario Klingemann <mario at quasimondo.com>
        // http://incubator.quasimondo.com
        // created Feburary 29, 2004
        // Android port : Yahel Bouaziz <yahel at kayenko.com>
        // http://www.kayenko.com
        // ported april 5th, 2012

        // This is a compromise between Gaussian Blur and Box blur
        // It creates much better looking blurs than Box Blur, but is
        // 7x faster than my Gaussian Blur implementation.
        //
        // I called it Stack Blur because this describes best how this
        // filter works internally: it creates a kind of moving stack
        // of colors whilst scanning through the image. Thereby it
        // just has to add one new block of color to the right side
        // of the stack and remove the leftmost color. The remaining
        // colors on the topmost layer of the stack are either added on
        // or reduced by one, depending on if they are on the right or
        // on the left side of the stack.
        //
        // If you are using this algorithm in your code please add
        // the following line:
        //
        // Stack Blur Algorithm by Mario Klingemann <mario@quasimondo.com>

        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);
        if (radius < 1) return null;

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Trace.log(Blur.class, "pix", w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) dv[i] = (i / divsum);

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {
                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) vmin[x] = Math.min(x + radius + 1, wm);
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) yp += w;
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) vmin[y] = Math.min(y + r1, hm) * w;
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Trace.log(Blur.class, "pix", w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);
        return bitmap;
    }

    /**
     * Render blurred image with configs
     *
     * @param configs configs
     *
     * @return BitmapDrawable
     */
    public static BitmapDrawable renderToDrawable(int[] configs) { return renderToDrawable(DEFAULT_RADIUS, configs); }

    /**
     * Render blurred image with configs
     *
     * @param radius  radius
     * @param configs configs
     *
     * @return BitmapDrawable
     */
    public static BitmapDrawable renderToDrawable(int radius, int[] configs) {
        Bitmap b = render(radius);
        return renderToDrawable(b, configs);
    }

    /**
     * Render blurred image with configs
     *
     * @param v       view
     * @param radius  radius
     * @param configs configs
     *
     * @return BitmapDrawable
     */
    public static BitmapDrawable renderToDrawable(View v, int radius, int[] configs) {
        Bitmap b = render(v, radius);
        return renderToDrawable(b, configs);
    }

    /**
     * Render blurred image with configs
     *
     * @param source  source bitmap
     * @param radius  radius
     * @param configs configs
     *
     * @return BitmapDrawable
     */
    public static BitmapDrawable renderToDrawable(Bitmap source, int radius, int[] configs) {
        Bitmap b = render(source, radius);
        return renderToDrawable(b, configs);
    }

    /**
     * Render blurred image with configs
     *
     * @param blurred blurred bitmap
     * @param configs configs
     *
     * @return BitmapDrawable
     */
    public static BitmapDrawable renderToDrawable(Bitmap blurred, int[] configs) {
        BitmapDrawable d = new BitmapDrawable(ResourceManager.get(), blurred);
        if (Check.isEmpty(configs) == false) {
            if (configs.length >= 1 && configs[0] != -1) d.setColorFilter(configs[0], PorterDuff.Mode.DST_ATOP);
            int alpha = configs.length >= 2 ? configs[1] : 100;
            d.setAlpha(alpha);
        }
        return d;
    }
}