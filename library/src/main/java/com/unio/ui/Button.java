package com.unio.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.unio.R;
import com.unio.base.URunnable;
import com.unio.debug.Trace;
import com.unio.ui.features.drawables.BorderDrawable;
import com.unio.ui.features.drawables.NoActionDrawable;
import com.unio.ui.features.drawables.RippleDrawable;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.util.helper.Views;
import com.unio.util.helper.Check;
import com.unio.util.helper.Color;
import com.unio.util.manager.ResourceManager;
import com.unio.util.helper.Layouts;
import com.unio.util.statics.constants.Time;

/**
 * Button
 *
 * Custom button with Ripple Effect
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Button extends RelativeLayout
{
    /** Button types */
    public static final int TYPE_NORMAL  = 1;
    public static final int TYPE_COUNTER = 2;
    /** Flag to remove counter */
    public static final int NONE = -999999999;

    /** Attributes */
    _Styleable s;
    private int _type;
    private AttributeSet _attrs;
    private int _defStyle;

    /** TextView */
    private TextView _View;

    /**
     * Constructor
     */
    public Button(Context context) { this(context, null); }

    /**
     * Constructor
     */
    public Button(Context context, AttributeSet attrs) { this(context, attrs, 0); }

    /**
     * Constructor
     */
    @SuppressLint("NewApi")
    public Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (this.isInEditMode()) ResourceManager.set(context);
        this.s = new _Styleable(context, attrs);
        this._attrs = attrs;
        this._defStyle = defStyle;

        RippleDrawable.attach(this);
        this.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this._View = new TextView(context, attrs, defStyle);

        // For counter option
        this._BaseContainer    = new FrameLayout(context);
        this._ButtonContainer  = new LinearLayout(context);
        this._CounterContainer = new LinearLayout(context);
        this._Subtitle = new TextView(context);
        this._Counter  = new TextView(context);

        // Custom
        int type              = TYPE_NORMAL;
        int counter           = NONE;
        int subtitleTextColor = Color.BLACK;
        int counterColor      = Color.WHITE;

        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.Button);
        type = typedArray.getInteger(R.styleable.Button_buttonType, type);

        String subtitleText = typedArray.getString(R.styleable.Button_buttonSubtitle);
        if (Check.isEmpty(subtitleText) == false) this._Subtitle.setText(subtitleText);

        subtitleTextColor = typedArray.getColor(R.styleable.Button_buttonSubtitleColor, subtitleTextColor);
        counter = typedArray.getInt(R.styleable.Button_buttonCounter, counter);
        counterColor = typedArray.getColor(R.styleable.Button_buttonCounterColor, counterColor);

        typedArray.recycle();

        this._Subtitle.setTextColor(subtitleTextColor);
        this._Counter.setTextColor(counterColor);

        this.setType(type);
        this.setCounter(counter);
    }

    /**
     * Shortcuts
     */
    public void setText(CharSequence text)        { this._View.setText(text);           }
    public void setTextSize(float size)           { this._View.setTextSize(size);       }
    public void setTextSize(int unit, float size) { this._View.setTextSize(unit, size); }
    public void setTextColor(int color)           { this._View.setTextColor(color);     }

    /**
     * Set type and configure
     *
     * @param type type
     */
    public void setType(int type) {
        if (this._type != type) {
            this.removeView(this._View);
            this.removeView(this._BaseContainer);
            this.removeView(this._ButtonContainer);
            this.removeView(this._CounterContainer);
            this.removeView(this._Subtitle);
            this.removeView(this._Counter);

            CharSequence text = this._View.getText();
            Typeface typeface = this._View.getTypeface();
            this._type = type;
            if (this._type == TYPE_COUNTER) {
                this._View = new TextView(this.getContext());
                this._counterButtonSettings();
                if (this.s.contains(android.R.attr.textColor))
                    this._View.setTextColor((ColorStateList) this.s.get(android.R.attr.textColor));
            } else {
                this._View = new TextView(this.getContext(), this._attrs, this._defStyle);
                this._normalButtonSettings();
            }
            if (Check.isEmpty(text) == false) {
                this._View.setTypeface(typeface);
                this._View.setText(text);
                this._updateButtonMeasure();
            }
        }
    }

    /**
     * Normal button settings
     */
    private void _normalButtonSettings() {
        LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(CENTER_HORIZONTAL, TRUE);
        this._View.setLayoutParams(params);
        Views.setBackground(this._View, NoActionDrawable.render());
        this._View.setPadding(Layouts.asDp(10), Layouts.asDp(10), Layouts.asDp(10), Layouts.asDp(10));
        this.addView(this._View);

        if (this.s.contains(android.R.attr.textSize) == false) this.setTextSize(14);
        if (this.s.contains(android.R.attr.textColor) == false) this.setTextColor(Color.GRAY);
        if (this.s.contains(android.R.attr.text)) this.setText(s.get(android.R.attr.text).toString());
    }

    /**
     * Counter button settings
     */
    private void _counterButtonSettings() {
        LinearLayout.LayoutParams linearParams;
        FrameLayout.LayoutParams frameParams;

        ////////////////////
        //** Containers **//
        ////////////////////
        this._BaseContainer.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        this.addView(this._BaseContainer);

        frameParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        frameParams.gravity = Gravity.CENTER_VERTICAL;
        this._ButtonContainer.setLayoutParams(frameParams);
        this._ButtonContainer.setOrientation(LinearLayout.VERTICAL);
        this._ButtonContainer.setMinimumHeight(Layouts.asDp(50));
        this._ButtonContainer.setMinimumWidth(Layouts.asDp(50));

        if (this.getBackground() == null) {
            BorderDrawable border = new BorderDrawable();
            border.setColor(Color.parseColor("#E0E0E0"));
            border.setBorder(Color.parseColor("#ACACAC"));
            border.setBorderRadius(3);
            Views.setBackground(this._ButtonContainer, border);
        } else {
            Views.setBackground(this._ButtonContainer, this.getBackground());
            Views.setBackground(this, new NoActionDrawable());
        }

        this._BaseContainer.addView(this._ButtonContainer);

        frameParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, Layouts.asDp(25));
        frameParams.gravity = Gravity.END;
        this._CounterContainer.setLayoutParams(frameParams);
        this._CounterContainer.setOrientation(LinearLayout.VERTICAL);
        this._CounterContainer.setMinimumWidth(Layouts.asDp(25));
        if (Check.isEmpty(this._Counter.getText().toString())) this._CounterContainer.setVisibility(GONE);

        GradientDrawable oval = new GradientDrawable();
        oval.setShape(GradientDrawable.OVAL);
        oval.setColor(Color.parseColor("#FF0000"));
        Views.setBackground(this._CounterContainer, oval);

        this._BaseContainer.addView(this._CounterContainer);

        ///////////////
        //** Texts **//
        ///////////////
        linearParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearParams.gravity = Gravity.CENTER;
        linearParams.weight = 1;
        this._View.setLayoutParams(linearParams);
        this._View.setTextSize(30);
        this._View.setTextColor(Color.BLACK);
        this._View.setGravity(Gravity.CENTER_VERTICAL);
        this._ButtonContainer.addView(this._View);

        linearParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearParams.gravity = Gravity.CENTER_HORIZONTAL;
        linearParams.setMargins(Layouts.asDp(3), 0, Layouts.asDp(3), Layouts.asDp(8));
        this._Subtitle.setLayoutParams(linearParams);
        this._Subtitle.setTextSize(11);
        if (Check.isEmpty(this._Subtitle.getText().toString())) this._Subtitle.setVisibility(GONE);
        this._ButtonContainer.addView(this._Subtitle);

        linearParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        linearParams.setMargins(Layouts.asDp(3), Layouts.asDp(3), Layouts.asDp(3), Layouts.asDp(3));
        this._Counter.setLayoutParams(linearParams);
        this._Counter.setGravity(Gravity.CENTER);
        this._Counter.setTypeface(this._Counter.getTypeface(), Typeface.BOLD);
        this._CounterContainer.addView(this._Counter);
        this._updateButtonMeasure();
    }



    /*************************************************************************************************************************/
    /***                                          Counter button attributes                                                ***/
    /*************************************************************************************************************************/

    /** Runnable to update counter */
    private URunnable _Routine;

    /** Views */
    private FrameLayout _BaseContainer;
    private LinearLayout _ButtonContainer;
    private LinearLayout _CounterContainer;
    private TextView _Subtitle;
    private TextView _Counter;

    /**
     * Set title, equal with {@link #setText(CharSequence)}
     *
     * @param title title
     */
    public void setTitle(String title) { this.setTitle(title, Color.BLACK); }

    /**
     * Set title
     *
     * @param title title
     * @param color color
     */
    public void setTitle(String title, int color) {
        this.setText(title);
        this._View.setTypeface(Typeface.DEFAULT);
        this.setTextColor(color);
        this._updateButtonMeasure();
    }

    /**
     * Set icon title
     *
     * @param ic IconCharacter
     */
    public void setTitle(IconCharacter ic) { this.setTitle(ic, Color.BLACK); }

    /**
     * Set icon title
     *
     * @param ic IconCharacter
     * @param color color
     */
    public void setTitle(IconCharacter ic, int color) {
        this.setText(String.valueOf(ic.icon()));
        this._View.setTypeface(ic.typeface());
        this.setTextColor(color);
        this._updateButtonMeasure();
    }

    /**
     * Define um subtítulo/descrição simples
     *
     * @param subtitle subtítulo
     */
    public void setSubtitle(String subtitle) {
        LinearLayout.LayoutParams linearParams;
        linearParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearParams.gravity = Gravity.CENTER;
        linearParams.weight = 1;
        if (Check.isEmpty(subtitle)) {
            linearParams.setMargins(0, 0, 0, 0);
            this._View.setLayoutParams(linearParams);
            this._Subtitle.setVisibility(GONE);
        } else {
            linearParams.setMargins(0, Layouts.asDp(4), 0, 0);
            this._View.setLayoutParams(linearParams);
            this._Subtitle.setVisibility(VISIBLE);
            this._Subtitle.setText(subtitle);
        }
        this._updateButtonMeasure();
    }

    /**
     * Set alpha
     *
     * @param alpha value
     */
    @Override
    public void setAlpha(float alpha) { this._ButtonContainer.setAlpha(alpha); }

    /**
     * Update measure to mantain equal
     */
    @SuppressWarnings("SuspiciousNameCombination")
    private void _updateButtonMeasure() {
        this._ButtonContainer.measure(0,0);
        int height = this._ButtonContainer.getMeasuredHeight();
        int width  = this._ButtonContainer.getMeasuredWidth();
        if (height != width) {
            if (height > width) {
                this._ButtonContainer.setMinimumWidth(height);
            } else {
                this._ButtonContainer.setMinimumHeight(width);
            }
        }
    }

    /**
     * Set static counter
     *
     * @param total total
     */
    public void setCounter(int total) {
        FrameLayout.LayoutParams frameParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (total == NONE) {
            if (this._CounterContainer.getVisibility() != GONE) this._CounterContainer.setVisibility(GONE);
            frameParams.gravity = Gravity.CENTER_VERTICAL;
            this._BaseContainer.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        } else {
            if (this._CounterContainer.getVisibility() != VISIBLE) this._CounterContainer.setVisibility(VISIBLE);
            this._Counter.setText(String.valueOf(total));

            frameParams.gravity = Gravity.BOTTOM;
            this._ButtonContainer.measure(0, 0);
            this._CounterContainer.measure(0, 0);
            int width = this._ButtonContainer.getMeasuredWidth()+(this._CounterContainer.getMeasuredWidth()/2);
            int height = this._ButtonContainer.getMeasuredHeight()+(this._CounterContainer.getMeasuredHeight()/2);
            this._BaseContainer.setLayoutParams(new LayoutParams(width, height));
        }
        this._ButtonContainer.setLayoutParams(frameParams);
    }

    /**
     * Set dinamic counter, with upadate option
     *
     * @param callback {@link Button.ICallback}
     */
    public void setCounter(final ICallback callback) {
        if (callback != null) {
            this._Routine = new URunnable() {
                @Override
                protected void settings() { this.interval = (callback != null && callback.interval() > 0) ? callback.interval() : 6 * Time.Milisecond.MINUTE; }

                @Override
                protected void onInit() {
                    Trace.log(this, "Start counter check routine");
                    int total = callback.run(Button.this);
                    if (total <= 0) total = NONE; // If total is 0, remove counter
                    setCounter(total);
                }
            };
            this._Routine.run();
        }
    }

    /**
     * Stop counter update
     */
    public void stopCounterUpdate() { if (this._Routine != null) this._Routine.stop(); }

    /**
     * ICallback
     *
     * Interface to process updatable counter
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 20/07/2015 20:13
     */
    public static abstract class ICallback
    {
        /**
         * Custom interval
         *
         * @return int, if "< 0", use as default 6 minutes
         */
        public int interval() { return 0; }

        /**
         * Run
         *
         * @param button button
         *
         * @return int counter total
         */
        public abstract int run(Button button);
    }
}