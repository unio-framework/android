package com.unio.core;

import android.content.Context;
import android.os.Bundle;
import com.unio.debug.EDebug;
import com.unio.util.helper.Check;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Config
 *
 * Framework settings
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
class Config
{
    /**
     * Define debug type
     *
     * @param metaData metadata
     *
     * @return EDebug
     */
    public static EDebug getDebugMode(Bundle metaData) {
        int debugMode = metaData == null ? EDebug.DEFAULT.ordinal() : metaData.getInt(Unio.META_DEBUG);
        switch (debugMode) {
            case 1: return EDebug.PRODUCTION;
            case 2: return EDebug.DEFAULT;
            default: return EDebug.DEVELOPMENT;
        }
    }

    /**
     * Format databases
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    public static Map<String, Integer> getDatabases(Context context, Bundle metaData) {
        Map<String, Integer> databases = new LinkedHashMap<>();
        databases.put(Unio.DATABASE_NAME, 3); // Framework private database

        int res = metaData.getInt(Unio.META_DB);
        if (res != 0) {
            String[] dbs = context.getResources().getStringArray(res);
            for (int i = 0; i < dbs.length; i++) {
                String db = dbs[i];
                if (Check.isEmpty(db)) continue;

                String[] details = db.split(":");
                String name = details[0];
                int version = details.length == 1 ? 1 : Integer.valueOf(details[1]);

                databases.put(name, version);
            }
        }

        return databases;
    }

    /**
     * Find trial period
     */
    public static int getTrialPeriod(Context context, Bundle metaData) { return metaData.getInt(Unio.META_TRIAL); }

    /**
     * Find url
     */
    public static String getUrl(String name, Bundle metaData) {
        if (metaData == null) return null;
        return metaData.getString(name);
    }
}