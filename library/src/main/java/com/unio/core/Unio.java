package com.unio.core;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.app.Application;
import com.unio.app.UnioVersion;
import com.unio.base.UModule;
import com.unio.database.preference.StorageItem;
import com.unio.database.sql.connection.Db;
import com.unio.debug.EDebug;
import com.unio.debug.Trace;
import com.unio.modules.Database;
import com.unio.modules.Trial;
import com.unio.modules.Version;
import com.unio.database.preference.Storage;
import com.unio.modules.VersionControl;
import com.unio.util.helper.Check;
import com.unio.util.helper.Methods;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Unio
 *
 * Framework core class
 *
 * The Framework will start automatically
 * Use these options to configure:
 *
 * - com.unio.DEBUG: Define framework log level
 * <meta-data android:name="com.unio.DEBUG" android:value="[int]" />
 *
 * - com.unio.TRIAL: App trial period use, in days
 * <meta-data android:name="com.unio.TRIAL" android:value="[int]" />
 *
 * - com.unio.DB: Array with database name and version, using pattern "name:version"
 *   If version is omitted, use as default version 1
 * <meta-data android:name="com.unio.DB" android:resource="[string-array]" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class Unio
{
    /** Framework current version */
    public static final String VERSION = History.CURRENT;

    /** Storage identifications */
    public  static final String CACHE    = "CACHE";
    public  static final String SESSION  = "SESSION";
    private static final String SYSTEM   = "SYSTEM";

    public static final String META_DEBUG = "com.unio.DEBUG";
    public static final String META_DB = "com.unio.DB";
    public static final String META_TRIAL = "com.unio.TRIAL";
    public static final String META_URL_DEBUG = "com.unio.URL_DEBUG";
    public static final String META_VERSION = "com.unio.VERSION";

    /** Flags */
    private static boolean _enabled = false;
    private static String _key;
    private static EDebug _debugMode;
    private static boolean _validVersion = false;

    /** Application context */
    private static Application _Application;

    /** LocalStorage instance */
    private static Storage _Cache;   /** to cache (Keep after restart)      */
    private static Storage _Session; /** to session (Clean on each restart) */
    private static Storage _System;  /** to Unio internal data              */

    /** Module cache */
    private static Map<Class<? extends UModule>, UModule> _modules = new LinkedHashMap<>();

    /** UI Handler */
    public static final Handler ui = new Handler();

    static final String DATABASE_NAME = "__unio";

    /**
     * Start framework
     */
    public static void start(Application application) throws PackageManager.NameNotFoundException {
        if (enabled()) return;

        _Application = application;
        Bundle metaData = metaData();

        // Set debug mode
        _debugMode = Config.getDebugMode(metaData);

        // If exists DB, add
        Map<String, Integer> dbs = Config.getDatabases(app(), metaData);
        for (Map.Entry<String, Integer> entry : dbs.entrySet())
            module(Database.class, entry.getKey(), entry.getValue());

        // Local storages
        _Session = new Storage(SESSION);
        _Cache   = new Storage(CACHE);
        _System  = new Storage(SYSTEM);

        // Trial
        int trialPeriod = Config.getTrialPeriod(app(), metaData);
        if (trialPeriod > 0) Unio.module(Trial.class, trialPeriod);

        // Version
        String versionClass = _getAppVersionClass(metaData);
        if (versionClass != null) {
            Class appVersion = Methods.loadClass(versionClass);
            Class parentClass = appVersion == null ? null : appVersion.getSuperclass();
            if (Check.isEqual(parentClass, VersionControl.class)) {
                _validVersion = module(Version.class).addApp(appVersion);
            } else {
                Trace.log(Unio.class, "Version class "+versionClass+" is invalid");
            }
        }

        _enabled = true;
        Trace.log(EDebug.ALL, Unio.class, "Unio Start", "success", "Version: "+VERSION);
        startDebugger(metaData);
    }

    /** Start UnioDebug service */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    private static void startDebugger(Bundle metaData) {
        if (debugMode() == EDebug.DEVELOPMENT) {
            Trace.log(Unio.class, "############### Debug mode: "+debugMode());

            Db[] dbs = dbs();
            for (int i = 0; i < dbs.length; i++) {
                Db db = dbs[i];
                if (Check.isEqual(db.getName(), DATABASE_NAME)) continue;
                Trace.log(Unio.class, "############### database "+dbs[i].getName()+": "+dbs[i]);
            }

            Trace.log(Unio.class, "############### Session: "+session());
            Trace.log(Unio.class, "############### Cache: "+cache());
            Trace.log(Unio.class, "############### Trial period: "+Config.getTrialPeriod(app(), metaData));

            String versionClass = _getAppVersionClass(metaData);
            if (_validVersion) {
                Class appVersion = Methods.loadClass(versionClass);
                String version = module(Version.class).getVersion(appVersion, true);
                Trace.log(Unio.class, "############### App version: "+version+" ("+versionClass+")");
            } else if (versionClass != null) {
                Trace.log(Unio.class, "############### App version: "+versionClass+" IS INVALID");
            }
        }
        module(Version.class).addLibrary(UnioVersion.class);
    }

    /***************************************************************/
    /**                   Métodos de uso geral                    **/
    /***************************************************************/

    /**
     * Check if Unio is enabled
     *
     * @return boolean
     */
    public static boolean enabled() { return _enabled; }

    /**
     * Unio serial key
     *
     * @return String
     */
    public static String key() { return _key; }

    /**
     * Current debug mode
     *
     * @return EDebug
     */
    public static EDebug debugMode() { return _debugMode; }

    /**
     * Get {@link Application} instance
     *
     * @return Application
     */
    public static Application app() { return _Application; }

    /**
     * Get cache instance
     *
     * @return Storage
     */
    public static Storage cache() { return _Cache; }

    /**
     * Get cache item
     *
     * @param sharedName storage key
     *
     * @return StorageItem
     */
    public static StorageItem cache(String sharedName) { return _Cache.search(sharedName); }

    /**
     * Get session instance
     *
     * @return Storage
     */
    public static Storage session() { return _Session; }

    /**
     * Get session item
     *
     * @param sharedName storage key
     *
     * @return StorageItem
     */
    public static StorageItem session(String sharedName) { return _Session.search(sharedName); }

    /**
     * Get system item
     *
     * @param sharedName storage key
     *
     * @return StorageItem
     */
    public static StorageItem system(String sharedName) { return _System.search(sharedName); }

    /**
     * Get default database instance
     *
     * @return Db
     */
    public static synchronized Db db() { return db(null); }

    /**
     * Get a database instance from name
     *
     * @param name database name
     *
     * @return Db
     */
    public static synchronized Db db(String name) { return module(Database.class).get(name); }

    /**
     * Get system private database
     *
     * @return Db
     */
    public static synchronized Db systemDb() { return db(DATABASE_NAME); }

    /**
     * Get all databases
     *
     * @return Db[]
     */
    public static Db[] dbs() { return module(Database.class).all(); }

    /**
     * Get metadata
     *
     * @return Bundle
     * @throws PackageManager.NameNotFoundException when package not found
     */
    public static Bundle metaData() throws PackageManager.NameNotFoundException {
        ApplicationInfo info = app().getPackageManager()
            .getApplicationInfo(app().getPackageName(), PackageManager.GET_META_DATA);
        return info.metaData;
    }

    /**
     * Get Trial module
     *
     * @return Trial
     */
    public static Trial trial() { return module(Trial.class); }

    /**
     * Get UI Handler
     *
     * @return Handler
     */
    public static Handler ui() { return ui; }

    /**
     * Get framework internal url
     *
     * @param name url meta name
     *
     * @return String
     */
    public static String url(String name) {
        try {
            ApplicationInfo info = app().getPackageManager()
                .getApplicationInfo(app().getPackageName(), PackageManager.GET_META_DATA);
            Bundle metaData = info.metaData;
            return Config.getUrl(name, metaData);
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    /**
     * Get version module
     *
     * @return UMVersion
     */
    public static Version version() { return Unio.module(Version.class); }

    /**
     * Check if module has called
     * If it has called from {@link #module(Class)} method, it will appear here
     * Note that it's different with {@link UModule#isRunning()}. Will exist cases when module has called,
     *   but some validations has declined, and not started
     *
     * @param moduleClass module class
     *
     * @return boolean
     */
    public static boolean calledModule(Class moduleClass) { return _modules.containsKey(moduleClass); }

    /**
     * Call a module
     * If module is called as first time, instantiate it
     * If module is called after first time, return instance.
     *   If {@link UModule#reloadWhenCalled} is true, call onInit method again before
     *
     * @param moduleClass classe do módulo
     *
     * @return <T extends UModule<T>>T
     */
    public static <T extends UModule>T module(Class<T> moduleClass) { return module(moduleClass, new Object[0]); }

    /**
     * Call a module
     * If module is called as first time, instantiate it
     * If module is called after first time, return instance.
     *   If {@link UModule#reloadWhenCalled} is true, call onInit method again before
     *
     * @return <T extends UModule<T>>T
     */
    public static <T extends UModule>T module(Class<T> moduleClass, Object... params) {
        if (_modules.containsKey(moduleClass) == false) _modules.put(moduleClass, null);

        UModule m = _modules.get(moduleClass);
        boolean reloadWhenCalled = m != null && m.reloadWhenCalled();
        if (m == null || reloadWhenCalled) {
            try {
                T module = m == null ? moduleClass.newInstance() : (T) m;
                _callInModule(module, "onInit", params);
                _modules.put(moduleClass, module);
                return module;
            } catch (InstantiationException e) {
                Trace.get(e);
                throw new IllegalStateException("Module "+moduleClass.getName()+" is invalid");
            } catch (IllegalAccessException e) {
                Trace.get(e);
                throw new IllegalStateException("Module "+moduleClass.getName()+" is invalid");
            }
        } else {
            _callInModule(m, "onCalled", params);
            return (T)m;
        }
    }

    /**
     * Call method of module
     *
     * @param module module
     * @param method method
     * @param params params
     */
    private static void _callInModule(UModule module, String method, Object[] params) {
        if (Check.isEmpty(method) == false) {
            if (params.length == 0 || Methods.exists(module, "onInit", params) == false) {
                switch (method) {
                    case "onInit":
                        module.onInit();
                        break;
                    case "onCalled":
                        module.onCalled();
                        break;
                }
            } else {
                Methods.call(module, "onInit", params);
            }
        }
    }

    /**
     * Find the app version file from metadata
     * If uses "." alias, use the current package
     * 
     * @param metaData Bundle
     *                 
     * @return String
     */
    private static String _getAppVersionClass(Bundle metaData) {
        String path = metaData.getString(Unio.META_VERSION);
        if (path == null) return null;
        
        if (path.startsWith(".")) {
            String packageName = app().getPackageName();
            if (Check.isEmpty(packageName)) return null;
            path = packageName + path;
        }
        return path;
    }
}