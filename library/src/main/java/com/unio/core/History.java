package com.unio.core;

import com.unio.app.ScannerActivity;
import com.unio.database.sql.orm.ModelFind;
import com.unio.database.sql.orm.Record;
import com.unio.database.preference.Storage;
import com.unio.modules.*;
import com.unio.ui.splash.SplashLoginActivity;
import com.unio.ui.*;
import com.unio.util.control.Accessibility;
import com.unio.util.control.AppInfo;
import com.unio.util.control.Date;
import com.unio.util.control.Network;
import com.unio.util.helper.*;
import com.unio.util.io.*;
import com.unio.util.manager.ActivityManager;
import com.unio.util.manager.ResourceManager;
import com.unio.util.rest.Http;

/**
 * History
 *
 * Unio version history
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
final class History
{
    /**
     * @version 1.0.0
     *
     * Versão inicial
     * Estrutura baseada no protótipo Yark, versão 4.4.0
     *
     * Destaque:
     * - Compatibilidade mínima API 5 (Android 2.0)
     * - Efeito próprio do ProgressBar e Ripple do Android Lollipop, não ficando dependente da API nativa
     * - Uso padrão do {@link android.support.v7.widget.RecyclerView} em substituição da
     *   {@link android.widget.ListView} e {@link android.widget.GridView}
     *
     * @release 07/05/2015
     */
    public static final String V100 = "1.0.0";

    /**
     * @version 1.0.1
     *
     * Novidades:
     * - Adicionado opção de setar um background para a com.unio.layout.UDDockView pelo xml
     * - Aprimoramento do {@link android.support.v7.widget.LinearLayoutManager} e {@link android.support.v7.widget.GridLayoutManager}
     * - Opção de alterar o {@link android.support.v7.widget.RecyclerView.LayoutManager} por um customizado no
     *   com.unio.layout.collection1.RecyclerBuilder
     *
     * @release 12/05/2015
     */
    public static final String V101 = "1.0.1";

    /**
     * @version 1.0.2
     *
     * Novidade:
     * - Adicionado forma de conseguir o nome da tabela pelo {@link com.unio.database.sql.orm.Model}
     *
     * @release 13/05/2015
     */
    public static final String V102 = "1.0.2";

    /**
     * @version 1.1.0
     *
     * Novidades:
     * - Adicionado método Format#getTime() para retornar o horário atual
     * - Criado cache para {@link android.graphics.Typeface}
     * - Ignora o {@link android.support.v4.app.FragmentActivity#onBackPressed()} no
     *   com.unio.ui.splash.SplashActivity, para não tirar os Fragments
     *
     * - Atualizado a biblioteca android-support para o atual rev 22.1.1
     * -- Troca do ActionBarActivity (deprecated) pelo {@link android.support.v7.app.AppCompatActivity}
     * -- Troca de RecyclerView#setOnScrollListener (deprecated) pelo
     *    {@link android.support.v7.widget.RecyclerView#addOnScrollListener(android.support.v7.widget.RecyclerView.OnScrollListener)}
     *
     * - Ajustes para Recycler Adapter/Builder
     * -- Ajustes no com.unio.layout.RippleView
     * -- Ajustado cor dos itens por causa da mudança de cor no toque em temas não Lollipop
     * -- Corrigido bug do com.unio.layout.collection.Adapter
     * -- Resolvendo problema no {@link android.support.v7.widget.RecyclerView.ItemDecoration}, que não deixa sobrescrever de forma simples
     *
     * - Introduzido funcionalidade com.unio.modules.UMRoute para controle de tarefas
     * -- Removido USubModule, deverá usar o com.unio.modules.UMRoute no lugar
     * -- Ajustes na busca de métodos em {@link Methods}
     *
     * - Melhorias no Webservice
     * -- Adicionado tratamento para com.unio.util.tag.UJSONObject e com.unio.util.tag.UJSONArray
     *    quando retornar string null, retornando vazio
     * -- Compatibilizado conversão de Map/List para Json, o uso do {@link org.json.JSONObject}
     *    para conversão é funcional a partir do {@link com.unio.shortcuts.Android#KITKAT}
     * -- Corrigido bug do {@link Http} de não exibir o texto no android.app.ProgressDialog
     *
     * - Melhorias nos módulos
     * -- Criado {@link Auth} como uma solução unificada do UMLogin e Auth, ambos deprecated até a versão 1.2
     * -- Melhorado uso do {@link Location}
     * -- Melhorado uso do {@link Phone}
     *
     * - Classes/métodos deprecated
     * com.unio.layout.recycler.decorations.DividerItemDecoration
     * com.unio.ui.splash.login.UMLogin
     * com.unio.modules.builder.Auth
     * Unio#auth()
     * UCConfig#setAuth(Class)
     * UCConfig#getAuth()
     *
     * @release 05/06/2015
     */
    public static final String V110 = "1.1.0";

    /**
     * @version 1.1.1
     *
     * Novidades:
     * - Adicionado item com.unio.layout.collection1.items.ItemIconComplete como item do
     *   {@link android.support.v7.widget.RecyclerView}
     * - Adicionado o método isFromNotification() em UController e
     *   com.unio.layout.menu.SideMenuActivityCompat para diferenciar o modo de chamar a Activity
     * - Adicionado modo de divisão com.unio.layout.collection1.RecyclerBuilder.EDecoration#NOTIFICATION
     *   para o {@link android.support.v7.widget.RecyclerView}
     * - Implementado as flags da {@link ActivityManager} em com.unio.layout.menu.SideMenuActivityCompat
     * - Melhoria no gerenciamento das notificações
     * - Melhorias em {@link ActivityManager}, adicioando Flags que identifiquem se deverá tentar criar a
     *   Activity mantendo as informações anteriores ou não
     * - Removido Trace.get() do com.unio.util.tag.UJSONObject e com.unio.util.tag.UJSONArray
     * - Renomeado UItemIconText para com.unio.layout.collection1.items.ItemIconSimple seguindo o padrão
     *
     * @release 12/06/2015
     */
    public static final String V111 = "1.1.1";

    /**
     * @version 1.2.0
     *
     * Novidades:
     * - Adaptação do layout para tablet
     * -- com.unio.layout.collection.Adapter, exceto com.unio.layout.collection1.items.ItemCustom
     * -- com.unio.ui.splash.SplashActivity
     *
     * - Início da unificação de classes com o "Unio for iOS" 1.0
     * -- @equal-[plataforma]      para caso a classe/método seja idêntico
     * -- @equivalent-[plataforma] para caso a classe/método tenha nome diferente por padrões da plataforma/linguagem
     *
     * - Melhorias em com.unio.ui.splash.SplashActivity e com.unio.ui.splash.SplashLoginActivity
     * -- Compatibilidade de função para API7+
     * -- Login offline
     * -- Opção de visualizar a senha
     *
     * - Melhoria em {@link com.unio.util}
     * -- Movido com.unio.util.helper.ParamHelper para helper e {@link Storage}
     *    para builder, eliminando storage
     * -- Adicionado com.unio.util.helper.HDate para centralizar os tratamentos de data
     * -- Removido Format#addSlash, por desuso
     * -- Movido com.unio.util.helper.HConvert#charSplit para com.unio.util.helper.HConvert por fazer mais sentido
     *
     * - Melhorias em com.unio.util.builder.AlertBuilder
     * -- Adicionado opção de incluir um layout customizado, seguindo o modelo proposto
     * -- Adicionado estilo Material Design para dispositivos anteriores ao Android Lollipop
     *
     * - Adicionado inicialização por parâmetro em módulos, obrigatório {@link com.unio.base.UModule#reloadWhenCalled()}
     *   ser true, caso altere parâmetros em no aplicativo
     * - Adicionado busca de endereço pela localização em {@link Location}
     * - Adicionado opção de inserir ícone de fonte no ActionMenu do com.unio.layout.menu.SideMenuActivityCompat
     * - Atualizado a biblioteca android-support para o atual rev 22.2
     * - Criado módulo com.unio.modules.UMVersion, ainda para decidir qual o melhor
     * - Criado {@link Button} para botões em Material Design com efeito Ripple
     * - Melhoria no {@link com.unio.debug.Trace#send(Throwable)}, adicionando mais campos para conseguir
     *   mais detalhes do app e do dispositivo
     * - Removido DividerItemDecoration, UMLogin, Auth e métodos referentes, já considerado deprecated na versão 1.1.0
     *
     * @release 15/07/2015
     */
    public static final String V120 = "1.2.0";

    /**
     * @version 1.2.1
     *
     * Novidades:
     * - Adicionado com.unio.layout.UDCounterButton, botão com contador
     * - Adicionado ícones do Font Awesome 4.3, que ainda não havia sido adicioando.
     * - Renomeado EExecution.ALL_ON_UI para {@link com.unio.util.statics.interfaces.IAsync.EExecution#SYNCHRONOUS}, faz mais sentido
     * - Renomeado Views próprios do Unio com inicial UD (Unio Design)
     *
     * - Classes/métodos deprecated
     * com.unio.layout.UGridView
     *
     * @release 21/07/2015
     */
    public static final String V121 = "1.2.1";

    /**
     * @version 1.3.0
     *
     * Novidades:
     * - Alterações na base
     * -- Alterado UController e {@link com.unio.base.UFragment}, retornando Object em #getLayout()
     * -- Ajustado Activity e Fragment de com.unio.ui.splash conforme alterações da base
     * -- Ajustado com.unio.layout.tab conforme alterações da base
     * -- Ajustado com.unio.layout.material conforme alterações da base
     *
     * - Alterações em com.unio.ui.splash.SplashLoginActivity
     * -- Adicionado tratamento para ícone de mostrar senha, dependendo de ter ou não conteúdo
     * -- Ao exibir ou esconder o conteúdo da senha, deixa o cursor no final, anteriormente ficava no começo
     *
     * - Adicionado ações de copiar, excluir e mover arquivos em {@link File}, além de ajustes em outros métodos
     * - Adicionado método #save em {@link Image}, para tratamento específico em imagens
     * - Adicionado {@link com.unio.base.UWidget} como um meio de padronizar o desenvolvimento de widgets usando o Framework
     * - Adicionado {@link DrawView} para captura de desenhos em tela
     * - Adicionado {@link com.unio.shortcuts.Manage#get()} para facilitar a checagem por um {@link android.content.Context} válido
     * - Adicionado com.unio.util.helper.UrlHelper para trabalhar com a interceptação de urls inseridos em browsers
     * - Corrigido {@link Http} requisições autenticadas tem um bug em versões antigas (testado com API10)
     * - Melhoria em {@link Location}, permitido consultar o endereço via web usando o GoogleAPI, entre outras correções
     * - Melhoria em {@link AppInfo}, alterado de método para atributo
     * - Melhoria em com.unio.util.tag.UJSONArray, permitido consultar usando o tipo primitivo int
     * - Módulos agora não precisam mais ser declarados em Application, caso ele não esteja na lista,
     *   é adicionada automáticamente, mas mantém a Exception caso a classe não exista
     *
     * @release 30/07/2015
     */
    public static final String V130 = "1.3.0";

    /**
     * @version 1.3.1
     *
     * Novidades:
     * - Melhorias em com.unio.layout.collection1.RecyclerBuilder com.unio.layout.collection1.Adapter
     * -- Adicionado função de mover os itens da lista, ver com.unio.layout.collection1.MoveAdapter
     * -- Adicionado função de expandir os itens da lista, ver com.unio.layout.collection1.ExpandAdapter
     * -- Renomeado conforme novo padrão
     *
     * - Ajuste de configurações (com.unio.ui.splash.login.SplashLoginSecondaryFragment e {@link Location})
     * - Ajuste em {@link Device} retornando -1 em #getConnectionType() caso não esteja online
     * - Ajuste em com.unio.util.builder.AlertBuilder com material design para versões inferiores ao Lollipop
     * - Ajuste em {@link Http} para upload de arquivos, adicionado métodos #getParam e #hasParam
     * - Resolvido problema de duplicidade de _id no model
     * - Tirado UGridView do deprecated, e renomeado para com.unio.layout.UDGridView
     *
     * @release 10/08/2015
     */
    public static final String V131 = "1.3.1";

    /**
     * @version 1.3.2
     *
     * Novidades:
     * - Adicionado Location.CNLocation para trabalhar com os dados do GPS
     *   separadamente, métodos anteriores como deprecated
     * - Adicionado com.unio.core.Unio#versionCheck() para automatizar o versionamento, obrigatório adicionar
     *   em com.unio.core.UCApp#settings()
     * - Alterado {@link com.unio.database.sql.builder.QueryBuilder} para seguir o padrão da versão para iOS
     * - Atualizado a biblioteca android-support para o atual rev 22.2.1
     * - Movido {@link com.unio.shortcuts.Android} para shortcuts
     * - Removido com.unio.modules.UMRoute#set com parâmetro em String
     * - Removido gestos para desenvolvedor em debug
     * - Removido CNNetwork de {@link Location}
     * - Renomeado todas as classes do helper
     *
     * @release 15/08/2015
     */
    public static final String V132 = "1.3.2";

    /**
     * @version 1.3.3
     *
     * Novidades:
     * - Correção de emergência para {@link com.unio.debug.Trace#send(Throwable)}
     *
     * @release 15/08/2015
     */
    public static final String V133 = "1.3.3";

    /**
     * @version 1.4.0
     *
     * Novidades:
     * - Adicionado com.unio.layout.UDCalendarView, antigo YCalendarView do Yark Plus
     * - Alteração em com.unio.layout.UDDockView, habilitado para passar uma String no lugar do ícone
     * - Alteração em com.unio.util.builder.VersionControl, renomeado onCheckUpdate para
     *   com.unio.util.builder.VersionControl#onSameVersion, faz mais sentido, e tirado abstract
     * - Avanço na unificação com o Unio for iOS
     * - Correções em com.unio.layout.collection.Adapter, renderizando o Front, para caso o
     *   layout customizado seja menor que o tamanho mínimo
     * - Correção de pequenos bugs em geral
     * - Corrigido bug no DB, para evitar a checagem do ErrorLog quando não tiver adicionado banco de dados
     * - Renomeado RVAdapter para com.unio.layout.collection.Adapter para manter uma maior unificação entre as versões
     * - Removido método deprecated, com excessão de ErrorLog, a ser removido na versão 1.5.0
     *
     * @release 28/08/2015
     */
    public static final String V140 = "1.4.0";

    /**
     * @version 1.4.1
     *
     * Novidades:
     * - Unificação com Unio for iOS 1.1.0
     * - Correção de pequenos bugs
     * - Ajustes em {@link File} e {@link Image}, seguindo o novo formato da versão para iOS
     * - Ajustes em {@link Http}, seguindo o novo formato da versão para iOS
     *
     * @release 03/09/2015
     */
    public static final String V141 = "1.4.1";

    /**
     * @version 1.5.0
     *
     * Novidades:
     * - Compatibilidade com Lib/Core:
     *   -- Compatível com o SDK do Android 6.0 Marshmallow (API 23)
     *   -- Compatível com as bibliotecas de suporte v4, v7-appcompat e v7-recyclerview versão 23.0.1
     *   -- Elevado compatibilidade mínimo para API 8, para evitar tratar alguns métodos básicos, além de,
     *      segundo a Google, é a versão mínima em uso atualmente
     *
     * - Melhorias em {@link com.unio.database.sql.orm.Model}:
     *   -- Adicionado opção de retornar os valores do {@link com.unio.database.sql.orm.Model} em JSON
     *   -- Implementado {@link Record}
     *   -- Alterado {@link ModelFind#findAll} para trabalhar com {@link Record}
     *
     * - Melhorias em {@link Storage}:
     *   -- Adicionado opção de retornar os valores do {@link Storage} em JSON
     *   -- Adicionado opção de armazenar e retornar Map, List e Array em Storage.Item
     *
     * - Melhorias no layout:
     *   -- Adicionado com.unio.layout.dialog.SnackBar, seguindo o mesmo padrão da classe introduzida no Lollipop
     *   -- Adicionado com.unio.layout.UDTextInputView, baseado no TextInputLayout, do android.support.design
     *   -- Adicionado com.unio.layout.dialog.MaterialProgressDialog, para usar o mesmo estilo nas versões antigas
     *   -- Adicionado PatternLockView, como opção de senha
     *   -- Alteração no MaterialActivity, agora chamando com.unio.layout.menu.SideMenuActivity,
     *      com uma versão nativa e outra para suportar versões inferiores a API 11
     *   -- Renomeado Adapter.Type para com.unio.layout.collection1.Adapter.EType para manter compatibilidade com Swift
     *   -- Adicionado {@link com.unio.ui.features.fonts.MaterialIcon}, ícones oficiais do Material Design,
     *      alternativa ao {@link com.unio.ui.features.fonts.FontAwesome}*
     *   -- Adicionado ícones do FontAwesome 4.4.0
     *   -- Adicionado novos estilos de decoration para com.unio.layout.collection1.RecyclerBuilder
     *   -- Adicionado com.unio.layout.UDFloatingActionButton e com.unio.layout.UDFloatingActionsMenu
     *      como uma opção ao com.unio.layout.UDDockView
     *   -- Adicionado ícone do aplicativo na ToolBar, em com.unio.layout.menu.SideMenuActivityCompat, caso exista
     *
     * - Melhorias no core:
     *   -- Criado o padrão dos serviços oferecidos pelo framework
     *   -- Url e filtros para a execução unificada e customizada
     *   -- Adicionado funcionalidade de trial
     *   -- Alterações em Config, trocado métodos por atributos
     *
     * - Melhorias em debug:
     *   -- Adicionado de volta o Developer Mode, como uma forma de adicionar funcionalidades para debug
     *      no aplicativo, e liberar acesso somente em determinadas condições
     *   -- Para habilitar a ativação do Developer Mode, obrigatório determinar pela flag
     *      Config#developerModeOptionEnabled, por padrão desabilitado
     *   -- Adicionado {@link com.unio.debug.EDebug#SEND} para envio do conteúdo do log via WebService,
     *      somente para {@link com.unio.debug.EDebug#PRODUCTION}
     *
     * - Adicionado {@link Encrypt}, para auxiliar na criptografia
     * - Adicionado serviço de sincronização dos dados do aplicativo, com com.unio.modules.UMSync
     * - Adicionado opção de definir encode diferente de UTF-8 em {@link Http},
     *   usar {@link Http#setEncode}
     * - Adicionado opção de obter a data de instalação do aplicativo e a data da última atualização em {@link AppInfo}
     * - Adicionado com.unio.util.helper.HFile para simplificar algumas checagens de arquivo/diretório
     * - Melhoria em {@link ActivityManager}, alterando a forma de trabalho para reaproveitamento de Activities
     * - Removido conteúdos anteriormente deprecated
     * - Renomeado classes do util.compatibility com sufixo Compat
     * - Adicionado opção de enviar mensagem adicional no Trace, para usar como por exemplo a
     *   resposta do servidor em caso de recusa de conexão
     * - Adicionado atalho para com.unio.layout.dialog.SnackBar em {@link Alert}
     *
     * - Classes/métodos deprecated, remover na próxima versão:
     * FontHelper (usar as classes de Fonte com.unio.ui.features.fonts.FontAwesome#typeface e
     *   com.unio.ui.features.fonts.MaterialIcon#typeface)
     *
     * @release 16/10/2015
     */
    public static final String V150 = "1.5.0";

    /**
     * @version 1.6.0
     *
     * - Upgrades
     *   -- Atualizado com android-support versão 23.1.1
     *   -- Atualizado FontAwesome versão 4.5.0
     *   -- Atualizado MaterialIcons versão 2.1.0
     *   -- Removido as fontes da lib, elas deverão ser adicionadas separadamente em /assets/fonts
     *   -- Uso do Java 1.8 para a compilação do .jar
     *   -- Adicionado Google Play Services, versão 29, para o {@link GPlayLocation}
     *
     * - Melhorias em layout
     *   -- Adicionado opção de adicionar Decoration customizado
     *   -- Adicionado opção de adicionar ou não separador no último item
     *   -- Animação de exibição dos Fragments
     *   -- Opção de configurar o botão de voltar do ActionBar
     *   -- Adicionado hook em com.unio.ui.splash.SplashLoginActivity quando o botão de login for pressionado
     *   -- Corrigido bug no Dialog, no Android 5.0.x, ele não interpreta perfeitamente o layout do Dialog
     *   -- Adicionado novos itens com.unio.layout.collection1.items.ItemIconNotificationSimple e
     *      com.unio.layout.collection1.items.ItemIconNotificationComplete
     *
     * - Melhorias em database
     *   -- Adicionado opção de obter o total de registros pelo find
     *   -- Melhorias em database, para simplificar a busca em tabelas filhas
     *   -- Correção de bugs
     *
     * - Melhorias em {@link Http}
     *   -- Adicionado métodos {@link com.unio.util.rest.Http#METHOD_PUT} e
     *      {@link com.unio.util.rest.Http#METHOD_DELETE} para RESTful
     *   -- Adicionado {@link Http#getUrl()} e {@link Http#getParams()}
     *      para visualização da url e parâmetros
     *
     * - Adicionado {@link Unio#version()} para usar a instância de com.unio.util.builder.VersionControl já iniciado
     * - Adicionado {@link XML} para auxiliar na criação de xml
     * - Adicionado {@link Check#isEmpty} para JSON{Array|Object}
     * - Melhorias em {@link com.unio.debug.Trace}, com envio do log de forma diferenciada
     * - Adicionado opção de buscar a data anterior e próximo da data de hoje
     * - Reformulação em {@link Location}, para uso mais preciso dos dados de localização
     * - Adição do método Application#onAfterInit() para executar ações após o Unio estar ativo
     * - Melhorias em com.unio.util.builder.AlertBuilder, com ação onDismiss
     * - Adicionado método para abrir o Google Play Store, na página do aplicativo
     * - Adicionado opção de enviar um Fragment, para carregar, como parâmetro na hora de chamar o AppActivity
     * - Arrumado com.unio.util.helper.HConvert#arrayToString para aceitar arrays primitivos
     * - Adicionado {@link GPlayLocation} para busca de localização usando também o
     *   {@link com.google.android.gms.location.LocationServices} do Google Play Service
     *
     * @release 21/12/2015
     */
    public static final String V160 = "1.6.0";

    /**
     * @version 1.7.0
     *
     * - Adicionado delete() em {@link Record} para excluir todos os models
     * - Adicionado com.unio.layout.collection1.items.ItemEmpty para criar espaços entre os itens,
     *   ou o espaço para não o Floating Button não se sobrepor
     * - Adicionado com.unio.util.builder.ProgressAlertBuilder para centralizar o uso do ProgressDialog
     * - Adicionado {@link Traffic} para obter o upload e download total e filtrado de um aplicativo
     * - Alterado com.unio.util.builder.AlertBuilder para usar o layout customizado por padrão
     * - Correção de bugs
     *
     * @release 22/01/2016
     */
    public static final String V170 = "1.7.0";

    /**
     * @version 1.7.1
     *
     * - Support release before Unio 2.0
     * - New improvements in {@link Location} to update location data and prevent wrong location
     * - Added Location.CNLocation#isMock to check if location is real
     * - Added option to define custom minimum time of location update
     * - Added flag to check if current Activity has showing a dialog, as default, only allow one dialog in Activity
     * - Fixed com.unio.layout.menu.UMainFragment#activityCompat NullPointerException
     * - Fixed fragment IllegalAccessException
     * - Added {@link File#lengthAsMegabyte()} to get file size as MB
     * - Added {@link AppInfo#fileSize} to get App .apk file size as MB
     * - Minor bug corrections
     *
     * @release 20/04/2016
     */
    public static final String V171 = "1.7.1";

    /**
     * @version 2.0.0
     *
     * - Added {@link android.os.Bundle} savedInstanceState in onInit() for life cycle control
     * - Added {@link com.unio.base.UAccessibilityService} for control app accessibilty actions
     * - Changed UCApp#settings() to Application#onBeforeInit(Config) to make equal with onAfterInit
     * - Changed Unio#checkVersion() to {@link Unio#version()}, now has a module to manage any system version
     * - Added {@link TextView} and {@link EditText}
     *   to create custom #setText using {@link Formatter}
     * - Improvements in all view to see in layout preview
     * - Implemented #setText with variables using {@link Formatter}
     * - Added {@link com.unio.ui.features.effects.Blur} to create a blur view/image layer
     * - Unified all "EditText-like" in {@link EditText}, including TextViewInput and AutoCompleteTextView
     * - Unified all "TextView-like" in {@link TextView}, including UDIconView
     * - Unified all "Button-like" in {@link Button}, including UDCounterButton
     * - Moved .drawables, .effects and .fonts to .features for organization
     * - Added {@link com.unio.ui.features.drawables.RippleDrawable} as RippleView repleacement
     * - Added annotations for identify activity/fragment layout and view ID as more simple
     * - Added com.unio.layout.UDCollectionView, Unio custom original android.support.v7.widget.RecyclerView implementation
     * - Added com.unio.layout.UDCollectionView.IBehavior to work with Swipe, Move, Expand and other behaviors
     * - Added com.unio.layout.collection1.FormAdapter to use form items
     * - Added com.unio.layout.collection1.DockAdapter to replace com.unio.layout.UDDockView
     * - Added version in com.unio.ui.splash.SplashLoginActivity, configurable using
     *   R.string.splash_version and R.color.splash_version_color
     * - Added com.unio.layout.collection1.item.form.ItemSignature for survey
     * - Added com.unio.layout.collection1.item.form.ItemPhoto for survey
     * - Added com.unio.layout.collection1.item.form.ItemRange for survey
     * - Added {@link Formatter} to create variable contained texts
     * - Fixes {@link Location} issues
     * - Added {@link com.unio.modules.Version} to control version of all dependencies too,
     *   app and library, and now works with version code too
     * - Added {@link Network} for all network management,
     *   {@link Device} network methods is all deprecated
     * - General classes renamed
     * - Removed app.action and extension
     * - Creates new section, control, for Android directly access, like Device, Phone, and Accessibility
     * - Added {@link Accessibility} for all control related about device accessibility
     * - Renamed all helper class to H[Class]. com.unio.shortcuts.Helper is deprecated, will be use class directly
     * - Added {@link Convert#singularOrPlural(int, String, String)}
     *   to help when need format text in singular or plural
     * - Added {@link Check#isEqual} to help 2 objects comparison, preventing NullPointerException if base is null
     * - Improvements in {@link ResourceManager} to prevent Exceptions in layout preview
     * - Moved com.unio.util.tag.UJSONArray and com.unio.util.tag.UJSONObject to tag package and setted as local class
     * - Added #put and #child to insert values directly in com.unio.util.tag.JSON,
     *   this will be the unique and unified class to work with json
     * - Added {@link Formatter} to create variable contained texts
     * - Added com.unio.layout.UDDialog as an universal Unio dialog layout manager, using Styles
     * - Added com.unio.layout.dialog.MaterialDialogStyle for own Material Design dialog
     * - Added com.unio.layout.dialog.MaterialProgressStyle for own Material Design progress dialog
     * - Added com.unio.layout.dialog.SnackBarStyle for own standard SnackBar and Material Design style
     * - Added com.unio.layout.UDTabView to work with Tab only as View, instead TabActivity/TabFragment
     * - Fixed {@link DrawView} when only touch, draw point
     * - Added {@link com.unio.base.UNotification} for full Unio supported notification control
     * - Added {@link Date} for completely control date
     * - Added {@link ScannerView} and {@link ScannerActivity},
     *   improving Barcode recognition from Unio core, and deprecating com.unio.plugin.barcodescanner
     * - Added {@link com.unio.database.sql.orm.Model#setAnswer} to add values in one time, and if is new record,
     *   check if this values already exists
     * - Added new methods in {@link com.unio.database.sql.orm.Record} to execute multiple insert/update based in setted answer
     * - Added setAnswerAll and saveAll methods in {@link com.unio.database.sql.orm.Model} and {@link com.unio.database.sql.orm.Record}
     *   to save current model and all children
     * - Added {@link com.unio.database.sql.orm.Model#errors} to get all errors in DB when save
     * - Added {@link com.unio.database.sql.orm.annotation.ARelation} to create relations between models, to share ids in common
     * - Added {@link com.unio.database.sql.orm.Model#equals} to compare 2 models values
     * - Fixed SQLite limitation that not allow AUTOINCREMENT when has 2 or more primary keys
     * - New implementation of Trace content. Now has a model to store and a service runnable to send
     * - Improvement in {@link SplashLoginActivity}, with full landscape support
     *   and option to scan authentication credentials from QRCode
     * - Added {@link Hash} for an universal array processor for Unio
     * - Added {@link Result} for an universal result processor for Unio
     *
     * Removed:
     * - com.unio.layout.UDAutoComplete
     * - com.unio.layout.UDCounterButton
     * - com.unio.layout.UDIconView
     * - com.unio.layout.textinput.*
     * - com.unio.layout.tab.*
     * - com.unio.layout.dialog.MaterialDialog
     * - com.unio.layout.dialog.MaterialProgressDialog
     * - com.unio.layout.dialog.SnackBar
     * - com.unio.util.builder.AlertBuilder
     * - com.unio.util.builder.ProgressAlertBuilder
     * - com.unio.layout.UDDockView
     * - com.unio.util.builder.NotificationBuilder
     *
     * @release 31/12/2017
     */
    public static final String V200 = "2.0.0";

    /**
     * @version 2.1.0
     *
     * - Security update
     * - Android version fixes
     * - Update BuildTools to Android SDK 26
     *
     * @release 25/06/2019
     */
    public static final String V210 = "2.1.0";

    /**
     * @version 3.0.0
     *
     * New milestone with drastical changes
     *
     * - Update to Android SDK 27 libraries
     * - Remove support for Android SDK 14 before versions
     * - Compatibility with Kotlin
     * - Changed layout package to com.unio.ui
     * - Refactored views, migrating to resource layout
     * - Removed class prefixes. Only com.unio.base remains
     * - Created new collection view using RecyclerView
     * - Added {@link com.unio.ui.annotation.RequestPermissions} annotation for runtime permission request from Activity
     * - Added {@link com.unio.ui.context.ContextActivity} as new solution for activity with menu
     * - Migrated {@link Unio} initializer to ContentProvider instead starting from {@link android.app.Application}
     * - Complete refactored classes
     * - Remove token block. Now the framework will be open source from this version release
     * - Added examples in Java and Kotlin
     *
     * @release 05/09/2019
     */
    public static final String V300 = "3.0.0";

    /**
     * @version 3.0.1
     * 
     * - Show current Unio version on the log
     * - Add logs for Adapter
     * - Use NO_WRAP base64 encode for File request
     * - Fix ContextActivity onBackPressed
     * - Improve use of alias on Version app in the metadata
     * 
     * @release 07/12/2019
     */
    public static final String V301 = "3.0.1";

    /** Current Framework version */
    public static final String CURRENT = V301;
}