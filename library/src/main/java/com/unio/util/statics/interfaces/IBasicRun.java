package com.unio.util.statics.interfaces;

/**
 * IBasicRun
 *
 * Basic callback interfaces
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 27/01/2015 21:08
 */
public abstract class IBasicRun
{
    /** External parameters, to prevent "final nighmare" */
    public final Object[] params;

    /**
     * Constructor method
     *
     * @param params external parameters
     */
    public IBasicRun(Object... params) { this.params = params; }

    /**
     * Execution method
     */
    public abstract void run();
}