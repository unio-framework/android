package com.unio.util.statics.constants;

/**
 * CNRequest
 *
 * onActivityResult request codes
 *
 * @author Leandro Akira Omiya Takagi <leandro.takagi@icloud.com>
 */
public class RequestCode
{
    /** Camera request */
    public static final int CAMERA = 1888;
}