package com.unio.util.statics.interfaces;

/**
 * IBasicId
 *
 * Basic interfaces with id option
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 15/04/2016 17:54
 */
public interface IBasicId<T>
{
    /**
     * Callback id
     *
     * @return T
     */
    T id();
}