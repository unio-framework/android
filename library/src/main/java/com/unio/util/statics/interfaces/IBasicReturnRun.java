package com.unio.util.statics.interfaces;

/**
 * IBasicReturnRun
 *
 * Basic callback interfaces with an return
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 03/02/2016 0:51
 */
@SuppressWarnings("TypeParameterExplicitlyExtendsObject")
public abstract class IBasicReturnRun<T extends Object>
{
    /** External parameters, to prevent "final nighmare" */
    public final Object[] params;

    /**
     * Constructor method
     *
     * @param params external parameters
     */
    public IBasicReturnRun(Object... params) { this.params = params; }

    /**
     * Execution method
     */
    public abstract T run();
}