package com.unio.util.statics.constants;

/**
 * Time
 *
 * Time management constants
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class Time
{
    /**
     * Milisecond
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public static class Milisecond
    {
        /** Segundos em milisegundos **/
        public static final int SECOND = 1000;

        /** Minutos em milisegundos **/
        public static final int MINUTE = SECOND * 60;

        /** Horas em milisegundos **/
        public static final int HOUR = MINUTE * 60;

        /** Dias em milisegundos **/
        public static final int DAY = HOUR * 24;
    }
}