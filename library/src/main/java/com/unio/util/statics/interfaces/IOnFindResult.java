package com.unio.util.statics.interfaces;

import android.app.Activity;

/**
 * IOnFindResult
 *
 * Interface for process an information
 *
 * @author  Leandro Akira Omiya Takagi <leandro.takagi@icloud.com>
 * @version 1.0 14/11/2016 21:32
 */
public interface IOnFindResult<T>
{
    /**
     * Process response
     * This callback can finish single started activity, when finished, need to return true
     *   to prevent finish again from method
     *
     * @param activity current activity instance
     * @param response found response
     *
     * @return boolean
     */
    boolean onFindResult(Activity activity, T response);
}