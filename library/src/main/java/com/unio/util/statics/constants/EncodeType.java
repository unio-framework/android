package com.unio.util.statics.constants;

/**
 * CNEncode
 *
 * Encodes comuns
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class EncodeType
{
    /** Constantes de encode */
    public static final String UTF_8      = "UTF-8";
    public static final String ISO_8859_1 = "ISO-8859-1";
    public static final String LATIN_1    = ISO_8859_1; // Alias, latin1 é o mesmo que ISO-8851-1 (Oficialmente ISO-LATIN-1)
}