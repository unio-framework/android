package com.unio.util.statics.exceptions;

/**
 * HashGenerationException
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class HashGenerationException extends Exception
{
    /** ID */
    private static final long serialVersionUID = -5242375518357520154L;

    /**
     * Construtor
     */
    public HashGenerationException() { super(); }
    public HashGenerationException(String detailMessage, Throwable throwable) { super(detailMessage, throwable); }
    public HashGenerationException(String detailMessage) { super(detailMessage); }
    public HashGenerationException(Throwable throwable) { super(throwable); }
}