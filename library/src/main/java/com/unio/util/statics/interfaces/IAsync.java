package com.unio.util.statics.interfaces;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;

/**
 * IAsync
 *
 * Interface for async/sync executions
 * It's an Absctract Class with Interface behavior
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 27/01/2016 20:21
 */
public abstract class IAsync extends AsyncTask<Void, Void, Void>
{
    /** List if UI execute callbacks */
    private List<IOnUI> _runOnUI = new ArrayList<>();

    /** Execution type */
    private EExecution _executionType;

    /**
     * Background Thread execution content
     */
    protected abstract void onInit();

    /**
     * Add an UI callback
     *
     * @param callback callback
     */
    public void addOnUI(IOnUI callback) { if (callback != null) this._runOnUI.add(callback); }

    /**
     * Return execution type
     *
     * @return EExecution
     */
    public EExecution executionType() { return this._executionType; }

    /**
     * Execute background commands
     *
     * @param params params
     *
     * @return Boolean
     */
    @Override
    protected synchronized Void doInBackground(Void... params) {
        if (this._executionType != EExecution.SYNCHRONOUS) this.onInit();
        return null;
    }

    /**
     * Commands after finish background task
     *
     * @param result result
     */
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (this._executionType == EExecution.SYNCHRONOUS) this.onInit();
        if (this._runOnUI != null && this._runOnUI.size() > 0)
            for (IOnUI callback : this._runOnUI) callback.onExecuteInUI();
    }

    /**
     * Start execution
     */
    public void execute() { this.execute(EExecution.ASYNCHRONOUS); }

    /**
     * Start execution
     *
     * @param type execution type
     *             ASYNCRONOUS:
     *                 Default execution, execute in background
     *             MULTIPLE:
     *                 After API 10 (3.0), execute multiple async tasks using executeOnExecutor
     *             SYNCHRONOUS:
     *                 Execute all tasks in UI Thread
     */
    @SuppressLint("NewApi")
    public void execute(EExecution type) {
        this._executionType = type;
        switch (this._executionType) {
            case MULTIPLE:
                this.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                break;
            case SYNCHRONOUS:
                // Call onPostExecute directly
                this.onPostExecute(null);
                break;
            default:
                super.execute();
        }
    }

    /**
     * EExecution
     *
     * Execution types enum
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 17/01/2015 16:08
     */
    public enum EExecution { ASYNCHRONOUS, SYNCHRONOUS, MULTIPLE }

    /**
     * IOnUI
     *
     * Interface for UI execute tasks on onPostExecute
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 03/01/2015 12:24
     */
    public static abstract class IOnUI
    {
        /** External parameters, to prevent "final nighmare" */
        public final Object[] params;

        /**
         * Constructor method
         *
         * @param params external parameters
         */
        public IOnUI(Object... params) { this.params = params; }

        /**
         * Execution method
         */
        public abstract void onExecuteInUI();
    }
}