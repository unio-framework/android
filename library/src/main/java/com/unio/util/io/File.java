package com.unio.util.io;

import android.net.Uri;
import android.os.Environment;
import android.webkit.MimeTypeMap;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.helper.Convert;
import java.io.*;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * File
 *
 * Unio own file management class
 *
 * Required permission:
 *     <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class File
{
    /** Identification flags */
    public static final int DEFAULT   = 0;
    public static final int FILE      = 1;
    public static final int DIRECTORY = 2;

    /** Attributes */
    public final String path;
    public final String name;
    public final String extension;
    public final String mime;

    /** Original {@link java.io.File}, instance */
    public final java.io.File Raw;

    /** Flag */
    private int _flag;

    /**
     * Delete file
     *
     * @param path path
     *
     * @return boolean
     */
    public static boolean delete(String path) {
        // Se estiver vazio, considera como inexistente
        if (Check.isEmpty(path)) return true;
        File f = new File(path);
        return f.delete();
    }

    /**
     * Check if file/folder exists
     *
     * @param path path
     *
     * @return boolean
     */
    public static boolean exists(String path) {
        if (Check.isEmpty(path)) return false;
        File f = new File(path);
        return f.exists();
    }

    /**
     * Constructor
     *
     * @param path path. If set only the name, uses root
     */
    public File(String path) { this(path, DEFAULT); }

    /**
     * Constructor
     *
     * @param path path. If set only the name, uses root
     * @param flag flag. If will create, and need to set a custom type
     */
    public File(String path, int flag) {
        this._flag = flag;

        if (path == null) path = "undefined.file";
        String externalStorage = Environment.getExternalStorageDirectory().toString();
        path = path.replace("//", "/"); // Removes double slash
        String[] paths = path.split("/");
        if (path.startsWith("/") == false) {
            List<String> base  = new LinkedList<>(Arrays.asList(externalStorage.split("/")));
            for (String p : paths) if (base.contains(p) == false) base.add(p);
            path = Convert.toString(base, "/");
        }

        this.path = path;
        this.name = paths[paths.length-1];
        this.Raw  = new java.io.File(this.path);

        String[] names = this.name.split("\\.");
        this.extension = this.isFile() ? names[names.length-1] : null;

        String type = null;
        if (this.isFile()) {
            String extension = MimeTypeMap.getFileExtensionFromUrl(this.name);
            if (extension == null) {
                type = "application/octet-stream";
            } else {
                type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            }
        }
        this.mime = type;
    }

    /**
     * File content
     *
     * @return String
     */
    public String content() {
        if (this.exists() == false || this.isDirectory()) return "";

        try {
            FileInputStream fis = new FileInputStream(this.Raw);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) sb.append(line);
            return sb.toString();
        } catch (Exception e) {
            Trace.get(e);
            return "";
        }
    }

    /**
     * Content as byte
     *
     * @return byte[]
     */
    public byte[] contentAsByte() {
        if (this.exists() == false || this.isDirectory()) return new byte[0];

        try {
            // Open file
            RandomAccessFile f = new RandomAccessFile(this.Raw, "r");
            // Get and check length
            long longlength = f.length();
            int length = (int) longlength;
            // Read file and return data
            byte[] data = new byte[length];
            f.readFully(data);
            f.close();
            return data;
        } catch (Exception e) {
            Trace.get(e);
            return new byte[0];
        }
    }

    /**
     * Create
     * If is file, creates as empty file
     * If is directory, creates all parent directories, if needed
     *
     * @return boolean
     */
    public boolean create() {
        if (this.exists()) return true;

        if (this.isFile()) {
            this.createDir();
            try {
                return this.Raw.createNewFile();
            } catch (IOException e) {
                Trace.get(e);
                return false;
            }
        } else {
            return this.Raw.mkdirs();
        }
    }

    /**
     * Create directory
     * creates all parent directories too, if needed
     *
     * @return boolean, se já existir retorna true
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean createDir() {
        if (this.isFile()) {
            List<String> paths = new LinkedList<>(Arrays.asList(this.path.split("/")));
            paths.remove(paths.size() - 1); // Remove last item if is file
            File parent = new File(Convert.toString(paths, "/"), DIRECTORY);
            return parent.create();
        } else {
            return this.create();
        }
    }

    /**
     * Copy a file/directory
     *
     * @param toPath destination path
     *
     * @return boolean
     */
    public boolean copyTo(String toPath) {
        if (Check.isEmpty(toPath)) return false;
        File to = new File(toPath);

        if (this.isDirectory()) {
            if (to.isDirectory() == false) return false; // If destination is not a directory, stop here
            String[] children = this.Raw.list();
            boolean response = false;
            for (File child : this.subdirectories()) response = child.copyTo(to.path+"/"+child.name);
            return response;
        } else {
            try {
                // Create destination path if not exists
                if (to.exists() == false && to.create() == false) return false;

                InputStream in = new FileInputStream(this.Raw);
                OutputStream out = new FileOutputStream(to.Raw);

                byte[] buffer = new byte[1024];
                int read;
                while ((read = in.read(buffer)) != -1) out.write(buffer, 0, read);
                in.close();

                // Write content to destination
                out.flush();
                out.close();
                return true;
            } catch (Exception e) {
                Trace.get(e);
                return false;
            }
        }
    }

    /**
     * Delete the file/directory
     *
     * @return boolean
     */
    public boolean delete() {
        if (this.exists() == false) return true;

        if (this.isFile()) {
            return this.Raw.delete();
        } else {
            try {
                for (File child : this.subdirectories()) child.delete();
                return this.Raw.delete();
            } catch (Exception e) {
                Trace.get(e);
                return false;
            }
        }
    }

    /**
     * Check if exists
     *
     * @return boolean
     */
    public boolean exists() { return this.Raw.exists(); }

    /**
     * Get the child path
     *
     * @param path path
     *
     * @return File
     */
    public File child(String path) {
        if (this.exists() == false || this.isFile()) return null;
        String newPath = this.path+"/"+path;
        return new File(newPath);
    }

    /**
     * Check if is directory
     *
     * @return boolean
     */
    public boolean isDirectory() {
        if (this.exists()) {
            return this.Raw.isDirectory();
        } else {
            if (this._flag == DIRECTORY) {
                return true;
            } else {
                // Se começar com ponto (.), ignora ele da checagem
                String name = this.name.replaceFirst("^\\.", "");
                return name.contains(".") == false;
            }
        }
    }

    /**
     * Check if is file
     *
     * @return boolean
     */
    public boolean isFile() {
        if (this.exists()) {
            return this.Raw.isFile();
        } else {
            if (this._flag == FILE) {
                return true;
            } else {
                // Se começar com ponto (.), ignora ele da checagem
                String name = this.name.replaceFirst("^\\.", "");
                return name.contains(".");
            }
        }
    }

    /**
     * Get size
     *
     * @return long
     */
    public long length() { return this.Raw.length(); }

    /**
     * Get size in MB
     *
     * @return long
     */
    public double lengthAsMegabyte() { return this.length() / Math.pow(1024, 2); }

    /**
     * Move a file/directory
     *
     * @param toPath destination path
     *
     * @return boolean
     */
    public boolean moveTo(String toPath) {
        if (this.copyTo(toPath) && this.delete()) {
            // mover é o mesmo que a junção de copiar e depois excluir a origem
            // por isso, se as duas ações retornar true, considera como sucesso
            return true;
        } else {
            // Se algum dos dois não deu certo, exclui o destino e retorna false para nova tentativa
            File to = new File(toPath);
            if (to.exists()) to.delete();
            return false;
        }
    }

    /**
     * Rename a file or directory
     *
     * @param name new name
     *
     * @return IOFile, null if unsuccessful
     */
    public File renameTo(String name) {
        String[] path = this.path.split("/");
        if (this.isFile() && name.contains("\\.") == false) name += "."+this.extension;
        path[path.length-1] = name;
        String fullFormattedPath = Convert.toString(path, "/");
        if (fullFormattedPath.startsWith("/") == false) fullFormattedPath = "/"+fullFormattedPath;
        File newFile = new File(fullFormattedPath);
        return this.Raw.renameTo(newFile.Raw) ? newFile : null;
    }

    /**
     * Put a content
     *
     * @param value content
     *
     * @return boolean, return false if is directory
     */
    public boolean save(byte[] value) {
        if (this.isFile() == false) return false;
        this.create();

        try {
            FileOutputStream outputStream = new FileOutputStream(this.Raw);
            outputStream.write(value);
            outputStream.flush();
            outputStream.close();
            Trace.log(this, "file "+this.path+" saved");
            return true;
        } catch (Exception e) {
            Trace.get(e);
            return false;
        }
    }

    /**
     * Put a InputStream content
     *
     * @param stream content
     *
     * @return boolean, return false if is directory
     */
    public boolean save(InputStream stream) {
        if (this.isFile() == false) return false;
        if (this.exists() == false) this.create();

        try {
            FileOutputStream outputStream = new FileOutputStream(this.Raw);
            byte[] buffer = new byte[1024];
            int len1;
            while ((len1 = stream.read(buffer)) != -1) outputStream.write(buffer, 0, len1);
            outputStream.flush();
            outputStream.close();
            return true;
        } catch (Exception e) {
            Trace.get(e);
            return false;
        }
    }

    /**
     * List all subdirectories of current directory
     *
     * @return List<IOFile>, empty if is file
     */
    public List<File> subdirectories() {
        List<File> subs = new LinkedList<>();
        if (this.exists() == false || this.isFile()) return subs;

        for (String name : this.Raw.list()) {
            String path = this.path+"/"+name;
            File sub = new File(path);
            subs.add(sub);
        }
        return subs;
    }

    /**
     * Get file URI
     *
     * @return Uri
     */
    public Uri uri() { return Uri.fromFile(this.Raw); }

    /**
     * Format to string visualization
     *
     * @return String
     */
    @Override
    public String toString() {
        return "name: " + this.name +
            "\n" +
            "path: " + this.path +
            "\n" +
            "length: " + this.length() +
            "\n" +
            "exists: " + this.exists() +
            "\n" +
            "isFile: " + this.isFile() +
            "\n" +
            "isDirectory: " + this.isDirectory() +
            "\n" +
            "content: " + this.content() +
            "\n" +
            "subdirectories: " + this.subdirectories().size() +
            "\n";
    }
}