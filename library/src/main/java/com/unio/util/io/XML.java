package com.unio.util.io;

import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.util.control.Date;
import com.unio.util.helper.Check;
import com.unio.util.statics.constants.EncodeType;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.*;

/**
 * XML
 *
 * Create XML from object
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class XML
{
    /** Structure control */
    public static final int PARENT   = 1;
    public static final int CHILD    = 2;
    public static final int READABLE = 4; // Create structure with indentation

    /** Content */
    private String _tag; // Tag do objeto, quando for o pai, pode ser que não exista
    private List<Object> _contents = new LinkedList<>(); // Lista do conteúdo próprio

    /** Header */
    public String version     = "1.0";
    public String encoding    = EncodeType.UTF_8;
    public boolean standalone = true;

    /** Key for Session with last error exception content */
    public static final String LAST_ERROR_DATE = "LAST_ERROR_DATE";
    public static final String LAST_ERROR      = "LAST_ERROR";

    /**
     * Initial constructor
     */
    public XML() {}

    /**
     * Constructor with tag
     *
     * @param tag tag
     */
    public XML(String tag) { this._tag = tag; }

    /**
     * Add child
     *
     * @param tag tag
     *
     * @return XML, child instance
     */
    public XML child(String tag) {
        XML child = new XML(tag);
        this.content(child);
        return child;
    }

    /**
     * Add a child with content
     *
     * @param tag tag
     * @param o   content
     *
     * @return XML, current instance
     */
    public XML child(String tag, Object o) {
        XML child = new XML(tag);
        child.content(o);
        this.content(child);
        return this;
    }

    /**
     * Add content
     *
     * @param o content
     *
     * @return XML
     */
    public XML content(Object o) {
        this._contents.add(o);
        return this;
    }

    /**
     * Render the XML
     *
     * @return String
     */
    public String render() { return this.render(PARENT); }

    /**
     * Render the XML
     * {@link #READABLE} generates with indentation
     *
     * @param params parameter
     *
     * @return String
     */
    public String render(int params) {
        List<Integer> p = this._getParams(params);
        int childParams = CHILD;
        if (p.contains(READABLE)) childParams |= READABLE;
        String xml = "";
        if (this._contents.size() > 0) {
            if (p.contains(CHILD) == false) {
                String version    = Check.isEmpty(this.version)    ? "1.0"          : this.version;
                String encoding   = Check.isEmpty(this.encoding)   ? EncodeType.UTF_8 : this.encoding;
                String standalone = Check.isEmpty(this.standalone) ? "yes"          : String.valueOf(this.standalone);
                xml += "<?xml version='"+version+"' encoding='"+encoding+"' standalone='"+standalone+"' ?>";
                if (p.contains(READABLE)) xml += "\n";
            }
            if (Check.isEmpty(this._tag) == false) xml += "<"+this._tag+">";
            StringBuilder content = new StringBuilder();
            for (Object o : this._contents) {
                if (Check.isEmpty(o) == false) {
                    if (o instanceof XML) {
                        content.append(((XML) o).render(childParams));
                    } else if (o instanceof String) {
                        content.append(o.toString());
                    } else {
                        content.append(o);
                    }
                }
            }
            if (Check.isEmpty(content.toString()) == false) {
                if (p.contains(READABLE) && content.toString().contains("\n")) {
                    xml += "\n";
                    String temp = content.toString();
                    content = new StringBuilder();
                    for (String c : temp.split("\n")) {
                        if (Check.isEmpty(c) == false) {
                            c = "- "+c;
                            content.append(c).append("\n");
                        }
                    }
                }
                xml += content;
                if (p.contains(READABLE) && content.toString().contains("\n")) xml += "\n";
            }
            if (Check.isEmpty(this._tag) == false) xml += "</"+this._tag+">";
            if (p.contains(READABLE) && p.contains(CHILD)) xml += "\n";
        }
        return xml;
    }

    /**
     * Structure management
     *
     * @param params parameters
     *
     * @return List<Integer>
     */
    private List<Integer> _getParams(int params) {
        List<Integer> p = new LinkedList<>();
        if (params - READABLE >= 0) {
            p.add(READABLE);
            params -= READABLE;
        }
        if (params - CHILD >= 0) {
            p.add(CHILD);
            params -= CHILD;
        }
        if (params - PARENT >= 0) p.add(PARENT);
        return p;
    }

    /**
     * Convert to Map
     *
     * @return Map
     */
    public Map toMap() { return parse(this.render()); }

    /**
     * Convert to Map
     *
     * @return Map
     */
    public Hash toHash() { return new Hash(toMap()); }

    /**
     * Parse to map
     *
     * @param xml string
     *
     * @return Map
     */
    public static Map parse(String xml) { return _parse(xml, null); }

    /**
     * Parse to map
     *
     * @param xml file
     *
     * @return Map
     */
    public static Map parse(File xml) { return _parse(null, xml); }

    /**
     * Parser
     *
     * @param xmlString string xml
     * @param xmlFile   arquivo xml
     *
     * @return Map
     */
    private static Map _parse(String xmlString, File xmlFile) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc;

            if (xmlFile == null) {
                doc = dBuilder.parse(new ByteArrayInputStream(xmlString.getBytes()));
            } else {
                doc = dBuilder.parse(xmlFile);
            }

            doc.getDocumentElement().normalize();

            NodeList resultNode = doc.getChildNodes();

            HashMap result = new HashMap();
            MyNodeList tempNodeList = new MyNodeList();

            String emptyNodeName = null, emptyNodeValue = null;

            for(int index = 0; index < resultNode.getLength(); index ++) {
                Node tempNode = resultNode.item(index);
                if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                    tempNodeList.addNode(tempNode);
                }
                emptyNodeName = tempNode.getNodeName();
                emptyNodeValue = tempNode.getNodeValue();
            }

            if (tempNodeList.getLength() == 0 && emptyNodeName != null
                && emptyNodeValue != null) {
                result.put(emptyNodeName, emptyNodeValue);
                return result;
            }

            _parseXMLNode(tempNodeList, result);
            return result;
        } catch (Exception ex) {
            Unio.session(LAST_ERROR).set(Trace.format(ex));
            Unio.session(LAST_ERROR_DATE).set(Date.withTime());
            return null;
        }
    }

    private static void _parseXMLNode(NodeList nList, HashMap result) {
        for (int temp = 0; temp < nList.getLength(); temp++) {
            Node nNode = nList.item(temp);
            if (nNode.getNodeType() == Node.ELEMENT_NODE
                && nNode.hasChildNodes()
                && nNode.getFirstChild() != null
                && (nNode.getFirstChild().getNextSibling() != null
                || nNode.getFirstChild().hasChildNodes())) {
                NodeList childNodes = nNode.getChildNodes();
                MyNodeList tempNodeList = new MyNodeList();
                for(int index = 0; index < childNodes.getLength(); index ++) {
                    Node tempNode = childNodes.item(index);
                    if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                        tempNodeList.addNode(tempNode);
                    }
                }
                HashMap dataHashMap = new HashMap();
                if (result.containsKey(nNode.getNodeName()) && result.get(nNode.getNodeName()) instanceof List) {
                    List mapExisting = (List) result.get(nNode.getNodeName());
                    mapExisting.add(dataHashMap);
                } else if(result.containsKey(nNode.getNodeName())) {
                    List counterList = new ArrayList();
                    counterList.add(result.get(nNode.getNodeName()));
                    counterList.add(dataHashMap);
                    result.put(nNode.getNodeName(), counterList);
                } else {
                    result.put(nNode.getNodeName(), dataHashMap);
                }
                if (nNode.getAttributes().getLength() > 0) {
                    Map attributeMap = new HashMap();
                    for(int attributeCounter = 0;
                        attributeCounter < nNode.getAttributes().getLength();
                        attributeCounter++) {
                        attributeMap.put(
                            nNode.getAttributes().item(attributeCounter).getNodeName(),
                            nNode.getAttributes().item(attributeCounter).getNodeValue()
                        );
                    }
                    dataHashMap.put("<<attributes>>", attributeMap);
                }
                _parseXMLNode(tempNodeList, dataHashMap);
            } else if (nNode.getNodeType() == Node.ELEMENT_NODE
                && nNode.hasChildNodes() && nNode.getFirstChild() != null
                && nNode.getFirstChild().getNextSibling() == null) {
                _putValue(result, nNode);
            } else if(nNode.getNodeType() == Node.ELEMENT_NODE) {
                _putValue(result, nNode);
            }
        }
    }

    private static void _putValue(HashMap result, Node nNode) {
        HashMap attributeMap = new HashMap();
        Object nodeValue = null;
        if (nNode.getFirstChild() != null) {
            nodeValue = nNode.getFirstChild().getNodeValue();
            if (nodeValue != null) nodeValue = nodeValue.toString().trim();
        }
        HashMap nodeMap = new HashMap();
        nodeMap.put("<<value>>", nodeValue);
        Object putNode = nodeValue;
        if (nNode.getAttributes().getLength() > 0) {
            for (int attributeCounter = 0;
                attributeCounter < nNode.getAttributes().getLength();
                attributeCounter++) {
                attributeMap.put(
                    nNode.getAttributes().item(attributeCounter).getNodeName(),
                    nNode.getAttributes().item(attributeCounter).getNodeValue()
                );
            }
            nodeMap.put("<<attributes>>", attributeMap);
            putNode = nodeMap;
        }
        if (result.containsKey(nNode.getNodeName()) && result.get(nNode.getNodeName()) instanceof List) {
            List mapExisting = (List) result.get(nNode.getNodeName());
            mapExisting.add(putNode);
        } else if(result.containsKey(nNode.getNodeName())) {
            List counterList = new ArrayList();
            counterList.add(result.get(nNode.getNodeName()));
            counterList.add(putNode);
            result.put(nNode.getNodeName(), counterList);
        } else {
            result.put(nNode.getNodeName(), putNode);
        }
    }

    static class MyNodeList implements NodeList {
        List<Node> nodes = new ArrayList<>();
        int length = 0;
        public MyNodeList() {}

        public void addNode(Node node) {
            nodes.add(node);
            length++;
        }

        @Override
        public Node item(int index) {
            try {
                return nodes.get(index);
            } catch (Exception ex) {
                Trace.get(ex);
            }
            return null;
        }

        @Override
        public int getLength() { return length; }
    }
}