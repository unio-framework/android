package com.unio.util.io;

import com.unio.util.helper.Check;

/**
 * Result
 *
 * Default result class, accepts any value, with cast/convert to common type
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 23/06/2017 21:15
 */
public class Result<V>
{
    /** Flag if value is null */
    public final boolean isNull;

    /** Flag if value is empty, considers empty a null value and "" value */
    public final boolean isEmpty;

    /** Received value */
    private V _value;

    /**
     * Constructor
     *
     * @param value value with a V type
     */
    public Result(V value) {
        this._value = value;
        this.isNull = this._value == null;
        this.isEmpty = Check.isEmpty(this._value);
    }

    /**
     * String formatted String
     *
     * @return String
     */
    @Override
    public String toString() { return this._value == null ? null : String.valueOf(this._value); }

    /**
     * Received value
     *
     * @return V
     */
    public V toRaw() { return this._value; }

    /**
     * Integer formatted value
     * If value is a float, double or long, round to return int valid
     *
     * @return int
     */
    public int toInt() {
        if (this._value == null) {
            return 0;
        } else if (this._value instanceof Integer) {
            return (Integer) this._value;
        } else if (this._value instanceof Float) {
            return Math.round((Float) this._value);
        } else if (this._value instanceof Double) {
            return (int) Math.round((Double) this._value);
        } else if (this._value instanceof Long) {
            return Math.round((Long) this._value);
        }
        return Integer.valueOf(this.toString());
    }

    /**
     * Float formatted value
     *
     * @return float, value or 0 if null
     */
    public float toFloat() { return this._value == null ? 0 : Float.valueOf(this.toString()); }

    /**
     * Double formatted value
     *
     * @return double, value or 0 if null
     */
    public double toDouble() { return this._value == null ? 0 : Double.valueOf(this.toString()); }

    /**
     * Long formatted value
     *
     * @return long, value or 0 if null
     */
    public long toLong() { return this._value == null ? 0 : Long.valueOf(this.toString()); }

    /**
     * Boolean formatted value
     * If value is a number, considers 0 as false, and any other as true
     *
     * @return boolean
     */
    public boolean toBoolean() { return this.toBoolean(false); }

    /**
     * Boolean formatted value, with a default return if is null
     *
     * @param defaultValue default value
     * If value is a number, considers 0 as false, and any other as true
     *
     * @return boolean
     */
    public boolean toBoolean(boolean defaultValue) {
        if (this._value == null) return defaultValue;

        if (this._value instanceof Boolean) {
            return (Boolean) this._value;
        } else if (this._value instanceof Number) {
            int v = this.toInt();
            return v != 0;
        } else {
            return Boolean.valueOf(this.toString());
        }
    }

    /**
     * Hash formatted value
     *
     * @return Hash
     */
    public Hash toHash() { return this._value == null ? null : new Hash(this._value); }
}