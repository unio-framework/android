package com.unio.util.io;

import android.graphics.*;
import android.support.media.ExifInterface;
import com.unio.debug.Trace;
import com.unio.shortcuts.Android;
import com.unio.util.helper.Check;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Image
 *
 * Image file management
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Image extends File
{
    /** Best compression size **/
    public static final double BEST_HEIGHT = 516.0;
    public static final double BEST_WIDTH  = 312.0;

    /**
     * Constructor
     *
     * @param path path. If set only the name, uses root
     */
    public Image(String path) { super(path, FILE); }

    /**
     * Convert content to bitmap
     *
     * @return Bitmap
     */
    public Bitmap contentAsBitmap() { return BitmapFactory.decodeFile(this.path); }

    /**
     * Get bitmap options
     *
     * @return BitmapFactory.Options
     */
    public BitmapFactory.Options bitmapOptions() {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(this.path, options);
        return options;
    }

    /**
     * Compress the image
     *
     * @return Image
     */
    public Image compress() { return this.compress(BEST_HEIGHT, BEST_WIDTH); }

    /**
     * Compress the image, with custom size
     *
     * @param height height
     * @param width  width
     *
     * @return Image
     */
    public Image compress(double height, double width) { return this.compress(null, height, width); }

    /**
     * Compress the image with new name
     *
     * @param newName name
     *
     * @return Image
     */
    public Image compress(String newName) { return this.compress(newName, BEST_HEIGHT, BEST_WIDTH); }

    /**
     * Compress the image, with custom size, and a new name
     *
     * @param newName name
     * @param height  height
     * @param width   width
     *
     * @return Image
     */
    @SuppressWarnings("deprecation")
    public Image compress(String newName, double height, double width) {
        if (this.exists() == false) return this;

        String filePath = this.path;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        if (actualHeight == 0 && actualWidth == 0) return this;

        float maxHeight = (float) height;
        float maxWidth  = (float) width;
        float imgRatio  = actualWidth / actualHeight;
        float maxRatio  = maxWidth / maxHeight;

        // width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

        // setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = this._calculateInSampleSize(options, actualWidth, actualHeight);

        // inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

        // this options allow android to claim the bitmap memory if it runs low on memory
        // inPurgeable e inInputShareable são ignorados a partir do Lollipop
        if (Android.CURRENT_VERSION > Android.KITKAT_WATCH) {
            options.inPurgeable = true;
            options.inInputShareable = true;
        }
        options.inTempStorage = new byte[16 * 1024];

        try {
            // load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError e) {
            Trace.get(e);
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        //noinspection IntegerDivisionInFloatingPointContext
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        // check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            Trace.get(e);
        }

        FileOutputStream out;
        Image file = this._getCompressFileName(newName, height, width);
        try {
            out = new FileOutputStream(file.Raw);
            // write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
        } catch (FileNotFoundException e) {
            Trace.get(e);
        }
        return file;
    }

    /**
     * Save the image
     *
     * @param content bitmap content
     *
     * @return boolean
     */
    public boolean save(Bitmap content) {
        this.create();

        try {
            FileOutputStream ostream = new FileOutputStream(this.Raw);
            content.compress(Bitmap.CompressFormat.PNG, 100, ostream);
            ostream.close();
            return true;
        } catch (Exception e) {
            Trace.get(e);
            return false;
        }
    }

    /**
     * Calculate the size
     *
     * @param options   BitmapFactory.Options
     * @param reqWidth  original width
     * @param reqHeight original height
     *
     * @return int
     */
    private int _calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height/ (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) inSampleSize++;

        return inSampleSize;
    }

    /**
     * Generate new name
     *
     * @param newName   new name
     * @param height    height
     * @param width     width
     *
     * @return IOImage instância da imagem
     */
    private Image _getCompressFileName(String newName, double height, double width) {
        String absoluteFull = this.path;
        StringBuilder imageName = new StringBuilder(this.name);
        String path = absoluteFull.replace(imageName.toString(), ""); // Get path without name

        if (Check.isEmpty(newName)) {
            String[] tempNames = imageName.toString().split("\\.");
            int total = tempNames.length;
            imageName = new StringBuilder();
            for (int i = 0; i < total; i++) {
                if (i == total-1) {
                    imageName.append("_c_").append((int) height).append("x").append((int) width).append(".").append(tempNames[i]);
                } else {
                    imageName.append(tempNames[i]);
                    if (i < total-2) imageName.append(".");
                }
            }
        } else {
            imageName = new StringBuilder(newName);
        }
        return new Image(path+"/"+imageName);
    }
}