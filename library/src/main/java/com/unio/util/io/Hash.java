package com.unio.util.io;

import android.support.annotation.NonNull;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.helper.Convert;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.lang.reflect.Array;
import java.util.*;

/**
 * Hash
 *
 * Global PHP-like array/map utility for Unio
 * This class is prepared for accept any Object to parse
 * This class is prepared to for/forEach iteration
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Hash<K, V> implements Iterable<K>
{
    /** Global types */
    private static final int ARRAY = 1;
    private static final int HASH  = 2;

    /** For int array */
    private LinkedList<V> _array;

    /** For any type array */
    private LinkedHashMap<K, V> _hash;

    /**
     * Basic constructor
     */
    public Hash() {}

    /**
     * Constructor to parse an Object
     * Accepts:
     *   - JSON/XML {@link String}
     *   - {@link Hash}
     *   - {@link JSONObject}/{@link JSONArray}
     *   - {@link XML}
     *   - {@link Collection}
     *   - {@link Map}
     *
     * @param parse parse object
     */
    public Hash(Object parse) {
        if (parse != null)
            this._parse(parse);
    }

    /**
     * Default put for array, if this is used, always considers an array
     *
     * @param value value
     *
     * @return Hash
     */
    public Hash put(V value) {
        // If is not started, considers a array
        this._start(ARRAY);
        if (this._is(ARRAY)) {
            this._array.add(value);
        } else {
            throw new RuntimeException("Method is invalid for non-array");
        }
        return this;
    }

    /**
     * Put for hash or array key update
     *
     * @param key   key
     * @param value value
     *
     * @return Hash
     */
    public Hash put(K key, V value) {
        // If is not started, considers a hash
        this._start(HASH);

        if (this._is(ARRAY)) {
            if (key instanceof Integer) {
                int k = (Integer) key;
                this._array.set(k, value);
            } else {
                throw new RuntimeException("key is not an int");
            }
        } else {
            this._hash.put(key, value);
        }
        return this;
    }

    /**
     * Create a {@link Hash} child for array case
     *
     * @return Hash
     */
    public Hash child() {
        // If is not started, considers a array
        this._start(ARRAY);

        if (this._is(ARRAY)) {
            Hash child = new Hash();
            this.put((V) child);
            return child;
        } else {
            throw new RuntimeException("key is mandatory");
        }
    }

    /**
     * Create a {@link Hash} child for hash case
     *
     * @param key key
     *
     * @return Hash
     */
    public Hash child(K key) {
        // If is not started, considers a hash
        this._start(HASH);

        if (this._is(ARRAY) && (key instanceof Integer) == false) {
            throw new RuntimeException("key is not an int");
        } else {
            Hash child = new Hash();
            this.put(key, (V) child);
            return child;
        }
    }

    /**
     * Recover a value
     *
     * @param key referenced key
     *
     * @return Result
     */
    public Result<V> get(K key) {
        if (this._started()) {
            V value;
            if (this._is(ARRAY)) {
                if (key instanceof Integer) {
                    int k = (Integer) key;
                    value = this._array.get(k);
                } else {
                    throw new RuntimeException("key is not an Integer");
                }
            } else {
                value = this._hash.get(key);
            }
            return new Result(value);
        } else {
            return new Result(null);
        }
    }

    /**
     * Remove a value by key
     *
     * @param key K instance
     */
    public void remove(K key) {
        if (this._started()) {
            if (this._is(ARRAY)) {
                if (key instanceof Integer) {
                    Integer k = Integer.valueOf(key.toString());
                    this._array.remove(k.intValue());
                }
            } else {
                this._hash.remove(key);
            }
        }
    }

    /**
     * Get all keys
     *
     * @return LinkedList
     */
    public LinkedList keys() {
        if (this._started()) {
            if (this._is(ARRAY)) {
                LinkedList<Integer> keys = new LinkedList<>();
                for (int i = 0; i < this._array.size(); i++) keys.add(i);
                return keys;
            } else {
                return new LinkedList(this._hash.keySet());
            }
        } else {
            return new LinkedList<>();
        }
    }

    /**
     * Check if a key exists
     *
     * @param key key
     *
     * @return boolean
     */
    public boolean containsKey(K key) {
        LinkedList<K> keys = this.keys();
        return keys.contains(key);
    }

    /**
     * Get all values
     *
     * @return LinkedList
     */
    public LinkedList<V> values() {
        if (this._started()) {
            return this._is(ARRAY) ? this._array : new LinkedList(this._hash.values());
        } else {
            return new LinkedList<>();
        }
    }

    /**
     * Check if a value exists
     *
     * @param value value
     *
     * @return boolean
     */
    public boolean containsValue(V value) {
        LinkedList<V> values = this.values();
        return values.contains(value);
    }

    /**
     * Get hash total of contents
     *
     * @return int
     */
    public int length() {
        if (this._started()) {
            return this._is(ARRAY) ? this._array.size() : this._hash.size();
        } else {
            return 0;
        }
    }

    /**
     * Format content to String
     *
     * @return String
     */
    @Override
    public String toString() {
        List keys = this.keys();
        String[] formatted = new String[this.keys().size()];
        for (int i = 0; i < keys.size(); i++) {
            formatted[i] = "";
            if (this._is(HASH)) formatted[i] += keys.get(i)+"=";
            formatted[i] += this.get((K) keys.get(i));
        }
        return "["+ Convert.toString(formatted, ", ")+"]";
    }

    /**
     * Format content to JSON
     *
     * @return Object, will be {@link JSONObject} or {@link JSONArray}
     */
    public Object toJson() {
        Object json = this._toJsonConvert(this);
        if (Check.isEmpty(json)) json = new JSONArray(); // If not exists, return an empty json: []
        return json;
    }

    /**
     * Format content to human readable JSON
     *
     * @param indentSpaces indent spaces
     *
     * @return String
     */
    public String toJson(int indentSpaces) {
        Object json = this.toJson();
        try {
            if (json instanceof JSONArray) {
                return ((JSONArray) json).toString(indentSpaces);
            } else {
                return ((JSONObject) json).toString(indentSpaces);
            }
        } catch (JSONException e) {
            return "[]";
        }
    }

    /**
     * Check if current hash is an array type
     *
     * @return boolean
     */
    public boolean isArray() { return this._is(ARRAY); }

    /**
     * Keys for for/forEach iteration
     *
     * @return Iterator
     */
    @Override
    public @NonNull Iterator<K> iterator() { return this.keys().iterator(); }

    /**
     * Check if array or hash is started
     *
     * @return boolean
     */
    private boolean _started() { return (this._array == null && this._hash == null) == false; }

    /**
     * Start array or hash, by type, if not started
     *
     * @param type type
     */
    private void _start(int type) {
        if (this._started() == false) {
            switch (type) {
                case ARRAY:
                    this._array = new LinkedList<>();
                    break;
                case HASH:
                    this._hash = new LinkedHashMap<>();
                    break;
            }
        }
    }

    /**
     * Check if this instance is array or hash
     *
     * @param type type
     *
     * @return boolean
     */
    private boolean _is(int type) {
        if (this._started() == false) return false;
        switch (type) {
            case ARRAY:
                return this._array != null;
            case HASH:
                return this._hash != null;
        }
        return false;
    }

    /**
     * External object parser
     *
     * @param p Object
     */
    private void _parse(Object p) {
        try {
            if (p instanceof Hash) {
                this._array = ((Hash) p)._array;
                this._hash  = ((Hash) p)._hash;
            } else if (p instanceof JSONObject) {
                JSONObject json = (JSONObject) p;
                if (json != JSONObject.NULL) this._toHash(json);
            } else if (p instanceof JSONArray) {
                JSONArray json = (JSONArray) p;
                if (json != null && json.length() > 0) this._toArray(json);
            } else if (p instanceof Collection) {
                this._array = new LinkedList<>((Collection) p);
            } else if (p instanceof Map) {
                this._hash = new LinkedHashMap<>((Map) p);
            } else if (p instanceof String) {
                String params = (String) p;
                if (Check.isJson(params)) {
                    if (params.startsWith("[")) {
                        JSONArray json = new JSONArray(params);
                        this._parse(json);
                    } else {
                        JSONObject json = new JSONObject(params);
                        this._parse(json);
                    }
                } else if (Check.isXml(params)) {
                    this._parse(XML.parse(params));
                }
            }
        } catch (JSONException e) {
            Trace.get(e);
        }
    }

    /**
     * Parse {@link JSONObject} to current {@link Hash} instance
     *
     * @param o JSONObject
     */
    private void _toHash(JSONObject o) throws JSONException {
        this._hash = new LinkedHashMap<>();
        Iterator<String> keysItr = o.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = o.get(key);
            this._hash.put((K) key, (V) value);
        }
    }

    /**
     * Parse {@link JSONArray} to current {@link Hash} instance
     *
     * @param o JSONArray
     */
    private void _toArray(JSONArray o) throws JSONException {
        this._array = new LinkedList<>();
        for(int i = 0; i < o.length(); i++) {
            Object value = o.get(i);
            this._array.add((V) value);
        }
    }

    /**
     * Conversion to JSON
     *
     * @param o Object
     *
     * @return Object
     */
    private Object _toJsonConvert(Object o) {
        if (o == null) return "";
        if (o instanceof JSONArray || o instanceof JSONObject) return o;
        try {
            if (o instanceof Collection) {
                return _collectionToJson((Collection) o);
            } else if (o.getClass().isArray()) {
                return _arrayToJson(o);
            }
            if (o instanceof Map) return _mapToJson((Map) o);
            if (o instanceof Hash) {
                Hash h = (Hash) o;
                if (h._array != null) {
                    return _collectionToJson(h._array);
                } else if (h._hash != null) {
                    return this._mapToJson(h._hash);
                }
                return "";
            }
            if (o instanceof Boolean ||
                o instanceof Byte ||
                o instanceof Character ||
                o instanceof Double ||
                o instanceof Float ||
                o instanceof Integer ||
                o instanceof Long ||
                o instanceof Short ||
                o instanceof String)
            {
                return o;
            }
            if (o.getClass().getPackage().getName().startsWith("java.")) return o.toString();
        } catch (Exception ignored) {}
        return "";
    }

    /**
     * Convert Map to JSONObject
     *
     * @param data data
     *
     * @return JSONObject
     */
    private JSONObject _mapToJson(Map<?, ?> data) {
        JSONObject object = new JSONObject();
        for (Map.Entry<?, ?> entry : data.entrySet()) {
            String key = (String) entry.getKey();
            if (key == null) continue;
            try {
                object.put(key, _toJsonConvert(entry.getValue()));
            } catch (JSONException ignore) {}
        }
        return object;
    }

    /**
     * Convert Collection to JSONArray
     *
     * @param data data
     *
     * @return UJSONArray
     */
    private JSONArray _collectionToJson(Collection data) {
        JSONArray jsonArray = new JSONArray();
        if (data != null) for (Object aData : data) jsonArray.put(_toJsonConvert(aData));
        return jsonArray;
    }

    /**
     * Convert array to UJSONArray
     *
     * @param data data
     *
     * @return UJSONArray
     */
    private JSONArray _arrayToJson(Object data) {
        if (!data.getClass().isArray()) return null;
        final int length = Array.getLength(data);
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < length; ++i) jsonArray.put(_toJsonConvert(Array.get(data, i)));
        return jsonArray;
    }
}