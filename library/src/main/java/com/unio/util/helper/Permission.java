package com.unio.util.helper;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import com.unio.R;
import com.unio.base.UActivity;
import com.unio.core.Unio;
import com.unio.shortcuts.Android;
import com.unio.util.manager.ActivityManager;

/**
 * Permission
 *
 * Helper class for manage Android permission
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Permission
{
    /** Permission response code */
    public static final int RESPONSE = 64;

    /**
     * Check if all required permission are enabled
     *
     * @param permissions permissions
     *
     * @return boolean
     */
    public static boolean isEnabled(String... permissions) {
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(Unio.app(), permission) != PackageManager.PERMISSION_GRANTED)
                return false;
        }
        return true;
    }

    /**
     * Request all permissions already calling dialog
     *
     * @param permissions permissions
     */
    public static void request(String... permissions) { request(ActivityManager.get(), permissions); }

    /**
     * Request all permissions already calling dialog
     *
     * @param activity    activity for request callback
     * @param permissions permissions
     */
    public static void request(final Activity activity, final String[] permissions) { request(activity, permissions, null); }

    /**
     * Request all permissions already calling dialog
     *
     * @param activity    activity for request callback
     * @param permissions permissions
     */
    public static void request(final Activity activity, final String[] permissions, final Class<?> classBased) {
        new AlertDialog.Builder(activity)
            .setTitle(R.string.warning)
            .setMessage(R.string.permission_request)
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    rawRequest(activity, permissions);
                    dialog.dismiss();
                }
            })
            .setNegativeButton(R.string.exit, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (Android.CURRENT_VERSION > Android.LOLLIPOP_MR1) {
                        activity.onRequestPermissionsResult(Permission.RESPONSE, new String[0], new int[0]);
                    } else if (activity instanceof UActivity && classBased != null) {
                        ((UActivity) activity).onPermissionsResult(classBased, new String[0], new int[0]);
                    }
                    dialog.dismiss();
                }
            })
            .setCancelable(false)
            .show();
    }

    /**
     * Call request dialog
     *
     * @param permissions permissions
     */
    public static void rawRequest(String... permissions) { rawRequest(ActivityManager.get(), permissions); }

    /**
     * Call request dialog
     *
     * @param activity    activity for request callback
     * @param permissions permissions
     */
    public static void rawRequest(Activity activity, String... permissions) {
        ActivityCompat.requestPermissions(activity, permissions, RESPONSE);
    }
}