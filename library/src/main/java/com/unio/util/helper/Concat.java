package com.unio.util.helper;

/**
 * Concat
 *
 * Helper class for concatenation
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Concat
{
    /**
     * Concatenate arrays
     *
     * @param arrays array group
     *
     * @return Object[]
     */
    public static Object[] array(Object[]... arrays) {
        int total = 0;
        for (Object[] array : arrays) {
            if (array != null) total += array.length;
        }
        Object[] concat = new String[total];

        int key = 0;
        for (Object[] array : arrays) {
            if (array != null) {
                for (Object content : array)
                    concat[key++] = content;
            }
        }
        return concat;
    }
}