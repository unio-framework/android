package com.unio.util.helper;

import android.text.TextUtils;
import com.google.zxing.common.StringUtils;

/**
 * HFormat
 *
 * Helper class for text format
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 07/03/2014 10:53
 */
public class Format
{
    /**
     * Captalize
     *
     * @param v value
     *
     * @return String
     */
    public static String capitalize(String v) {
        if (Check.isEmpty(v)) return "";
        char first = v.charAt(0);
        return Character.isUpperCase(first) ? v : Character.toUpperCase(first) + v.substring(1);
    }

    /**
     * Left value pad
     *
     * @param v        value
     * @param pad      character
     * @param quantity length
     *
     * @return String
     */
    public static String lpad(Object v, char pad, int quantity) {
        String number = String.valueOf(v);
        if (number.length() >= quantity) return number;

        String finalValue = "";
        int padTotal = quantity-(number.length());
        for (int key = 0; key < padTotal; key++) finalValue += pad;
        finalValue += number;

        return finalValue;
    }

    /**
     * Right value pad
     *
     * @param v        value
     * @param pad      character
     * @param quantity length
     *
     * @return String
     */
    public static String rpad(Object v, char pad, int quantity) {
        String number = String.valueOf(v);
        if (number.length() >= quantity) return number;

        String finalValue = number;
        int padTotal = quantity-(number.length());
        for (int key = 0; key < padTotal; key++) finalValue += pad;

        return finalValue;
    }
}