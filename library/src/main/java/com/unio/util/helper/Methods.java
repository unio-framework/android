package com.unio.util.helper;

import com.unio.debug.Trace;
import java.lang.reflect.Method;

/**
 * HMethod
 *
 * Helper class for class/method call from reflection
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 15/07/2014 23:50
 */
public class Methods
{
    /**
     * Load class from String
     *
     * @param className class
     *
     * @return Class
     */
    public static Class loadClass(String className) {
        Class c;
        try {
            c = Class.forName(className);
        } catch (ClassNotFoundException e) {
            Trace.get(e);
            c = null;
        }
        return c;
    }

    /**
     * Instantiate class
     *
     * @param clazz Class
     *
     * @return Object
     */
    public static Object instantiate(Class clazz) {
        if (clazz == null) return null;

        try {
            return clazz.newInstance();
        } catch (IllegalAccessException e) {
            Trace.get(e);
            return null;
        } catch (InstantiationException e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * Instantiate class with parameters
     *
     * @param clazz  Class
     * @param args   parameter types
     * @param params parameter
     *
     * @return Object
     */
    @SuppressWarnings("unchecked")
    public static Object instantiate(Class clazz, Class[] args, Object... params) {
        try {
            return clazz.getDeclaredConstructor(args).newInstance(params);
        } catch (Exception e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * Check if a method exists
     *
     * @param object Class instance
     * @param method method name
     *
     * @return boolean
     */
    public static boolean exists(Object object, String method) {
        try {
            object.getClass().getMethod(method);
            return true;
        } catch (NoSuchMethodException | SecurityException e) {
            // Não é necessário o trace da Exception, por precisar somente de uma confirmação
            return false;
        }
    }

    /**
     * Check if a method with parameters exists
     *
     * @param object Class instance
     * @param method method name
     * @param args   parameters
     *
     * @return boolean
     */
    public static boolean exists(Object object, String method, Object... args) { return _getMethod(object, method, args) != null; }

    /**
     * Call a class method
     *
     * @param object class
     * @param method method name
     *
     * @return Object
     */
    public static Object call(Object object, String method) {
        try {
            Method classMethod = object.getClass().getMethod(method);
            return classMethod.invoke(object);
        } catch (Exception e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * Call a class method, with parameters
     *
     * @param object class
     * @param method method name
     * @param args   parameters
     *
     * @return Object
     */
    public static Object call(Object object, String method, Object... args) {
        if (Check.isEmpty(args)) return call(object, method);
        Method classMethod = _getMethod(object, method, args);
        if (classMethod == null) {
            return null;
        } else {
            try {
                return classMethod.invoke(object, args);
            } catch (Exception e) {
                Trace.get(e);
                return null;
            }
        }
    }

    /**
     * Find method for execution
     *
     * @param object class
     * @param method method name
     * @param args   parameters
     *
     * @return Method
     */
    private static Method _getMethod(Object object, String method, Object... args) {
        try {
            Method classMethod = null;
            Method[] methods = object.getClass().getMethods();
            for (Method m : methods) {
                if (m.getName().equals(method) == false) continue; // If method name is not equal, ignore
                Class[] parameterTypes = m.getParameterTypes();
                if (parameterTypes.length != args.length) continue; // If method quantity is not equal, ignore
                int matches = 0;
                for (int key = 0; key < parameterTypes.length; key++) {
                    if (_isInstance(parameterTypes[key], args[key]) == false) continue; // If parameter is not equal, ignore
                    matches++;
                }
                if (parameterTypes.length != matches) continue; // If total of matched parameters is not equal, ignore
                classMethod = m;
                break; // If is here, is the method, and finish loop
            }
            return classMethod;
        } catch (Exception e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * Check parameter type of an object
     * Need method for primitive type comparation
     *
     * @param c class
     * @param o instance
     *
     * @return boolean
     */
    private static boolean _isInstance(Class c, Object o) {
        if (c.isPrimitive()) {
            if (c.equals(int.class)) {
                c = Integer.class;
            } else if (c.equals(float.class)) {
                c = Float.class;
            } else if (c.equals(long.class)) {
                c = Long.class;
            } else if (c.equals(double.class)) {
                c = Double.class;
            } else if (c.equals(boolean.class)) {
                c = Boolean.class;
            }
        }
        return c.isInstance(o);
    }
}