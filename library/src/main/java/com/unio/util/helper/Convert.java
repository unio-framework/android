package com.unio.util.helper;

import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import com.amulyakhare.textdrawable.TextDrawable;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.util.manager.ResourceManager;
import java.io.*;
import java.util.*;

/**
 * HConvert
 *
 * Helper class for object convertion
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 26/03/2014 19:26
 */
public class Convert
{
    /**
     * Convert any array to Object[], use for primitive arrays
     *
     * @param v value
     *
     * @return Object[]
     */
    public static Object[] toObjectArray(Object v) {
        Class c = v.getClass();
        if (c.equals(int[].class)) {
            int[] vv = (int[]) v;
            Object[] values = new Object[vv.length];
            for (int i = 0; i < vv.length; i++) values[i] = vv[i];
            return values;
        } else if (c.equals(float[].class)) {
            float[] vv = (float[]) v;
            Object[] values = new Object[vv.length];
            for (int i = 0; i < vv.length; i++) values[i] = vv[i];
            return values;
        } else if (c.equals(long[].class)) {
            long[] vv = (long[]) v;
            Object[] values = new Object[vv.length];
            for (int i = 0; i < vv.length; i++) values[i] = vv[i];
            return values;
        } else if (c.equals(double[].class)) {
            double[] vv = (double[]) v;
            Object[] values = new Object[vv.length];
            for (int i = 0; i < vv.length; i++) values[i] = vv[i];
            return values;
        } else if (c.equals(boolean[].class)) {
            boolean[] vv = (boolean[]) v;
            Object[] values = new Object[vv.length];
            for (int i = 0; i < vv.length; i++) values[i] = vv[i];
            return values;
        } else {
            return (Object[]) v;
        }
    }

    /**
     * Convert an Array/List to String, with ", " separator
     *
     * @param v value
     *
     * @return String
     */
    public static String toString(Object v) { return toString(v, ", "); }

    /**
     * Convert an Array/List to String
     *
     * @param v         value
     * @param delimiter each item separator
     *
     * @return String
     */
    public static String toString(Object v, String delimiter) {
        if (v == null) return null;
        if (v instanceof List) {
            return _listToString((List) v, delimiter);
        } else if (v.getClass().isArray()) {
            return _arrayToString(toObjectArray(v), delimiter);
        }
        return v.toString();
    }

    /**
     * Convert byte to hexadecimal
     *
     * @param bytes byte
     *
     * @return String
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuilder stringBuffer = new StringBuilder();
        for (byte aByte : bytes) stringBuffer.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
        return stringBuffer.toString();
    }

    /**
     * Convert byte to String
     *
     * @param bytes byte
     *
     * @return String
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    public static String byteToString(byte[] bytes) {
        try {
            return new String(bytes, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * Convert String to char array
     *
     * @param text text
     *
     * @return String[]
     */
    public static String[] stringToCharArray(String text) { return text.split("(?<!^)"); }

    /**
     * Convert String to char array, with delimiter
     *
     * @param text      text
     * @param delimiter each item separator
     *
     * @return String[]
     */
    public static String[] stringToCharArray(String text, int delimiter) {
        List<String> result = new ArrayList<>();
        StringBuilder buffer = new StringBuilder();
        String[] splited = stringToCharArray(text);
        for (String aSplited : splited) {
            buffer.append(aSplited);
            if (buffer.length() == delimiter) {
                result.add(buffer.toString());
                buffer = new StringBuilder();
            }
        }
        if (Check.isEmpty(buffer.toString()) == false) result.add(buffer.toString());
        return result.toArray(new String[0]);
    }

    /**
     * Convert icon to drawable
     *
     * @param ic icon
     *
     * @return Drawable
     */
    public static Drawable iconToDrawable(IconCharacter ic) { return iconToDrawable(ic, Color.BLACK); }

    /**
     * Convert icon to drawable
     *
     * @param ic icon
     *
     * @return Drawable
     */
    public static Drawable iconToDrawable(IconCharacter ic, int color) { return iconToDrawable(ic, color, Layouts.asSp(25)); }

    /**
     * Convert icon to drawable
     *
     * @param ic icon
     *
     * @return Drawable
     */
    public static Drawable iconToDrawable(IconCharacter ic, int color, int fontSize) {
        return TextDrawable.builder()
            .beginConfig()
            .fontSize(fontSize)
            .useFont(ic.typeface())
            .endConfig()
            .buildRect(String.valueOf(ic.icon()), color);
    }

    /**
     * Convert dp to px
     *
     * @param dp dp
     *
     * @return int
     */
    public static int dpToPx(int dp) {
        DisplayMetrics displayMetrics = ResourceManager.get().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    /**
     * Convert px to dp
     *
     * @param px px
     *
     * @return int
     */
    public static int pxToDp(int px) {
        DisplayMetrics displayMetrics = Unio.app().getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    /**
     * Retorn a formatted text, with conditional check if is singular or plural
     * Use variable {{total}} to set number
     *
     * @param total      total to compare
     * @param ifSingular text if singular
     * @param ifPlural   text if plural
     *
     * @return String, empty String if text is empty
     */
    public static String singularOrPlural(int total, String ifSingular, String ifPlural) {
        if (Check.isEmpty(ifSingular) && Check.isEmpty(ifPlural)) {
            return "";
        } else {
            String formattedSingular = Check.isEmpty(ifSingular) ? "" : ifSingular.replace("{{total}}", String.valueOf(total));
            String formattedPlural   = Check.isEmpty(ifPlural)   ? "" : ifPlural.replace("{{total}}", String.valueOf(total));
            if (total == 1) {
                return formattedSingular;
            } else {
                return formattedPlural;
            }
        }
    }

    /**
     * Convert InputStream to String
     *
     * @param is stream
     *
     * @return String
     */
    public static String streamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder response = new StringBuilder();
        String line;

        try {
            while ((line = reader.readLine()) != null) response.append(line).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return response.toString();
    }

    /**
     * Convert String to byte array
     *
     * @param value value
     *
     * @return byte[]
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    public static byte[] stringToByte(String value) {
        if (value == null) value = "";
        try {
            return value.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            Trace.get(e);
            return new byte[0];
        }
    }

    /**
     * Convert List to String
     *
     * @param list      List
     * @param delimiter each item separator
     *
     * @return String
     */
    private static String _listToString(List list, String delimiter) {
        Object[] array = list.toArray(new Object[0]);
        return toString(array, delimiter);
    }

    /**
     * Convert Object[] to String, with delimiter
     *
     * @param array     array
     * @param delimiter each item separator
     *
     * @return String
     */
    private static String _arrayToString(Object[] array, String delimiter) {
        StringBuilder finalValue = new StringBuilder();
        int key = 0;
        int total = array.length-1;
        for (Object value : array) {
            finalValue.append(toString(value, delimiter));
            if (key < total) finalValue.append(delimiter);
            key++;
        }
        return finalValue.toString();
    }
}