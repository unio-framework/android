package com.unio.util.helper;

import android.app.Activity;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.unio.util.manager.ActivityManager;

/**
 * Layouts
 *
 * Methods and attributes for layout
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class Layouts
{
    /**
     * Calcule device inch
     *
     * @return double
     */
    private static double _getDeviceInch() {
        DisplayMetrics metrics = new DisplayMetrics();
        Activity activity = ActivityManager.get();
        if (activity == null) return 0;
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float yInches = metrics.heightPixels/metrics.ydpi;
        float xInches = metrics.widthPixels/metrics.xdpi;
        return Math.sqrt(xInches*xInches + yInches*yInches);
    }

    /**
     * Get SW for version after HoneyComb MR1
     *
     * @return float
     */
    private static float _getSmallestScreenWidthDp() { return Resources.getSystem().getConfiguration().smallestScreenWidthDp; }

    /** Get DP value */
    public static final float DP = Resources.getSystem().getDisplayMetrics().density;

    /** Get device inch */
    public static final double DEVICE_INCH = _getDeviceInch();

    /** Get SP value */
    public static final float SP = Resources.getSystem().getDisplayMetrics().scaledDensity;

    /** Get SW value */
    public static final float SW = _getSmallestScreenWidthDp();

    /**
     * Get value as DP
     *
     * @param v value
     *
     * @return int
     */
    public static int asDp(double v) { return (int) (v * DP); }

    /**
     * Get value as SP
     *
     * @param v value
     *
     * @return int
     */
    public static int asSp(double v) { return (int) (v * SP); }
}