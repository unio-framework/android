package com.unio.util.helper;

import android.support.annotation.NonNull;

/**
 * HColor
 *
 * Helper and extension of {@link android.graphics.Color}
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 26/03/2015 0:01
 */
public class Color extends android.graphics.Color
{
    public static final int SPLASH_BACKGROUND = parseColor("#384E80");
    public static final int WORD_BLUE         = parseColor("#2C579B");
    public static final int SEPARATOR_GRAY    = parseColor("#D3D3D3");

    public static final int VIVID_GREEN = parseColor("#009100");
    public static final int VIVID_BLUE  = parseColor("#346BB6");
    public static final int VIVID_RED   = parseColor("#D04735");

    public static final int HOLO_BLUE_DARK  = parseColor("#FF0099CC");
    public static final int HOLO_BLUE_LIGHT = parseColor("#FF33B5E5");
    public static final int DARKER_GRAY     = parseColor("#AAAAAA");

    public static final int MATERIAL_DARK  = 0x4dffffff;
    public static final int MATERIAL_LIGHT = 0x1f000000;

    /**
     * Parse color with alpha in float percent
     *
     * @param color color
     * @param alpha alpha
     *
     * @return int
     */
    public static int parseColor(@NonNull String color, double alpha) {
        if (Check.isEmpty(color)) return WHITE;
        if (color.charAt(0) != '#' || color.length() > 7) return parseColor(color);
        String alphaHex = getHexAlpha(alpha);
        color = color.replace("#", "#"+alphaHex);
        return parseColor(color);
    }

    /**
     * Load hex equivalent of an alpha value
     *
     * @param v value
     *
     * @return String
     */
    public static String getHexAlpha(double v) {
        if (v > 1) {
            return getHexAlpha(1);
        } else if (v < 0) {
            return getHexAlpha(0);
        } else {
            v = Math.round(v * 100) / 100.0d;
            int alpha = (int) Math.round(v * 255);
            String hex = Integer.toHexString(alpha).toUpperCase();
            if (hex.length() == 1) hex = "0" + hex;
            return hex;
        }
    }
}