package com.unio.util.helper;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.unio.util.manager.ActivityManager;
import com.unio.util.manager.ResourceManager;
import com.unio.R;

/**
 * HAlert
 *
 * Class for call simple alert dialog
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Alert
{
    /***********************************************************/
    /**                          Basic                        **/
    /***********************************************************/

    /**
     * Shortcut of dialog for success
     *
     * @param message dialog message
     */
    public static void success(String message) { dialog(R.string.success, message); }

    /**
     * Shortcut of dialog for success
     *
     * @param message  dialog message
     * @param callback onClick callback
     */
    public static void success(String message, DialogInterface.OnClickListener callback) { dialog(R.string.success, message, callback); }

    /**
     * Shortcut of dialog for warning
     *
     * @param message dialog message
     */
    public static void warning(String message) { dialog(R.string.warning, message); }

    /**
     * Shortcut of dialog for warning
     *
     * @param message  dialog message
     * @param callback onClick callback
     */
    public static void warning(String message, DialogInterface.OnClickListener callback) { dialog(R.string.warning, message, callback); }

    /**
     * Shortcut of dialog for error
     *
     * @param message dialog message
     */
    public static void error(String message) { dialog(R.string.error, message); }

    /**
     * Shortcut of dialog for error
     *
     * @param message  dialog message
     * @param callback onClick callback
     */
    public static void error(String message, DialogInterface.OnClickListener callback) { dialog(R.string.error, message, callback); }

    /**
     * Shortcut of dialog
     *
     * @param titleId resource ID of title
     * @param message dialog message
     */
    public static void dialog(int titleId, String message) {
        String title = ResourceManager.getString(titleId);
        dialog(title, message);
    }

    /**
     * Shortcut of dialog
     *
     * @param title   dialog title
     * @param message dialog message
     */
    public static void dialog(String title, String message) { dialog(title, message, null); }

    /**
     * Shortcut of dialog
     *
     * @param titleId  resource ID of title
     * @param message  dialog message
     * @param callback onClick callback
     */
    public static void dialog(int titleId, String message, DialogInterface.OnClickListener callback) {
        String title = ResourceManager.getString(titleId);
        dialog(title, message, callback);
    }

    /**
     * Shortcut of dialog
     *
     * @param title   dialog title
     * @param message dialog message
     * @param callback onClick callback
     */
    public static void dialog(String title, String message, DialogInterface.OnClickListener callback) {
        if (callback == null) {
            callback = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) { dialog.dismiss(); }
            };
        }

        new AlertDialog.Builder(ActivityManager.get())
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, callback)
            .show();
    }

    /*************************************************/
    /**                    Toast                    **/
    /*************************************************/

    /**
     * Create tooltip like alert
     *
     * @param message message
     */
    public static void toast(String message) { toast(message, Gravity.TOP | Gravity.CENTER_HORIZONTAL); }

    /**
     * Create tooltip like alert
     *
     * @param message message
     * @param gravity position of alert
     */
    public static void toast(String message, int gravity) { toast(ActivityManager.get(), message, gravity); }

    /**
     * Create tooltip like alert
     *
     * @param context activity
     * @param message message
     * @param gravity position of alert
     */
    public static void toast(Context context, String message, int gravity) {
        Toast toast = new Toast(context);

        TextView layout = new TextView(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        layout.setLayoutParams(params);
        layout.setBackgroundColor(Color.parseColor("#444844"));
        layout.setPadding((int) (10 * Layouts.DP), (int) (5 * Layouts.DP), (int) (10 * Layouts.DP), (int) (5 * Layouts.DP));
        layout.setTextColor(Color.WHITE);
        layout.setText(message);

        toast.setView(layout);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setGravity(gravity, 0, 10);
        toast.show();
    }

    /*************************************************/
    /**                   SnackBar                  **/
    /*************************************************/

    /**
     * Create simple SnackBar
     *
     * @param message message
     */
    public static void snack(@NonNull View view, String message) { snack(view, message, null, null); }

    /**
     * Create simple SnackBar
     *
     * @param message message
     * @param onClick click action
     */
    public static void snack(@NonNull View view, String message, View.OnClickListener onClick) { snack(view, message, null, onClick); }

    /**
     * Create simple SnackBar
     *
     * @param message message
     * @param button  button name
     */
    public static void snack(@NonNull View view, String message, String button) { snack(view, message, button, null); }

    /**
     * Create simple SnackBar
     *
     * @param message message
     * @param button  button name
     * @param onClick click action
     */
    public static void snack(@NonNull View view, String message, String button, View.OnClickListener onClick) {
        Snackbar d = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        if (Check.isEmpty(button) && onClick != null) {
            button = view.getResources().getString(android.R.string.ok);
            d.setAction(button, onClick);
        }
        d.show();
    }
}