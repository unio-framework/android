package com.unio.util.helper;

import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.unio.shortcuts.Android;
import com.unio.ui.features.fonts.base.IconCharacter;
import com.unio.util.manager.ActivityManager;

/**
 * Views
 *
 * {@link View} helpers
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 20/07/2015 18:33
 */
public class Views
{
    /**
     * HTML format, using compatibility
     *
     * @param source string source
     *
     * @return Spanned
     */
    @SuppressWarnings("deprecation")
    public static Spanned htmlFormat(String source) {
        if (Android.CURRENT_VERSION > Android.M) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    /**
     * Add IconCharacter for MenuItem
     *
     * @param item MenuItem
     * @param ic   IconCharacter
     *
     * @return MenuItem
     */
    public static MenuItem setMenuFontIcon(MenuItem item, IconCharacter ic) { return item.setIcon(Convert.iconToDrawable(ic)); }

    /**
     * Add IconCharacter for MenuItem
     *
     * @param item  MenuItem
     * @param ic    IconCharacter
     * @param color icon color
     *
     * @return MenuItem
     */
    public static MenuItem setMenuFontIcon(MenuItem item, IconCharacter ic, int color) {
        return item.setIcon(Convert.iconToDrawable(ic, color));
    }

    /**
     * Add IconCharacter for MenuItem
     *
     * @param item  MenuItem
     * @param ic    IconCharacter
     * @param color icon color
     *
     * @return MenuItem
     */
    public static MenuItem setMenuFontIcon(MenuItem item, IconCharacter ic, int color, int fontSize) {
        return item.setIcon(Convert.iconToDrawable(ic, color, fontSize));
    }

    /**
     * Set drawable background
     *
     * @param v View
     * @param d Drawable
     */
    @SuppressWarnings("deprecation")
    public static void setBackground(View v, Drawable d) {
        if (Android.CURRENT_VERSION > Android.ICE_CREAM_SANDWICH_MR1) {
            v.setBackground(d);
        } else {
            v.setBackgroundDrawable(d);
        }
    }

    /**
     * setTextAppearance
     */
    @SuppressWarnings("deprecation")
    public static void setTextAppearance(TextView v, int resId) {
        if (Android.CURRENT_VERSION > Android.LOLLIPOP_MR1) {
            v.setTextAppearance(resId);
        } else {
            v.setTextAppearance(ActivityManager.get(), resId);
        }
    }
}