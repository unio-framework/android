package com.unio.util.helper;

import android.util.Base64;
import com.unio.debug.Trace;
import com.unio.util.statics.exceptions.HashGenerationException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * HEncrypt
 *
 * Helper class for encryption
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 09/09/2015 18:27
 */
public class Encrypt
{
    /**
     * Encode to Base64
     *
     * @param value value
     *
     * @return String
     */
    public static String base64(String value) {
        byte[] v = Convert.stringToByte(value);
        return Base64.encodeToString(v, Base64.NO_WRAP);
    }

    /**
     * Decode from base64
     *
     * @param value value
     *
     * @return String
     */
    public static String base64Decode(String value) {
        byte[] v = Base64.decode(value, Base64.NO_WRAP);
        return Convert.byteToString(v);
    }

    /**
     * HMAC-SHA1 encrypt
     *
     * @param value value
     * @param key  key
     *
     * @return String
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    public static String hmacSha1(String value, String key) {
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secret = new SecretKeySpec(value.getBytes(), "HmacSHA1");
            mac.init(secret);
            byte[] digest = mac.doFinal(key.getBytes());
            return new String(Base64.encode(digest, Base64.DEFAULT), "UTF-8");
        } catch (Exception e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * MD5 encrypt
     *
     * @param message value
     *
     * @return String
     */
    public static String md5(String message) throws HashGenerationException { return _hashString(message, "MD5"); }

    /**
     * SHA-1 encrypt
     *
     * @param message value
     *
     * @return String
     */
    public static String sha1(String message) throws HashGenerationException { return _hashString(message, "SHA-1"); }

    /**
     * SHA-256 encrypt
     *
     * @param message value
     *
     * @return String
     */
    public static String sha256(String message) throws HashGenerationException { return _hashString(message, "SHA-256"); }

    /**
     * Encrypt
     *
     * @param message   value
     * @param algorithm encrypt type
     *
     * @return String
     */
    @SuppressWarnings("CharsetObjectCanBeUsed")
    private static String _hashString(String message, String algorithm) throws HashGenerationException {
        if (message == null) return null;
        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            byte[] hashedBytes = digest.digest(message.getBytes("UTF-8"));
            return Convert.bytesToHex(hashedBytes);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
            throw new HashGenerationException("Could not generate hash from String", ex);
        }
    }
}