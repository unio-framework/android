package com.unio.util.helper;

import android.content.res.Resources;
import android.graphics.Point;
import android.view.Display;
import com.unio.util.manager.ActivityManager;

/**
 * Screen
 *
 * Screen element helper
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 08/11/2014 19:32
 */
public class Screen
{
    /**
     * Get current screen orientation
     *
     * @return int
     */
    public static int getOrientation() { return ActivityManager.get().getResources().getConfiguration().orientation; }

    /**
     * The screen height
     *
     * @return int
     */
    public static int getHeight() {
        Display display = ActivityManager.get().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    /**
     * Get status bar height
     *
     * @return int
     */
    public static int getStatusBarHeight() {
        int result = 0;
        int resourceId = Resources.getSystem().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) result = Resources.getSystem().getDimensionPixelSize(resourceId);
        return result;
    }

    /**
     * Get screen width
     *
     * @return int
     */
    public static int getWidth() {
        Display display = ActivityManager.get().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }
}