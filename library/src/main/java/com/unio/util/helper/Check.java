package com.unio.util.helper;

import com.unio.util.io.Hash;
import com.unio.util.io.XML;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.List;
import java.util.Map;

/**
 * Check
 *
 * Helper class for value check
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Check
{
    /**
     * Check if is empty
     *
     * @param value value
     *
     * @return boolean
     */
    public static boolean isEmpty(Object value) {
        if (value == null) return true;

        if (value instanceof CharSequence) {
            CharSequence c = (CharSequence) value;
            return c.equals("") || c.length() == 0;
        } else if (value instanceof Hash) {
            return ((Hash) value).length() == 0;
        } else if (value instanceof Map) {
            return ((Map) value).size() == 0;
        } else if (value instanceof Iterable) {
            int total = 0;
            for (Object o : (Iterable) value) total++;
            return total == 0;
        } else if (value.getClass().isArray()) {
            if (value instanceof int[]) {
                return ((int[]) value).length == 0;
            } else if (value instanceof float[]) {
                return ((float[]) value).length == 0;
            } else if (value instanceof double[]) {
                return ((double[]) value).length == 0;
            } else {
                return ((Object[]) value).length == 0;
            }
        } else if (value instanceof JSONObject) {
            JSONObject v = (JSONObject) value;
            return v != null && v.length() == 0;
        } else if (value instanceof JSONArray) {
            JSONArray v = (JSONArray) value;
            return v != null && v.length() == 0;
        } else if (value instanceof Number) {
            return ((Number) value).intValue() == 0;
        } else {
            return value.equals("");
        }
    }

    /**
     * Check if 2 values is equal, using #equals method with null validation
     *
     * @param v1 base value
     * @param v2 compare value
     *
     * @return boolean
     */
    public static boolean isEqual(Object v1, Object v2) { return v1 != null && v1.equals(v2); }

    /**
     * Check if value is one of array item
     *
     * @param v1 base value
     * @param v2 values to compare
     *
     * @return boolean
     */
    public static boolean includes(Object v1, Object... v2) {
        for (Object v : v2)
            if (isEqual(v1, v))
                return true;
        return false;
    }

    /**
     * Check if is JSON
     *
     * @param value value
     *
     * @return boolean
     */
    public static boolean isJson(String value) {
        try {
            new JSONObject(value);
            return true;
        } catch(JSONException e) {
            try {
                new JSONArray(value);
                return true;
            } catch (JSONException e2) {
                return false;
            }
        }
    }

    /**
     * Shortcut to check if variable is not equal
     *
     * @param v1 base value
     * @param v2 compare value
     *
     * @return boolean
     */
    public static boolean isNotEqual(Object v1, Object v2) { return isEqual(v1, v2) == false; }

    /**
     * Shortcut to check if variable is not null and is not empty
     *
     * @param value value to check
     *
     * @return boolean
     */
    public static boolean isPresent(Object value) { return isEmpty(value) == false; }

    /**
     * Check if is XML
     *
     * @param value value
     *
     * @return boolean
     */
    public static boolean isXml(String value) { return XML.parse(value) != null; }

    /**
     * Check if is a valid url
     * Determine as valid url:
     * - http://
     * - https://
     * - www.
     *
     * @param value url
     *
     * @return boolean
     */
    public static boolean isUrl(String value) {
        return value != null &&
            value.matches("((([A-Za-z]{3,9}:(?://)?)(?:[-;:&=+$,\\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=+$,\\w]+@)[A-Za-z0-9.-]+)((?:/[+~%/.\\w-_]*)?\\??(?:[-+=&;%@.\\w_]*)#?(?:[\\w]*))?)");
    }

    /**
     * Check if has array index
     * List or Array Object doesn't has this check, for these check if total size is bigger than current position
     *
     * @param v        value
     * @param position position
     *
     * @return boolean
     */
    public static boolean hasPosition(Object v, int position) {
        if (v instanceof List) {
            return !(isEmpty(v) || ((List)v).size() < position);
        } else if (v.getClass().isArray()) {
            if (v instanceof int[]) {
                int[] a = (int[]) v;
                return !(isEmpty(a) || a.length < position);
            } else if (v instanceof float[]) {
                float[] a = (float[]) v;
                return !(isEmpty(a) || a.length < position);
            } else if (v instanceof double[]) {
                double[] a = (double[]) v;
                return !(isEmpty(a) || a.length < position);
            } else {
                Object[] a = (Object[]) v;
                return !(isEmpty(a) || a.length < position);
            }
        }
        return false;
    }
}