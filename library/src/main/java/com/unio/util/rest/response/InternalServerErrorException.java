package com.unio.util.rest.response;

/**
 * InternalServerErrorException
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class InternalServerErrorException extends Exception
{
    public InternalServerErrorException(String info) { super(info); }
}