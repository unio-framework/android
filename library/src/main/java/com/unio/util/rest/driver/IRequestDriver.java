package com.unio.util.rest.driver;

import com.unio.util.helper.Check;
import com.unio.util.io.File;
import com.unio.util.io.Hash;
import com.unio.util.rest.Http;
import com.unio.util.rest.response.AuthenticationException;
import com.unio.util.rest.response.InternalServerErrorException;
import com.unio.util.rest.response.MethodNotAllowedException;
import com.unio.util.rest.response.HttpResponse;
import java.io.InputStream;

/**
 * IRequestDriver
 *
 * Interface to enable to execute {@link Http} using a specific HTTP request module
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 21/07/2016 16:53
 */
public abstract class IRequestDriver
{
    /** Response code */
    protected int responseCode;

    /**
     * Execution
     *
     * @param request Http
     *
     * @return WSResponse
     * @throws Exception any request error
     */
    public abstract HttpResponse execute(Http request) throws Exception;

    /**
     * Get request error, if throws Exception
     *
     * @return String
     */
    public abstract String serverError();

    /**
     * Cancel request
     */
    public abstract void cancel();

    /**
     * Check if request is in execution
     *
     * @return boolean
     */
    public abstract boolean inProgress();

    /**
     * Get request response code
     *
     * @return int
     */
    public int responseCode() { return responseCode; }

    /**
     * Validate request code
     *
     * @param code       code
     * @throws Exception Exception if is not 200
     */
    protected void validateRequestCode(int code) throws Exception {
        switch (code) {
            case 401: throw new AuthenticationException("401 Unauthorized");
            case 405: throw new MethodNotAllowedException("405 Method Not Allowed");
            case 500: throw new InternalServerErrorException("500 Internal Server Error");
            default:
        }
    }

    /**
     * Prepare response on download case
     *
     * Returns:
     * {
     *     "file":"...",
     *     "content":"..."
     * }
     *
     * O desenvolvedor que usar, deverá checar o código de requisição, juntamente com o conteúdo retornado
     * Irão existir situações onde não retorna um código de erro, retornando uma tela simples de html
     * Por isso fica a cargo do desenvolvedor fazer as devidas checagens e determinar como usará o método de DOWNLOAD
     *
     * @param streamResponse response in InputStream
     *
     * @return String, response in jsonString
     */
    protected String writeFile(InputStream streamResponse, String savePath) {
        File file = new File(savePath);
        String path = file.save(streamResponse) ? file.path : "";
        String content = Check.isEmpty(path) ? "" : file.content();

        Hash response = new Hash();
        response.put("file", path);
        response.put("size", String.valueOf(file.length()));
        response.put("content", content);
        return response.toJson().toString();
    }
}