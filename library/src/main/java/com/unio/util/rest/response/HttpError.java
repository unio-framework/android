package com.unio.util.rest.response;

/**
 * HttpError
 *
 * Format request error
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class HttpError
{
    /** Attributes **/
    public final Class<? extends Exception> exception;
    public final String exceptionMessage;
    public final String message;

    /**
     * Constructor
     *
     * @param e Exception
     * @param serverErrorMessage server returned error message
     */
    public HttpError(Exception e, String serverErrorMessage) {
        this.exception        = e.getClass();
        this.exceptionMessage = e.getMessage();
        this.message          = serverErrorMessage;
    }

    /**
     * Format all to String
     *
     * @return String
     */
    @Override
    public String toString() {
        return "exception: " + this.exception +
            "\n" +
            "exceptionMessage: " + this.exceptionMessage +
            "\n" +
            "message: " + this.message;
    }
}