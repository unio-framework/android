package com.unio.util.rest;

import android.support.annotation.IntDef;
import android.support.annotation.StringDef;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.util.io.Hash;
import com.unio.util.rest.driver.HttpNativeDriver;
import com.unio.util.rest.driver.IRequestDriver;
import com.unio.util.rest.response.*;
import com.unio.util.statics.constants.EncodeType;
import com.unio.util.statics.interfaces.IAsync;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.*;

/**
 * Http
 *
 * Unio universal HTTP requests
 *
 * Need permission:
 *   <uses-permission android:name="android.permission.INTERNET" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Http extends IAsync
{
    /** Application default save directory */
    public static String APP_DATA_PATH = "Android/data/" + Unio.app().getPackageName();

    /** Methods */
    public static final String METHOD_GET    = "GET";
    public static final String METHOD_POST   = "POST";
    public static final String METHOD_PUT    = "PUT";
    public static final String METHOD_DELETE = "DELETE";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({ METHOD_GET, METHOD_POST, METHOD_PUT, METHOD_DELETE })
    public @interface Method {}

    /** Methods */
    public static final int TYPE_IGNORE   =  0; // To customize with own header
    public static final int TYPE_JSON     =  1;
    public static final int TYPE_XML      =  2;
    public static final int TYPE_AJAX     =  4;
    public static final int TYPE_DOWNLOAD =  8;
    public static final int TYPE_UPLOAD   = 16;
    public static final int TYPE_FILE     = 32;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ TYPE_IGNORE, TYPE_JSON, TYPE_XML, TYPE_AJAX, TYPE_DOWNLOAD, TYPE_UPLOAD, TYPE_FILE })
    public @interface Type {}

    /**
     * CNReserved
     *
     * Constants for identify some params
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 11/03/2015 20:03
     */
    public static final class CNReserved
    {
        public static final String HEADER_AUTHENTICATION = "__AUTHENTICATION__";
        public static final String PARAM_SINGLE          = "__SINGLE__";
    }

    /** Params **/
    @Type   private int _type = TYPE_JSON;
    @Method private String _method;
    private int _timeout   = 10000;
    private String _url;
    private String _downloadPath;
    private String _encode = EncodeType.UTF_8;
    private IRequestDriver _driver;

    /** Params store **/
    private Map<String, Object> _params;
    private Map<String, Object> _headers;
    private List<String> _files;

    /** Callbacks **/
    private List<IOnFinish> _onFinishes = new LinkedList<>();

    /** Request's responses **/
    private HttpResponse _Response;
    private HttpError _Error;

    /** Flags */
    private boolean _trace = true;
    private boolean _success = false;

    /**
     * Constructor
     *
     * @param url url
     */
    public Http(String url) { this(METHOD_GET, url); }

    /**
     * Constructor with custom method
     *
     * @param method method, see {@link Method}
     * @param url    url
     */
    public Http(@Method String method, String url) {
        this._method  = method;
        this._url     = url;
        this._params  = new HashMap<>();
        this._headers = new HashMap<>();
        this._files   = new ArrayList<>();
        this._driver = new HttpNativeDriver();
    }

    /**
     * Set a request type, for automatic header settings
     *
     * @param type type, see {@link Type}
     *
     * @return Http
     */
    public Http setType(@Type int type) {
        this._type = type;
        return this;
    }

    /**
     * Set request timeout
     *
     * @param timeout timeout in miliseconds
     *
     * @return Http
     */
    public Http setTimeout(int timeout) {
        this._timeout = timeout;
        return this;
    }

    /**
     * Set an encode, default is {@link EncodeType#UTF_8}
     *
     * @param encode encode
     *
     * @return Http
     */
    public Http setEncode(String encode) {
        this._encode = encode;
        return this;
    }

    /**
     * Set a custom driver
     *
     * @param driver IRequestDriver implemented class
     *
     * @return Http
     */
    public Http setDriver(IRequestDriver driver) {
        this._driver = driver;
        return this;
    }

    /**
     * Add a header
     *
     * @param name  identification
     * @param value value
     *
     * @return Http
     */
    @SuppressWarnings("UnusedReturnValue")
    public Http addHeader(String name, Object value) {
        this._headers.put(name, value);
        return this;
    }

    /**
     * Add authentication
     *
     * @param username username
     * @param password password
     *
     * @return Http
     */
    public Http setAuthentication(String username, String password) {
        this._headers.put(CNReserved.HEADER_AUTHENTICATION, new String[] { username, password });
        return this;
    }

    /**
     * Add a param
     *
     * @param name  name
     * @param value value
     *
     * @return Http
     */
    public Http addParam(String name, Object value) {
        this._params.put(name, value);
        return this;
    }

    /**
     * Add a single param
     * If this is added, other key=value param will be ignored
     *
     * @param value value
     *
     * @return Http
     */
    public Http addParam(Object value) {
        if (value instanceof Hash) value = ((Hash)value).toJson();
        this._params.put(CNReserved.PARAM_SINGLE, value);
        return this;
    }

    /**
     * Get a specific param
     * To get single param use {@link CNReserved#PARAM_SINGLE}
     *
     * @param name name
     *
     * @return Object
     */
    public Object getParam(String name) { return this.hasParam(name) ? this._params.get(name) : null; }

    /**
     * Check if param exists
     *
     * @param name name
     *
     * @return boolean
     */
    public boolean hasParam(String name) { return this._params.containsKey(name); }

    /**
     * Add a file
     *
     * @param fileName file path
     *
     * @return Http
     */
    public Http addFile(String fileName) {
        this._files.add(fileName);
        if (this._type != TYPE_UPLOAD) this.setType(TYPE_UPLOAD);
        return this;
    }

    /**
     * Set path to save downloaded file
     *
     * @param path path
     *
     * @return Http
     */
    public Http setDownloadPath(String path) {
        this._downloadPath = path;
        return this;
    }

    /**
     * Add callback actions when request finish
     *
     * @param callback callback
     *
     * @return Http
     */
    public Http addOnFinish(IOnFinish callback) {
        if (callback != null) this._onFinishes.add(callback);
        return this;
    }

    /**
     * Enable {@link Trace#send(Throwable)}
     *
     * @return Http
     */
    public Http enableTrace(boolean enable) {
        this._trace = enable;
        return this;
    }

    /**
     * Get HttpResponse
     *
     * @return HttpResponse
     */
    public HttpResponse response() { return this._Response; }

    /**
     * Get HttpError
     *
     * @return HttpError
     */
    public HttpError error() { return this._Error; }

    /**
     * Check if request complete successfully
     *
     * @return boolean
     */
    public boolean isSuccess() { return this._success; }

    /**
     * Execute the request
     *
     * @param method REST method
     */
    @Deprecated
    public void execute(String method) { this.execute(method, EExecution.ASYNCHRONOUS); }

    /**
     * Execute with method and type
     *
     * @param method REST method
     * @param type   execution type. See {@link com.unio.util.statics.interfaces.IAsync.EExecution}
     */
    @Deprecated
    public void execute(String method, EExecution type) {
        this._method = method;
        this.execute(type);
    }

    /**
     * Cancel request
     */
    public void cancel() { this._driver.cancel(); }

    /**
     * Check if request is in execution
     *
     * @return boolean
     */
    public boolean inProgress() { return this._driver.inProgress(); }

    /**
     * Execution
     */
    @Override
    protected void onInit() {
        this._success = true;
        try {
            this._Response = this._driver.execute(this);
            Trace.log(this, this.getUrl(), "result", this._Response);
        } catch (Exception e) {
            final String serverError = this._driver.serverError();
            final int responseCode   = this._driver.responseCode();
            this._Error    = new HttpError(e, serverError);
            Trace.log(this, this.getUrl(), "error", this._Error);
            if (this._trace) {
                Hash message = new Hash();
                message.put("url", this._url);
                message.put("method", this._method);
                if (this._method.equals(METHOD_POST)) {
                    String formattedParams = this._formatParams();
                    message.put("params", formattedParams);
                }
                message.put("response", serverError);
                Trace.get(e, message.toJson().toString());
            }
            this._Response = new HttpResponse(responseCode, serverError); // Para poder trabalhar com o código de rasposta
            this._success  = false;
        }
        if (this._onFinishes.size() > 0) {
            for (IOnFinish callback : this._onFinishes)
                callback.onRequestFinish(this._success, this);
        }
    }

    /**
     * Prepare post/get params
     */
    private String _formatParams() {
        final Map<String, Object> params = this._params;
        if (params.containsKey(Http.CNReserved.PARAM_SINGLE)) {
            return params.get(Http.CNReserved.PARAM_SINGLE).toString();
        } else if (params.size() > 0) {
            StringBuilder combinedParams = new StringBuilder();
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                Object v = entry.getValue();
                String value;
                if (v == null) {
                    value = ""; // Para não ter problema do NullPointerException
                } else if (v instanceof Object[]) {
                    value = Arrays.asList((Object[])v).toString();
                } else {
                    value = v.toString();
                }
                String paramString = entry.getKey() + "=" + value;
                combinedParams.append(combinedParams.length() > 1 ? "&" + paramString : paramString);
            }
            return combinedParams.toString();
        }
        return "";
    }

    /** Get url */
    public String getUrl() { return this._url; }

    /** Get method */
    public String getMethod() { return this._method; }

    /** Get request type */
    public int getType() { return this._type; }

    /** Get request encode */
    public String getEncode() { return this._encode; }

    /** Get request timeout */
    public int getTimeout() { return this._timeout; }

    /** Get headers */
    public Map<String, Object> getHeaders() { return this._headers; }

    /**
     * Get all params
     * For single line param, identify using {@link CNReserved#PARAM_SINGLE}
     */
    public Map<String, Object> getParams() { return this._params; }

    /** Get files path to upload */
    public List<String> getFiles() { return this._files; }

    /** Get download file destination path */
    public String getDownloadPath() { return this._downloadPath; }

    /**
     * IOnFinish
     *
     * Default callback when request finish
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 28/06/2014 14:20
     */
    public interface IOnFinish { void onRequestFinish(boolean success, Http request); }
}