package com.unio.util.rest.response;

/**
 * MethodNotAllowedException
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class MethodNotAllowedException extends Exception
{
    public MethodNotAllowedException(String info) { super(info); }
}