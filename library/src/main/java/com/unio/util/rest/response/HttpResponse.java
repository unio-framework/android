package com.unio.util.rest.response;

import com.unio.util.io.Hash;
import com.unio.util.helper.Check;

/**
 * HttpResponse
 *
 * {@link com.unio.util.rest.Http} request response
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class HttpResponse
{
    /** StatusCode */
    public final int code;

    /** Responses */
    private String _stringResponse;
    private Hash _hashResponse;

    /**
     * Flag to prevent calling Hash only one time
     */
    private boolean _hashChecked = false;

    /**
     * Constructor
     *
     * @param stringResponse response as String
     */
    public HttpResponse(int statusCode, String stringResponse) {
        this.code = statusCode;
        this._stringResponse = stringResponse;
    }

    /**
     * Get answer as String
     *
     * @return String
     */
    @Override
    public String toString() { return this._stringResponse; }

    /**
     * Get answer as Hash formatted
     * {@link Hash} will convert XML and JSON, if is one of two
     *
     * @return Hash
     */
    public Hash toHash() {
        if (this._hashResponse == null && this._hashChecked == false) {
            this._hashResponse = new Hash(this._stringResponse);
            this._hashChecked = true;
        }
        return this._hashResponse;
    }

    /**
     * Shortcut to check if return is JSON
     *
     * @return boolean
     */
    public boolean isJson() { return Check.isJson(this.toString()); }

    /**
     * Shortcut to check if return is XML
     *
     * @return boolean
     */
    public boolean isXml() { return Check.isXml(this.toString()); }
}