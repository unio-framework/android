package com.unio.util.rest.driver;

import android.util.Base64;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.helper.Convert;
import com.unio.util.io.File;
import com.unio.util.rest.Http;
import com.unio.util.rest.response.HttpResponse;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * HttpNativeDriver
 *
 * Execute http request using Android native {@link HttpURLConnection}
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 21/07/2016 16:54
 */
public class HttpNativeDriver extends IRequestDriver
{
    /** Delimitadores para enviar arquivo **/
    private static final String TWO_HYPHENS = "--";
    private static final String BONDARY     = "*****";
    private static final String CRLF        = "\r\n";

    /** Http */
    private Http _request;
    private HttpURLConnection _httpRequest;

    /** All params formatted for http request **/
    private String _formattedParams;

    /**
     * Execute http request using Android native {@link HttpURLConnection}
     *
     * @param request Http
     *
     * @return WSResponse
     * @throws Exception any request error
     */
    @Override
    public HttpResponse execute(Http request) throws Exception {
        this._request = request;

        this._prepareType();
        this._prepareParams();
        String uri = this._request.getUrl();

        final String method = this._request.getMethod();
        final String encode = this._request.getEncode();
        final int timeout   = this._request.getTimeout();
        final int type      = this._request.getType();

        if (method.equals(Http.METHOD_GET) && Check.isEmpty(this._formattedParams) == false)
            uri += "?"+this._formattedParams;
        Trace.log(this, method, uri);

        URL url = new URL(uri);
        this._httpRequest = (HttpURLConnection) url.openConnection();
        this._httpRequest.setRequestMethod(method);
        if (timeout > 0) {
            this._httpRequest.setConnectTimeout(timeout);
            this._httpRequest.setReadTimeout(timeout);
        }
        this._httpRequest.setInstanceFollowRedirects(true);
        this._httpRequest.setUseCaches(false);
        this._httpRequest.setDoInput(true);

        // setDoOutput acaba considerando a requisição como POST, mesmo se setRequestMethod for GET
        if (method.equals(Http.METHOD_POST)) this._httpRequest.setDoOutput(true);

        Map<String, Object> headers = this._request.getHeaders();
        for (Map.Entry<String, Object> entry : headers.entrySet()) {
            if (Http.CNReserved.HEADER_AUTHENTICATION.equals(entry.getKey())) {
                String[] param = (String[]) entry.getValue();
                String userCredentials = param[0] + ":" + param[1];
                String basicAuth;
                // Versões iniciais do Android, testado no 2.3 (API10), não aceitam Base64.DEFAULT, por isso precisa manter com Base64.NO_WRAP
                basicAuth = "Basic " + Base64.encodeToString(userCredentials.getBytes(), Base64.NO_WRAP);
                this._httpRequest.setRequestProperty("Authorization", basicAuth);
            } else {
                this._httpRequest.setRequestProperty(entry.getKey(), entry.getValue().toString());
            }
        }

        String postParams = this._getPostParams();
        if (method.equals(Http.METHOD_POST) && Check.isEmpty(postParams) == false) {
            Trace.log(this, "post-params", this._formattedParams);
            OutputStream os = this._httpRequest.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, encode));
            writer.write(postParams);
            writer.flush();
            writer.close();
            os.close();
        }
        this._httpRequest.connect();

        this.responseCode = this._httpRequest.getResponseCode();
        this.validateRequestCode(this.responseCode);

        InputStream streamResponse = this._httpRequest.getInputStream();
        String resp;
        if (type == Http.TYPE_DOWNLOAD) {
            resp = this.writeFile(streamResponse, this._request.getDownloadPath());
        } else {
            resp = Convert.streamToString(streamResponse);
        }
        HttpResponse response = new HttpResponse(this.responseCode, resp.trim());
        streamResponse.close();
        this._httpRequest.disconnect();

        return response;
    }

    /**
     * Get request error, if throws Exception
     *
     * @return String
     */
    @Override
    public String serverError() {
        String serverError = "";
        if (this._httpRequest != null && this._httpRequest.getErrorStream() != null) serverError = Convert.streamToString(this._httpRequest.getErrorStream());
        return serverError;
    }

    /**
     * Cancel request
     */
    @Override
    public void cancel() { if (this.inProgress()) this._httpRequest.disconnect(); }

    /**
     * Check if request is in execution
     *
     * @return boolean
     */
    @Override
    public boolean inProgress() { return this._httpRequest != null; }

    /**
     * Define header default settings
     */
    private void _prepareType() {
        switch (this._request.getType()) {
            case Http.TYPE_JSON:
                this._request.addHeader("Cache-Control", "no-cache");
                this._request.addHeader("Connection", "Keep-Alive");
                this._request.addHeader("Content-Type", "application/json; charset="+this._request.getEncode().toLowerCase());
                break;
            case Http.TYPE_XML:
                this._request.addHeader("Content-Type", "application/xml; charset="+this._request.getEncode().toLowerCase());
                this._request.addHeader("Connection", "Keep-Alive");
                break;
            case Http.TYPE_DOWNLOAD:
                this._request.addHeader("Content-Type", "application/x-www-form-urlencoded");
                this._request.addHeader("charset", this._request.getEncode().toLowerCase());
                break;
            case Http.TYPE_UPLOAD:
                this._request.addHeader("Cache-Control", "no-cache");
                this._request.addHeader("Connection",    "Keep-Alive");
                this._request.addHeader("Content-Type",  "multipart/form-data; boundary=" + BONDARY);
        }
    }

    /**
     * Prepare post/get params
     *
     * @throws Exception any request error
     */
    private void _prepareParams() throws Exception {
        final Map<String, Object> params = this._request.getParams();
        if (params.containsKey(Http.CNReserved.PARAM_SINGLE)) {
            this._formattedParams = params.get(Http.CNReserved.PARAM_SINGLE).toString();
        } else if (params.size() > 0) {
            final String encode = this._request.getEncode();
            StringBuilder combinedParams = new StringBuilder();
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                Object v = entry.getValue();
                String value;
                if (v == null) {
                    value = ""; // Para não ter problema do NullPointerException
                } else if (v instanceof Object[]) {
                    value = Arrays.asList((Object[])v).toString();
                } else {
                    value = v.toString();
                }
                String paramString = entry.getKey() + "=" + URLEncoder.encode(value, encode);
                combinedParams.append(combinedParams.length() > 1 ? "&" + paramString : paramString);
            }
            this._formattedParams = combinedParams.toString();
        }
    }

    /**
     * Prepara POST para casos que incluam arquivos
     * PARAM_SINGLE não é considerado porque parâmetros primitivos são inválidos em requisições multipart
     * Ele poderá ser acessado pelo próprio nome do parâmetro (__SINGLE__)
     *
     * @return String
     * @throws Exception any request error
     */
    private String _getPostParams() throws Exception {
        if (this._request.getMethod().equals(Http.METHOD_GET)) return null;
        if (Check.isEmpty(this._request.getFiles())) return this._formattedParams;

        StringBuilder finalValue = new StringBuilder();
        final Map<String, Object> params = this._request.getParams();
        final String encode = this._request.getEncode();

        if (Check.isEmpty(params) == false) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                Object v = entry.getValue();
                String value;
                if (v == null) {
                    value = ""; // Para não ter problema do NullPointerException
                } else if (v instanceof Object[]) {
                    value = Arrays.asList((Object[])v).toString();
                } else {
                    value = v.toString();
                }
                String paramString = entry.getKey() + "=" + URLEncoder.encode(value, encode);

                // Formato para texto simples, são os campos do $_POST no PHP
                finalValue.append("--" + BONDARY);
                finalValue.append(CRLF);
                finalValue.append("Content-Disposition: form-data; name=\"").append(entry.getKey()).append("\"");
                finalValue.append(CRLF);
                finalValue.append("Content-Type: text/plain; charset=UTF-8");
                finalValue.append(CRLF);
                finalValue.append(CRLF);
                finalValue.append(URLEncoder.encode(value, encode));
                finalValue.append(CRLF);
            }
        }

        final List<String> files = this._request.getFiles();
        for (int key = 0; key < files.size(); key++) finalValue.append(this._formatFile(key, files.get(key)));

        return finalValue.toString();
    }

    /**
     * Format file content, adding $_FILES like and $_POST identification
     *
     * @param filePath file path
     *
     * @return String
     */
    private String _formatFile(int key, String filePath) {
        File file = new File(filePath);

        String fileName = file.name;
        String name     = fileName.split("\\.")[0];
        String value    = "";

        value += TWO_HYPHENS+BONDARY+CRLF;
        value += "Content-Disposition: form-data; name=\"__FILENAME__["+key+"][name]\""+CRLF;
        value += CRLF;
        value += fileName;
        value += CRLF;

        value += TWO_HYPHENS+BONDARY+CRLF;
        value += "Content-Disposition: form-data; name=\"__FILENAME__["+key+"][content]\""+CRLF;
        value += CRLF;
        value += Base64.encodeToString(file.contentAsByte(), Base64.NO_WRAP);
        value += CRLF;

        value += TWO_HYPHENS+BONDARY+CRLF;
        value += "Content-Disposition: form-data; name=\""+name+"\"; filename=\""+fileName+"\""+CRLF;
        value += "Content-Type: "+file.mime+CRLF+CRLF;
        value += CRLF;
        // File
        value += file.content();
        value += CRLF;
        value += TWO_HYPHENS+BONDARY+TWO_HYPHENS+CRLF;

        // Only for Trace.log visualization, for upload self._formattedParams will not be used
        this._formattedParams += "&__FILENAME__["+key+"]="+fileName;
        return value;
    }
}