package com.unio.util.rest.response;

/**
 * AuthenticationException
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class AuthenticationException extends Exception
{
    public AuthenticationException(String info) { super(info); }
}