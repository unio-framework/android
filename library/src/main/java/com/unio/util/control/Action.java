package com.unio.util.control;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import com.unio.R;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.util.manager.ActivityManager;
import com.unio.util.manager.ResourceManager;

/**
 * AppAction
 *
 * Intents, for integration with external apps
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class Action
{
    /**
     * Open Location Screen in Settings app
     */
    public static void openLocationSettings() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityManager.get().startActivity(intent);
        ActivityManager.get().finish();
    }

    /**
     * Open a mail app, with pre-defined params
     *
     * @param to      destination
     * @param subject subject
     */
    public static void openMail(String to, String subject) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{ to });
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        try {
            String title = ResourceManager.getString(R.string.redirect_to_app);
            ActivityManager.get().startActivity(Intent.createChooser(intent, title));
        } catch (android.content.ActivityNotFoundException e) {
            Trace.get(e);
        }
    }

    /**
     * Open apk on Google Play Store, or if is not installed, in browser
     *
     * @param packageName apk package
     */
    public static void openPlayStore(String packageName) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ActivityManager.get().startActivity(intent);
        } catch (android.content.ActivityNotFoundException e) {
            openUrl("https://play.google.com/store/apps/details?id=" + packageName);
        }
    }

    /**
     * Share a content
     *
     * @param title       content title
     * @param description content description
     */
    public static void openSharePanel(String title, String description) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, description);
        ActivityManager.get().startActivity(Intent.createChooser(intent, "choose one"));
    }

    /**
     * Open Usage Access Settings
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void openUsageStats() {
        Intent intent = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityManager.get().startActivity(intent);
    }

    /**
     * Open url from an external browser
     *
     * @param url url
     */
    public static void openUrl(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityManager.get().startActivity(intent);
    }

    /**
     * Open an app from package name
     *
     * @param packageName app package name
     */
    public static void openApp(String packageName) {
        Intent intent = Unio.app().getPackageManager().getLaunchIntentForPackage(packageName);
        Unio.app().startActivity(intent);
    }

    /**
     * Open an activity as new task
     *
     * @param activity activity class
     */
    public static void openNewActivity(Class<? extends Activity> activity) {
        Intent newActivity = new Intent(Unio.app(), activity);
        newActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Unio.app().startActivity(newActivity);
    }
}