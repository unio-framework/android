package com.unio.util.control;

import android.accessibilityservice.AccessibilityService;
import android.content.ContentResolver;
import android.content.Intent;
import android.provider.Settings;
import android.text.TextUtils;
import com.unio.core.Unio;
import com.unio.debug.Trace;

/**
 * CAccessibility
 *
 * Class for all control in Accessibility section
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 30/06/2016 16:19
 */
public class Accessibility
{
    /**
     * Check if an Accessibility service is enabled
     *
     * @param serviceClass .class of {@link AccessibilityService}
     *
     * @return boolean
     */
    public static boolean isEnabled(Class<? extends AccessibilityService> serviceClass) {
        ContentResolver resolver = Unio.app().getContentResolver();
        final String service     = serviceClass.getPackage().getName()+"/"+serviceClass.getName();
        int accessibilityEnabled = 0;

        try {
            accessibilityEnabled = Settings.Secure.getInt(resolver, Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            Trace.get(e);
        }
        TextUtils.SimpleStringSplitter splitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(resolver, Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                splitter.setString(settingValue);
                while (splitter.hasNext()) {
                    String accessabilityService = splitter.next();
                    if (accessabilityService.equalsIgnoreCase(service)) return true;
                }
            }
        }
        return false;
    }

    /**
     * Open Accessibility Setting screen to enable/disable
     */
    public static void openSetting() {
        Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        Unio.app().startActivity(intent);
    }
}