package com.unio.util.control;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.annotation.RequiresPermission;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.shortcuts.Android;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import static android.Manifest.permission.ACCESS_NETWORK_STATE;
import static android.Manifest.permission.ACCESS_WIFI_STATE;
import static android.Manifest.permission.READ_PHONE_STATE;

/**
 * UMNetwork
 *
 * Module for network management
 *
 * #getConnectionType and #isOnline will need
 *     <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 27/07/2016 14:57
 */
@SuppressLint("MissingPermission")
public class Network
{
    public static final int MOBILE_NONE = 0;
    public static final int MOBILE_2G   = 1;
    public static final int MOBILE_3G   = 2;
    public static final int MOBILE_4G   = 3;

    /** Last signal strength */
    private static SignalStrength _lastSignalStrength;

    /**
     * Start signal trace
     */
    public static void startSignalTrace() {
        final PhoneStateListener listener = new PhoneStateListener() {
            @Override
            public void onSignalStrengthsChanged(SignalStrength signalStrength) {
                super.onSignalStrengthsChanged(signalStrength);
                _lastSignalStrength = signalStrength;
            }
        };
        TelephonyManager telManager = (TelephonyManager) Unio.app().getSystemService(Context.TELEPHONY_SERVICE);
        telManager.listen(listener, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    /**
     * Need android.permission.ACCESS_NETWORK_STATE
     *
     * Get connection type
     * @see ConnectivityManager
     *
     * @return int
     */
    @RequiresPermission(ACCESS_NETWORK_STATE)
    public static int getConnectionType() {
        NetworkInfo info = getNetworkInfo();
        return info == null ? -1 : info.getType(); // -1 é ConnectivityManager#TYPE_NONE, definido como hide
    }

    /**
     * Get mobile connection type
     *
     * @return int
     */
    @RequiresPermission(allOf = { ACCESS_NETWORK_STATE, READ_PHONE_STATE })
    public static int getMobileConnectionType() {
        if (getConnectionType() == ConnectivityManager.TYPE_MOBILE) {
            TelephonyManager mTelephonyManager = (TelephonyManager) Unio.app().getSystemService(Context.TELEPHONY_SERVICE);
            int networkType = mTelephonyManager.getNetworkType();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN:
                    return MOBILE_2G;
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B:
                case TelephonyManager.NETWORK_TYPE_EHRPD:
                case TelephonyManager.NETWORK_TYPE_HSPAP:
                    return MOBILE_3G;
                case TelephonyManager.NETWORK_TYPE_LTE:
                    return MOBILE_4G;
            }
        }
        return MOBILE_NONE;
    }

    /**
     * Format mobile connection type to string
     *
     * @return String
     */
    @RequiresPermission(allOf = { ACCESS_NETWORK_STATE, READ_PHONE_STATE })
    public static String getMobileConnectionTypeAsString() {
        int type = getMobileConnectionType();
        switch (type) {
            case MOBILE_2G: return "2G";
            case MOBILE_3G: return "3G";
            case MOBILE_4G: return "4G";
            default:        return "Invalid";
        }
    }

    /**
     * Get current network connection level
     *
     * @return int
     */
    @RequiresPermission(allOf = { ACCESS_WIFI_STATE, READ_PHONE_STATE })
    public static int getConnectionLevel() {
        NetworkInfo info = getNetworkInfo();
        if (info.getType() == ConnectivityManager.TYPE_WIFI)
            return getWifiConnectionLevel();
        return getMobileConnectionLevel();
    }

    /**
     * Get reason for connection lost
     *
     * @return String
     */
    public static String getConnectionLostReason() {
        NetworkInfo info = getNetworkInfo();
        return info == null ? null : info.getReason();
    }

    /**
     * Returns MAC address of the given interfaces name.
     *
     * @param interfaceName eth0, wlan0 or NULL=use first interfaces
     *
     * @return String, mac address or empty string
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    public static String getMACAddress(String interfaceName) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (interfaceName != null) {
                    if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
                }
                byte[] mac = intf.getHardwareAddress();
                if (mac==null) return "";
                StringBuilder buf = new StringBuilder();
                for (int idx=0; idx<mac.length; idx++)
                    buf.append(String.format("%02X:", mac[idx]));
                if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
                return buf.toString();
            }
        } catch (Exception ex) {
            Trace.get(ex);
        }
        return null;
    }

    /**
     * Get current connection IPv4 address
     *
     * @return String
     */
    public static String getIPAdress() { return _getIPAddress(true); }

    /**
     * Get current connection IPv6 address
     *
     * @return String
     */
    public static String getIPv6Adress() { return _getIPAddress(false); }

    /**
     * Get IP address from first non-localhost interfaces
     * @param useIPv4  true=return ipv4, false=return ipv6
     * @return  address or empty string
     */
    private static String _getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4) return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) {
            Trace.get(ex);
        }
        return null;
    }

    /**
     * Get mobile connection level
     *
     * @return int
     */
    @RequiresPermission(READ_PHONE_STATE)
    public static int getMobileConnectionLevel() {
        if (_lastSignalStrength == null) return 0;

        if (Android.CURRENT_VERSION > Android.LOLLIPOP_MR1) {
            return _lastSignalStrength.getLevel();
        } else {
            int level = 0;
            if (_lastSignalStrength.isGsm()) {
                int signalStrengthValue;
                if (_lastSignalStrength.getGsmSignalStrength() != 99) {
                    signalStrengthValue = _lastSignalStrength.getGsmSignalStrength() * 2 - 113;
                } else {
                    signalStrengthValue = _lastSignalStrength.getGsmSignalStrength();
                }
                if (signalStrengthValue > 30) {
                    level = 4;
                } else if (signalStrengthValue > 20) {
                    level = 3;
                } else if(signalStrengthValue > 10) {
                    level = 2;
                } else if(signalStrengthValue > 5) {
                    level = 1;
                }
            } else {
                final int snr = _lastSignalStrength.getEvdoSnr();
                final int cdmaDbm = _lastSignalStrength.getCdmaDbm();
                final int cdmaEcio = _lastSignalStrength.getCdmaEcio();
                int levelDbm;
                int levelEcio;

                if (snr == -1) {
                    if (cdmaDbm >= -75) levelDbm = 4;
                    else if (cdmaDbm >= -85) levelDbm = 3;
                    else if (cdmaDbm >= -95) levelDbm = 2;
                    else if (cdmaDbm >= -100) levelDbm = 1;
                    else levelDbm = 0;

                    // Ec/Io are in dB*10
                    if (cdmaEcio >= -90) levelEcio = 4;
                    else if (cdmaEcio >= -110) levelEcio = 3;
                    else if (cdmaEcio >= -130) levelEcio = 2;
                    else if (cdmaEcio >= -150) levelEcio = 1;
                    else levelEcio = 0;

                    level = (levelDbm < levelEcio) ? levelDbm : levelEcio;
                } else {
                    if (snr == 7 || snr == 8) level =4;
                    else if (snr == 5 || snr == 6 ) level =3;
                    else if (snr == 3 || snr == 4) level = 2;
                    else if (snr ==1 || snr ==2) level =1;
                }
            }
            return level;
        }
    }

    /**
     * Get network information
     *
     * @return NetworkInfo
     */
    public static NetworkInfo getNetworkInfo(){
        ConnectivityManager cm = (ConnectivityManager) Unio.app().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Get WiFi connection strenght, between 0 and 4
     *
     * @return int
     */
    @RequiresPermission(ACCESS_WIFI_STATE)
    public static int getWifiConnectionLevel() {
        WifiManager wifiManager = (WifiManager) Unio.app().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        int numberOfLevels = 5;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        return WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
    }

    /**
     * Check if current conection is in good state
     *
     * @return boolean
     */
    @RequiresPermission(ACCESS_NETWORK_STATE)
    public static boolean isAvalable() {
        NetworkInfo info = getNetworkInfo();
        return info != null && info.isAvailable();
    }

    /**
     * Validate if current connection is a bad quality
     * Validations:
     * - isOnline
     * - isAvalable
     * - Wifi Connection level > 2
     * - Mobile connection type > 2G
     *
     * @return boolean
     */
    @RequiresPermission(allOf = { ACCESS_NETWORK_STATE, ACCESS_WIFI_STATE, READ_PHONE_STATE })
    public static boolean isBadConnection() {
        return isOnline() == false ||
            isAvalable() == false ||
            (getConnectionType() == ConnectivityManager.TYPE_WIFI && getConnectionLevel() <= 2) ||
            (getConnectionType() == ConnectivityManager.TYPE_MOBILE && getMobileConnectionType() <= MOBILE_2G );
    }

    /**
     * Need android.permission.ACCESS_NETWORK_STATE
     *
     * Check if device is online
     *
     * @return boolean
     */
    public static boolean isOnline() {
        NetworkInfo info = getNetworkInfo();
        return info != null && info.isConnected();
    }

    /**
     * Shortcut to check if current connection is Wifi
     *
     * @return boolean
     */
    public static boolean isWifi() { return getConnectionType() == ConnectivityManager.TYPE_WIFI; }
}