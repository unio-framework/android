package com.unio.util.control;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.graphics.drawable.Drawable;
import com.unio.core.Unio;
import com.unio.util.helper.Convert;
import com.unio.util.io.File;

/**
 * AppInfo
 *
 * Class for get app information
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class AppInfo
{
    /** Attributes */
    public final PackageInfo     packageInfo;
    public final ApplicationInfo applicationInfo;
    public final int             uid;
    public final String          name;
    public final String          packageName;
    public final String          versionName;
    public final int             versionCode;
    public final Drawable        icon;
    public final int             iconId;
    public final boolean         isSystem;
    public final String          installationDate;
    public final String          lastUpdateDate;
    public final PermissionInfo[] permissions;
    public final String[] requestedPermissions;
    public final double fileSize; // APK size as MB

    /**
     * Constructor
     *
     * @param packageInfo PackageInfo
     */
    public AppInfo(PackageInfo packageInfo) {
        final PackageManager packageManager = Unio.app().getPackageManager();
        this.packageInfo     = packageInfo;
        this.applicationInfo = this.packageInfo.applicationInfo;

        this.uid                  = this.applicationInfo.uid;
        this.name                 = this._getName(packageManager, this.applicationInfo);
        this.packageName          = this.packageInfo.packageName;
        this.versionName          = this.packageInfo.versionName;
        this.versionCode          = this.packageInfo.versionCode;
        this.icon                 = this.packageInfo.applicationInfo.loadIcon(packageManager);
        this.iconId               = this.packageInfo.applicationInfo.icon;
        this.isSystem             = (this.packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
        this.installationDate     = Date.format(packageInfo.firstInstallTime, "yyyy-MM-dd HH:mm:ss");
        this.lastUpdateDate       = Date.format(packageInfo.lastUpdateTime, "yyyy-MM-dd HH:mm:ss");
        this.permissions          = this.packageInfo.permissions == null ? new PermissionInfo[0] : this.packageInfo.permissions;
        this.requestedPermissions = this.packageInfo.requestedPermissions == null ? new String[0] : this.packageInfo.requestedPermissions;
        this.fileSize             = new File(this.applicationInfo.publicSourceDir).lengthAsMegabyte();
    }

    /**
     * Constructor, search by package name
     *
     * @param pn package name
     */
    public AppInfo(String pn) {
        final PackageManager packageManager = Unio.app().getPackageManager();

        int uid, versionCode, iconId;
        String name, packageName, versionName, installationDate, lastUpdateDate;
        Drawable icon;
        boolean isSystem;
        PackageInfo packageInfo;
        ApplicationInfo applicationInfo;
        PermissionInfo[] permissions;
        String[] requestedPermissions;
        double fileSize;

        try {
            packageInfo = packageManager.getPackageInfo(pn, 0);
            applicationInfo = packageInfo.applicationInfo;

            uid                  = applicationInfo.uid;
            name                 = this._getName(packageManager, applicationInfo);
            packageName          = packageInfo.packageName;
            versionName          = packageInfo.versionName;
            versionCode          = packageInfo.versionCode;
            icon                 = packageInfo.applicationInfo.loadIcon(packageManager);
            iconId               = packageInfo.applicationInfo.icon;
            isSystem             = (packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
            installationDate     = Date.format(packageInfo.firstInstallTime, "yyyy-MM-dd HH:mm:ss");
            lastUpdateDate       = Date.format(packageInfo.lastUpdateTime, "yyyy-MM-dd HH:mm:ss");
            permissions          = packageInfo.permissions == null ? new PermissionInfo[0] : packageInfo.permissions;
            requestedPermissions = packageInfo.requestedPermissions == null ? new String[0] : packageInfo.requestedPermissions;
            fileSize             = new File(applicationInfo.publicSourceDir).lengthAsMegabyte();
        } catch (PackageManager.NameNotFoundException e) {
            packageInfo = null;
            applicationInfo = null;

            uid                  = 0;
            name                 = this._getName(packageManager, applicationInfo);
            packageName          = pn;
            versionName          = null;
            versionCode          = 0;
            icon                 = null;
            iconId               = 0;
            isSystem             = false;
            installationDate     = null;
            lastUpdateDate       = null;
            permissions          = new PermissionInfo[0];
            requestedPermissions = new String[0];
            fileSize             = 0;
        }
        this.packageInfo = packageInfo;
        this.applicationInfo = applicationInfo;

        this.uid                  = uid;
        this.name                 = name;
        this.packageName          = packageName;
        this.versionName          = versionName;
        this.versionCode          = versionCode;
        this.icon                 = icon;
        this.iconId               = iconId;
        this.isSystem             = isSystem;
        this.installationDate     = installationDate;
        this.lastUpdateDate       = lastUpdateDate;
        this.permissions          = permissions;
        this.requestedPermissions = requestedPermissions;
        this.fileSize             = fileSize;
    }

    /**
     * Format content to read
     *
     * @return String
     */
    @Override
    public String toString() {
        return "uid: " + this.uid +
            "\n" +
            "name: " + this.name +
            "\n" +
            "packageName: " + this.packageName +
            "\n" +
            "versionName: " + this.versionName +
            "\n" +
            "versionCode: " + this.versionCode +
            "\n" +
            "iconId: " + this.iconId +
            "\n" +
            "installationDate: " + this.installationDate +
            "\n" +
            "lastUpdateDate: " + this.lastUpdateDate +
            "\n" +
            "permissions: " + Convert.toString(this.permissions) +
            "\n" +
            "requestedPermissions: " + Convert.toString(this.requestedPermissions) +
            "\n" +
            "fileSize: " + this.fileSize;
    }

    /**
     * Get app name, unknown if not find
     *
     * @return String
     */
    private String _getName(PackageManager packageManager, ApplicationInfo appInfo) {
        return (String)(appInfo == null ? "unknown" : packageManager.getApplicationLabel(appInfo));
    }
}