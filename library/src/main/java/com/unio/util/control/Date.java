package com.unio.util.control;

import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Date
 *
 * Custom control {@link java.util.Date} class for operation with other dates
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
@SuppressWarnings("UnusedReturnValue")
public class Date extends java.util.Date
{
    /** Formats */
    public static final String DATE     = "yyyy-MM-dd";
    public static final String TIME     = "HH:mm:ss";
    public static final String DATETIME = DATE+" "+TIME;
    public static final String DATE_BR  = "dd/MM/yyyy";

    /** Attributes */
    private long _rawTime;
    private final Calendar _Calendar;

    /** Get date/time string */
    public static String get()      { return new Date().format(DATE);     }
    public static String time()     { return new Date().format(TIME);     }
    public static String withTime() { return new Date().format(DATETIME); }

    /**
     * Constructor with current date
     */
    public Date() { this(System.currentTimeMillis()); }

    /**
     * Constructor with string date/time
     *
     * @param v value
     */
    public Date(String v) { this(v, null); }

    /**
     * Constructor with string date/time
     *
     * @param v       value
     * @param pattern current date pattern
     */
    public Date(String v, String pattern) {
        this(_parse(v, pattern));
        if (this._rawTime == -1) throw new RuntimeException("Invalid date/time: "+v);
    }

    /**
     * Constructor with miliseconds
     *
     * @param millisecondsTime miliseconds
     */
    public Date(long millisecondsTime) {
        super(millisecondsTime);
        this._rawTime = millisecondsTime;

        this._Calendar = Calendar.getInstance();
        this._Calendar.setTime(this);
    }

    /**
     * Get tomorrow date
     *
     * @return CDate, tomorrow, without modification in current instance
     */
    public Date tomorrow() {
        Date date = new Date(this.getTime());
        date.addDay();
        return date;
    }

    /**
     * Get yesterday date
     *
     * @return CDate, yesterday, without modification in current instance
     */
    public Date yesterday() {
        Date date = new Date(this.getTime());
        date.removeDay();
        return date;
    }

    /** Return {@link Calendar} of this date */
    public Calendar asCalendar() { return (Calendar) this._Calendar.clone(); }

    /** Get datetime piece */
    public int day()    { return this.get(Calendar.DAY_OF_MONTH); }
    public int month()  { return this.get(Calendar.MONTH)+1;      }
    public int year()   { return this.get(Calendar.YEAR);         }
    public int hour()   { return this.get(Calendar.HOUR_OF_DAY);  }
    public int minute() { return this.get(Calendar.MINUTE);       }
    public int second() { return this.get(Calendar.SECOND);       }

    /** Additional piece */
    public int weekOfMonth() { return this.get(Calendar.WEEK_OF_MONTH); }
    public int weekOfYear()  { return this.get(Calendar.WEEK_OF_YEAR);  }
    public int dayOfWeek()   { return this.get(Calendar.DAY_OF_WEEK);   }

    /** Some add */
    public Date addDay() { return this.addDay(1); }
    public Date addDay(int quantity) { return this.add(quantity, Calendar.DAY_OF_MONTH); }
    public Date addMonth() { return this.addMonth(1); }
    public Date addMonth(int quantity) { return this.add(quantity, Calendar.MONTH); }
    public Date addYear() { return this.addYear(1); }
    public Date addYear(int quantity) { return this.add(quantity, Calendar.YEAR); }

    /** Some remove */
    public Date removeDay() { return this.removeDay(1); }
    public Date removeDay(int quantity) { return this.remove(quantity, Calendar.DAY_OF_MONTH); }
    public Date removeMonth() { return this.removeMonth(1); }
    public Date removeMonth(int quantity) { return this.remove(quantity, Calendar.MONTH); }
    public Date removeYear() { return this.removeYear(1); }
    public Date removeYear(int quantity) { return this.remove(quantity, Calendar.YEAR); }

    /**
     * Add a specified amount of time to the given calendar field
     *
     * @param v    amount
     * @param type type
     *
     * @return CDate
     */
    public Date add(int v, int type) {
        this._Calendar.add(type, v);
        this.setTime(this._Calendar.getTimeInMillis());
        return this;
    }

    /**
     * Difference from 2 dates, in day
     *
     * @param date diff date
     *
     * @return long
     */
    public long daysDiff(Date date) {
        Calendar current = this.before(date) ? this.asCalendar() : date.asCalendar();
        Calendar param   = this.before(date) ? date.asCalendar() : this.asCalendar();

        long daysBetween = 0;
        while (current.before(param)) {
            current.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    /**
     * Difference from 2 dates, returning corresponding date
     *
     * @param date diff date
     *
     * @return CDate
     */
    public Date diff(java.util.Date date) {
        if (date == null) return this;
        long currentTime = this.getTime();
        long diffTime    = date.getTime();
        long resultTime = currentTime > diffTime ?
            currentTime-diffTime : diffTime-currentTime;
        return new Date(resultTime);
    }

    /**
     * Format date by a pattern
     *
     * @param format format pattern
     *
     * @return String
     */
    public String format(String format) {
        SimpleDateFormat f = new SimpleDateFormat(format, Locale.US);
        return f.format(this);
    }

    /**
     * Get calendar piece
     *
     * @param type type, see {@link Calendar}
     *
     * @return int
     */
    public int get(int type) { return this._Calendar.get(type); }

    /**
     * Remove a specified amount of time to the given calendar field
     *
     * @param v    amount
     * @param type type
     *
     * @return CDate
     */
    public Date remove(int v, int type) {
        this._Calendar.add(type, -v);
        this.setTime(this._Calendar.getTimeInMillis());
        return this;
    }

    /**
     * Format with custom from and to pattern
     *
     * @param date date in string
     * @param from current date pattern
     * @param to   format pattern
     *
     * @return String
     */
    public static String format(String date, String from, String to) {
        if (Check.isEmpty(date) || Check.isEmpty(from) || Check.isEmpty(to)) return null;
        return new Date(date, from).format(to);
    }

    /**
     * Format shortcut using milliseconds
     *
     * @param milliseconds milliseconds
     * @param to           format pattern
     *
     * @return String
     */
    public static String format(long milliseconds, String to) { return new Date(milliseconds).format(to); }

    /**
     * Parse a string date, getting time since 1970-01-01
     *
     * @param date date string
     *
     * @return long
     */
    private static long _parse(String date, String sPattern) {
        if (Check.isEmpty(date) || date.equals("null")) return System.currentTimeMillis();
        if (Check.isEmpty(sPattern)) {
            sPattern = date.contains("-") ? "yyyy-MM-dd" : "dd/MM/yyyy";
            if (date.split(" ").length == 2) {
                sPattern += "HH:mm";
                if (date.split(":").length == 3) sPattern += ":ss";
            }
        }
        SimpleDateFormat pattern = new SimpleDateFormat(sPattern, Locale.US);
        try {
            return pattern.parse(date).getTime();
        } catch (ParseException e) {
            Trace.get(e);
            return -1;
        }
    }
}