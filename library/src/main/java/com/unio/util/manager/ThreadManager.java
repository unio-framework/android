package com.unio.util.manager;

import com.unio.util.helper.Check;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * ThreadManager
 *
 * Load a list of all active threads, and get from thread name and prevent duplication
 *
 * Thread is background process
 * If the app that creates is finished, the thread is finished too
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 06/07/2014 00:11
 */
public class ThreadManager
{
    /**
     * Get a thread from name
     *
     * @param name thread name
     *
     * @return Thread
     */
    public static Thread get(String name) {
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        for (Thread thread : threadSet) {
            if (Check.isEqual(thread.getName(), name))
                return thread;
        }
        return null;
    }

    /**
     * Get all active thread
     *
     * @return Map
     */
    public static Map<String, Thread> getAll() {
        Map<String, Thread> threads = new LinkedHashMap<>();
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        for (Thread thread : threadSet) threads.put(thread.getName(), thread);
        return threads;
    }
}