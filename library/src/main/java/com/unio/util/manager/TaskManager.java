package com.unio.util.manager;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.util.control.AppInfo;
import java.util.ArrayList;
import java.util.List;

/**
 * TaskManager
 *
 * App task management class
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class TaskManager
{
    /**
     * Get all installed apps, except system apps
     *
     * @return List
     */
    public static List<AppInfo> get() { return get(false); }

    /**
     * Get all installed apps
     *
     * @param systemApps flag if will include system apps
     *
     * @return List
     */
    public static List<AppInfo> get(boolean systemApps) {
        List<PackageInfo> packs = getPackages();
        List<AppInfo> apps = new ArrayList<>();
        PackageManager packageManager = Unio.app().getPackageManager();
        for (PackageInfo pack : packs) {
            // Add only apps
            if (packageManager.getLaunchIntentForPackage(pack.packageName) == null) continue;
            // Add system apps based on flag
            if (systemApps == false && ((pack.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0)) continue;
            apps.add(new AppInfo(pack));
        }
        return apps;
    }

    /**
     * Get all apps from package name
     *
     * @param packageNames app package name
     *
     * @return List
     */
    public static List<AppInfo> get(List<String> packageNames) {
        List<AppInfo> apps = new ArrayList<>();
        List<PackageInfo> packs = getPackages();
        for (PackageInfo pack : packs) {
            if (packageNames.contains(pack.packageName)) apps.add(new AppInfo(pack));
        }
        return apps;
    }

    /**
     * Get first app filtering from package name
     *
     * @param packageName app package name
     *
     * @return App
     */
    public static AppInfo get(String packageName) {
        if (packageName == null) return null;

        try {
            PackageManager packageManager = Unio.app().getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(packageName, 0);
            return new AppInfo(packageInfo);
        } catch (PackageManager.NameNotFoundException e) {
            Trace.get(e);
            return null;
        }
    }

    /**
     * Get all installed packages
     *
     * @return List
     */
    private static List<PackageInfo> getPackages() {
        PackageManager packageManager = Unio.app().getPackageManager();
        return packageManager.getInstalledPackages(PackageManager.GET_PERMISSIONS);
    }
}