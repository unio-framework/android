package com.unio.util.manager;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.unio.core.Unio;
import com.unio.debug.Trace;

/**
 * FragmentManager
 *
 * Fragment management class
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class FragmentManager
{
    /**
     * Attach fragment
     */
    public static Fragment change(Class<? extends Fragment> fragmentClass, @IdRes int resId) {
        return change(ActivityManager.get(), fragmentClass, resId, null);
    }

    /**
     * Attach fragment
     */
    public static Fragment change(Activity parent, Class<? extends Fragment> fragmentClass, @IdRes int resId) {
        return change(parent, fragmentClass, resId, null);
    }

    /**
     * Attach fragment
     */
    public static Fragment change(Class<? extends Fragment> fragmentClass, @IdRes int resId, ActivityManager.Param params) {
        return change(ActivityManager.get(), fragmentClass, resId, params);
    }

    /**
     * Attach fragment
     */
    public static Fragment change(Activity parent, Class<? extends Fragment> fragmentClass, final @IdRes int resId, final ActivityManager.Param p) {
        final Activity activity = parent == null ? ActivityManager.get() : parent;
        if (activity == null) {
            throw new RuntimeException("Activity is null. Need to add current Activity instance");
        } else {
            final ActivityManager.Param params = p == null ? new ActivityManager.Param() : p;
            try {
                final Fragment fragment = fragmentClass.newInstance();
                Bundle args = fragment.getArguments();
                if (args == null) args = new Bundle();
                params.attachExtras(args);
                fragment.setArguments(args);

                if (activity instanceof FragmentActivity) {
                    final FragmentActivity fragmentActivity = (FragmentActivity) activity;
                    final FragmentTransaction transaction = fragmentActivity.getSupportFragmentManager().beginTransaction();
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            try {
                                params.performOnBeforeRun();
                                transaction.replace(resId, fragment);
                                transaction.addToBackStack(null);
                                transaction.commit();

                                fragmentActivity.invalidateOptionsMenu(); // Update toolbar
                            } catch (Exception e) {
                                Trace.get(e);
                            }
                        }
                    };
                    Unio.ui().postDelayed(runnable, 300);
                } else {
                    throw new RuntimeException("Activity is not a FragmentActivity instance");
                }
                return fragment;
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Shortcut, as is used in this class too
     */
    public static class Param extends ActivityManager.Param {}
}