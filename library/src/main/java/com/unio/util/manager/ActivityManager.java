package com.unio.util.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.unio.core.Unio;
import com.unio.shortcuts.Manage;
import com.unio.util.io.Hash;
import com.unio.util.statics.interfaces.IBasicRun;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.*;

/**
 * ActivityManager
 *
 * Activity management class
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class ActivityManager
{
    /** Store instances **/
    private static WeakReference<Activity> _current;
    private static List<Class> _openeds = new LinkedList<>();

    /**
     * Change activity
     */
    public static void change(Class activityClass) { change(Manage.get(), activityClass); }

    /**
     * Change activity, with actions
     */
    public static void change(Class activityClass, Integer... action) { change(Manage.get(), activityClass, action); }

    /**
     * Change activity, with actions
     */
    public static void change(Class activityClass, Param params) { change(Manage.get(), activityClass, params); }

    /**
     * Change activity, with actions
     */
    public static void change(Context activity, Class activityClass, Integer... action) {
        Param params = new Param();
        params.addAction(action);
        change(activity, activityClass, params);
    }

    /**
     * Change activity, with actions
     */
    public static void change(final Context activity, final Class activityClass, Param params) {
        final Intent newActivity = new Intent(activity, activityClass);

        if (params == null) params = new Param();
        if (params.actions().size() == 0) params.addAction(Param.CLOSE_CURRENT);

        final List<Integer> actions = params.actions();
        if (actions.contains(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT) && _openeds.contains(activityClass)) {
            // This flag will reorder to front
            newActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        }

        params.attachExtras(newActivity);
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                activity.startActivity(newActivity);
                if (_openeds.contains(activityClass) == false) _openeds.add(activityClass);
                if (actions.contains(Param.CLOSE_CURRENT)) {
                    // Se for para fechar o atual, verifica se existe e a activity é diferente da que vai chamar, e remove da lista
                    if (_openeds.contains(activity.getClass()) && activityClass != activity.getClass())
                        _openeds.remove(activity.getClass());

                    if (activity instanceof Activity) {
                        // Dá mais um delay para a transição ficar mais suave
                        Runnable runnableCloseActivity = new Runnable() {
                            @Override
                            public void run() { ((Activity) activity).finish(); }
                        };
                        Unio.ui().postDelayed(runnableCloseActivity, 300);
                    }
                }
            }
        };
        Unio.ui().postDelayed(runnable, 300);
    }

    /**
     * Remove an activity from cache
     *
     * @param activity activity
     */
    public static void remove(Activity activity) {
        Class activityClass = activity.getClass();
        if (_openeds.contains(activityClass) == false) _openeds.remove(activityClass);
    }

    /**
     * Set current activity
     *
     * @param activity activity
     */
    public static void set(Activity activity) { _current = new WeakReference<>(activity); }

    /**
     * Get current activity
     *
     * @return Activity
     */
    public static Activity get() { return _current == null ? null : _current.get(); }

    /**
     * Param
     *
     * Activity and Fragment params
     *
     * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     */
    public static class Param
    {
        public static final int CLOSE_CURRENT = 9000001;

        /** Params */
        private List<Integer> _actions = new LinkedList<>();
        private Hash<String, Object> _extras = new Hash<>();
        private List<IBasicRun> _beforeRun = new LinkedList<>();

        /**
         * Add an action
         *
         * @param actions Action
         */
        public void addAction(Integer... actions) {
            if (actions != null && actions.length > 0)
                Collections.addAll(this._actions, actions);
        }

        /**
         * Add an extra
         *
         * @param name  name
         * @param value value
         */
        public void addExtra(String name, Object value) {
            if (value != null) {
                Object v;
                if (value instanceof Class) {
                    v = ((Class) value).getName();
                } else if (value instanceof Enum) {
                    v = ((Enum) value).ordinal();
                } else {
                    v = value;
                }
                this._extras.put(name, v);
            }
        }

        /**
         * Add callback to run before change action
         *
         * @param callback callback
         */
        public void addOnBeforeRun(IBasicRun callback) {
            if (this._beforeRun.contains(callback) == false)
                this._beforeRun.add(callback);
        }

        /**
         * Remove callback to run before change action
         *
         * @param callback callback
         */
        public void removeOnBeforeRun(IBasicRun callback) { this._beforeRun.remove(callback); }

        /**
         * Clear all callbacks to run before change action
         */
        public void clearOnBeforeRun() { this._beforeRun.clear(); }

        /**
         * Get actions
         *
         * @return List
         */
        public List<Integer> actions() { return this._actions; }

        /**
         * Get actions
         *
         * @return List
         */
        public Hash<String, Object> extras() { return this._extras; }

        /**
         * Run callbacks before change
         */
        public void performOnBeforeRun() {
            if (this._beforeRun.size() > 0)
                for (IBasicRun c : this._beforeRun) c.run();
        }

        /**
         * Attach added extra to intent
         *
         * @param intent intent
         *
         * @return Intent
         */
        @SuppressWarnings("UnusedReturnValue")
        public Intent attachExtras(Intent intent) {
            if (this.extras().length() > 0) {
                Hash<String, Object> extras = this.extras();
                for (String key : extras) {
                    Object value = extras.get(key);
                    if (value == null) continue;
                    if (value instanceof Serializable) {
                        intent.putExtra(key, (Serializable) value);
                    } else if (value instanceof Integer) {
                        intent.putExtra(key, (int) value);
                    } else if (value instanceof Float) {
                        intent.putExtra(key, (float) value);
                    } else if (value instanceof Double) {
                        intent.putExtra(key, (double) value);
                    } else if (value instanceof Long) {
                        intent.putExtra(key, (long) value);
                    } else {
                        String v = String.valueOf(value);
                        intent.putExtra(key, v);
                    }
                }
            }
            return intent;
        }

        /**
         * Attach added extra to Bundle
         *
         * @param intent intent
         *
         * @return Bundle
         */
        public Bundle attachExtras(Bundle intent) {
            if (this.extras().length() > 0) {
                Hash<String, Object> extras = this.extras();
                for (String key : extras) {
                    Object value = extras.get(key);
                    if (value == null) continue;
                    if (value instanceof Serializable) {
                        intent.putSerializable(key, (Serializable) value);
                    } else if (value instanceof Integer) {
                        intent.putInt(key, (int) value);
                    } else if (value instanceof Float) {
                        intent.putFloat(key, (float) value);
                    } else if (value instanceof Double) {
                        intent.putDouble(key, (double) value);
                    } else if (value instanceof Long) {
                        intent.putLong(key, (long) value);
                    } else {
                        String v = String.valueOf(value);
                        intent.putString(key, v);
                    }
                }
            }
            return intent;
        }
    }
}