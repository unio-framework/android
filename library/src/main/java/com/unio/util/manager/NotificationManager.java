package com.unio.util.manager;

import android.app.Activity;
import android.content.Context;
import com.unio.core.Unio;
import com.unio.debug.Trace;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * NotificationManager
 *
 * App's current notification management class
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class NotificationManager
{
    /**
     * Notifications cache with referenced id
     */
    private static Map<Integer, Class<? extends Activity>> _notifications = new HashMap<>();
    private static List<Integer> _dismissed = new ArrayList<>();

    /**
     * ID generator count
     */
    private static int _idGenerator = 1;

    /**
     * Generate an id
     *
     * @return int
     */
    public static int generateId() {
        int id = _idGenerator++;
        while (_notifications.containsKey(id)) id = _idGenerator++; // evita sobrescrever uma notificação
        return id;
    }

    /**
     * Add a new mapper with id and target activity to open
     *
     * @param target activity to open
     *
     * @return int ID da notificação
     */
    public static int add(Class<? extends Activity> target) {
        int id = generateId();
        _notifications.put(id, target);
        return id;
    }

    /**
     * Update mapper
     *
     * @param id     notification id
     * @param target activity target
     *
     * @return int ID da notificação
     */
    public static int update(int id, Class<? extends Activity> target) {
        _notifications.put(id, target);
        removeFromDismiss(id);
        return id;
    }

    /**
     * Remove an ID
     *
     * @param id notification id
     */
    public static void remove(int id) {
        if (exists(id)) {
            _notifications.remove(id);
            removeFromDismiss(id);
        }
    }

    /**
     * Add a mapper with custom id
     *
     * @param id     notification id
     * @param target activity to open
     */
    public static void add(int id, Class<? extends Activity> target) {
        _notifications.put(id, target);
        removeFromDismiss(id);
    }

    /**
     * Get target activity from id
     *
     * @param id notification id
     *
     * @return Class
     */
    public static Class<? extends Activity> get(int id) { return _notifications.get(id); }

    /**
     * Check if map exists from id
     *
     * @param id notification id
     *
     * @return boolean
     */
    public static boolean exists(int id) { return _notifications.containsKey(id); }

    /**
     * Add id to dismissed mapping
     *
     * @param id notification id
     */
    public static void dismiss(int id) {
        android.app.NotificationManager notification = (android.app.NotificationManager) Unio.app().getSystemService(Context.NOTIFICATION_SERVICE);
        notification.cancel(id);
        if (_dismissed.contains(id) == false) _dismissed.add(id);
        Trace.log(NotificationManager.class, "Dismissed notification id #"+id);
    }

    /**
     * Remove an id from dismissed map
     * id need to be {@link Integer}, to avoid removing from list index
     *
     * @param id ID da notificação
     */
    public static void removeFromDismiss(Integer id) { _dismissed.remove(id); }

    /**
     * Check if an id has dismissed
     *
     * @param id notification id
     *
     * @return boolean
     */
    public static boolean dismissed(int id) { return _dismissed.contains(id); }
}