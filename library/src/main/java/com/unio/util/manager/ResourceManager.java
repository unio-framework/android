package com.unio.util.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.shortcuts.Android;

import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.Locale;

/**
 * ResourceManager
 *
 * Class for resource management
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 25/10/2014 22:52
 */
public class ResourceManager
{
    /** Flags to {@link #exists(String, String)} e {@link #getId(String, String)} */
    public static final String ANIMATION = "anim";
    public static final String COLOR     = "color";
    public static final String DRAWABLE  = "drawable";
    public static final String ID        = "id";
    public static final String INTEGER   = "integer";
    public static final String LAYOUT    = "layout";
    public static final String STRING    = "string";
    public static final String STYLEABLE = "attr";
    public static final String STYLE     = "style";

    /** Context to use when is in edit mode */
    private static WeakReference<Context> _$Context;

    /**
     * Get Resources instance for work
     *
     * @return Resources
     */
    public static Resources get() { return _$Context == null ? Unio.app().getResources() : _$Context.get().getResources(); }

    /**
     * Set context to use when is in edit mode
     *
     * @param context context
     */
    public static void set(Context context) { _$Context = new WeakReference<>(context); }

    /**
     * Get package name to internal use
     *
     * @return String
     */
    public static String _getPackageName() { return _$Context == null ? Unio.app().getPackageName() : _$Context.get().getPackageName(); }


    /*************************************************************************************/
    /***                            Basic Getter from int id                           ***/
    /*************************************************************************************/

    /**
     * Return resource ID value by type
     *
     * @param resource resource ID
     * @param type     type
     *
     * @return Object
     */
    public static Object get(int resource, String type) {
        switch (type) {
            case STRING:
                return getString(resource);
            case COLOR:
                return getColor(resource);
            case DRAWABLE:
                return getDrawable(resource);
            default:
                return "";
        }
    }

    /**
     * Convert resource ID to this value
     */
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static int getColor(int resource)         { return Android.CURRENT_VERSION > Android.LOLLIPOP_MR1 ? get().getColor(resource, null) : get().getColor(resource); }
    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    public static Drawable getDrawable(int resource) { return Android.CURRENT_VERSION > Android.KITKAT_WATCH ? get().getDrawable(resource, null) : get().getDrawable(resource); }
    public static String getString(int resource)     { return getString(resource, Locale.getDefault().getLanguage()); }
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static String getString(int resource, String local) {
        if (local.equals(Locale.getDefault().getLanguage())) return get().getString(resource);

        Configuration conf = get().getConfiguration();
        if (Android.CURRENT_VERSION > Android.JELLY_BEAN) {
            conf.setLocale(new Locale(local));
        } else {
            conf.locale = new Locale(local);
        }

        DisplayMetrics metrics = new DisplayMetrics();
        ActivityManager.get().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Resources resources = new Resources(Unio.app().getAssets(), metrics, conf);
        return resources.getString(resource);
    }

    /**
     * Return a styleable
     *
     * @param name styleable name
     *
     * @return int
     */
    public static int getStyleable(String name) {
        try {
            //use reflection to access the resource class
            Field[] fields = Class.forName(_getPackageName()+".R$styleable").getFields();

            //browse all fields
            for (Field f : fields) {
                //pick matching field
                if (f.getName().equals(name)) return (int)f.get(null);
            }
        } catch (Exception e) {
            Trace.get(e);
        }
        return 0;
    }

    /**
     * Retorn a group of styleables
     *
     * @param name styleables name
     *
     * @return int[]
     */
    public static int[] getStyleables(String name) {
        try {
            //use reflection to access the resource class
            Field[] fields = Class.forName(_getPackageName()+".R$styleable").getFields();

            //browse all fields
            for (Field f : fields) {
                //pick matching field
                if (f.getName().equals(name)) return (int[])f.get(null);
            }
        } catch (Exception e) {
            if (Unio.app() != null) Trace.get(e);
        }
        return null;
    }


    /*************************************************************************************/
    /***                           Basic Getter from String id                         ***/
    /*************************************************************************************/

    /**
     * Check if resource ID exists
     *
     * @param resource resource name
     * @param type     resource type (flogas from {@link ResourceManager})
     *
     * @return boolean
     */
    public static boolean exists(String resource, String type)                     { return exists(resource, type, _getPackageName()); }
    public static boolean exists(String type, String resource, String packageName) { return getId(type, resource, packageName) != 0;   }

    /**
     * Get resource ID from String name
     *
     * @param resource resource name
     * @param type     resource type
     *
     * @return int
     */
    public static int getId(String resource, String type)                     { return getId(resource, type, _getPackageName());           }
    public static int getId(String resource, String type, String packageName) {
        try {
            Class c = Class.forName(_getPackageName()+".R$"+type);
            Field field = c.getDeclaredField(resource);
            return (int)field.get(null);
        } catch (ClassNotFoundException e) {
            return 0;
        } catch (IllegalAccessException e) {
            return 0;
        } catch (NoSuchFieldException e) {
            return 0;
        }
    }

    /**
     * Get resource string name from id
     *
     * @param id id
     *
     * @return String
     */
    public static String getNameFromId(int id) {
        try {
            return get().getResourceName(id);
        } catch(Resources.NotFoundException e) {
            return null;
        }
    }
}