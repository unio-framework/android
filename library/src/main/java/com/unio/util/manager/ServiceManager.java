package com.unio.util.manager;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import java.lang.ref.WeakReference;

/**
 * ServiceManager
 *
 * Service management class
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public class ServiceManager
{
    /** Last started Service **/
    private static WeakReference<Service> _current;

    /**
     * Start a service
     *
     * @param serviceClass service class
     */
    public static void start(Class<?> serviceClass) { start(Unio.app(), serviceClass); }

    /**
     * Start a service
     *
     * @param context     context
     * @param serviceClass service class
     */
    public static void start(Context context, Class<?> serviceClass) {
        Intent service = new Intent(context, serviceClass);
        try {
            context.startService(service);
        } catch(IllegalStateException e) {
            // From Android O, startService is not allowed if app not exists in foreground
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(service);
            } else {
                Trace.get(e);
            }
        }
    }

    /**
     * Stop a service
     *
     * @param serviceClass service class
     */
    public static void finish(Class serviceClass) { finish(Unio.app(), serviceClass); }

    /**
     * Stop a service
     *
     * @param context     context
     * @param serviceClass service class
     */
    public static void finish(Context context, Class<?> serviceClass) {
        Intent service = new Intent(context, serviceClass);
        context.stopService(service);
    }

    /**
     * Check if a service is running
     *
     * @param serviceClass service class
     *
     * @return boolean
     */
    public static boolean running(Class serviceClass) { return running(Unio.app(), serviceClass); }

    /**
     * Check if a service is running
     * Method ActivityManager#getRunningServices is deprecated since Android 26
     *
     * @param context      context
     * @param serviceClass service class
     *
     * @return boolean
     */
    @SuppressWarnings("deprecation")
    public static boolean running(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            // Checagem extra do pid, por um bug do Android 4.2 a 4.4, que não consegue reiniciar o serviço
            if (serviceClass.getName().equals(service.service.getClassName()) && service.pid != 0) return true;
        }
        return false;
    }

    /**
     * Set last started service
     *
     * @param service service instance
     */
    public static void set(Service service) { _current = new WeakReference<>(service); }

    /**
     * Get last started service
     *
     * @return Service
     */
    public static Service get() { return _current == null ? null : _current.get(); }
}