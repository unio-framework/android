package com.unio.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.unio.ui.annotation.LayoutAnnotation;
import com.unio.shortcuts.Manage;
import com.unio.util.manager.ActivityManager;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * UFragment
 *
 * Class to manage fragments
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 3.0 06/07/2016 15:16
 */
public abstract class UFragment<A extends Activity, V extends View> extends Fragment
{
    /** onBackPressed return values */
    public static final int BACK_DEFAULT  = 1;
    public static final int BACK_ONLY_POP = 2;
    public static final int BACK_NONE     = 3;

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({ BACK_DEFAULT, BACK_ONLY_POP, BACK_NONE })
    public @interface BackEvent {}

    /** Layout **/
    protected V layout;
    private A _activity;

    /**
     * Get layout
     * If use {@link com.unio.ui.annotation.FindViewById}, will set automatically
     *
     * @return Object
     */
    protected Object getLayout() { return LayoutAnnotation.findLayoutId(this.activity(), this.getClass()); }

    /**
     * Abstract
     * Start method
     */
    protected abstract void onInit(Bundle savedInstanceState);

    /**
     * onCreateView
     *
     * @return View
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.setRetainInstance(true);
        Object v = this.getLayout();
        if (v != null) {
            if (v.getClass().equals(int.class) || v instanceof Integer) {
                if (((int)v) != 0) this.layout = (V)inflater.inflate((int)v, container, false);
            } else if (v instanceof View) {
                this.layout = (V)v;
            }
        }
        LayoutAnnotation.attributeViews(this.activity(), this);
        this.onInit(savedInstanceState);
        return this.layout;
    }

    /**
     * Shortcut to findViewById
     *
     * @param id resourceID
     *
     * @return View
     */
    public View findViewById(int id) { return this.layout.findViewById(id); }

    /**
     * onResume
     */
    @Override
    public void onResume() {
        super.onResume();
        if (ActivityManager.get() != this.activity()) {
            // Use if return to an paused Activity
            Manage.set(this.activity());
        }
    }

    /**
     * Get Activity
     *
     * @param activity activity
     */
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this._activity = (A)activity;
    }

    /**
     * Get Activity
     *
     * @param context activity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this._activity = (A)context;
    }

    /**
     * Get Activity with more accuracy
     *
     * @return A
     */
    public A activity() {
        Activity activity = this.getActivity();
        if (activity == null) {
            return this._activity;
        } else {
            return (A)activity;
        }
    }

    /**
     * Action when press back button
     * Need to implement, if will use in Activity
     */
    public @BackEvent int onBackPressed() { return BACK_DEFAULT; }
}