package com.unio.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.unio.R;
import com.unio.app.UnioService;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.ui.annotation.LayoutAnnotation;
import com.unio.shortcuts.Manage;
import com.unio.util.helper.Permission;
import com.unio.util.manager.ActivityManager;
import com.unio.util.statics.interfaces.IOnFindResult;
import java.util.LinkedList;
import java.util.List;

/**
 * UActivity
 *
 * Activity base content
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class UActivity<V extends View> extends AppCompatActivity
{
    /** If layout is View object, use for manage internal components **/
    protected V layout;
    protected Toolbar toolbar;

    /** Flag to identify if Activity is called from notification */
    private UNotification.Call _Notification = new UNotification.Call(false, -1);

    /** Receive callbacks for single invoke an Activity */
    protected static List<IOnFindResult> singleCallbacks = new LinkedList<>();

    protected Class classToValidate; // Class to validate permission
    private Bundle savedInstance; // savedInstanceState for onInit()
    private boolean isInitCalled = false;

    /**
     * Retorn layout's resource ID or {@link View} instance
     *
     * @return Object
     */
    protected Object getLayout() { return LayoutAnnotation.findLayoutId(this, this.getClass()); }

    /**
     * Abstract
     * Initiation method
     */
    protected abstract void onInit(Bundle savedInstanceState);

    /**
     * Creation method
     * Start Activity process
     *
     * @param savedInstanceState params
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Manage.set(this);
        this._Notification = new UNotification.Call(this.getIntent());

        // Start debug settings
        // Need to set here because need an Activity instance
        Trace.start(this);

        // Check app version
        // Need to set here because need an Activity instance
        Unio.version().start();

        Object v = this.getLayout();
        if (v != null) {
            if (v.getClass().equals(int.class) || v instanceof Integer) {
                if (((int)v) != 0) this.layout = (V)this.getLayoutInflater().inflate((int)v, null, false);
            } else if (v instanceof View) {
                this.layout = (V)v;
            }
        }
        this.onRenderLayout();
        this.savedInstance = savedInstanceState;
        // Start service from Activity to avoid ServerException
        UnioService.start();

        if (this.requestPermission(this, this.getClass())) {
            this.onInit(savedInstanceState);
            this.isInitCalled = true;
        }
    }

    /**
     * Set layou to Activity view
     */
    protected void onRenderLayout() {
        if (this.layout != null) {
            this.setContentView(this.layout);
            LayoutAnnotation.attributeViews(this, this);
        }
        this.findToolbar();
    }

    /**
     * onResume
     */
    @Override
    public void onResume() {
        super.onResume();
        if (ActivityManager.get() != this) {
            // Para repor mesmo fora do onCreate
            // onResume é chamado mesmo na hora de criar a Activity
            Manage.set(this);
        }
    }

    /**
     * Método finish
     */
    @Override
    public void finish() {
        ActivityManager.remove(this);
        super.finish();
    }

    /**
     * Called after permission validation
     *
     * @param requestCode  permission code
     * @param permissions  validated permissions, receives {@link android.Manifest.permission} string
     * @param grantResults permission grant result, receives -1 for denied, 0 for accepted
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Permission.RESPONSE) {
            if (classToValidate == getClass() && LayoutAnnotation.isPermissionsEnabled(classToValidate) && !isInitCalled) {
                this.onInit(this.savedInstance);
                this.isInitCalled = true;
            }
            this.onPermissionsResult(this.classToValidate, permissions, grantResults);
        }
    }

    /**
     * Called after permission validation, only for {@link Permission#RESPONSE}
     *
     * @param permissionClass validated class
     * @param permissions     validated permissions
     * @param grantResults    permission grant result
     */
    public void onPermissionsResult(@NonNull Class<?> permissionClass, @NonNull String[] permissions, @NonNull int[] grantResults) {}

    /**
     * Validate permission from RequestPermissions annotation
     *
     * @param activity activity for result callback and permission validation
     *
     * @return boolean, return if need to request permission
     */
    @SuppressWarnings("UnusedReturnValue")
    public boolean requestPermission(Activity activity) { return requestPermission(activity, activity.getClass()); }

    /**
     * Validate permission from RequestPermissions annotation
     *
     * @param activity    activity for result callback
     * @param targetClass activity to check required permission
     *
     * @return boolean, return if need to request permission
     */
    public boolean requestPermission(Activity activity, Class<?> targetClass) {
        boolean isValid = LayoutAnnotation.validatePermissions(activity, targetClass);
        if (isValid == false) this.classToValidate = targetClass;
        return isValid;
    }

    /**
     * Retorna dados da notificação
     *
     * @return UNotification.Call
     */
    public UNotification.Call notification() { return this._Notification; }

    /**
     * Add callback when single invoke an Activity
     */
    public static void onCaptured(IOnFindResult callback) { if (callback != null) singleCallbacks.add(callback); }

    /**
     * Call stored single activity invoke callbacks
     *
     * @param result result for invoke
     */
    protected void callSingleCallbacks(Object result) {
        boolean isFinished = false;
        if (singleCallbacks.size() > 0) {
            for (IOnFindResult c : singleCallbacks) isFinished = c.onFindResult(this, result);
            // After call, clear all callback for new usage
            // This is static
            singleCallbacks.clear();
        }
        // For single Activities, always finish after called
        if (isFinished == false) this.finish();
    }

    /**
     * Search default Toolbar instance. It's not accessible easly
     */
    private void findToolbar() {
        View toolbarView = this.getWindow().findViewById(R.id.action_bar);
        if (toolbarView instanceof Toolbar)
            this.toolbar = (Toolbar) toolbarView;
    }
}