package com.unio.base;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import com.unio.shortcuts.Compatibility;
import com.unio.util.helper.Check;
import com.unio.util.helper.Color;
import com.unio.util.manager.ActivityManager;
import com.unio.util.manager.NotificationManager;
import com.unio.util.manager.ResourceManager;

/**
 * UNotification
 *
 * Class to create notification
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 12/10/2016 0:24
 */
public class UNotification extends NotificationCompat.Builder
{
    /** Notification identification */
    public static final String TAG = "UNotification";
    public static final String ID  = "UNotification#ID";

    /** Notification Builder */
    private NotificationCompat.BigTextStyle _BigStyle;
    private Context _Context;

    /** Attributes */
    private int _id = -1;
    private String _message;
    private Class _target;
    private Intent _intent;

    /** Internal flags */
    private boolean _isAutoCancel = true;
    private boolean _useHeadsUp   = true;

    /**
     * Constructor
     *
     * @param icon    icon
     * @param title   title
     * @param message message
     */
    public UNotification(@DrawableRes int icon, Object title, Object message) { this(ActivityManager.get(), icon, title, message); }

    /**
     * Constructor
     *
     * @param context context
     * @param icon    icon
     * @param title   title
     * @param message message
     */
    public UNotification(@NonNull Context context, @DrawableRes int icon, Object title, Object message) {
        super(context, context.getPackageName());
        this._Context = context;
        this.setSmallIcon(icon);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            this.setColor(Color.TRANSPARENT);

        String sTitle = null;
        if (title != null) {
            if (title instanceof Integer) {
                sTitle = ResourceManager.getString((int) title);
            } else if (title instanceof String) {
                sTitle = (String) title;
            } else {
                sTitle = title.toString();
            }
        }
        if (Check.isEmpty(sTitle) == false) this.setContentTitle(sTitle);

        String sMessage = null;
        if (message != null) {
            if (message instanceof Integer) {
                sMessage = ResourceManager.getString((int) message);
            } else if (message instanceof String) {
                sMessage = (String) message;
            } else {
                sMessage = message.toString();
            }
        }
        this._message = sMessage;
    }

    /**
     * Set a custom id
     *
     * @param id custom id
     *
     * @return UNotification
     */
    public UNotification setId(int id) {
        this._id = id;
        return this;
    }

    /**
     * Get notification id
     *
     * @return int
     */
    public int getId() { return this._id; }

    /**
     * Set Activity class to open on click
     *
     * @param target Activity class
     *
     * @return UNotification
     */
    public UNotification setTarget(Class target) {
        this._target = target;
        return this;
    }

    /**
     * Set target as intent
     *
     * @param intent Intent
     *
     * @return UNotification
     */
    public UNotification setTargetIntent(Intent intent) {
        this._intent = intent;
        return this;
    }

    /**
     * Set auto cancel
     *
     * @param autoCancel flag
     *
     * @return UNotification
     */
    @Override
    public UNotification setAutoCancel(boolean autoCancel) {
        super.setAutoCancel(autoCancel);
        this._isAutoCancel = autoCancel;
        return this;
    }

    /**
     * Enable/disable heads up
     *
     * @param use flag
     *
     * @return UNotification
     */
    public UNotification setUseHeadsUp(boolean use) {
        this._useHeadsUp = use;
        return this;
    }

    /**
     * Add an action button
     *
     * @param icon   drawable icon
     * @param title  title
     * @param target target Activity
     *
     * @return UNotification
     */
    public UNotification addAction(@DrawableRes int icon, @StringRes int title, Class target) {
        String sTitle = ResourceManager.getString(title);
        return this.addAction(icon, sTitle, target);
    }

    /**
     * Add an action button
     *
     * @param icon   drawable icon
     * @param title  title
     * @param target target Activity
     *
     * @return UNotification
     */
    public UNotification addAction(@DrawableRes int icon, @NonNull String title, Class target) {
        Intent intent = new Intent(this._Context, target);
        return this.addAction(icon, title, intent);
    }

    /**
     * Add an action button
     *
     * @param icon   drawable icon
     * @param title  title
     * @param intent Intent instance
     *
     * @return UNotification
     */
    public UNotification addAction(@DrawableRes int icon, @NonNull String title, Intent intent) {
        if (this._id == -1) this._id = NotificationManager.add(this._target);
        intent.putExtra("action", title);
        intent.putExtra(TAG, true); // Identificação para saber se veio da notificação
        if (this._isAutoCancel) intent.putExtra(ID, this._id);
        PendingIntent targetIntent =
            PendingIntent.getActivity(this._Context, Compatibility.getNewViewId(), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        super.addAction(icon, title, targetIntent);
        return this;
    }

    /**
     * Show notification
     */
    public void show() {
        if (NotificationManager.exists(this._id)) {
            if (this._target == null) {
                this._target = NotificationManager.get(this._id);
            } else {
                NotificationManager.update(this._id, this._target);
            }
        } else {
            if (this._id == -1) {
                this._id = NotificationManager.add(this._target);
            } else {
                NotificationManager.add(this._id, this._target);
            }
        }
        this._execute();
    }

    /**
     * Execute notification build
     */
    @SuppressWarnings("NewApi")
    private void _execute() {
        this.setAutoCancel(this._isAutoCancel);
        if (this._useHeadsUp) {
            // must give priority to High, Max which will considered as heads-up notification
            this.setPriority(NotificationCompat.PRIORITY_MAX);
        }

        this.setCategory(NotificationCompat.CATEGORY_MESSAGE);
        if (this._target != null || this._intent != null) this.setContentIntent(this._getContentIntent());
        android.app.NotificationManager notification = (android.app.NotificationManager) this._Context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Check.isEmpty(this._message) == false) {
            this.setContentText(this._message);
            this.setTicker(this._message);
            this._BigStyle = new NotificationCompat.BigTextStyle(this);
            this._BigStyle.bigText(this._message);
        }
        Notification notify = this._BigStyle == null ? this.build() : this._BigStyle.build();

        Intent delete = new Intent(this._Context, Receiver.class);
        delete.putExtra(ID, this._id);
        notify.deleteIntent = PendingIntent.getBroadcast(this._Context, 0, delete, PendingIntent.FLAG_CANCEL_CURRENT);

        // mId allows you to update the notification later on.
        if (NotificationManager.exists(this._id)) notification.cancel(this._id);
        notification.notify(this._id, notify);
    }

    /**
     * Add content intent
     *
     * @return PendingIntent
     */
    private PendingIntent _getContentIntent() {
        if (this._intent == null) {
            // Creates an explicit intent for an Activity in your app
            this._intent = new Intent(this._Context, this._target);
        }
        this._intent.putExtra(TAG, true); // Identificação para saber se veio da notificação
        if (this._isAutoCancel) this._intent.putExtra(ID, this._id);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this._Context);
        if (this._target != null) {
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(this._target);
        }
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(this._intent);
        return stackBuilder.getPendingIntent(Compatibility.getNewViewId(), PendingIntent.FLAG_CANCEL_CURRENT);
    }

    /**
     * Receiver
     *
     * Class to receive notification swipe event
     * Need to add on AndroidManifest.xml:
     *   <receiver android:name="com.unio.base.UNotification$Receiver" />
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 12/06/2015 14:09
     */
    public static class Receiver extends BroadcastReceiver
    {
        @Override
        public void onReceive(Context context, Intent intent) {
            int id = intent.getIntExtra(ID, -1);
            if (id != -1) NotificationManager.dismiss(id);
        }
    }

    /**
     * Call
     *
     * Class to receive notification callback
     * Commonly used in Activity
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 2.0 27/06/2017 14:35
     */
    public static class Call
    {
        /** Attributes */
        public final boolean called;
        public final int id;

        /**
         * Constructor
         *
         * @param intent context intent
         */
        public Call(Intent intent) {
            boolean called = intent.getBooleanExtra(TAG, false);
            int id = intent.getIntExtra(ID, -1);
            if (id != -1) NotificationManager.dismiss(id);

            this.called = called;
            this.id = id;
        }

        /**
         * Constructor
         * For direct attribution, only used locally
         *
         * @param called Activity is called
         * @param id     Notification id
         */
        Call(boolean called, int id) {
            this.called = called;
            this.id = id;
        }
    }
}