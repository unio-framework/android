package com.unio.base;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.SuppressLint;
import android.view.accessibility.AccessibilityEvent;
import com.unio.debug.Trace;
import com.unio.util.control.Accessibility;

/**
 * UAccessibilityService
 *
 * Service for receive accessibility broadcasts
 *
 * - Need permission: android.permission.BIND_ACCESSIBILITY_SERVICE
 * - Need enable in Accessibility Setting Screen (See {@link Accessibility}) for work
 *
 * Add this lines in AndroidManifest.xml for each Accessibility service:
 * <service android:name="[Accessibility service package]"
 *     android:label="[Name to show in Accessibility Setting Screen]"
 *     android:permission="android.permission.BIND_ACCESSIBILITY_SERVICE"
 *     android:enabled="true"
 *     android:exported="false"
 *     >
 *     <intent-filter>
 *         <action android:name="android.accessibilityservice.AccessibilityService" />
 *     </intent-filter>
 * </service>
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 30/06/2016 17:24
 */
public abstract class UAccessibilityService extends AccessibilityService
{
    /**
     * Action in onServiceConnected
     */
    protected void onInit() {}

    /**
     * onInterrupt
     */
    @Override
    public void onInterrupt() {}

    /**
     * Action when service connected
     */
    @Override
    public final void onServiceConnected() {
        Trace.log(this, "service connected");
        AccessibilityServiceInfo info = this.getAccessibilityServiceInfo();
        this.setServiceInfo(info);
        this.onInit();
    }

    /**
     * Create accessibility service info
     *
     * @return AccessibilityServiceInfo
     */
    @SuppressLint("NewApi")
    protected AccessibilityServiceInfo getAccessibilityServiceInfo() {
        AccessibilityServiceInfo info;
        info = this.getServiceInfo();
        info.eventTypes          = this.getEventTypes();
        info.notificationTimeout = this.getNotificationTimeout();
        info.feedbackType        = this.getFeedbackType();
        return info;
    }

    /**
     * @see AccessibilityServiceInfo#eventTypes
     *
     * @return int
     */
    protected int getEventTypes() { return AccessibilityEvent.TYPES_ALL_MASK; }

    /**
     * @see AccessibilityServiceInfo#notificationTimeout
     *
     * @return int
     */
    protected long getNotificationTimeout() { return 100; }

    /**
     * @see AccessibilityServiceInfo#feedbackType
     *
     * @return int
     */
    protected int getFeedbackType() { return AccessibilityEvent.TYPES_ALL_MASK; }
}