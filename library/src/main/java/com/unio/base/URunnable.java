package com.unio.base;

import android.os.Handler;
import com.unio.core.Unio;
import com.unio.util.statics.constants.Time;

/**
 * URunnable
 *
 * Make runnable to rerun with an interval
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class URunnable implements Runnable
{
    /** Check if is running */
    private boolean _isRunning = false;

    /**
     * Run interval, default is 30 minutes
     */
    protected long interval = Time.Milisecond.MINUTE*30;

    /**
     * UI handler
     */
    protected Handler handler = Unio.ui();

    /**
     * onInit
     */
    protected abstract void onInit();

    /**
     * Add settings
     */
    protected void settings() {}

    /**
     * Start looping
     */
    public void start() {
        if (this._isRunning == false) {
            this._isRunning = true;
            this.settings();
            this.run();
        }
    }

    /**
     * Stop looping
     */
    public void stop() {
        this.handler.removeCallbacks(this);
        this._isRunning = false;
    }

    /**
     * Check if is running
     *
     * @return boolean
     */
    public boolean isRunning() { return this._isRunning; }

    /**
     * Runnable run
     */
    @Override
    public void run() {
        if (this._isRunning) {
            this.onInit();
            this.handler.postDelayed(this, this.interval);
        }
    }
}