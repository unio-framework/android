package com.unio.base;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.widget.RemoteViews;

/**
 * UWidget
 *
 * Widget base class
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class UWidget extends AppWidgetProvider
{
    /** Attributes */
    protected AppWidgetManager Manager;
    protected ComponentName    Provider;
    protected RemoteViews      Layout;
    protected Context          context;

    /**
     * Widget layout
     *
     * @return Object
     */
    protected abstract int getLayout();

    /**
     * Run content
     */
    protected abstract void onInit();

    /**
     * Is called when widget updates
     *
     * @param context          Context
     * @param appWidgetManager Manager
     * @param appWidgetIds     IDs
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        this.context = context;
        this.Manager  = appWidgetManager;
        this.Provider = new ComponentName(context, this.getClass());

        int v = this.getLayout();
        if (v != 0) this.Layout = new RemoteViews(context.getPackageName(), v);

        this.onInit();
    }
}