package com.unio.base;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.unio.shortcuts.Manage;

/**
 * UService
 *
 * Service base class
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 06/07/2014 01:35
 */
public abstract class UService extends Service
{
    /**
     * Start
     *
     * @param intent  intent
     * @param flags   flag
     * @param startId startId
     *
     * @return int
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) { return START_STICKY; }

    /**
     * onBind
     *
     * @param intent intent
     *
     * @return IBinder
     */
    @Override
    public IBinder onBind(Intent intent) {
        this.onInit(intent);
        return null;
    }

    /**
     * onCreate
     */
    @Override
    public void onCreate() {
        Manage.set(this);
        this.onInit(null);
    }

    /**
     * Abstract
     * Service content
     */
    public abstract void onInit(Intent intent);
}