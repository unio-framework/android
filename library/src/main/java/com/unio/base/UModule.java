package com.unio.base;

/**
 * UModule
 *
 * Integration with external or internal Unio modules
 * Is added using Unio class, maintaining in cache and usable in anywhere
 *
 * Called: Unio.module(Classe.class)
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 11/03/2015 17:46
 */
public abstract class UModule
{
    /** Flags */
    protected boolean isRunning = false;

    /**
     * Flag to define if module will be recreated in every call
     * False by default to use in cache
     *
     * @return boolean
     */
    public boolean reloadWhenCalled() { return false; }

    /**
     * Called when is instantiate, it's possible override this and add parameters
     */
    public void onInit() {}

    /**
     * Called when module is called in app, it's possible override this and add parameters
     * This method is not called if {@link #reloadWhenCalled()} is true and if onInit is called
     * This method is only a way to hook module if is called after instantiate
     */
    public void onCalled() {}

    /**
     * Check if module is running
     *
     * @return boolean
     */
    public boolean isRunning() { return this.isRunning; }
}