package com.unio.database.sql.builder;

import android.database.Cursor;
import com.unio.database.sql.orm.Cache;
import com.unio.database.sql.orm.TableInfo;
import com.unio.database.sql.orm.ModelFind;
import com.unio.database.sql.orm.Record;
import com.unio.debug.Trace;
import com.unio.core.Unio;
import com.unio.database.sql.orm.Model;
import com.unio.util.helper.Check;
import com.unio.util.helper.Concat;
import com.unio.util.helper.Convert;

/**
 * QueryBuilder
 *
 * Class to create Queries
 *
 * Ex:
 *   new QueryBuilder()
 *       .columns("*")
 *       .from("table")
 *       .join("table2",
 *           new String[] { "table2.column1=?", "table2.column2=?" },
 *           new String[] { "a", "b" }
 *       )
 *       .leftJoin("table3",
 *           new String[] { "table3.column1=?", "table3.column2=?" },
 *           new String[] { "a", "b" }
 *       )
 *       .filter(
 *           new String[] { "column1=?", "column2=?" },
 *           new String[] { "a", "b" }
 *       )
 *       .order("column1")
 *       .group("column1")
 *       .having("column1=?", new String[] { "5" })
 *       .limit(5) / .limit(0,5)
 *       .execute();
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 24/07/2014 00:08
 */
public class QueryBuilder
{
    /** table columns, default all */
    private String columns = "*";

    /** table name */
    protected String table;

    /** WHERE */
    protected String where;

    /** WHERE args */
    protected String[] whereArgs;

    /** ORDER BY */
    private String _order;

    /** GROUP BY */
    private String _group;

    /** HAVING */
    private String _having;

    /** HAVING args */
    private String[] _havingArgs;

    /** LIMIT */
    private String _limit;

    /** Pilha com join */
    private QueryBuilderJoin _join = new QueryBuilderJoin();

    /** Query */
    private String _sql;

    /** BindVar */
    private String[] _params;

    /** Model class to ORM integration */
    private Class<? extends Model> _c;

    /** Database name */
    private String _dbName;

    /**
     * Default constructor method
     */
    public QueryBuilder() {}

    /**
     * Constructor with {@link Model} integration
     *
     * @param c Class do model
     */
    public QueryBuilder(Class<? extends Model> c) {
        this._c = c;

        TableInfo info = Cache.get(c);
        this._dbName = info.dbName;
        this.table   = info.tableName;
    }

    /**
     * SELECT
     *
     * @param columns column names
     *
     * @return QueryBuilder
     */
    public QueryBuilder columns(String columns) {
        this.columns = columns;
        return this;
    }

    /**
     * FROM
     *
     * @param table tableName
     *
     * @return QueryBuilder
     */
    public QueryBuilder from(String table) {
        this.table = table;
        return this;
    }

    /**
     * Build JOINs with {@link QueryBuilder} instance
     *
     * @param child child instance
     *
     * @return QueryBuilder
     */
    public QueryBuilder join(QueryBuilder child) { return this.join(child.table, child.where, child.whereArgs); }

    /**
     * JOIN
     *
     * @param table  table name
     * @param on     ON filter
     * @param onArgs On filter argments
     *
     * @return QueryBuilder
     */
    public QueryBuilder join(String table, String on, String[] onArgs) {
        this._join.set(QueryBuilderJoin.JOIN, table, on, onArgs);
        return this;
    }

    /**
     * Build LEFT JOINs with {@link QueryBuilder} instance
     *
     * @param child child instance
     *
     * @return QueryBuilder
     */
    public QueryBuilder leftJoin(QueryBuilder child) { return this.leftJoin(child.table, child.where, child.whereArgs); }

    /**
     * LEFT JOIN
     *
     * @param table  table name
     * @param on     ON filter
     * @param onArgs On filter argments
     *
     * @return QueryBuilder
     */
    public QueryBuilder leftJoin(String table, String on, String[] onArgs) {
        this._join.set(QueryBuilderJoin.LEFT_JOIN, table, on, onArgs);
        return this;
    }

    /**
     * Build RIGHT JOINs with {@link QueryBuilder} instance
     *
     * @param child child instance
     *
     * @return QueryBuilder
     */
    public QueryBuilder rightJoin(QueryBuilder child) { return this.rightJoin(child.table, child.where, child.whereArgs); }

    /**
     * Alias for join
     *
     * @return QueryBuilder
     */
    public QueryBuilder on(String where) { return this.on(where, null); }

    /**
     * Alias for join
     *
     * @return QueryBuilder
     */
    public QueryBuilder on(String where, Object[] args) { return this.filter(where, args); }

    /**
     * RIGHT JOIN
     *
     * @param table  table name
     * @param on     ON filter
     * @param onArgs On filter argments
     *
     * @return QueryBuilder
     */
    public QueryBuilder rightJoin(String table, String on, String[] onArgs) {
        this._join.set(QueryBuilderJoin.RIGHT_JOIN, table, on, onArgs);
        return this;
    }

    /**
     * WHERE
     *
     * @param where condition
     *
     * @return QueryBuilder
     */
    public QueryBuilder filter(String where) { return this.filter(where, new Object[0]); }

    /**
     * WHERE
     *
     * @param where condition
     * @param args  argments
     *
     * @return QueryBuilder
     */
    public QueryBuilder filter(String where, Object[] args) {
        this.where     = where;
        this.whereArgs = this.argsToStringArray(args);
        return this;
    }

    /**
     * Convert Object to String
     *
     * @param oArgs Object[] argments
     *
     * @return String[]
     */
    private String[] argsToStringArray(Object[] oArgs) {
        if (Check.isEmpty(oArgs)) return new String[0];

        String[] args = new String[oArgs.length];
        for (int i = 0; i< oArgs.length; i++) {
            Object p = oArgs[i];
            String v;
            if (p instanceof String) {
                v = p.toString();
            } else {
                v = String.valueOf(p);
            }
            args[i] = v;
        }
        return args;
    }

    /**
     * GROUP BY
     *
     * @param group group params
     *
     * @return QueryBuilder
     */
    public QueryBuilder group(String group) {
        this._group = group;
        return this;
    }

    /**
     * ORDER BY
     *
     * @param order columns
     *
     * @return QueryBuilder
     */
    public QueryBuilder order(String order) {
        this._order = order;
        return this;
    }

    /**
     * HAVING
     *
     * @param having     condition
     * @param havingArgs argments
     *
     * @return QueryBuilder
     */
    public QueryBuilder having(String having, String[] havingArgs) {
        this._having = having;
        this._havingArgs = havingArgs;
        return this;
    }

    /**
     * LIMIT, only with max
     *
     * @param limit limit
     *
     * @return QueryBuilder
     */
    public QueryBuilder limit(int limit) { return this.limit(0, limit); }

    /**
     * LIMIT, with min and max
     *
     * @param start min limit
     * @param end   max limit
     *
     * @return QueryBuilder
     */
    public QueryBuilder limit(int start, int end) {
        this._limit = start+", "+end;
        return this;
    }

    /**
     * Prepare query
     */
    public void prepare() {
        String sql = "";
        int key;

        // select()
        sql += "SELECT "+this.columns+" ";
        // from()
        sql += "FROM "+this.table+" ";
        // join
        sql += this._join.get();
        // where()
        if (Check.isEmpty(this.where) == false) sql += "WHERE "+this.where+" ";
        // group()
        if (Check.isEmpty(this._group) == false)  sql += "GROUP BY "+ this._group+" ";
        // order()
        if (Check.isEmpty(this._order) == false)  sql += "ORDER BY "+this._order+" ";
        // having()
        if (Check.isEmpty(this._having) == false) sql += "HAVING "+this._having+" ";
        // limit()
        if (Check.isEmpty(this._limit) == false)  sql += "LIMIT "+this._limit;

        String[] params = (String[]) Concat.array(
            this._join.getArgs(),
            this.whereArgs,
            this._havingArgs
        );

        this._sql = sql;
        Trace.log(this, "sql", this._sql);
        this._params = params;
        if (Check.isEmpty(this._params) == false) Trace.log(this, "params", Convert.toString(this._params));
    }

    /**
     * Get sql query
     *
     * @return String
     */
    public String sql() { return this._sql; }

    /**
     * Get sql bindVar params
     *
     * @return String[]
     */
    public String[] params() { return this._params; }

    /**
     * Execute query and get Cursor
     *
     * @return Cursor
     */
    public Cursor execute() { return this.execute(null); }

    /**
     * Execute query with a specific database and get Cursor
     *
     * @return Cursor
     */
    public Cursor execute(String dbName) {
        if (this._sql == null) this.prepare();
        return Unio.db(dbName).fetch(this._sql, this._params);
    }

    /**
     * Integration with Model, find one record
     *
     * @return T
     */
    public <T extends Model>T find() {
        this.limit(1);

        Cursor reg = null;
        if (this._c != null) reg = this.execute(this._dbName);

        T model = reg == null ?
            null :
            (T) new ModelFind(this._c).find(this.execute(this._dbName));

        if (reg != null) reg.close();
        return model;
    }

    /**
     * Integration with Model, find all records filtered
     *
     * @return Record<T>
     */
    public <T extends Model>Record<T> findAll() {
        Cursor reg = null;
        if (this._c != null) reg = this.execute(this._dbName);

        Record<T> record = reg == null ?
            new Record<T>() :
            (Record<T>) new ModelFind(this._c).findAll(this.execute(this._dbName));

        if (reg != null) reg.close();
        return record;
    }

    /**
     * Retorna o total de registros
     *
     * @return int
     */
    public int count() {
        Cursor reg = null;
        if (this._c != null) reg = this.execute(this._dbName);
        int total = reg == null ? 0 : reg.getCount();
        if (reg != null) reg.close();
        return total;
    }
}