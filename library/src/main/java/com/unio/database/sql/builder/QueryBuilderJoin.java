package com.unio.database.sql.builder;

import com.unio.util.helper.Concat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * QueryBuilderJoin
 *
 * JOIN query builder
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 25/07/2014 23:01
 */
public class QueryBuilderJoin
{
    /** Flag with join types **/
    public static final String JOIN       = "JOIN";
    public static final String LEFT_JOIN  = "LEFT JOIN";
    public static final String RIGHT_JOIN = "RIGHT JOIN";

    /** Join table list */
    private ArrayList<String> _tables = new ArrayList<>();

    /** Table join type */
    private Map<String, String> _tableJoinType = new LinkedHashMap<>();

    /** ON query */
    private Map<String, String> _tableOn = new LinkedHashMap<>();

    /** ON args */
    private String[] _tableOnArgs;

    /**
     * Add join
     *
     * @param type   join type
     * @param table  table name
     * @param on     ON filter
     * @param onArgs ON args
     */
    public void set(String type, String table, String on, String[] onArgs) {
        this._tables.add(table);
        this._tableJoinType.put(table, type);
        this._tableOn.put(table, on);
        this._tableOnArgs = (String[]) Concat.array(this._tableOnArgs, onArgs);
    }

    /**
     * Get all joins
     *
     * @return String
     */
    public String get() {
        String sql = "";
        for (String table : this._tables) {
            sql += this._tableJoinType.get(table)+" "+table+" ";
            sql += "ON "+ this._tableOn.get(table);
            sql += " ";
        }
        return sql;
    }

    /**
     * Get all join args
     *
     * @return String[]
     */
    public String[] getArgs() { return this._tableOnArgs; }
}