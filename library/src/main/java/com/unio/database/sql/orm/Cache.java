package com.unio.database.sql.orm;

import com.unio.database.sql.connection.Db;
import com.unio.util.io.Hash;

import java.util.*;

/**
 * Cache
 *
 * ORM cache class
 * Store all table informations
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 25/07/2014 16:22
 */
public class Cache
{
    /** Constant for _id */
    public static final String PRIMARY_COLUMN = "_id";

    /** SQLite type map */
    private static final Map<Class<?>, String> COLUMN_TYPE = new LinkedHashMap<Class<?>, String>() {
        {
            put(int.class,     Db.TYPE_INTEGER);
            put(boolean.class, Db.TYPE_INTEGER);
            put(float.class,   Db.TYPE_REAL);
            put(double.class,  Db.TYPE_REAL);
            put(long.class,    Db.TYPE_REAL);
            put(String.class,  Db.TYPE_TEXT);
            put(Hash.class,    Db.TYPE_TEXT);
        }
    };

    /** Store all table informations */
    private static Map<Class<? extends Model>, TableInfo> _tableInfos = new LinkedHashMap<>();

    /**
     * Get column type from variable type
     *
     * @param type variable type
     *
     * @return String
     */
    public static String getColumnType(Class<?> type) { return COLUMN_TYPE.get(type); }

    /**
     * Return a table information
     *
     * @param c model class
     *
     * @return TableInfo
     */
    public static TableInfo get(Class<? extends Model> c) {
        if (_tableInfos.containsKey(c)) {
            return _tableInfos.get(c);
        } else {
            TableInfo t = new TableInfo(c);
            _tableInfos.put(c, t);
            return t;
        }
    }

    /**
     * Clear cache
     */
    public static void clear() { _tableInfos.clear(); }

    /**
     * Clear cache of a specific model
     *
     * @param c model class
     */
    public static void clear(Class<? extends Model> c) { _tableInfos.remove(c); }
}