package com.unio.database.sql.orm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * AColumn
 *
 * Annotation for table columns
 *
 * Attributes:
 *   - primaryKey
 *   - defaultValue
 *   - notNull
 *   - unique
 *   - index
 *   - relation
 *   - onDelete
 *   - onUpdate
 *   - onNullConfict
 *   - onUniqueConflict
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 12/02/2016 15:26
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AColumn
{
    /** Default column value */
    String DEFAULT_VALUE = "NULL";

    /** Is primary key? */
    boolean primaryKey() default false;

    /** Is autoincrement? */
    boolean autoIncroment() default false;

    /** NOT NULL? */
    boolean notNull() default false;

    /** Default value */
    String defaultValue() default DEFAULT_VALUE;

    /** Aliases that this column accepts for setAnswer method */
    String[] alias() default {};

    /** Alias for toJson() method */
    String jsonAlias() default "";

    /** Unique key? */
    boolean unique() default false;

    /** Need index? */
    boolean index() default false;

    /** Table relations */
    ARelation relation() default @ARelation;

    /** On null conflict action */
    ConflictAction onNullConflict() default ConflictAction.FAIL;

    /** On unique conflict action */
    ConflictAction onUniqueConflict() default ConflictAction.FAIL;

    /** Column name history */
    String[] history() default {};

    /**
     * ConflictAction
     *
     * Enum with conflict actions
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 11/07/2014 16:50
     */
    enum ConflictAction { ROLLBACK, ABORT, FAIL, IGNORE, REPLACE }
}