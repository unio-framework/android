package com.unio.database.sql.orm;

import android.content.ContentValues;
import android.database.Cursor;
import com.unio.core.Unio;
import com.unio.database.sql.builder.QueryBuilder;
import com.unio.database.sql.connection.Db;
import com.unio.database.sql.orm.annotation.AColumn;
import com.unio.database.sql.orm.annotation.ARelation;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.helper.Convert;
import com.unio.util.io.Hash;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Model
 *
 * Work like database table one record interfaces manager
 *
 * How works:
 * - Add {@link com.unio.database.sql.orm.annotation.ATable} annotation on the class
 * - Add {@link com.unio.database.sql.orm.annotation.AColumn} annotation for each table column fields
 *   It will be used to save, update and attribute on select
 * - For Kotlin, is mandatory add @JvmField for fields, for Java's getField recognize these fields
 * - For table/column settings, see annotation's description
 * - If an @AColumn with primary key settings is ommited, will create an _id column to use as
 *
 * Ex:
 * @ATable
 * class MyTable extends Model
 * {
 *     @AColumn
 *     public String myColumn1;
 *
 *     @AColumn
 *     public int myColumn2;
 *
 *     // Model handlers
 *     public static QueryBuilder model() { return model(MyTable.class); }
 *     public static Table table()        { return table(MyTable.class); }
 * }
 *
 * Kotlin:
 * @ATable
 * class MyTable : Model()
 * {
 *     @JvmField
 *     @AColumn
 *     val myColumn1: String = ""
 *
 *     @JvmField
 *     @AColumn
 *     val myColumn2: int = 0
 *
 *     companion object {
 *         // Model handlers
 *         fun model(): QueryBuilder { return model(MyTable::class.java) }
 *         fun table(): Table { return table(MyTable::class.java) }
 *     }
 * }
 *
 * To insert/update:
 * - Java:
 *   MyModel model = new MyModel();
 *   model.myColumn1 = "Col1";
 *   model.myColumn2 = 1;
 *   model.save();
 *
 * - Kotlin:
 *   val model = MyModel()
 *   model.myColumn1 = "Col1"
 *   model.myColumn2 = 1
 *   model.save()
 *
 * To find:
 * - Java:
 *   MyModel model = MyModel.model().filter("myColumn1 = ?", new Object[] { "Col1" }).find();
 *   model.myColumn1 (will print Col1)
 *   model.myColumn2 (will print 1)
 *
 * - Kotlin:
 *   val model = MyModel.model().filter("myColumn1 = ?", arrayOf("Col1")).find<MyModel>()
 *   or
 *   val model: MyModel = MyModel.model().filter("myColumn1 = ?", arrayOf("Col1")).find()
 *   model.myColumn1 (will print Col1)
 *   model.myColumn2 (will print 1)
 *
 * To find all:
 * {@link com.unio.database.sql.orm.Record} is instance of {@link java.util.LinkedList}
 * - Java:
 *   Record<MyModel> model = MyModel.model().filter("myColumn1 = ?", new Object[] { "Col1" }).findAll();
 *   model.get(0).myColumn1 (will print Col1)
 *   model.get(0).myColumn2 (will print 1)
 *
 * - Kotlin:
 *   val model = MyModel.model().filter("myColumn1 = ?", arrayOf("Col1")).findAll<MyModel>()
 *   or
 *   val model: Record<MyModel> = MyModel.model().filter("myColumn1 = ?", arrayOf("Col1")).findAll()
 *   model[0].myColumn1 (will print Col1)
 *   model[0].myColumn2 (will print 1)
 *
 * Structure:
 *   - model: find(), delete(...)
 *   - table: all table commands
 *   - class: save() and delete()
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
@SuppressWarnings("JavaDoc")
public class Model
{
    /******************************************************************************/
    /**                                  Static                                  **/
    /******************************************************************************/

    /**
     * Instantiate the {@link QueryBuilder}
     *
     * @param c {@link Model} class
     *
     * @return QueryBuilder
     */
    protected static QueryBuilder model(Class<? extends Model> c) { return new QueryBuilder(c); }

    /**
     * Instantiate the {@link Table}
     *
     * @param c {@link Model} class
     *
     * @return Table
     */
    protected static Table table(Class<? extends Model> c) { return new Table(c); }


    /******************************************************************************/
    /**                                  Start                                   **/
    /******************************************************************************/

    /** Contains all table information */
    public final TableInfo Info;

    /** Column to default primary key */
    public int _id;

    /** Formatted values to insert/update */
    protected ContentValues values;

    /** Flags */
    protected boolean _isNewRecord; // Determine if record is new to insert, or already exists to update
    private int _affectedLines;

    /** Contains all model answer */
    private Map<String, String> _answer    = new LinkedHashMap<>();
    private Map<String, String> _oldAnswer = new LinkedHashMap<>();
    private List<String> _errors = new LinkedList<>();

    /** Arguments to delete/update */
    private String _where;
    private String[] _args;

    /** Cache for children */
    private Map<String, Class> _alias;
    private Map<Class<? extends Model>, Model> _child     = new LinkedHashMap<>();
    private Map<Class<? extends Model>, Record> _children = new LinkedHashMap<>();

    /**
     * Constructor method
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    public Model() {
        this._isNewRecord = true;
        this.Info         = Cache.get(this.getClass());
        this._alias       = this.alias();

        // Set initial values
        List<String> columnFields = this.Info.columnFields;
        // Keep as for, as foreach cause ConcurrentModificationException
        for (int key = 0; key < columnFields.size(); key++) {
            String columnName = columnFields.get(key);
            try {
                AColumn annotations = this.Info.columnAnnomations.get(columnName);
                if (annotations == null) continue;
                if (annotations != null && annotations.autoIncroment() && this._isNewRecord) continue;
                Field field = this.getClass().getField(columnName);
                String defaultValue = annotations.defaultValue().equals(AColumn.DEFAULT_VALUE) ? null : annotations.defaultValue();
                if (field.getType().equals(int.class)) {
                    field.set(this, defaultValue == null ? 0 : Integer.valueOf(defaultValue));
                } else if (field.getType().equals(float.class)) {
                    field.set(this, defaultValue == null ? 0 : Float.valueOf(defaultValue));
                } else if (field.getType().equals(double.class)) {
                    field.set(this, defaultValue == null ? 0 : Double.valueOf(defaultValue));
                } else if (field.getType().equals(long.class)) {
                    field.set(this, defaultValue == null ? 0 : Long.valueOf(defaultValue));
                } else {
                    field.set(this, defaultValue);
                }
            } catch (NoSuchFieldException e) {
                Trace.get(e);
            } catch (IllegalAccessException e) {
                Trace.get(e);
            }
        }
    }

    /**
     * Model alias for child responses
     * By default add all {@link #relations()} models with default class name
     *
     * @return Map
     */
    protected Map<String, Class> alias() {
        Map<String, Class> alias = new LinkedHashMap<>();
        Map<Class, IRelation> rs = this.relations();
        if (rs != null && rs.size() > 0) for (Class c : rs.keySet()) alias.put(c.getSimpleName(), c);
        return alias;
    }

    /**
     * Other models relation
     *
     * @return Map
     */
    protected Map<Class, IRelation> relations() { return new LinkedHashMap<>(); }


    /******************************************************************************/
    /**                                 Children                                 **/
    /******************************************************************************/

    /**
     * Get current model's child model
     *
     * @param modelClass Model class
     *
     * @return T
     */
    public <T extends Model>T child(Class<T> modelClass) { return this.child(modelClass, false); }

    /**
     * Get current model's child model
     *
     * @param modelClass Model class
     * @param reload     refresh find
     *
     * @return T
     */
    public <T extends Model>T child(Class<T> modelClass, boolean reload) {
        if (this._child.containsKey(modelClass) && reload == false) return (T) this._child.get(modelClass);

        Map<Class, IRelation> relations = this.relations();
        if (relations != null && relations.containsKey(modelClass)) {
            QueryBuilder qb = this._getFromRelations(relations.get(modelClass));
            T m = qb.find();
            this._child.put(modelClass, m);
            return m;
        }
        return null;
    }

    /**
     * Get current model's child model
     *
     * @param modelClass Model class
     *
     * @return Record<T>
     */
    public <T extends Model>Record<T> children(Class<T> modelClass) { return this.children(modelClass, false); }

    /**
     * Get current model's child model
     *
     * @param modelClass Model class
     * @param reload     refresh find
     *
     * @return Record<T>
     */
    public <T extends Model>Record<T> children(Class<T> modelClass, boolean reload) {
        if (this._children.containsKey(modelClass) && reload == false) return (Record<T>) this._children.get(modelClass);

        Map<Class, IRelation> relations = this.relations();
        if (relations != null && relations.containsKey(modelClass)) {
            QueryBuilder qb = this._getFromRelations(relations.get(modelClass));
            Record<T> r = qb.findAll();
            this._children.put(modelClass, r);
            return r;
        }
        return new Record<>();
    }

    /**
     * Proceed with params capture and {@link IRelation} run
     *
     * @param relation IRelation
     *
     * @return QueryBuilder
     */
    private QueryBuilder _getFromRelations(IRelation relation) {
        Map<String, Object> params = new LinkedHashMap<>();
        for (String primary : this.Info.columnFields) {
            try {
                Field field = this.getClass().getField(primary);
                params.put(primary, field.get(this));
            } catch (NoSuchFieldException e) {
                Trace.get(e);
            } catch (IllegalAccessException e) {
                Trace.get(e);
            }
        }
        return relation.run(params);
    }


    /******************************************************************************/
    /**                                 Answers                                  **/
    /******************************************************************************/

    /**
     * Set model values from Map
     *
     * @param p parameters
     */
    public void setAnswer(Map<String, Object> p) {
        Hash jp = new Hash(p);
        this._setAnswer(jp, false);
    }

    /**
     * Set model values from Map, and children if exists
     *
     * @param p parameters
     */
    public void setAnswerAll(Map<String, Object> p) {
        Hash jp = new Hash(p);
        this._setAnswer(jp, true);
    }

    /**
     * Set model values from {@link Hash}
     *
     * @param p parameters
     */
    public void setAnswer(Hash p) { this._setAnswer(p, false); }

    /**
     * Set model values from {@link Hash}, and children if exists
     *
     * @param p parameters
     */
    public void setAnswerAll(Hash p) { this._setAnswer(p, true); }

    /**
     * Set model values from {@link Hash}
     *
     * @param p parameters
     */
    private void _setAnswer(Hash p, boolean all) {
        if (this._oldAnswer.size() == 0 && this._answer.size() > 0) this._oldAnswer = new LinkedHashMap<>(this._answer);
        if (Check.isPresent(p)) {
            Map<String, String> answer = this._answer;
            try {
                List<String> columnFields = this.Info.columnFields;
                for (String columnName : columnFields) {
                    AColumn annotations = this.Info.columnAnnomations.get(columnName);
                    // If not has annotation, is not model column
                    // Ignore if is _id, as not exists an annotation for this field
                    if (annotations == null && Check.isNotEqual(columnName, "_id")) continue;

                    // Mandatory use getField for _id processing
                    Field field = this.getClass().getField(columnName);
                    String matchedName = this._searchColumnName(p, columnName, annotations);
                    if (Check.isEmpty(matchedName)) continue; // Ignore omitted columns
                    String value;

                    if (field.getType().equals(int.class)) {
                        int v = p.get(matchedName).toInt();
                        value = String.valueOf(v);
                        field.set(this, v);
                    } else if (field.getType().equals(float.class)) {
                        float v = p.get(matchedName).toFloat();
                        value = String.valueOf(v);
                        field.set(this, v);
                    } else if (field.getType().equals(double.class)) {
                        double v = p.get(matchedName).toDouble();
                        value = String.valueOf(v);
                        field.set(this, v);
                    } else if (field.getType().equals(long.class)) {
                        long v = p.get(matchedName).toLong();
                        value = String.valueOf(v);
                        field.set(this, v);
                    } else if (field.getType().equals(boolean.class)) {
                        boolean v = p.get(matchedName).toBoolean();
                        value = String.valueOf(v ? 1 : 0);
                        field.set(this, v);
                    } else if (field.getType().equals(Hash.class)) {
                        Hash v = p.get(matchedName).toHash();
                        value = v.toJson().toString();
                        field.set(this, v);
                    } else {
                        value = p.get(matchedName).toString();
                        field.set(this, value);
                    }
                    answer.put(columnName, value);
                }

                if (this._isNewRecord) {
                    // If is new record, check if data already exists, to change from insert to update
                    List<String> condition = new LinkedList<>();
                    List<String> args      = new LinkedList<>();
                    for (String columnName : this.Info.primaryKeys) {
                        // Ignore if not has answer
                        if (answer.containsKey(columnName) == false) continue;

                        condition.add(columnName+"=?");
                        args.add(answer.get(columnName));
                    }
                    if (condition.size() > 0) {
                        Model m = new QueryBuilder(
                            this.getClass()).filter(Convert.toString(condition, " AND "),
                            args.toArray()
                        ).find();
                        this._isNewRecord = m == null;
                        // If has finded model, set answer as oldAnswer
                        if (this._isNewRecord == false) this._oldAnswer = new LinkedHashMap<>(m.answer());
                    }
                }

                if (all) this._setChildrenAnswer(p);
            } catch (NoSuchFieldException e) {
                Trace.get(e);
            } catch (IllegalAccessException e) {
                Trace.get(e);
            }
            this._answer = answer;
        }
    }

    /**
     * Set child model from answer
     *
     * @param p Hash
     */
    private void _setChildrenAnswer(Hash p) {
        for (String key : this._alias.keySet()) {
            if (p.containsKey(key)) {
                Class<? extends Model> mc = this._alias.get(key);
                Hash children = p.get(key).toHash();
                for (int i = 0; i < children.length(); i++) {
                    Hash child = children.get(i).toHash();
                    try {
                        Model m = mc.newInstance();
                        m.setAnswerAll(child);
                        if (children.length() == 1) {
                            this._child.put(mc, m);
                        } else {
                            Record<Model> r = this._children.containsKey(mc) ? this._children.get(mc) : new Record<>();
                            r.add(m);
                            this._children.put(mc, r);
                        }
                    } catch (InstantiationException e) {
                        Trace.get(e);
                    } catch (IllegalAccessException e) {
                        Trace.get(e);
                    }
                }
            }
        }
    }

    /**
     * Find value by column name and his alias if exists
     *
     * @param params     JSON param
     * @param columnName column name
     * @param annotation column annotation
     *
     * @return String, column name or equivalent alias, or {@null} if not found
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    private String _searchColumnName(Hash params, String columnName, AColumn annotation) {
        if (params.containsKey(columnName)) {
            return columnName;
        } else {
            String[] alias = annotation.alias();
            if (alias.length > 0) {
                for (int i = 0; i < alias.length; i++) {
                    if (params.containsKey(alias[i]))
                        return alias[i];
                }
            }
        }
        return null;
    }


    /******************************************************************************/
    /**                                   Save                                   **/
    /******************************************************************************/

    /**
     * Save a record
     * If is new, insert
     * If exists, update
     *
     * @return boolean
     */
    public boolean save() {
        this.onBeforeSave();
        if (this.validate(this._oldAnswer, this._answer)) {
            this._errors        = new LinkedList<>();
            boolean isNewRecord = this._isNewRecord;
            int code = 0;
            if (this._isNewRecord) {
                code = (int)Unio.db(this.Info.dbName).insert(this.Info.tableName, this.values);
                if (code > 0) {
                    Trace.log(this, "inserted");
                    this._affectedLines++;
                    this._isNewRecord = false;
                }
            } else {
                this._setWhere();
                this._affectedLines = Unio.db(this.Info.dbName).update(this.Info.tableName, values, this._where, this._args);
                if (this._affectedLines > 0) Trace.log(this, "updated");
            }

            String error = Unio.db(this.Info.dbName).getError();
            if (Check.isEmpty(error) == false) this._errors.add(error);

            this.onAfterSave(code, isNewRecord);
            Trace.log(this, "content after save", this.toString());
            return this._errors.size() == 0;
        }
        return false;
    }

    /**
     * Save current model and all children
     *
     * @return int all affected lines
     */
    public boolean saveAll() {
        int allLines = 0;
        Map<Class<? extends Model>, Model> child     = this._child;
        Map<Class<? extends Model>, Record> children = this._children;
        if (this.save()) {
            allLines += this._affectedLines;
            allLines += this._saveChildren(child, children);
        }
        this._affectedLines = allLines;
        return this._errors.size() == 0;
    }

    /**
     * Save all children
     *
     * @param child    child models
     * @param children children records
     *
     * @return int all affected lines
     */
    private int _saveChildren(Map<Class<? extends Model>, Model> child, Map<Class<? extends Model>, Record> children) {
        int allLines = 0;
        int lines    = 0;
        List<String> error;
        if (child.size() > 0) {
            for (Class<? extends Model> mc : child.keySet()) {
                Model m = child.get(mc);
                if (m != null) {
                    m = this._prepareChildrenWithParentRelation(m);
                    m.saveAll();
                    lines = m.affectedLines();
                    allLines += lines;
                    error = m.errors();
                    this._errors.addAll(error);
                }
            }
        }
        if (children.size() > 0) {
            for (Class<? extends Model> mc : children.keySet()) {
                Record<Model> r = children.get(mc);
                for (Model m : r) {
                    m = this._prepareChildrenWithParentRelation(m);
                    m.saveAll();
                    lines += m.affectedLines();
                }
                allLines += lines;
                error = r.errors();
                this._errors.addAll(error);
            }
        }
        return allLines;
    }

    /**
     * Prepare answers with parent relation
     *
     * @param model child model object
     *
     * @return int affected lines
     */
    private Model _prepareChildrenWithParentRelation(Model model) {
        if (model.isNewRecord()) {
            Map<String, String> answers = model.answer();
            Map<String, ARelation> relations = model.Info.relations;
            for (String column : relations.keySet()) {
                if (Check.isEmpty(answers.get(column)) == false) {
                    List<Object> attributes = Arrays.asList(model.Info.columns.get(column));
                    if (attributes.contains(Db.TYPE_TEXT) ||
                       (attributes.contains(Db.TYPE_INTEGER) && Check.isEqual(answers.get(column), "0") == false))
                        continue;
                }

                // If children answer columns with relation not exist, check if current model is parent and set these values
                ARelation parent = relations.get(column);
                if (this.getClass() == parent.model() && this._answer.containsKey(parent.column())) answers.put(column, this._answer.get(parent.column()));
            }
            model.setAnswer(new Hash(answers));
        }
        return model;
    }

    /**
     * Method to validate datas and allow save
     *
     * @return boolean
     */
    protected boolean validate(Map<String, String> oldAnswer, Map<String, String> answer) {
        Trace.log(this, "oldAnswer", oldAnswer);
        Trace.log(this, "answer", answer);
        return true;
    }

    /**
     * Actions before save
     */
    protected void onBeforeSave() {
        if (this._oldAnswer.size() == 0 && this._answer.size() > 0) this._oldAnswer = new LinkedHashMap<>(this._answer);
        List<String> columnFields = this.Info.columnFields;
        Trace.log(this, this.Info.columnFields);
        this.values = new ContentValues();
        for (String columnName : columnFields) {
            try {
                AColumn annotations = this.Info.columnAnnomations.get(columnName);
                if (annotations == null) continue;
                Field field = this.getClass().getField(columnName);

                if (field.get(this) == null) continue;
                if (this._isNewRecord && annotations.autoIncroment()) {
                    if (this.Info.primaryKeys.size() == 1) {
                        // If has only one primary key and it's auto increment, ignore to get id automatically
                        continue;
                    } else {
                        int id = this._getAutoIncrementId(columnName);
                        field.set(this, id); // Assign auto increment id
                    }
                }

                String value = (field.get(this) == null || field.get(this).equals("null")) ? null : field.get(this).toString();
                if (field.getType().equals(boolean.class)) value = field.getBoolean(this) ? "1" : "0";
                Trace.log(this, columnName, value);
                this._answer.put(columnName, value);
                this.values.put(columnName, value);
            } catch (NoSuchFieldException e) {
                Trace.get(e);
            } catch (IllegalAccessException e) {
                Trace.get(e);
            }
        }
    }

    /**
     * Get auto increment column next id
     *
     * @param columnName column name
     *
     * @return int
     */
    private int _getAutoIncrementId(String columnName) {
        Cursor cursor = new QueryBuilder(this.getClass())
            .order(columnName+" DESC")
            .limit(1)
            .execute();
        int lastId = cursor.moveToFirst() ? cursor.getInt(cursor.getColumnIndex(columnName)) : 0;
        return (lastId+1);
    }

    /**
     * Actions after save
     *
     * @param result      result
     * @param isNewRecord state before save
     */
    protected void onAfterSave(int result, boolean isNewRecord) {
        // Clear children find
        this._child    = new LinkedHashMap<>();
        this._children = new LinkedHashMap<>();

        if (isNewRecord) {
            for (String columnName : this.Info.primaryKeys) {
                if (Arrays.asList(this.Info.columns.get(columnName)).contains("AUTOINCREMENT")) {
                    try {
                        Field field;
                        if (columnName.equals("_id")) {
                            // _id is from base Model
                            field = this.getClass().getField(columnName);
                        } else {
                            field = this.getClass().getDeclaredField(columnName);
                        }
                        if (field.get(this).equals("0") == false) {
                            field.set(this, result);
                            this._answer.put(columnName, String.valueOf(result));
                        }
                    } catch (NoSuchFieldException e) {
                        Trace.get(e);
                    } catch (IllegalAccessException e) {
                        Trace.get(e);
                    }
                    break;
                }
            }
        }
    }


    /******************************************************************************/
    /**                                  Delete                                  **/
    /******************************************************************************/

    /**
     * Delete a record
     *
     * @return boolean
     */
    public boolean delete() {
        boolean response = false;
        if (this._isNewRecord == false) {
            this.onBeforeDelete();
            this._setWhere();
            int result = Unio.db(this.Info.dbName).delete(this.Info.tableName, this._where, this._args);
            if (result > 0) {
                response = true;
                Trace.log(this, "deleted");
            }
            this.onAfterDelete();
        }
        return response;
    }

    /**
     * Actions before delete
     */
    protected void onBeforeDelete() {}

    /**
     * Action after delete
     */
    protected void onAfterDelete() {}

    /**
     * Define primary key filter for update/delete
     */
    private void _setWhere() {
        this._where = "";
        this._args = new String[this.Info.primaryKeys.size()];
        int argsKey = 0;
        int primariesKey = 0;
        String[] primaries = new String[this.Info.primaryKeys.size()];
        for (String primary : this.Info.primaryKeys) {
            primaries[primariesKey++] = primary+"=?";
            try {
                Field field = this.getClass().getField(primary);
                this._args[argsKey++] = field.get(this).toString();
            } catch (NoSuchFieldException e) {
                Trace.get(e);
            } catch (IllegalAccessException e) {
                Trace.get(e);
            }
        }
        this._where = Convert.toString(primaries, " AND ");
    }

    /**
     * Check if models are equal
     *
     * @param o model
     *
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Model && o != null) {
            Map<String, String> from = this.answer();
            Map<String, String> to   = ((Model)o).answer();
            return Check.isEqual(from, to);
        }
        return false;
    }

    /******************************************************************************/
    /***                                Getters                                 ***/
    /******************************************************************************/

    /**
     * {@link #_isNewRecord} getter
     *
     * @return boolean
     */
    public boolean isNewRecord() { return this._isNewRecord; }

    /**
     * {@link #_affectedLines} getter
     *
     * @return int
     */
    public int affectedLines() { return this._affectedLines; }

    /**
     * {@link #_errors} getter
     *
     * @return List
     */
    public List<String> errors() { return this._errors; }

    /**
     * Syncronized {@link #_answer} getter
     *
     * @return Map
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public Map<String, String> answer() {
        this.toString();
        return this._answer;
    }

    /**
     * Format content to String
     *
     * @return String
     */
    @NotNull
    @Override
    public String toString() {
        List<String> columnFields = this.Info.columnFields;
        List<String> result = new ArrayList<>();
        for (String columnName : columnFields) {
            try {
                AColumn annotations = this.Info.columnAnnomations.get(columnName);
                Field field;
                if (columnName.equals("_id")) {
                    field = this.getClass().getField(columnName);
                } else {
                    if (annotations == null) continue;
                    field = this.getClass().getDeclaredField(columnName);
                }
                Object o = field.get(this);
                result.add(columnName+"="+o);
                String v = Check.isEmpty(o) ? "" : o.toString();

                // Use toString() to sync answer with column attributes
                if (Check.isEqual(this._answer.get(columnName), v) == false) this._answer.put(columnName, v);
            } catch (NoSuchFieldException e) {
                Trace.get(e);
            } catch (IllegalAccessException e) {
                Trace.get(e);
            }
        }
        return Convert.toString(result);
    }

    /**
     * Format content to Hash
     *
     * @return Hash
     */
    public Hash toHash() {
        try {
            Hash formatted = new Hash();
            List<String> columnFields = this.Info.columnFields;
            for (String columnName : columnFields) {
                AColumn annotations = this.Info.columnAnnomations.get(columnName);
                String name = columnName;
                // Se não tiver annotation e a coluna não for _id, não faz parte da tabela
                if (annotations == null) {
                    if (columnName.equals("_id")) {
                        Field field = this.getClass().getField(columnName);
                        formatted.put(columnName, field.get(this));
                    }
                    continue;
                } else if (Check.isEmpty(annotations.jsonAlias()) == false) {
                    name = annotations.jsonAlias();
                }
                Field field = this.getClass().getDeclaredField(columnName);
                formatted.put(name, field.get(this));
            }
            return formatted;
        } catch (NoSuchFieldException e) {
            Trace.get(e);
            return null;
        } catch (IllegalAccessException e) {
            Trace.get(e);
            return null;
        }
    }


    /******************************************************************************/
    /***                               Interfaces                               ***/
    /******************************************************************************/

    /**
     * IRelation
     *
     * Interface to create query to find child records
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 10/02/2015 18:01
     */
    public interface IRelation { QueryBuilder run(Map<String, Object> params); }
}