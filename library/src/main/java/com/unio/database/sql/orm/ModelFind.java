package com.unio.database.sql.orm;

import android.database.Cursor;
import com.unio.debug.Trace;
import java.lang.reflect.Field;
import java.util.*;

/**
 * ModelFind
 *
 * Class to run finds
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 09/04/2015 15:10
 */
public class ModelFind
{
    /** Table information */
    private TableInfo _Info;

    /** Model class */
    private Class<? extends Model> _c;

    /**
     * Constructor method
     *
     * @param c Model class
     */
    public ModelFind(Class<? extends Model> c) {
        this._c = c;
        this._Info = Cache.get(c);
    }

    /**
     * Create Model with cursor
     *
     * @param reg Cursor
     *
     * @return T
     */
    public <T extends Model>T find(Cursor reg) {
        if (reg.moveToFirst()) {
            return this.getModel(reg);
        } else {
            return null;
        }
    }

    /**
     * Create Record with cursor
     *
     * @param reg Cursor
     *
     * @return Record
     */
    public <T extends Model>Record<T> findAll(Cursor reg) {
        Record<T> records = new Record<>();
        if (reg.getCount() > 0) {
            while (reg.moveToNext()) {
                T model = this.getModel(reg);
                if (model == null) continue;
                records.add(model);
            }
            return records;
        } else {
            return records;
        }
    }

    /**
     * Create Model instance
     *
     * @param reg cursor
     *
     * @return T
     */
    private <T extends Model>T getModel(Cursor reg) {
        try {
            T model = (T)this._c.newInstance();
            model._isNewRecord = false;

            List<String> columnFields = this._Info.columnFields;

            // If _id exists in query result, add to column list
            if (reg.getColumnIndex(Cache.PRIMARY_COLUMN) != -1 && columnFields.contains(Cache.PRIMARY_COLUMN) == false)
                columnFields.add(Cache.PRIMARY_COLUMN);

            Map<String, Object> answer = new LinkedHashMap<>();
            for (String columnName : columnFields) {
                // Need to use getField, with getDeclaredField _id will not find, triggering NoSuchFieldException
                Field field = model.getClass().getField(columnName);
                int index = reg.getColumnIndex(columnName);
                if (index == -1) continue; // Ignore not found columns. Ex: _id
                answer.put(columnName, reg.getString(index));
            }
            model.setAnswer(answer);
            return model;
        } catch (InstantiationException e) {
            Trace.get(e);
        } catch (NoSuchFieldException e) {
            Trace.get(e);
        } catch (IllegalAccessException e) {
            Trace.get(e);
        }
        return null;
    }
}