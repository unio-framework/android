package com.unio.database.sql.connection;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.helper.Convert;
import com.unio.util.statics.interfaces.IBasicReturnRun;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Db
 *
 * Class to manage general SQL executions
 *
 * Example:
 * String sql = "SELECT * FROM tabela WHERE coluna1=? AND coluna2 LIKE ?";
 * String[] bindVar = { "valor1", "%valor%" };
 * Cursor reg = {}.fetch(sql, bindVar);
 * if (reg.moveToFirst()) {
 *     while (reg.moveToNext()) {
 *         String column1Value = reg.getString(reg.getColumnIndex("column1"));
 *         int column2Value    = reg.getInt(reg.getColumnIndex("column2"));
 *     }
 * }
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 03/05/2014 23:16
 */
public class Db extends SQLiteOpenHelper
{
    /** SQLite types */
    public static final String TYPE_INTEGER = "INTEGER";
    public static final String TYPE_TEXT    = "TEXT";
    public static final String TYPE_REAL    = "REAL";
    public static final String TYPE_BLOB    = "BLOB";

    /** SQLite instance */
    private SQLiteDatabase _$Db;

    /** DB name */
    private String _dbName;

    /** DB version */
    private int _dbVersion;

    /** All errors */
    private String _error;

    /**
     * Constructor method
     *
     * @param context Context
     * @param name    DB name
     * @param version version
     */
    public Db(Context context, String name, int version) {
        super(context, name, null, version);
        this._dbName = name;
        this._dbVersion = version;
        this.start();
    }

    /**
     * Get DB name
     *
     * @return String
     */
    public String getName() { return _dbName; }

    /**
     * Get DB version
     *
     * @return String
     */
    public int getVersion() { return _dbVersion; }

    /**
     * Return last error
     *
     * @return String
     */
    public String getError() { return this._error; }

    /**
     * Start DB connection, if is closed or not exists
     */
    public void start() { if (_$Db == null || _$Db.isOpen() == false) _$Db = this.getWritableDatabase(); }

    /**
     * Finish DB connection, if is opened or not in transaction
     */
    public void finish() { if (_$Db != null && _$Db.isOpen() && _$Db.inTransaction() == false) _$Db.close(); }

    /**
     * Check if database connection is open
     *
     * @return boolean
     */
    public boolean isOpen() { return _$Db != null && _$Db.isOpen(); }

    /**
     * Get SQLite instance
     *
     * @return SQLiteDatabase
     */
    public SQLiteDatabase sqlite() { return this._$Db; }

    /**
     * Insert datas in table
     *
     * Example:
     * ContentValues values = new ContentValues();
     * values.put("column1", "value1");
     * values.put("column2", "value2");
     * {}.insert("my_table", values);
     *
     * @param tableName table name
     * @param values    values (column=value)
     *
     * @return long total inserted lines
     */
    public long insert(String tableName, ContentValues values) { return this.insert(tableName, values, false); }

    /**
     * Insert datas in table
     *
     * Example:
     * ContentValues values = new ContentValues();
     * values.put("column1", "value1");
     * values.put("column2", "value2");
     * {}.insert("my_table", values);
     *
     * @param tableName table name
     * @param values    values (column=value)
     * @param ignore    INSERT IGNORE?
     *
     * @return long total inserted lines
     */
    public long insert(String tableName, ContentValues values, boolean ignore) {
        try {
            int flag = ignore ? SQLiteDatabase.CONFLICT_IGNORE : SQLiteDatabase.CONFLICT_NONE;
            return this._$Db.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_NONE);
        } catch (SQLException e) {
            Trace.get(e);
            this._error = e.toString();
            return -1;
        }
    }

    /**
     * Update table data
     *
     * Example:
     *   ContentValues values = new ContentValues();
     *   values.put("column1", "value1");
     *   values.put("column2", "value2");
     *   {}.update("my_table", values, "column1=?", { "foo" });
     *
     * @param tableName table name
     * @param values    values (column=value)
     * @param where     WHERE filter
     *
     * @return int total affected lines
     */
    public int update(String tableName, ContentValues values, String where) { return this.update(tableName, values, where, null); }

    /**
     * Update table data
     *
     * Example:
     *   ContentValues values = new ContentValues();
     *   values.put("column1", "value1");
     *   values.put("column2", "value2");
     *   {}.update("my_table", values, "column1=?", { "foo" });
     *
     * @param tableName table name
     * @param values    values (column=value)
     * @param where     WHERE filter
     * @param args      filter args
     *
     * @return int total affected lines
     */
    public int update(String tableName, ContentValues values, String where, String[] args) {
        try {
            return this._$Db.update(tableName, values, where, args);
        } catch (SQLException e) {
            this._error = e.toString();
            return -1;
        }
    }

    /**
     * Delete table data
     *
     * Example:
     *   {}.delete("my_table", "column1=?", { "foo" });
     *
     * @param tableName table name
     *
     * @return int total affected lines
     */
    public int delete(String tableName) { return this.delete(tableName, null); }

    /**
     * Delete table data
     *
     * Example:
     *   {}.delete("my_table", "column1=?", { "foo" });
     *
     * @param tableName table name
     * @param where     WHERE filter
     *
     * @return int total affected lines
     */
    public int delete(String tableName, String where) { return this.delete(tableName, where, null); }

    /**
     * Exclui dados do DB
     *
     * Example:
     *   {}.delete("my_table", "column1=?", { "foo" });
     *
     * @param tableName table name
     * @param where     WHERE filter
     * @param args      filter args
     *
     * @return int total affected lines
     */
    public int delete(String tableName, String where, String[] args) {
        try {
            return this._$Db.delete(tableName, where, args);
        } catch (SQLException e) {
            this._error = e.toString();
            return -1;
        }
    }

    /**
     * Create table in DB
     *
     * Example:
     * Map<String, ArrayList<String>> params = new LinkedHashMap<>();
     * params.put("column1", { Db.TYPE_TEXT, "PRIMARY_KEY" });
     * params.put("column2", { Db.TYPE_TEXT });
     *
     * Contraints:
     *   A_UK: Unique Key
     *   A_FK: Foreign Key
     *   A_IX: Index
     *
     * {}.create("my_table", params);
     *
     * @param tableName table name
     * @param params    params (column=atributes, contraints)
     *
     * @return boolean
     */
    public boolean create(String tableName, Map<String, Object[]> params) {
        ArrayList<String> columns = new ArrayList<>();
        ArrayList<String> constraints = new ArrayList<>();
        String indexStatement = null;
        boolean multiplePrimaryKey = false;
        for (Map.Entry<String, Object[]> entry : params.entrySet()) {
            switch (entry.getKey()) {
                case "A_PK":
                    if (entry.getValue().length > 1) {
                        multiplePrimaryKey = true;
                        constraints.add("PRIMARY KEY (" + Convert.toString(entry.getValue()) + ")");
                    }
                    break;
                case "A_UK":
                case "A_FK":
                    constraints.add(Convert.toString(entry.getValue()));
                    break;
                case "A_IX":
                    String indexColumns = Convert.toString(entry.getValue());
                    indexStatement = "CREATE INDEX idx_" + tableName + " ON " + tableName + "(" + indexColumns + ")";
                    break;
                default:
                    columns.add(entry.getKey() + " " + Convert.toString(entry.getValue(), " "));
                    break;
            }
        }

        String create = "CREATE TABLE IF NOT EXISTS "+tableName+" (";
        if (multiplePrimaryKey) {
            List<String> replaced = new ArrayList<>();
            for (String column : columns) {
                replaced.add(column.replace("PRIMARY KEY ", ""));
            }
            create += Convert.toString(replaced.toArray());
        } else {
            create += Convert.toString(columns.toArray());
        }
        if (Check.isEmpty(constraints) == false) {
            create += ", "+ Convert.toString(constraints.toArray());
        }
        create += ")";
        if (indexStatement != null) create += "; "+indexStatement;
        Trace.log(this, "create", create);

        this.execute(create);
        return this.tableExists(tableName);
    }

    /**
     * Execute a query, returning Cursor
     *
     * @param sql query
     *
     * @return Cursor
     */
    public Cursor fetch(String sql) { return this.fetch(sql, null); }

    /**
     * Execute a query, returning Cursor
     *
     * @param sql    query
     * @param params params
     *
     * @return Cursor
     */
    public Cursor fetch(String sql, String[] params) { return _$Db.rawQuery(sql, params); }

    /**
     * Check if table exists
     *
     * @param tableName table name
     *
     * @return boolean
     */
    public boolean tableExists(String tableName) {
        String sql = "" +
            "SELECT name" +
            "  FROM sqlite_master" +
            " WHERE type='table'" +
            "   AND name=?" +
            "";
        String[] bindVar = { tableName };
        Cursor reg = fetch(sql, bindVar);
        return reg.getCount() > 0;
    }

    /**
     * Execute a query
     *
     * @param sql query
     */
    public void execute(String sql) { this._$Db.execSQL(sql); }

    /**
     * Execute a quer
     *
     * @param sql  query
     * @param args bindVar
     */
    public void execute(String sql, String[] args) { this._$Db.execSQL(sql, args); }

    /**
     * Execute commands in Transaction, improves performance in multiple exections
     *
     * @param callback callback
     *
     * @return int to use to get total affected lines or boolean success (int 0|1)
     */
    public int executeOnTrasaction(IOnTransaction callback) {
        int success = 0;
        try {
            this._$Db.beginTransactionNonExclusive();
            if (callback != null) success = callback.run();
            this._$Db.setTransactionSuccessful();
        } finally {
            this._$Db.endTransaction();
        }
        return success;
    }

    /**
     * ITransaction
     *
     * Transaction callback
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 02/01/2015 12:36
     */
    public static abstract class IOnTransaction extends IBasicReturnRun<Integer> {}

    /**
     * Required method
     *
     * @param sqLiteDatabase DB instance
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {}

    /**
     * Required method
     *
     * @param sqLiteDatabase DB instance
     * @param oldVersion     DB old version
     * @param newVersion     DB new version
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {}
}