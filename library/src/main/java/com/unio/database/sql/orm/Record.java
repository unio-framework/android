package com.unio.database.sql.orm;

import com.unio.core.Unio;
import com.unio.database.sql.connection.Db;
import com.unio.debug.Trace;
import com.unio.util.helper.Check;
import com.unio.util.io.Hash;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Record
 *
 * Receive all models from findAll
 * Used to save multiple same model records in single time too
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 28/01/2016 16:25
 */
@SuppressWarnings("unchecked")
public class Record<T extends Model> extends LinkedList<T>
{
    /** All model answer */
    private List<Map<String, String>> _answers = new LinkedList<>();

    /**
     * Default constructor, use for findAll
     */
    public Record() {}

    /**
     * Constructor for new record saves using {@link List}
     *
     * @param p parameters
     */
    public Record(Class<T> modelClass, List p) { this(modelClass, new Hash(p)); }

    /**
     * Constructor for new record saves using {@link Hash}
     *
     * @param p parameters
     */
    public Record(Class<T> modelClass, Hash p) {
        if (p.isArray()) {
            for (int i = 0; i < p.length(); i++) {
                Hash m = p.get(i).toHash();
                try {
                    T model = modelClass.newInstance();
                    Trace.log(this, model.getClass());
                    model.setAnswerAll(m);
                    this.add(model);
                } catch (InstantiationException e) {
                    Trace.get(e);
                } catch (IllegalAccessException e) {
                    Trace.get(e);
                }
            }
        } else {
            throw new RuntimeException("Record<> only accepts JSONArray type JSON");
        }
    }

    /**
     * Add a model in list
     * Add model answer too
     *
     * @param model model
     *
     * @return boolean
     */
    @Override
    public boolean add(T model) {
        this._answers.add(model.answer());
        return super.add(model);
    }

    /**
     * Remove a model from list
     *
     * @param location index
     *
     * @return T
     */
    @Override
    public T remove(int location) {
        this._answers.remove(location);
        return super.remove(location);
    }

    /**
     * Get model table name
     *
     * @return String
     */
    public String tableName() { return Check.isEmpty(this) ? null : this.get(0).Info.tableName; }

    /**
     * Get model table columns
     *
     * @return List<String>
     */
    public List<String> columns() { return Check.isEmpty(this) ? null : this.get(0).Info.columnFields; }

    /**
     * Retorn all model.answer()
     *
     * @return List
     */
    public List<Map<String, String>> answers() { return this._answers; }

    /**
     * Save all new models in {@link Record}
     *
     * @return boolean, false if not save, or hasn't new record
     */
    public boolean save() {
        boolean success = false;
        if (this.size() > 0) {
            int response = Unio.db(this.get(0).Info.dbName).executeOnTrasaction(new Db.IOnTransaction() {
                @Override
                public Integer run() {
                    boolean success = false;
                    for (T model : Record.this) success = model.save();
                    return success ? 1 : 0;
                }
            });
            success = response == 1;
        }
        return success;
    }

    /**
     * Save all new models in {@link Record}, with children
     *
     * @return int all affected lines
     */
    public boolean saveAll() {
        boolean success = false;
        if (this.size() > 0) {
            int response = Unio.db(this.get(0).Info.dbName).executeOnTrasaction(new Db.IOnTransaction() {
                @Override
                public Integer run() {
                    boolean success = false;
                    for (T model : Record.this) success = model.saveAll();
                    return success ? 1 : 0;
                }
            });
            success = response == 1;
        }
        return success;
    }

    /**
     * Get all affected lines after save/saveAll
     *
     * @return int
     */
    public int affectedLines() {
        int affectedLines = 0;
        if (this.size() > 0) {
            for (T model : this)
                affectedLines += model.affectedLines();
        }
        return affectedLines;
    }

    /**
     * Get all models error
     *
     * @return List
     */
    public List<String> errors() {
        List<String> errors = new LinkedList<>();
        if (this.size() > 0) {
            for (T model : this)
                errors.addAll(model.errors());
        }
        return errors;
    }

    /**
     * Delete all models in list
     *
     * @return boolean
     */
    public boolean delete() {
        boolean success = false;
        if (this.size() > 0) {
            for (T model : this) {
                if (model == null) continue;
                success = model.delete();
            }
        }
        return success;
    }

    /**
     * Get model contents as {@link Hash}
     *
     * @return UJSONArray
     */
    public Hash toHash() {
        Hash records = new Hash();
        for (T model : this) {
            if (model == null) continue;
            records.put(model.toHash());
        }
        return records;
    }

    /**
     * Get model contents as String
     * Each model is JSON
     *
     * @return String
     */
    @Override
    public String toString() { return this.toHash().toJson().toString(); }
}