package com.unio.database.sql.orm;

import com.unio.database.sql.connection.Db;
import com.unio.database.sql.orm.annotation.AColumn;
import com.unio.database.sql.orm.annotation.ARelation;
import com.unio.database.sql.orm.annotation.ATable;
import com.unio.util.helper.Check;
import java.lang.reflect.Field;
import java.util.*;

/**
 * TableInfo
 *
 * Receive all table information
 * - Table name
 * - Database name where table exists
 * - Table's columns data
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 09/04/2015 13:32
 */
public class TableInfo
{
    /** Attributes */
    public final String                 tableName;
    public final String                 dbName;
    public final Map<String, Object[]>  columns;
    public final List<String>           columnFields;
    public final Map<String, AColumn>   columnAnnomations;
    public final List<String>           primaryKeys;
    public final List<String>           indexes;
    public final List<String>           uniqueKeys;
    public final Map<String, ARelation> relations;

    /**
     * Constructor method
     * Define all attributes to use final attributes
     *
     * @param c {@link Model} Class
     */
    public TableInfo(Class<? extends Model> c) {
        // Pega o nome da tabela
        final ATable tableAnnotation = c.getAnnotation(ATable.class);
        if (tableAnnotation == null) {
            this.tableName = c.getSimpleName().toLowerCase();
            this.dbName = null;
        } else {
            this.tableName = Check.isEmpty(tableAnnotation.name()) ? c.getSimpleName().toLowerCase() : tableAnnotation.name();
            this.dbName    = Check.isEmpty(tableAnnotation.db())   ? null : tableAnnotation.db();
        }

        // Pega os dados das colunas
        Field[] fields         = c.getDeclaredFields();
        this.columns           = new LinkedHashMap<>();
        this.columnAnnomations = new LinkedHashMap<>();
        this.columnFields      = new LinkedList<>();
        this.primaryKeys       = new LinkedList<>();
        this.indexes           = new LinkedList<>();
        this.uniqueKeys        = new LinkedList<>();
        this.relations         = new LinkedHashMap<>();

        boolean hasPrimaryKey = false;
        boolean hasAutoIncrement = false;
        for (Field field : fields) {
            if (field.isAnnotationPresent(AColumn.class)) {
                String statement;
                this.columnFields.add(field.getName());

                List<String> attributes = new LinkedList<>();
                final AColumn columnAnnotation = field.getAnnotation(AColumn.class);

                this.columnAnnomations.put(field.getName(), columnAnnotation);
                attributes.add(Cache.getColumnType(field.getType()));

                if (columnAnnotation.primaryKey()) {
                    hasPrimaryKey = true;
                    this.primaryKeys.add(field.getName());
                    attributes.add("PRIMARY KEY");
                }
                if (columnAnnotation.autoIncroment()) hasAutoIncrement = true;

                if (columnAnnotation.notNull() || columnAnnotation.primaryKey()) {
                    statement = "NOT NULL";
                    statement += " ON CONFLICT "+columnAnnotation.onNullConflict().toString();
                    attributes.add(statement);
                }
                if (!columnAnnotation.primaryKey()) attributes.add("DEFAULT "+columnAnnotation.defaultValue());

                // Index
                if (columnAnnotation.index()) this.indexes.add(field.getName());

                // Unique
                if (columnAnnotation.unique()) {
                    statement = "UNIQUE("+field.getName()+")";
                    statement += " ON CONFLICT "+columnAnnotation.onUniqueConflict().toString();
                    this.uniqueKeys.add(statement);
                }

                // Column relations
                if (columnAnnotation.relation().model() != Model.class) {
                    this.relations.put(field.getName(), columnAnnotation.relation());
                    // All relations are added in index list, if is not
                    // Check if is not primary key. All primary key is setted with index
                    if (columnAnnotation.index() == false && columnAnnotation.primaryKey() == false) this.indexes.add(field.getName());
                }

                this.columns.put(field.getName(), attributes.toArray());
            }
        }
        if (hasPrimaryKey == false) {
            this.columnFields.add(Cache.PRIMARY_COLUMN);
            this.columns.put(Cache.PRIMARY_COLUMN, new String[] {
                Db.TYPE_INTEGER,
                "PRIMARY KEY",
                "AUTOINCREMENT",
                "NOT NULL ON CONFLICT FAIL",
            });
            this.primaryKeys.add(Cache.PRIMARY_COLUMN);
        } else if (hasAutoIncrement && this.primaryKeys.size() == 1) {
            Object[] as = this.columns.get(this.primaryKeys.get(0));
            List<Object> attributes = new LinkedList<>();
            for (Object a : as) {
                attributes.add(a);
                if (a.equals("PRIMARY KEY")) attributes.add("AUTOINCREMENT");
            }
            this.columns.put(this.primaryKeys.get(0), attributes.toArray());
        }
    }
}