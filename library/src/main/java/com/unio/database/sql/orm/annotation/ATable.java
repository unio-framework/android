package com.unio.database.sql.orm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ATable
 *
 * Annotations for table
 *
 * Attributes:
 *   - name
 *   - db
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 2.0 12/02/2016 15:26
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ATable
{
	/** Custom table name */
	String name() default "";

	/** Custom database */
	String db() default "";
}