package com.unio.database.sql.orm.annotation;

import com.unio.database.sql.orm.Model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ARelation
 *
 * Annotation for column relation
 *
 * Attributes:
 *   - model
 *   - column
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 12/02/2016 15:26
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ARelation
{
    /** Model class */
    Class<? extends Model> model() default Model.class;

    /** Model column */
    String column() default "";
}