package com.unio.database.sql.orm;

import android.content.ContentValues;
import android.database.Cursor;
import com.unio.core.Unio;
import com.unio.database.sql.builder.QueryBuilder;
import com.unio.database.sql.orm.annotation.AColumn;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Table
 *
 * Class to manage table
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 15/07/2014 00:58
 */
public class Table
{
    /** Table name */
    public final String name;

    /** Table data */
    private TableInfo _Info;

    /** Model Class */
    private Class<? extends Model> _c;

    /**
     * Constructor method
     *
     * @param c Model Class
     */
    public Table(Class<? extends Model> c) {
        this._c = c;
        this._Info = Cache.get(c);
        this.name = this._Info.tableName;
    }

    /**
     * Create table
     *
     * @return boolean
     */
    public boolean create() {
        if (this.exists()) return true;

        Map<String, Object[]> params = this._Info.columns;
        params.put("A_PK", this._Info.primaryKeys.toArray());
        if (this._Info.uniqueKeys.isEmpty() == false)  params.put("A_UK", this._Info.uniqueKeys.toArray());
        if (this._Info.indexes.isEmpty() == false)     params.put("A_IX", this._Info.indexes.toArray());

        return Unio.db(this._Info.dbName).create(this._Info.tableName, params);
    }

    /**
     * Update table schema
     * If is not empty, update table with respective values
     *
     * @return boolean
     */
    public boolean update() {
        boolean isEmpty = this.empty();
        List<ContentValues> records = null;
        if (isEmpty == false) {
            Cursor reg = new QueryBuilder(this._c).execute(this._Info.dbName);
            records = new LinkedList<>();
            Map<String, Object[]> columns = this._Info.columns;
            while (reg.moveToNext()) {
                ContentValues values = new ContentValues();
                for (Map.Entry<String, Object[]> entry : columns.entrySet()) {
                    String name = entry.getKey();
                    String value = null;
                    if (reg.getColumnIndex(name) == -1) {
                        AColumn annotations = this._Info.columnAnnomations.get(name);
                        if (annotations == null) continue;
                        if (!annotations.defaultValue().equals("NULL")) value = annotations.defaultValue();

                        if (annotations.history().length > 0) {
                            boolean finded = false;
                            for (String oldName : annotations.history()) {
                                if (reg.getColumnIndex(oldName) != -1 && finded == false) {
                                    finded = true;
                                    value = reg.getString(reg.getColumnIndex(oldName));
                                }
                            }
                        }
                    } else {
                        value = reg.getString(reg.getColumnIndex(name));
                    }
                    values.put(name, value);
                }
                records.add(values);
            }
            reg.close();
        }
        this.delete();
        this.create();
        if (isEmpty == false && records != null) {
            for (ContentValues insertValues : records)
                Unio.db(this._Info.dbName).insert(this._Info.tableName, insertValues);
        }
        return true;
    }

    /**
     * Drop table
     *
     * @return boolean
     */
    public boolean delete() {
        if (this.exists() == false) return true;
        Unio.db(this._Info.dbName).execute("DROP TABLE IF EXISTS " + this._Info.tableName);
        return this.exists() == false;
    }

    /**
     * Check if table exists
     *
     * @return boolean
     */
    public boolean exists() { return Unio.db(this._Info.dbName).tableExists(this._Info.tableName); }

    /**
     * Check if table is empty
     *
     * @return boolean
     */
    public boolean empty() { return new QueryBuilder(this._c).count() == 0; }

    /**
     * Clear table content
     *
     * @return boolean
     */
    public boolean clear() {
        Unio.db(this._Info.dbName).delete(this._Info.tableName);
        return this.empty();
    }

    /**
     * Clear filtered table content
     *
     * @param where filter
     *
     * @return boolean
     */
    public boolean clear(String where) { return this.clear(where, null); }

    /**
     * Clear filtered table content
     *
     * @param where filter
     * @param args  args
     *
     * @return boolean
     */
    public boolean clear(String where, String[] args) { return Unio.db(this._Info.dbName).delete(this._Info.tableName, where, args) > 0; }
}