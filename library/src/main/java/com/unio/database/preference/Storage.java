package com.unio.database.preference;

import android.content.Context;
import android.content.SharedPreferences;
import com.unio.core.Unio;
import com.unio.util.io.Hash;

/**
 * LocalStorage
 *
 * Shared preferences handler class
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.1 01/05/2014 16:38
 */
public final class Storage
{
    /** Default storage identification */
    public static final String STORAGE_NAME = "LOCAL_STORAGE";

    /** Storage name */
    public final String name;

    /** SharedPreferences instance */
    private SharedPreferences _Storage;

    /**
     * Constructor
     */
    public Storage() { this(STORAGE_NAME); }

    /**
     * Constructor
     *
     * @param storageName shared preferences identification
     */
    public Storage(String storageName) {
        this.name = storageName;
        this._Storage = Unio.app().getSharedPreferences(storageName, Context.MODE_PRIVATE);
    }

    /**
     * Static way to open a shared preferences as LocalStorage
     *
     * @param storageName shared preferences identification
     *
     * @return LocalStorage
     */
    public static Storage open(String storageName) { return new Storage(storageName); }

    /**
     * Open a storage item
     *
     * @param sharedName item identification
     *
     * @return Item
     */
    public StorageItem search(String sharedName) { return new StorageItem(this._Storage, sharedName); }

    /**
     * Clear all current shared preferences data
     */
    public void clear() {
        SharedPreferences.Editor editor = this._Storage.edit();
        editor.clear().apply();
    }

    /**
     * Get all content as Hash
     *
     * @return Hash
     */
    public Hash toHash() { return new Hash(this._Storage.getAll()); }
}