package com.unio.database.preference;

import android.content.SharedPreferences;
import com.unio.util.io.Hash;
import com.unio.util.io.Result;

/**
 * StorageItem
 *
 * SharedPreferences each item handler
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class StorageItem
{
    /** SharedPreferences instance */
    private SharedPreferences _Storage;

    /** Item name */
    private String _name;

    /**
     * Constructor
     *
     * @param shared SharedPreferences
     * @param name   item name
     */
    protected StorageItem(SharedPreferences shared, String name) {
        this._Storage = shared;
        this._name = name;
    }

    /**
     * Set a value
     *
     * @param value value to store
     */
    public void set(Object value) {
        // If is Hash, convert to JSON
        if (value instanceof Hash) value = ((Hash) value).toJson();
        this._insert(value);
    }

    /**
     * Check if value exists
     *
     * @return boolean
     */
    public boolean exists() { return get().isNull == false; }

    /**
     * Get value as string (raw)
     *
     * @return String
     */
    public Result get() { return new Result<>(this._Storage.getString(this._name, null)); }

    /**
     * Internal data insert
     *
     * @param value value to insert
     */
    private void _insert(Object value) {
        String insertValue = value == null ? null : value.toString();
        SharedPreferences.Editor editor = this._Storage.edit();
        editor.putString(this._name, insertValue);
        editor.apply();
    }

    /**
     * Clear this item content
     */
    public void clear() { this._insert(null); }
}