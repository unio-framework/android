package com.unio.app;

import android.content.Intent;
import com.unio.app.model.UnioDebug;
import com.unio.base.URunnable;
import com.unio.base.UService;
import com.unio.database.sql.orm.Record;
import com.unio.debug.Trace;
import com.unio.util.control.Network;
import com.unio.util.manager.ServiceManager;
import com.unio.util.statics.constants.Time;

/**
 * UnioService
 *
 * Service for send UnioDebug model content
 * <service android:name="com.unio.app.UnioService" android:enabled="true" android:exported="true" />
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class UnioService extends UService
{
    /** UnioDebug send request runnable */
    private static final DebugSendRunnable debugSendRunnable = new DebugSendRunnable();

    /**
     * onInit
     */
    @Override
    public void onInit(Intent intent) {
        Trace.log(this, "Starting UnioService");
        start();
    }

    /**
     * Start service or runnable
     */
    public static void start() {
        if (ServiceManager.running(UnioService.class)) {
            if (debugSendRunnable.isRunning() == false)
                debugSendRunnable.start();
        } else {
            ServiceManager.start(UnioService.class);
        }
    }

    /**
     * DebugSendRunnable
     *
     * Runnable for send UnioDebug model content
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 27/10/2016 14:13
     */
    static class DebugSendRunnable extends URunnable
    {
        /**
         * Settings
         */
        @Override
        protected void settings() {
            super.settings();
            interval = Time.Milisecond.MINUTE*5;
        }

        /**
         * onInit
         */
        @Override
        protected void onInit() {
            Trace.log(this, "Starting DebugSendRunnable");
            sendTraceLog();
        }

        /**
         * Send Trace stored logs
         */
        private void sendTraceLog() {
            if (UnioDebug.table().exists() == false) return;

            final Record<UnioDebug> record = UnioDebug.model().limit(100).findAll();
            if (record.size() > 0 && Network.isOnline()) UnioDebug.send(record);
        }
    }
}