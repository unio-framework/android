package com.unio.app;

import com.unio.util.io.Image;
import com.unio.util.statics.interfaces.IOnFindResult;

/**
 * USinglePhotoActivity
 *
 * Activity for single invoke {@link PhotoActivity}
 *
 * @author  Leandro Akira Omiya Takagi <leandro.takagi@icloud.com>
 * @version 1.0 14/11/2016 21:50
 */
public class SinglePhotoActivity extends PhotoActivity
{
    /**
     * On find result
     *
     * @param response response
     */
    @Override
    public void onFind(Image response) { this.callSingleCallbacks(response); }

    /**
     * IOnAfterCapture
     *
     * Interface for external process
     *
     * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
     * @version 1.0 13/08/2016 16:23
     */
    public interface IOnAfterCapture extends IOnFindResult<Image> {}
}