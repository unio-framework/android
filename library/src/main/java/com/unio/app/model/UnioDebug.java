package com.unio.app.model;

import android.os.StrictMode;
import com.unio.core.Unio;
import com.unio.database.sql.builder.QueryBuilder;
import com.unio.database.sql.orm.Model;
import com.unio.database.sql.orm.Record;
import com.unio.database.sql.orm.Table;
import com.unio.database.sql.orm.annotation.AColumn;
import com.unio.database.sql.orm.annotation.ATable;
import com.unio.debug.Trace;
import com.unio.shortcuts.Compatibility;
import com.unio.util.control.Network;
import com.unio.util.helper.Check;
import com.unio.util.io.Hash;
import com.unio.util.rest.Http;

/**
 * UnioDebug
 *
 * Model to store all system crash and send log messages
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
@ATable(db = "__unio")
public class UnioDebug extends Model
{
    /** Internal unique id */
    @AColumn(primaryKey = true, autoIncroment = true)
    public int id;

    /** identification */
    @AColumn(notNull = true)
    public String idUnique;

    /** Application package */
    @AColumn(notNull = true)
    public String packageName;

    /** Record catch date */
    @AColumn(notNull = true)
    public String dateReal;

    /** System type */
    @AColumn(notNull = true)
    @Trace.SystemType
    public String systemName;

    /** Android version */
    @AColumn(notNull = true)
    public String systemVersion;

    /** Application version */
    @AColumn(notNull = true)
    public String appVersion;

    /** Unio version */
    @AColumn(notNull = true)
    public String unioVersion;

    /** Mobile phone manufacturer */
    @AColumn(notNull = true)
    public String manufacturer;

    /** Mobile phone model */
    @AColumn(notNull = true)
    public String model;

    /** Primary IMEI, this is unique method to identify a phone */
    @AColumn(notNull = true)
    public String imid;

    /** Error message if not catch IMEI */
    @AColumn
    public String imidError;

    /** Send exception */
    @AColumn
    public String exception;

    /** Other additional message */
    @AColumn
    public String message;

    /** Model handler */
    public static QueryBuilder model() { return model(UnioDebug.class); }
    public static Table table()        { return table(UnioDebug.class); }

    /**
     * After save, if is in Wifi, send in real time
     *
     * @param result      result
     * @param isNewRecord state before save
     */
    @Override
    protected void onAfterSave(int result, boolean isNewRecord) {
        super.onAfterSave(result, isNewRecord);
        if (Network.isOnline() && Network.isWifi()) {
            Record<UnioDebug> record = new Record<>();
            record.add(this);
            send(record);
        }
    }

    /**
     * Format model record to Unio API model
     *
     * @return Hash
     */
    public Hash toSend() {
        Hash param = new Hash();

        param.put("idUnique", idUnique);
        param.put("packageName", packageName);
        param.put("unioVersion", unioVersion);
        param.put("dateReal",    dateReal);

        Hash system = param.child("system");
        system.put("name", systemName);
        system.put("version", systemVersion);

        Hash app = param.child("app");
        app.put("name", packageName);
        app.put("version", appVersion);

        Hash device = param.child("device");
        device.put("manufacturer", manufacturer);
        device.put("model", model);
        device.put("imid", imid);

        Hash trace = param.child("trace");
        trace.put("exception", exception);
        trace.put("message", message);

        return param;
    }

    /**
     * Send values
     *
     * @param record model records
     */
    public static void send(final Record<UnioDebug> record) {
        final String url = Unio.url(Unio.META_URL_DEBUG);
        if (Check.isEmpty(url) || record == null || record.size() == 0) return;

        // Need to change strict mode policy for send request in UI Thread
        // This limitation has added from API 9 to prevent requests in MainThread
        StrictMode.ThreadPolicy policy = StrictMode.getThreadPolicy(); // Get original policy to revert after
        Compatibility.allowAllThreadPolicy();

        new Http(Http.METHOD_POST, url)
            .addParam(format(record).toJson().toString())
            .enableTrace(false) // To ignore Connection Exception
            .addOnFinish(new Http.IOnFinish() {
                @Override
                public void onRequestFinish(boolean success, Http request) { if (success) record.delete(); }
            })
            .execute();

        // Revert
        StrictMode.setThreadPolicy(policy);
    }

    /**
     * Format model record to Unio API model
     *
     * @param record Record
     *
     * @return Hash
     */
    @SuppressWarnings("ForLoopReplaceableByForEach")
    private static Hash format(final Record<UnioDebug> record) {
        Hash params = new Hash();
        for (int i = 0; i < record.size(); i++) {
            UnioDebug model = record.get(i);
            params.put(model.toSend());
        }
        return params;
    }
}