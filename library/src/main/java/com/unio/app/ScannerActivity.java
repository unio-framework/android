package com.unio.app;

import android.os.Bundle;
import com.google.zxing.BarcodeFormat;
import com.unio.base.UActivity;
import com.unio.ui.ScannerView;
import com.unio.ui.TextView;
import com.unio.util.helper.Check;
import com.unio.R;

/**
 * UScannerActivity
 *
 * Activity for work with {@link ScannerView}
 *
 * Required permission:
 *     <uses-permission android:name="android.permission.CAMERA" />
 *     RequestPermissions(Manifest.permission.CAMERA)
 *
 * @author  Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 * @version 1.0 19/10/2016 0:40
 */
public abstract class ScannerActivity extends UActivity implements ScannerView.IOnFind
{
    protected ScannerView scanner;
    protected TextView scannerText;

    /**
     * Layout
     *
     * @return Object
     */
    @Override
    protected Object getLayout() { return R.layout.activity_scanner; }

    /**
     * Barcode format filter
     *
     * @return BarcodeFormat[]
     */
    protected BarcodeFormat[] filter() { return null; }

    /**
     * Start method
     */
    @Override
    protected void onInit(Bundle savedInstanceState) {
        scanner = findViewById(R.id.view_scanner);
        scannerText = findViewById(R.id.view_scanner_text);

        BarcodeFormat[] filter = this.filter();
        if (Check.isEmpty(filter) == false) scanner.setFormats(filter);
        scanner.addOnFind(this);
        this.start();
    }

    /**
     * Set a text
     *
     * @param text text
     */
    public void setText(String text) { scannerText.setText(text); }

    /**
     * Start camera
     */
    public void start() { scanner.startCamera(); }

    /**
     * Stop preview
     */
    public void stop() { scanner.stopCamera(); }

    /**
     * Resume camera
     */
    public void resume() {
        this.stop();
        this.start();
    }
}