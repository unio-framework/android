package com.unio.app;

import com.unio.ui.ScannerView;
import com.unio.util.statics.interfaces.IOnFindResult;

/**
 * USimpleScannerActivity
 *
 * Activity for single invoke {@link ScannerActivity}
 *
 * @author  Leandro Akira Omiya Takagi <leandro.takagi@icloud.com>
 * @version 1.0 14/11/2016 21:50
 */
public class SingleScannerActivity extends ScannerActivity
{
    /**
     * On find result
     *
     * @param response response
     */
    @Override
    public void onFind(ScannerView.Response response) { this.callSingleCallbacks(response); }

    /**
     * IOnAfterCapture
     *
     * Interface for external process
     *
     * @author  Leandro Akira Omiya Takagi <leandro.takagi@icloud.com>
     * @version 1.0 14/11/2016 21:50
     */
    public interface IOnAfterCapture extends IOnFindResult<ScannerView.Response> {}
}