package com.unio.app;

import com.unio.app.model.UnioDebug;
import com.unio.database.preference.StorageItem;
import com.unio.debug.EDebug;
import com.unio.debug.Trace;
import com.unio.modules.VersionControl;

/**
 * Version
 *
 * Unio version control
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public final class UnioVersion extends VersionControl
{
    /**
     * Get all version historic, in update order
     * The update process is based here, and is possible update in any old version
     *
     * @return String[]
     */
    @Override
    public String[] getAllVersions() {
        return new String[] {
            "3.0.0",
        };
    }

    /**
     * Actions before start update process
     *
     * @param cache Unio.cache of assigned version
     */
    @Override
    protected void onBeforeInit(StorageItem cache) {}

    /**
     * Actions when is first use
     */
    @Override
    public void onFirstUse() { UnioDebug.table().create(); }

    /**
     * Actions after update finish
     */
    @Override
    protected void onAfterUpdate() { Trace.log(EDebug.ALL, this, "Unio version updated to "+getCurrentVersion()); }
}