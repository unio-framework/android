package com.unio.app;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import com.unio.base.UActivity;
import com.unio.util.helper.Check;
import com.unio.util.io.Image;
import com.unio.util.statics.constants.RequestCode;

/**
 * PhotoActivity
 *
 * Activity for photo capture
 *
 * @author Leandro Akira Omiya Takagi <leakira.uac@gmail.com>
 */
public abstract class PhotoActivity extends UActivity
{
    /** Attributes */
    protected Uri uri;

    /**
     * Layout, null because will use camera
     *
     * @return Object
     */
    @Override
    protected Object getLayout() { return null; }

    /**
     * Start
     *
     * @param savedInstanceState parameters for recriation
     */
    @Override
    protected void onInit(Bundle savedInstanceState) {
        String path = this.getIntent().getStringExtra("path");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, path);
        this.uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, this.uri);
        this.startActivityForResult(cameraIntent, RequestCode.CAMERA);
    }

    /**
     * On receive result
     *
     * @param requestCode request code
     * @param resultCode  result status
     * @param data        Intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Image photo = null;
        if (requestCode == RequestCode.CAMERA && resultCode == Activity.RESULT_OK) {
            String path     = this.getPhotoPath();
            String fullPath = this.getIntent().getStringExtra("path");
            if (Check.isEqual(path, fullPath)) {
                photo = new Image(path);
            } else {
                Image temp = new Image(path);
                if (temp.copyTo(fullPath)) {
                    photo = new Image(fullPath);
                    temp.delete();
                } else {
                    photo = temp;
                }
            }
        }
        this.onFind(photo);
    }

    /**
     * On find result
     *
     * @param response response
     */
    public abstract void onFind(Image response);

    /**
     * Get photo path
     *
     * @return String
     */
    @SuppressWarnings("deprecation")
    protected String getPhotoPath() {
        String path = null;
        Cursor cursor = null;
        try {
            String [] proj = {MediaStore.Images.Media.DATA, MediaStore.Images.Media._ID, MediaStore.Images.ImageColumns.ORIENTATION };
            cursor = this.managedQuery(this.uri,
                proj,  // Which columns to return
                null,  // WHERE clause; which rows to return (all rows)
                null,  // WHERE clause selection arguments (none)
                null); // Order-by clause (ascending by name)
            int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            int orientation_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.ImageColumns.ORIENTATION);
            if (cursor.moveToFirst()) {
                String orientation =  cursor.getString(orientation_ColumnIndex);
                path = cursor.getString(file_ColumnIndex);
            }
        } finally {
            if (cursor != null) cursor.close();
        }
        return path;
    }
}