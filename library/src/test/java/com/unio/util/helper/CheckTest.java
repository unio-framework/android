package com.unio.util.helper;

import com.unio.util.io.Hash;
import org.junit.Test;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.*;

/**
 * {@link Check} tests
 */
public class CheckTest
{
    @Test
    public void isEmpty_shouldBeTrueForNull() {
        String testVar = null;
        assertTrue(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeTrueForEmpty() {
        String testVar = "";
        assertTrue(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeTrueForEmptyHash() {
        Hash testVar = new Hash();
        assertTrue(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeTrueForEmptyMap() {
        Map testVar = new LinkedHashMap<>();
        assertTrue(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeTrueForEmptyList() {
        List<String> testVar = new LinkedList<>();
        assertTrue(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeTrueForEmptyArray() {
        int[] testVar = {};
        assertTrue(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeTrueForZeroNumber() {
        long testVar = 0;
        assertTrue(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeFalseForNonEmptyString() {
        String testVar = "Value exists";
        assertFalse(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeFalseForNonEmptyHash() {
        Hash testVar = new Hash();
        testVar.put("Value exists");
        assertFalse(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeFalseForNonEmptyMap() {
        Map<String, String> testVar = new LinkedHashMap<>();
        testVar.put("test", "Value exists");
        assertFalse(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeFalseForNonEmptyList() {
        List<String> testVar = new LinkedList<>();
        testVar.add("Value exists");
        assertFalse(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeFalseForNonEmptyArray() {
        int[] testVar = { 1, 2, 3, 4, 5 };
        assertFalse(Check.isEmpty(testVar));
    }

    @Test
    public void isEmpty_shouldBeFalseForNonZeroNumber() {
        double testVar = 1.25;
        assertFalse(Check.isEmpty(testVar));
    }


    @Test
    public void isEqual_shouldBeCorrect() {
        String testA = "My test string";
        String testB = "My test string";
        assertTrue(Check.isEqual(testA, testB));
    }

    @Test
    public void isEqual_shouldBeCorrectForClass() {
        Class testA = Integer.class;
        Class testB = Integer.class;
        assertTrue(Check.isEqual(testA, testB));
    }

//    @Test
//    public void isEqual_shouldBeCorrectForIn() {
//        String testA = "Test 1";
//        String[] testB = { "Test 1", "Test 2" };
//        assertTrue(Check.includes(testA, testB));
//    }

    @Test
    public void isEqual_shouldBeIncorrect() {
        String testA = "My test string 1";
        String testB = "My test string 2";
        assertFalse(Check.isEqual(testA, testB));
    }

    @Test
    public void isEqual_shouldBeIncorrectForClass() {
        Class testA = String.class;
        Class testB = Integer.class;
        assertFalse(Check.isEqual(testA, testB));
    }

//    @Test
//    public void isEqual_shouldBeIncorrectForIn() {
//        String testA = "Test 1";
//        String[] testB = { "Test 3", "Test 4" };
//        assertFalse(Check.includes(testA, testB));
//    }

    @Test
    public void isJson_shouldBeCorrect() {
        String testA = "{ \"Teste\": \"Teste 1\" }";
        assertTrue(Check.isJson(testA));
    }

//    @Test
//    public void isJson_shouldBeIncorrect() {
//        String testA = "Simple text";
//        assertFalse(Check.isJson(testA));
//    }

    @Test
    public void isXml_shouldBeCorrect() {
        String testA = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<note>\n" +
            "  <to>Tove</to>\n" +
            "  <from>Jani</from>\n" +
            "  <heading>Reminder</heading>\n" +
            "  <body>Don't forget me this weekend!</body>\n" +
            "</note>";
        assertTrue(Check.isXml(testA));
    }

    @Test
    public void isXml_shouldBeIncorrect() {
        String testA = "Simple text";
        assertFalse(Check.isXml(testA));
    }

    @Test
    public void isUrl_shouldBeCorrect() {
        String testA = "http://www.unio.ga";
        assertTrue(Check.isUrl(testA));
    }

    @Test
    public void isUrl_shouldBeCorrectWithHttps() {
        String testA = "https://www.unio.ga";
        assertTrue(Check.isUrl(testA));
    }

    @Test
    public void isUrl_shouldBeIncorrect() {
        String testA = "Simple text";
        assertFalse(Check.isUrl(testA));
    }

    @Test
    public void isUrl_shouldBeIncorrectWithWww() {
        String testA = "www.unio.ga";
        assertFalse(Check.isUrl(testA));
    }
}