package com.unio.util.helper;

import com.unio.util.helper.Concat;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * {@link Concat} tests
 */
public class ConcatTest
{
    @Test
    public void array_shouldBeEqual() {
        String[] testA = { "Test 1", "Test 2" };
        String[] testB = { "Test 3", "Test 4" };
        String[] expected = { "Test 1", "Test 2", "Test 3", "Test 4" };
        assertArrayEquals(expected, Concat.array(testA, testB));
    }

    @Test
    public void array_notShouldBeEqual() {
        String[] testA = { "Test 1", "Test 2" };
        String[] testB = { "Test 3", "Test 4" };
        String[] unexpected = { "Test 1", "Test 2", "Test 3" };
        assertNotEquals(unexpected, Concat.array(testA, testB));
    }
}