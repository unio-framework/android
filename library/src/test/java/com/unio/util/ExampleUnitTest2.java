package com.unio.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest2
{
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }
}