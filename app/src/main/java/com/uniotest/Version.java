package com.uniotest;

import com.unio.core.Unio;
import com.unio.database.preference.StorageItem;
import com.unio.debug.EDebug;
import com.unio.debug.Trace;
import com.unio.modules.VersionControl;

/**
 * Created by akira on 15/05/24.
 */
public class Version extends VersionControl
{
    @Override
    public String getIdentification() { return Unio.app().getPackageName(); }

    @Override
    public String getCurrentVersion() {
        return "1.1.0_1102";
    }

    @Override
    public String[] getAllVersions() {
        return new String[] { "1.0.0", "1.0.1", "1.0.2", "1.0.3", "1.0.4", "1.0.5", "1.1.0", "1.1.0_1101", "1.1.0_1102" };
    }

    @Override
    public void onFirstUse() {
        //
    }

    @Override
    protected void onBeforeInit(StorageItem cache) {
        Trace.log(EDebug.ALL, this, "fdfdf", cache.get());
    }

    public void update1_1_0_1102() {
        Trace.log(this, "dfdfd");
    }

    @Override
    protected void onAfterUpdate() {
        Trace.log(this, "Updated to "+getCurrentVersion());
    }

    public void update1_0_1() { Trace.log(this, "1.0.1"); }
    public void update1_0_2() { Trace.log(this, "1.0.2"); }
    public void update1_0_3() { Trace.log(this, "1.0.3"); }
    public void update1_0_4() { Trace.log(this, "1.0.4"); }
}