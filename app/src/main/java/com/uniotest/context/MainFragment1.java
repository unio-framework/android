package com.uniotest.context;

import android.os.Bundle;
import android.view.View;
import com.unio.base.UFragment;
import com.unio.core.Unio;
import com.unio.debug.Trace;
import com.unio.ui.SwipeRefreshLayout;
import com.unio.ui.annotation.FindViewById;
import com.unio.ui.annotation.LayoutId;
import com.unio.ui.context.IGlobalContext;
import com.unio.ui.context.ILocalContext;
import com.unio.ui.context.IRefreshContext;
import com.unio.ui.recycler.Adapter;
import com.unio.ui.recycler.AdapterItem;
import com.unio.ui.recycler.item.SimpleItem;
import com.uniotest.R;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

@LayoutId(R.layout.main)
public class MainFragment1 //extends UFragment<ContextActivityExample, View> implements IGlobalContext, IRefreshContext, ILocalContext
{
//    @FindViewById
//    public RecyclerView collection;
//
//    @Override
//    protected void onInit(Bundle savedInstanceState) {
//        Adapter adapter = new Adapter();
//        adapter.add(new SimpleItem("Teste 1"));
//        adapter.add(new SimpleItem("Teste 2"));
//        adapter.add(new SimpleItem("Teste 3"));
//        adapter.add(new SimpleItem("Teste 4"));
//        adapter.add(new SimpleItem("Teste 5"));
//        adapter.add(new SimpleItem("Teste 6"));
//        adapter.add(new SimpleItem("Teste 7"));
//        adapter.add(new SimpleItem("Teste 8"));
//        adapter.add(new SimpleItem("Teste 9"));
//        adapter.add(new SimpleItem("Teste 10"));
//        adapter.add(new SimpleItem("Teste 11"));
//        adapter.add(new SimpleItem("Teste 12"));
//
//        adapter.addOnItemCustomize(new Adapter.IOnItemCustomize() {
//            @Adapter.Types({ 2 })
//            @Override
//            public void onItemCustomize(int position, RecyclerView.ViewHolder holder, AdapterItem item) {
//                Trace.log(this, position);
//            }
//        });
//
//        this.collection.setLayoutManager(new LinearLayoutManager(this.activity()));
//        this.collection.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this.activity()).build());
//        this.collection.setAdapter(adapter);
//    }
//
//    @Override
//    public Class<? extends Fragment> getGlobalFragment() {
//        return MenuFragment.class;
//    }
//
//    @Override
//    public void onRefresh(final SwipeRefreshLayout layout, int id) {
//        Unio.ui().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                layout.setRefreshing(false);
//            }
//        }, 5000);
//    }
//
//    @Override
//    public boolean isUserInteractionEnabled() {
//        return true;
//    }
//
//    @Override
//    public int onBackPressed() {
//        Trace.log(this, "teste");
//        return super.onBackPressed();
//    }
//
//    @Override
//    public LocalItem[] getLocalMenu() {
//        return new LocalItem[] {
//            new LocalItem(R.drawable.plus, "Teste 1", R.id.teste1),
//            new LocalItem(R.drawable.plus, "Teste 2", R.id.teste2),
//            new LocalItem(R.drawable.plus, "Teste 3", R.id.teste3),
//            new LocalItem(R.drawable.plus, "Teste 4", R.id.teste4),
//        };
//    }
//
//    @Override
//    public void onLocalItemRenderize(FloatingActionButton item, int id, int position) {
//        Trace.log(this, "onLocalItemRenderize", item.getTitle(), id, R.id.teste1, position);
//    }
//
//    @Override
//    public boolean onLocalItemClick(FloatingActionButton item, int id, int position) {
//        Trace.log(this, "onLocalItemClick", item.getTitle(), id, R.id.teste2, position);
//        return true;
//    }
}