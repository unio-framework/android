package com.uniotest;

import com.unio.debug.Trace;
import com.unio.modules.Auth;
import com.unio.util.statics.interfaces.IAsync;

public class SplashLoginActivity extends com.unio.ui.splash.SplashLoginActivity
{
    @Override
    protected void onLogged() {
        Trace.log(this, "Logged");
    }

    @Override
    public boolean onValidate(Auth auth, String username, String password) {
        Trace.log(this, username, password, auth.isLogged());
        return true;
    }

    @Override
    public boolean onLogin(Auth auth, IAsync task, String username, String password, boolean isParallel) {
        Trace.log(this, username, password, auth.isLogged(), isParallel);
        return false;
    }
}