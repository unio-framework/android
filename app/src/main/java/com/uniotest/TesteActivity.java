package com.uniotest;

import android.Manifest;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import com.unio.base.UActivity;
import com.unio.debug.Trace;
import com.unio.ui.annotation.FindViewById;
import com.unio.ui.annotation.LayoutId;
import com.unio.ui.annotation.RequestPermissions;
import com.unio.ui.recycler.Adapter;
import com.unio.ui.recycler.AdapterItem;
import com.unio.ui.recycler.item.SimpleItem;

@RequestPermissions({Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE})
@LayoutId(R.layout.main)
public class TesteActivity extends UActivity
{
    @FindViewById
    public RecyclerView collection;

    @Override
    protected void onInit(Bundle savedInstanceState) {
        Adapter adapter = new Adapter();
        adapter.add(new SimpleItem("Teste 1"));
        adapter.add(new SimpleItem("Teste 2"));
        adapter.add(new SimpleItem("Teste 3"));
        adapter.add(new SimpleItem("Teste 4"));
        adapter.add(new SimpleItem("Teste 5"));
        adapter.add(new SimpleItem("Teste 6"));
        adapter.add(new SimpleItem("Teste 7"));
        adapter.add(new SimpleItem("Teste 8"));
        adapter.add(new SimpleItem("Teste 9"));
        adapter.add(new SimpleItem("Teste 10"));
        adapter.add(new SimpleItem("Teste 11"));
        adapter.add(new SimpleItem("Teste 12"));

        adapter.addOnItemCustomize(new Adapter.IOnItemCustomize() {
            @Adapter.Types({ 2 })
            @Override
            public void onItemCustomize(int position, RecyclerView.ViewHolder holder, AdapterItem item) {
                Trace.log(this, position);
            }
        });

        this.collection.setLayoutManager(new LinearLayoutManager(this));
        this.collection.setAdapter(adapter);

//        new android.support.v7.app.AlertDialog.Builder(this)
//            .setTitle("Nuke planet Jupiter?")
//            .setMessage("Note that nuking planet Jupiter will destroy everything in there.")
//            .setPositiveButton("Nuke", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Log.d("MainActivity", "Sending atomic bombs to Jupiter");
//                }
//            })
//            .setNegativeButton("Abort", new DialogInterface.OnClickListener() {
//                @Override
//                public void onClick(DialogInterface dialog, int which) {
//                    Log.d("MainActivity", "Aborting mission...");
//                }
//            })
//            .show();
    }
}