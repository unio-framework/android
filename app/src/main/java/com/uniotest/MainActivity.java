package com.uniotest;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import com.unio.base.UActivity;
import com.unio.debug.Trace;
import com.unio.ui.annotation.FindViewById;
import com.unio.ui.annotation.LayoutAnnotation;
import com.unio.ui.annotation.LayoutId;
import com.unio.ui.annotation.RequestPermissions;
import com.unio.util.helper.Convert;

@RequestPermissions(Manifest.permission.CAMERA)
@LayoutId(name = "main")
public class MainActivity extends UActivity
{
    @FindViewById(name = "collection")
    private RecyclerView collection;

    @Override
    protected void onInit(Bundle savedInstanceState) {
//        Trace.log(this, "ok?");
//        if (this.requestPermission(this, TesteActivity.class)) {
//            MActivity.change(TesteActivity.class, MParam.CLOSE_CURRENT);
//        }
        //Trace.log(this, collection);
//        MActivity.change(KotlinActivity.class, MParam.CLOSE_CURRENT);
    }

    @Override
    public void onPermissionsResult(@NonNull Class permissionClass, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onPermissionsResult(permissionClass, permissions, grantResults);
        Trace.log(this, permissionClass, Convert.toString(permissions), Convert.toString(grantResults));
        if (permissionClass == MainActivity.class) {
            if (LayoutAnnotation.isPermissionsEnabled(this.getClass())) {
                Trace.log(this, "ok!");
                this.requestPermission(this, TesteActivity.class);
            } else {
                this.requestPermission(this);
            }
        } else if (permissionClass == TesteActivity.class) {
            if (LayoutAnnotation.isPermissionsEnabled(TesteActivity.class)) {
                Trace.log(this, "ok!!");
            } else {
                this.requestPermission(this, TesteActivity.class);
            }
        }
    }
}