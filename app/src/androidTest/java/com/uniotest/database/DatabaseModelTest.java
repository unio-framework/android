package com.uniotest.database;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import com.unio.database.sql.orm.Record;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class DatabaseModelTest
{
    @Before
    public void onBefore() { TestTable.table().create(); }

    @After
    public void onAfter() { TestTable.table().delete(); }

    @Test
    public void insertTest() {
        boolean resulted = insert();
        assertTrue(resulted);
    }

    @Test
    public void insertAndFindTest() {
        insert();
        boolean resulted = findAfterInsert();
        assertTrue(resulted);
    }

    @Test
    public void updateTest() {
        insert();
        boolean resulted = update();
        assertTrue(resulted);
    }

    @Test
    public void updateAndFindTest() {
        insert();
        update();
        boolean resulted = updateAndFind();
        assertTrue(resulted);
    }

    @Test
    public void deleteTest() {
        insert();
        TestTable model = TestTable.model().filter("myColumn1=?", new Object[] { "col1" }).find();
        boolean resulted = model != null && model.delete();
        assertTrue(resulted);
    }

    @Test
    public void findAllTest() {
        insert();
        insert();
        Record<TestTable> model = TestTable.model().filter("myColumn1=?", new Object[] { "col1" }).findAll();
        assertEquals(2, model.size());
    }

    private boolean insert() {
        TestTable model = new TestTable();
        model.myColumn1 = "col1";
        model.myColumn2 = 2;
        return model.save();
    }

    private boolean findAfterInsert() {
        TestTable model = TestTable.model().filter("myColumn1=?", new Object[] { "col1" }).find();
        return model != null && model.myColumn2 == 2;
    }

    private boolean update() {
        TestTable model = TestTable.model().filter("myColumn1=?", new Object[] { "col1" }).find();
        if (model == null) return false;
        model.myColumn2 = 3;
        return model.save();
    }

    private boolean updateAndFind() {
        TestTable model = TestTable.model().filter("myColumn1=?", new Object[] { "col1" }).find();
        return model != null && model.myColumn2 == 3;
    }
}