package com.uniotest.database;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class DatabaseCreationTest
{
    @Test
    public void createDatabase() {
        boolean resulted = TestTable.table().create();
        boolean expected = TestTable.table().exists();
        assertEquals(resulted, expected);
    }

    @Test
    public void deleteDatabase() {
        boolean resulted = TestTable.table().delete();
        boolean expected = TestTable.table().exists() == false;
        assertEquals(resulted, expected);
    }
}