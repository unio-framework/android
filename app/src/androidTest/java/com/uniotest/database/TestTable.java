package com.uniotest.database;

import com.unio.database.sql.builder.QueryBuilder;
import com.unio.database.sql.orm.Model;
import com.unio.database.sql.orm.Table;
import com.unio.database.sql.orm.annotation.AColumn;
import com.unio.database.sql.orm.annotation.ATable;

@ATable
public class TestTable extends Model
{
    @AColumn
    public String myColumn1;

    @AColumn
    public int myColumn2;

    // Model handlers
    public static QueryBuilder model() { return model(TestTable.class); }
    public static Table table()        { return table(TestTable.class); }
}